let LABEL = {
    required: 'Bắt buộc',
    invalid_email: 'Email không hợp lệ',
    invalid_phone: 'Số điện thoại không hợp lệ',
    email_exist: 'Email đã tồn tại',
    min_length: 'Độ dài tối thiểu {0} ký tự',
    date_format: 'YYYY-MM-DD',
    local_date_format: 'DD/MM/YYYY',
    date_time_format: 'YYYY-MM-DD HH:mm',
    local_date_time_foramt: 'HH:mm DD/MM/YYYY',
    default_seo_title: 'Tiêu đề page website không vượt quá 70 kí tự (tốt nhất từ 60-70 kí tự)',
    default_seo_des: 'Mô tả page website không vượt quá 155 kí tự (tốt nhất từ 100-155 kí tự). Là những đoạn mô tả ngắn gọn về website, bài viết...'
};

let URL = {
    upload_image: '/admin/image_upload',
    active_image: '/admin/image_upload/change'
};

let PHONE_REGEX = /(0188|0160|0161|0162|0163|0164|0165|0166|0167|0168|0169|0120|0121|0122|0123|0124|0125|0126|0127|0128|0129|090|091|092|093|094|095|096|097|098|099|086|087|088|089)\d{7}$/;