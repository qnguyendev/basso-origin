var ALERT = {
    error: function (msg, callback) {
        swal({
            icon: 'error',
            title: 'Oops...',
            text: msg
        }).then(callback);
    },
    success: function (msg) {
        swal({
            icon: 'success',
            title: msg
        }).then(function () {
        });
    },
    confirm: function (title, msg, callback) {
        swal({
            title: title,
            text: msg,
            icon: "warning",
            buttons: {
                cancel: !0,
                confirm: {
                    text: "OK",
                    value: !0,
                    visible: !0,
                    className: "bg-danger",
                    closeModal: !0
                }
            }
        }).then(function (result) {
            if (result)
                callback(result);
        });
    },
    prompt: function (title, msg, callback) {
        swal({
            icon: 'info',
            title: title,
            text: msg,
            input: 'text',
            showCancelButton: true
        }).then(function (name) {
            if (name)
                callback(name);
        }, function () {

        });
    }
};

var NOTI = {
    info: function (message) {
        var hulla = new hullabaloo();
        hulla.send(message, 'info');
    },
    waring: function (message) {
        var hulla = new hullabaloo();
        hulla.send(message, 'warning');
    },
    success: function (message) {
        var hulla = new hullabaloo();
        hulla.send(message, 'success');
    },
    danger: function (message) {
        var hulla = new hullabaloo();
        hulla.send(message, 'danger');
    }
};

var AJAX = {
    get: function (url, data, show_loader, success_callback) {
        $.ajax({
            type: 'get',
            url: url,
            data: data,
            beforeSend: function () {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').show();
            },
            error: function (jqXHR) {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').fadeOut();

                var res = JSON.parse(jqXHR.responseText);
                if (res.expired != null) {
                    window.location = res.redirect_url;
                    return;
                }
                success_callback(res);
            },
            success: function (res) {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').fadeOut();

                try {
                    res = JSON.parse(res);
                } catch (err) {
                }
                success_callback(res);
            }
        })
    },
    post: function (url, data, show_loader, success_callback) {
        $.ajax({
            type: 'post',
            url: url,
            data: data,
            beforeSend: function () {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').show();
            },
            error: function (jqXHR) {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').fadeOut();

                var res = JSON.parse(jqXHR.responseText);
                if (res.expired != null) {
                    window.location = res.redirect_url;
                    return;
                }
                success_callback(res);
            },
            success: function (res) {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').fadeOut();

                try {
                    res = JSON.parse(res);
                } catch (err) {
                }
                success_callback(res);
            }
        })
    },
    delete: function (url, data, show_loader, success_callback) {
        $.ajax({
            type: 'DELETE',
            url: url,
            data: data,
            beforeSend: function () {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').show();
            },
            error: function (jqXHR) {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').fadeOut();

                var res = JSON.parse(jqXHR.responseText);
                if (res.expired != null) {
                    window.location = res.redirect_url;
                    return;
                }
                success_callback(res);
            },
            success: function (res) {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').fadeOut();

                try {
                    res = JSON.parse(res);
                } catch (err) {
                }
                success_callback(res);
            }
        })
    },
    upload: function (url, formData, show_loader, success_callback) {
        $.ajax({
            type: 'post',
            url: url,
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            beforeSend: function () {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').show();
            },
            error: function (jqXHR) {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').fadeOut();

                var res = JSON.parse(jqXHR.responseText);
                if (res.expired != null) {
                    window.location = res.redirect_url;
                    return;
                }
                success_callback(res);
            },
            success: function (res) {
                if (show_loader && $('#loader').length > 0)
                    $('#loader').fadeOut();

                try {
                    res = JSON.parse(res);
                } catch (err) {
                }
                success_callback(res);
            }
        });
    }
};

var MODAL = {
    show: function (ele) {
        $(ele).modal();
    },
    hide: function (ele) {
        $(ele).modal('hide');
    },
    onHide: function (ele, callback) {
        $(ele).on('hidden.bs.modal', function () {
            callback();
        });
    }
};

function toNumber(str) {
    if (str === undefined) return 0;
    if (str === null) return 0;

    str = str.toString();
    if (str.length === 0) return 0;
    return parseInt(str.replace(/[^0-9]/g, ''));
};

Number.prototype.toMoney = function (c, d, t) {
    var n = this;
    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d == undefined ? "." : d;
    t = t == undefined ? "," : t;
    s = n < 0 ? "-" : "";
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c)));
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + ' ₫';
};

function create_slug(text) {
    text = text.toString().trim().toLocaleLowerCase();
    // remove accents, swap ñ for n, etc
    var from = "áàảãâấấầẩẫậắằẳẵặăóòỏõọốồỗổộớờởỡợôơưứừửữựúùủũụêéèẻẽẹếềểễệíìĩỉịýỳỹỷỵđ";
    var to = "aaaaaaaaaaaaaaaaaooooooooooooooooouuuuuuuuuuueeeeeeeeeeeiiiiiyyyyyd";
    for (var i = 0, l = from.length; i < l; i++) {
        text = text.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    return text.toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
}

function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16) + "000000000").substr(2, 8);
        return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
    }

    return _p8() + _p8(true) + _p8(true) + _p8();
}