<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
if (!isset($theme_config)) {
    $theme_config = [];
}

$theme_config['license'] = [
    'domain' => ['basso.io', 'basso.vn', 'basso-origin.local'],
    'expire' => strtotime('3019-04-30 00:00:00')
];