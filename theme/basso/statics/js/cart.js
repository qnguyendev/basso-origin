function viewModel() {
    var self = this;
    self.first_init = false;

    self.cart = {
        total_qty: ko.observable(0),
        total: ko.observable(),
        sub_total: ko.observable(),
        voucher: ko.observable(),
        calc: function () {
            var total = 0;
        },
        cities: ko.mapping.fromJS([]),
        districts: ko.mapping.fromJS([]),
        change_city: function () {
            if (self.first_init) {
                AJAX.get(window.location, {
                    city_id: self.shipping_info.city_id(),
                    action: 'change-city'
                }, null, function (res) {
                    ko.mapping.fromJS(res.data, self.cart.districts);
                });
            }
        },
        payment_limits: ko.mapping.fromJS(payment_limits),
        payment_method: ko.observable(),
        update: function () {
            var data = {action: 'update-cart', voucher: self.voucher.code()};
            AJAX.post(window.location, data, false, function (res) {
                ko.mapping.fromJS(res.data, self.items.list);
                self.cart.total(res.total);
                self.cart.sub_total(res.sub_total);

                if (res.voucher == null) {
                    self.voucher.detail.value(0);
                    self.voucher.detail.max_value(0);
                    self.voucher.detail.type('amount');
                } else {
                    self.voucher.detail.value(res.voucher.discount_value);
                    self.voucher.detail.max_value(res.voucher.max_value);
                    self.voucher.detail.type(res.voucher.discount_type);
                }
            });
        },
        create: function () {
			
            if (!self.shipping_info.isValid()) {
                self.shipping_info.errors.showAllMessages();
                $('body, html').animate({
                    scrollTop: $("form.order-contact").offset().top - 20
                }, 1000);

                NOTI.danger('Vui lòng nhập các thông tin bắt buộc');
            } else {
                if ($('input[type=checkbox]#agree:checked').length == 0) {
                    NOTI.danger('Bạn chưa đồng ý với điều khoản và điều kiện');
                    return;
                }

                var data = {
                    name: self.shipping_info.name(),
                    phone: self.shipping_info.phone(),
                    email: self.shipping_info.email(),
                    address: self.shipping_info.address(),
                    city_id: self.shipping_info.city_id(),
                    district_id: self.shipping_info.district_id(),
                    payment_method: self.cart.payment_method(),
                    payment_limit: $('input[name=headship]:checked').val(),
                    note: self.shipping_info.note(),
                    action: 'create',
                    voucher: self.voucher.code()
                };

                AJAX.post(window.location, data, true, function (res) {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        if (res.redirect_url != null)
                            window.location = res.redirect_url;
                    }
                });
            }
        },
        update_total_qty: function () {
            var total = 0;
            self.items.list.each(function (x) {
                total += parseInt(x.qty());
            });

            self.cart.total_qty(total);
            $('span#cart-total').html(self.cart.total_qty());
        }
    };

    self.voucher = {
        code: ko.observable(typeof discount_code != 'undefined' ? '2OFF' : undefined),
        total_discount: ko.observable(0),
        detail: {
            value: ko.observable(0),
            type: ko.observable('amount'),
            max_value: ko.observable(0)
        },
        apply: function () {
            if (self.voucher.code() == undefined) {
                self.voucher.detail.value(0);
                self.voucher.detail.max_value(0);
                self.voucher.detail.type('amount');
            } else if (self.voucher.code() == null) {
                self.voucher.detail.value(0);
                self.voucher.detail.max_value(0);
                self.voucher.detail.type('amount');
            } else if (self.voucher.code() == '') {
                self.voucher.detail.value(0);
                self.voucher.detail.max_value(0);
                self.voucher.detail.type('amount');
            } else {
                var data = {
                    voucher: self.voucher.code(),
                    action: 'apply-voucher'
                };

                AJAX.post(window.location, data, true, function (res) {
                    if (res.error) {
                        ALERT.error(res.message);
                        self.voucher.detail.value(0);
                        self.voucher.detail.max_value(0);
                        self.voucher.detail.type('amount');
                    } else {
                        self.voucher.detail.value(res.voucher.discount_value);
                        self.voucher.detail.max_value(res.voucher.max_value);
                        self.voucher.detail.type(res.voucher.discount_type);
                    }
                });
            }
        }
    };

    self.shipping_info = {
        name: ko.observable().extend({
            params: true,required: {message: '(*) Họ và tên là thông tin bắt buộc'}}),
        phone: ko.observable().extend({
            params: true,required: {message: '(*) Số điện thoại là thông tin bắt buộc'}}),
        email: ko.observable().extend({
            params: true,required: {message: '(*) Email là thông tin bắt buộc'}}),
        address: ko.observable().extend({required: {message: '(*) Địa chỉ là thông tin bắt buộc'}}),
        city_id: ko.observable().extend({required: {message: '(*) Thành phố là thông tin bắt buộc'}}),
        district_id: ko.observable().extend({required: {message: '(*) Quận huyện là thông tin bắt buộc'}}),
        note: ko.observable()
    };

    self.items = {
        list: ko.mapping.fromJS([]),
        delete: function (item) {
            ALERT.confirm(`Xác nhận xóa sản phẩm`, `Bạn muốn xóa ${item.name()} khỏi đơn hàng`, function () {
                AJAX.delete(window.location, {id: item.rowid()}, true, function (res) {
                    if (!res.error) {
                        self.items.list.remove(item);
                        self.cart.update();
                        self.cart.update_total_qty();

                        NOTI.success(`Đã xóa <b>${item.name()}</b> khỏi đơn hàng`);
                    } else
                        ALERT.error(res.message);
                });
            });
        },
        change_qty: function (item) {
            var data = {
                id: item.rowid(),
                qty: item.qty(),
                action: 'change-qty'
            };
			if(item.qty()<1){
				$(".btnpayment").attr("disabled", true);
                 ALERT.error("Số lượng sản phẩm phải lớn hơn 1.");
			}else{
				$(".btnpayment").attr("disabled", false);
				AJAX.post(window.location, data, true, function () {
					self.cart.update();
					self.cart.update_total_qty();
				});
			}
        },
        edit: function (item) {
            self.items.modal.id(item.rowid());
            self.items.modal.name(item.name());
            self.items.modal.image(item.image());
            self.items.modal.note(item.note());
            MODAL.show('#item-detail-modal');
        },
        minus: function (item) {
            if (parseInt(item.qty()) > 1) {
                item.qty(parseInt(item.qty()) - 1);
                self.items.change_qty(item);
            }
        },
        plus: function (item) {
            item.qty(parseInt(item.qty()) + 1);
            self.items.change_qty(item);
        },
        modal: {
            id: ko.observable(),
            name: ko.observable(),
            image: ko.observable(),
            note: ko.observable(),
            update: function () {
                self.items.list.each(function (x) {
                    if (x.rowid() == self.items.modal.id()) {
                        x.note(self.items.modal.note());

                        var data = {
                            action: 'update-item',
                            id: x.rowid(),
                            note: x.note()
                        };

                        AJAX.post(window.location, data, false, function (res) {
                            if (!res.error) {
                                NOTI.success('Cập nhật thành công');
                                MODAL.hide('#item-detail-modal');
                            }
                        });
                    }
                });
            }
        }
    };

    self.init = function () {
        AJAX.get(window.location, {action: 'init'}, true, function (res) {
            ko.mapping.fromJS(res.data, self.items.list);
            ko.mapping.fromJS(res.cities, self.cart.cities);
            ko.mapping.fromJS(res.districts, self.cart.districts);
            self.cart.update();
            self.cart.update_total_qty();

            if (res.user != null) {
                self.shipping_info.name(res.user.name);
                self.shipping_info.email(res.user.email);
                self.shipping_info.phone(res.user.phone);
                self.shipping_info.district_id(res.user.district_id);
                self.shipping_info.address(res.user.address);
                self.shipping_info.city_id(res.user.city_id);
            }

            window.setTimeout(function () {
                self.shipping_info.errors.showAllMessages(false);
            }, 0);
            self.cart.payment_method($('#accordionPayment div.payment-method:first').attr('rel'));
            self.first_init = true;
            self.cart.update_total_qty();
        });

        $('#cart-page .order-menu a').click(function () {
            var div = $(this).attr('rel');
            self.cart.payment_method(div);
            $('#cart-page .order-menu div.li').removeClass('active');
            $(this).parent().addClass('active');

            $('div.payment-method').removeClass('d-none').hide();
            $(`div.payment-method[rel=${div}]`).show();
        });
    };

    self.calc_sub_total = function (total, discount_percent) {
        return total - self.calc_discount_value(total, discount_percent);
    };

    self.calc_discount_value = function (total, discount_percent) {
        var discount = total * discount_percent / 100;
        if (discount > self.voucher.detail.max_value() && self.voucher.detail.max_value() > 0)
            discount = self.voucher.detail.max_value();
        return discount;
    };

    self.term_modal = function () {
        $.get('/cac-dieu-khoan-va-dieu-kien?no_layout', function (data) {
            $('#term-modal .modal-body').html(data);
            MODAL.show('#term-modal');
        });
    };
}

ko.validation.init({
	decorateElement: true,
	errorElementClass: 'err'
});
var model = new viewModel();
model.init();
model.cart.update();
ko.validatedObservable(model.shipping_info);
ko.applyBindings(model, document.getElementById('cart-page'));