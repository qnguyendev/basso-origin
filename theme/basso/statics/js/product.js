function amzProductModel() {
    var self = this;
    if (product == null)
        product = null;

    self.id = ko.observable(product.id);
    self.images = ko.observableArray(product.images);
    self.condition = ko.observable(product.condition);
    self.price = ko.observable(product.price);
    self.weight = ko.observable(product.weight);
    self.product_id = ko.observable(product.id);
    self.thumbnail = ko.observable(product.images[product.images.length - 1].large);
    self.attributes = ko.observableArray(product.attributes);
    self.variations = ko.mapping.fromJS(product.variation_list);
    self.is_loading = ko.observable(false);
    self.origin_price = ko.observable(product.origin_price);
    self.term_fee = ko.observable(product.term_fee);
    self.shipping_fee = ko.observable(product.shipping_fee);

    self.change_variant = function (data, event) {
        if (!self.is_loading()) {
            var selected_val = [];
            self.re_check_variant(event.currentTarget.name);

            $('#cart-form select[rel=variant]').each(function () {
                selected_val.push($(this).val());
            });

            for (var i = 0; i < product.variations.length; i++) {
                var item = product.variations[i];
                var diff = self.diff(item.value, selected_val);

                if (diff == 0) {
                    AJAX.get(window.location, {asin: item.asin}, true, function (res) {
                        if (!res.error) {
                            try {
                                $('.gallery').slick("unslick");
                            } catch (e) {
                            }

                            self.attributes(res.attributes);
                            self.images(res.images);
                            self.thumbnail(res.images[res.images.length - 1].large);
                            console.log(res.images[0].large);
                            MagicZoom.update('#zoomImg', res.images[0].large, res.images[0].large);

                            self.condition(res.condition);
                            self.price(res.price);
                            self.weight(res.weight);
                            self.product_id(item.asin);
                            self.origin_price(res.origin_price);
                            self.shipping_fee(res.shipping_fee);
                            self.term_fee(res.term_fee);

                            $('.gallery').slick({
                                autoplay: true,
                                vertical: false,
                                verticalSwiping: false,
                                prevArrow: '<a class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
                                nextArrow: '<a class="slick-next"><i class="fas fa-chevron-right"></i></button>',
                                //mobileFirst: true,
                                slidesToShow: 5,
                                responsive: [
                                    {
                                        breakpoint: 1025,
                                        settings: {
                                            slidesToShow: 4
                                        }
                                    },
                                    {
                                        breakpoint: 769,
                                        settings: {
                                            slidesToShow: 4
                                        }
                                    },
                                    {
                                        breakpoint: 480,
                                        settings: {
                                            slidesToShow: 3
                                        }
                                    }
                                ]
                            });
                            self.is_loading(false);
                        }
                    });
                    break;
                }
            }
        }
    };

    self.re_check_variant = function (selected_name) {

        if ($('input[rel=variant]').length > 0) {
            var selected_val = $(`input[rel=variant][name=${selected_name}]`).val();

            //  Những giá trị thuộc tính đi kèm với asin theo first_val
            var available_values = [];
            for (var i = 0; i < product.variations.length; i++) {
                var item = product.variations[i];
                if (item.value.includes(selected_val)) {
                    for (var j = 0; j < item.value.length; j++) {
                        if (item.value[j] != selected_val)
                            available_values.push(item.value[j]);
                    }
                }
            }

            self.variations.each(function (x) {
                if (x.name() !== selected_name) {
                    x.values.each(function (t) {
                        t.enable(available_values.indexOf(t.value()) >= 0);
                    });
                }
            });
        }
    };

    self.diff = function (a1, a2) {
        return a1.concat(a2).filter(function (val, index, arr) {
            return arr.indexOf(val) === arr.lastIndexOf(val);
        });
    };

    self.init = function () {
        if (self.id() !== undefined) {
            for (var i = 0; i < product.variations.length; i++) {
                if (product.variations[i].asin == self.id()) {
                    var values = product.variations[i].value;
                    for (var j = 0; j < values.length; j++) {
                        $('select[rel=variant]').eq(j).val(values[j]);
                    }
                    break;
                }
            }
        }
    };

    self.init_slide = function () {
        $('.gallery').slick({
            autoplay: true,
            vertical: false,
            verticalSwiping: false,
            prevArrow: '<a class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
            nextArrow: '<a class="slick-next"><i class="fas fa-chevron-right"></i></button>',
            mobileFirst: true,
            slidesToShow: 5,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3
                    }
                }
            ]
        });

        $('#cart-form input[type=radio]').change(function () {

            var selected_val = $(this).val();
            var selected_name = $(this).parent().attr('name');

            //  Những giá trị thuộc tính đi kèm với asin theo first_val
            var available_values = [];
            for (var i = 0; i < product.variations.length; i++) {
                var item = product.variations[i];
                if (item.value.includes(selected_val)) {
                    for (var j = 0; j < item.value.length; j++) {
                        if (item.value[j] != selected_val)
                            available_values.push(item.value[j]);
                    }
                }
            }

            self.variations.each(function (x) {
                if (x.name() !== selected_name) {
                    x.values.each(function (t) {
                        t.enable(available_values.indexOf(t.value()) >= 0);
                    });
                }
            });

            if (!self.is_loading()) {
                var selected_val_ = [];
                self.is_loading(true);

                $('#cart-form input[rel=variant]').each(function () {
                    if ($(this).is(':checked'))
                        console.log($(this).val());
                });
                var selected_asin = null;

                for (var i = 0; i < product.variations.length; i++) {
                    var item = product.variations[i];
                    var diff = self.diff(item.value, selected_val_);

                    if (diff == 0) {
                        AJAX.get(window.location, {asin: item.asin}, true, function (res) {
                            if (!res.error) {
                                self.condition(res.condition);
                                self.price(res.price);
                                self.weight(res.weight);
                                self.product_id(item.asin);
                                self.origin_price(res.origin_price);
                                self.shipping_fee(res.shipping_fee);
                                self.term_fee(res.term_fee);

                                $('.gallery').slick({
                                    autoplay: true,
                                    vertical: false,
                                    verticalSwiping: false,
                                    prevArrow: '<a class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
                                    nextArrow: '<a class="slick-next"><i class="fas fa-chevron-right"></i></button>',
                                    //mobileFirst: true,
                                    slidesToShow: 5,
                                    responsive: [
                                        {
                                            breakpoint: 1025,
                                            settings: {
                                                slidesToShow: 4
                                            }
                                        },
                                        {
                                            breakpoint: 769,
                                            settings: {
                                                slidesToShow: 4
                                            }
                                        },
                                        {
                                            breakpoint: 480,
                                            settings: {
                                                slidesToShow: 3
                                            }
                                        }
                                    ]
                                });
                                self.is_loading(false);

                                try {
                                    $('.gallery').slick("unslick");
                                } catch (e) {
                                }

                                self.attributes(res.attributes);
                                self.images(res.images);
                            }
                        });
                        break;
                    }
                }
            }
        });
    };

    self.change_variant_label = function (variant) {
        if (!variant.enable())
            return;

        if ($('#cart-form input[rel=variant]').length == 0)
            return;

        if (!self.is_loading()) {
            var selected_val = [];
            //  Những giá trị thuộc tính đi kèm với asin theo first_val
            var available_values = [];

            for (var i = 0; i < product.variations.length; i++) {
                var item = product.variations[i];
                if (item.value.includes(variant.value())) {
                    for (var j = 0; j < item.value.length; j++) {
                        if (item.value[j] != variant.value())
                            available_values.push(item.value[j]);
                    }
                }
            }

            self.variations.each(function (x) {
                if (x.selected() != null)
                    selected_val.push(x.selected());
                else {
                    x.selected(x.values()[0].value());
                    selected_val.push(x.values()[0].value());
                }

                if (x.name() == variant.name()) {
                    x.selected(variant.value());
                } else {
                    x.values.each(function (t) {
                        t.enable(available_values.indexOf(t.value()) >= 0);
                    });
                }
            });

            var selected_asin = null
            for (var i = 0; i < product.variations.length; i++) {
                var item = product.variations[i];
                var diff = self.diff(item.value, selected_val);

                if (diff == 0) {
                    selected_asin = item.asin;
                    break;
                }
            }

            if (selected_asin != null) {
                AJAX.get(window.location, {asin: selected_asin}, true, function (res) {
                    if (!res.error) {
                        try {
                            $('.gallery').slick("unslick");
                        } catch (e) {
                        }

                        self.attributes(res.attributes);
                        self.images(res.images);
                        self.thumbnail(res.images[res.images.length - 1].large);
                        $('#zoomImg img').attr('src', res.images[0].large);
                        MagicZoom.update('#zoomImg', res.images[0].large, res.images[0].large);

                        self.condition(res.condition);
                        self.price(res.price);
                        self.weight(res.weight);
                        self.product_id(item.asin);
                        self.origin_price(res.origin_price);
                        self.shipping_fee(res.shipping_fee);
                        self.term_fee(res.term_fee);

                        $('.gallery').slick({
                            autoplay: true,
                            vertical: false,
                            verticalSwiping: false,
                            prevArrow: '<a class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
                            nextArrow: '<a class="slick-next"><i class="fas fa-chevron-right"></i></button>',
                            //mobileFirst: true,
                            slidesToShow: 5,
                            responsive: [
                                {
                                    breakpoint: 1025,
                                    settings: {
                                        slidesToShow: 4
                                    }
                                },
                                {
                                    breakpoint: 769,
                                    settings: {
                                        slidesToShow: 4
                                    }
                                },
                                {
                                    breakpoint: 480,
                                    settings: {
                                        slidesToShow: 3
                                    }
                                }
                            ]
                        });
                        self.is_loading(false);
                    }
                });
            }
        }
    };
}

var amzModel = new amzProductModel();
amzModel.init();
ko.applyBindings(amzModel, document.getElementById('product-page'));
amzModel.init_slide();
amzModel.re_check_variant($('input[rel=variant]:first').attr('name'));