function registerModel() {
    var self = this;
    self.name = ko.observable().extend({required: {message: 'Bắt buộc'}});
    self.email = ko.observable().extend({required: {message: 'Bắt buộc'}});
    self.phone = ko.observable().extend({required: {message: 'Bắt buộc'}});
    self.pass = ko.observable().extend({required: {message: 'Bắt buộc'}});
    self.re_pass = ko.observable().extend({required: {message: 'Bắt buộc'}});

    self.register = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            var data = {
                email: self.email(),
                phone: self.phone(),
                name: self.name(),
                pass: self.pass(),
                re_pass: self.re_pass()
            };

            AJAX.post('/dang-ky', data, true, function (res) {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    self.name(undefined);
                    self.email(undefined);
                    self.phone(undefined);
                    self.pass(undefined);
                    self.re_pass(undefined);
                    self.errors.showAllMessages(false);

                    $('#modal-register').modal('hide');
                    window.setTimeout(function () {
                        Swal.fire({
                            type: 'success',
                            title: '<strong>TẠO TÀI KHOẢN THÀNH CÔNG!</strong>',
                            html: '<p><center>Chúng tôi đã gửi email thông tin tài khoản của bạn.<br/>Xin vui lòng kiểm tra email</p>' +
                                'Bạn có thể <a href="javascript:" onclick="javascript: show_login_modal()">đăng nhập tại đây</a></center>'
                        });
                    }, 100);
                }
            });
        }
    };
}

var regModel = new registerModel();
ko.validatedObservable(regModel);
//ko.applyBindings(regModel, document.getElementById('register-page'));
ko.applyBindings(regModel, document.getElementById('modal-register'));