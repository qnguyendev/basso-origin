<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
if ($this->session->flashdata('error_msg') != null)
    $error_msg = $this->session->flashdata('error_msg');

if ($this->session->flashdata('success_msg') != null)
    $success_msg = $this->session->flashdata('success_msg');
?>
@layout('default_layout')
@section('content')
    <main class="order-page">
        <div class="container">
            <h2 class="h2-title mb-5">QUÊN MẬT KHẨU</h2>
            <div class="row">
                <div class="col-md-12 col-lg-6 offset-lg-3">
                    @if(isset($error_msg))
                        <div class="alert alert-warning">{{$error_msg}}</div>
                    @endif

                    @if(!isset($success_msg))
                        <form method="post" autocomplete="off">
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4 col-xl-3">
                                    Địa chỉ email
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-12 col-lg-8 col-xl-9">
                                    <input type="email" required class="form-control" name="email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-lg-8 col-xl-9 offset-lg-4 offset-xl-3">
                                    <button class="btn button" type="submit">
                                        KHÔI PHỤC
                                    </button>
                                </div>
                            </div>
                        </form>
                    @else
                        <div class="bg-light border border-white p-4">
                            Cảm ơn quý khách,<br/>
                            <p class="mt-1">Basso đã gửi cho bạn email hướng dẫn khôi phục mật khẩu, vui lòng kiểm tra email và làm
                                theo hướng dẫn để lấy lại mật khẩu mới.</p>
                            Basso trân trọng!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </main>
@endsection