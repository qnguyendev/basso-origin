<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
if ($this->session->flashdata('error_msg') != null)
    $error_msg = $this->session->flashdata('error_msg');

if ($this->session->flashdata('success_msg') != null)
    $success_msg = $this->session->flashdata('success_msg');
?>
@layout('default_layout')
@section('content')
    <main class="order-page" id="register-page">
        <div class="container">
            <h2 class="h2-title mb-5">ĐĂNG KÝ</h2>
            <div class="row">
                <div class="col-md-12 col-lg-6 offset-lg-3">
                    @if(isset($error_msg))
                        <div class="alert alert-warning">{{$error_msg}}</div>
                    @endif

                    @if(!isset($success_msg))
                        <form method="post" autocomplete="off">
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4 col-xl-3">
                                    Họ tên
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: name"/>
                                </label>
                                <div class="col-md-12 col-lg-8 col-xl-9">
                                    <input type="text" required class="form-control" data-bind="value: name"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4 col-xl-3">
                                    Địa chỉ email
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: email"/>
                                </label>
                                <div class="col-md-12 col-lg-8 col-xl-9">
                                    <input type="email" required class="form-control" autocomplete="off"
                                           data-bind="value: email"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4 col-xl-3">
                                    Điện thoại
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: phone"/>
                                </label>
                                <div class="col-md-12 col-lg-8 col-xl-9">
                                    <input type="text" required class="form-control" data-bind="value: phone"
                                           autocomplete="off"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4 col-xl-3">
                                    Mật khẩu
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: pass"
                                          autocomplete="off"/>
                                </label>
                                <div class="col-md-12 col-lg-8 col-xl-9">
                                    <input type="password" class="form-control" autocomplete="off" required
                                           data-bind="value: pass"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4 col-xl-3">
                                    Xác nhận mật khẩu
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: re_pass"/>
                                </label>
                                <div class="col-md-12 col-lg-8 col-xl-9">
                                    <input type="password" class="form-control" autocomplete="off" required
                                           data-bind="value: re_pass"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-lg-8 col-xl-9 offset-lg-4 offset-xl-3">
                                    <p><span class="text-danger">*</span> thông tin bắt buộc</p>
                                    <button class="btn button" data-bind="click: register">
                                        ĐĂNG KÝ
                                    </button>
                                </div>
                            </div>
                        </form>
                    @else
                        <div class="alert alert-success">
                            <p>Chúng tôi đã gửi email thông báo việc hoàn tất tài khoản tại Basso.vn</p>
                            <p>Bạn có thể đăng nhập để bắt đầu đặt hàng với <b>Basso</b></p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </main>
@endsection

@section('footer')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{theme_dir('js/register.js?v='.time())}}"></script>
    <script>
        function show_login_modal() {
            Swal.close();
            $('#modal-login').modal();
        }
    </script>
@endsection

@section('head')
    <style>
        .validationMessage {
            display: block;
            font-size: .9em;
            color: red;
            font-style: italic;
        }
    </style>
@endsection