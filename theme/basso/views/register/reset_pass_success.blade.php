<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('default_layout')
@section('content')
    <main class="order-page">
        <div class="container">
            <h2 class="h2-title mb-5">ĐẶT LẠI MẬT KHẨU</h2>
            <div class="row">
                <div class="col-md-12 col-lg-6 offset-lg-3">
                    <div class="bg-light border border-white p-4">
                        <div class="text-center text-success" style="font-size: 6em">
                            <i class="fa fa-check-circle"></i>
                        </div>
                        <p>
                            Basso đã cập nhật mật khẩu mới cho quý khách. Quý khách có thể bắt đầu
                            <a href="javascript:" data-toggle="modal" data-target="#modal-login">
                                <strong>đăng nhập tại đây</strong>
                            </a>.
                        </p>
                        Basso trân trọng!
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection