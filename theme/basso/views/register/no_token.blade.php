<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('default_layout')
@section('content')
    <main class="order-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-6 offset-lg-3">
                    <div class="bg-light border border-white p-4">
                        <div class="text-center text-danger" style="font-size: 6em">
                            <i class="fa fa-exclamation-triangle"></i>
                        </div>
                        Rất xin lỗi, chúng tôi không tìm thấy đường dẫn theo yêu cầu của quý khách.<br/>
                        <p>
                            Nếu có bất kỳ thắc mắc hoặc góp ý, quý khách xin vui lòng gọi cho Basso tới số <a class="font-weight-bold"
                                    href="tel: +84965687790">0965 687 790</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection