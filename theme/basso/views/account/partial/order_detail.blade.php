<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$index = 1;
$tongiavnd = 0;
?>
@layout('account_layout')
@section('right-content')
    <h3 class="name">CHI TIẾT ĐƠN HÀNG: {{$order->order_code}}</h3>
    <h5 class="account-sub-name">Tình trạng vận chuyển</h5>
    
	<div class="account-ship-step">
                        <div class="step-li step-li-1">
                            @if(count($payments) > 0 || $order_info['in_warehouse_date'] != null || $order_info['in_inventory_date'] != null || $order_info['shipped_time'] != null)
                                <img src="{{theme_dir('images/icon/order_steps/1.png')}}" width="100"/>
                            @else
                                <img src="{{theme_dir('images/icon/order_steps/1_.png')}}" width="100"/>
                            @endif
                            <div>
							@if(count($payments) > 0)
								Đã thanh toán<br>
								<!--{{date('d/m/Y', $payments[0]->created_time)}}-->
							@else
								Chưa thanh toán
							@endif
                            </div>
                        </div>
                        <div class="step-li step-li-2">
                            @if($order_info['in_warehouse_date'] != null  || $order_info['in_inventory_date'] != null || $order_info['shipped_time'] != null)
                                <img src="{{theme_dir('images/icon/order_steps/2.png')}}" width="100"/>
                            @else
                                <img src="{{theme_dir('images/icon/order_steps/2_.png')}}" width="100"/>
                            @endif
                            <div>
                                 @if($order_info['in_warehouse_date'] != null  || $order_info['in_inventory_date'] != null || $order_info['shipped_time'] != null)
									Hàng về kho {{$order->country}}<br>
                                    <!--{{$order_info['in_warehouse_date']}}-->
                                @else
                                    Chưa về kho {{$order->country}}
                                @endif
                            </div>
                        </div>
                        <div class="step-li step-li-3">
                            @if($order_info['in_inventory_date'] != null)
                                <img src="{{theme_dir('images/icon/order_steps/3.png')}}" width="100"/>
                            @else
                                <img src="{{theme_dir('images/icon/order_steps/3_.png')}}" width="100"/>
                            @endif
                            <div>
                                @if($order_info['in_inventory_date'] != null  || $order_info['shipped_time'] != null)
                                    Hàng về Việt Nam<br>
                                   <!-- {{$order_info['in_inventory_date']}}-->
                                @else
                                    Chưa về Việt Nam
                                @endif
                            </div>
                        </div>
                        <div class="step-li step-li-4">
                            @if($order_info['shipped_time'] != null)
                                <img src="{{theme_dir('images/icon/order_steps/4.png')}}" width="100"/>
                            @else
                                <img src="{{theme_dir('images/icon/order_steps/4_.png')}}" width="100"/>
                            @endif
                            <div>
                                @if($order_info['shipped_time'] != null)
                                    Giao hàng<br>
                                    {{$order_info['shipped_time']}}
                                @else
                                    Chưa giao hàng
                                @endif
                            </div>
                        </div>
                    </div>

                    <h4 class="page-header border-bottom pb-1 mb-4">Thông tin người đặt</h4>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th width="200">Họ tên</th>
                                    <td>{{$order->name}}</td>
                                </tr>
                                <tr>
                                    <th>Điện thoại</th>
                                    <td>{{$order->phone}}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{$order->email}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6 col-12">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th width="200">Địa chỉ</th>
                                    <td>{{$order->shipping_address}}</td>
                                </tr>
                                <tr>
                                    <th>Quận/huyện</th>
                                    <td>{{$order->district}}</td>
                                </tr>
                                <tr>
                                    <th>Tỉnh/thành phố</th>
                                    <td>{{$order->city}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <h4 class="page-header border-bottom pb-1 mb-4 mt-4">Thông tin đơn hàng</h4>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th width="200">Mã đơn hàng</th>
                                    <td>{{$order->order_code}}</td>
                                </tr>
                                <tr>
                                    <th>Ngày đặt hàng</th>
                                    <td>{{date('d/m/Y', $order->created_time)}}</td>
                                </tr>
                                <tr>
                                    <th>Tổng giá trị đơn hàng</th>
                                    <td>{{format_money($order->sub_total + is_enull($order->discount_amount, 0))}}</td>
                                </tr>
                                <tr>
                                    <th>Trạng thái đơn hàng</th>
                                    <td>
                                        @if($order->status == CustomerOrderStatus::COMPLETED)
                                            Đã hoàn tất
                                        @elseif($order->status == CustomerOrderStatus::CANCELLED)
                                            Đã hủy
                                        @else
                                            Đã tiếp nhận
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6 col-12">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>Hình thức thanh toán</th>
                                    <td>{{$order->payment_method}}</td>
                                </tr>
                                <tr>
                                    <th>Trạng thái thanh toán</th>
                                    <td>{{$order->total_paid == 0 ? 'Chưa đặt cọc' : 'Đã đặt cọc'}}</td>
                                </tr>
                                <tr>
                                    <th> Số tiền đã thanh toán</th>
                                    <td>{{format_money($order->total_paid)}}</td>
                                </tr>
                                <tr>
                                    <th>Số tiền còn lại</th>
                                    <td>{{format_money($order->sub_total - $order->total_paid)}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    
    <div class="table-responsive mb-4">
        <table class="table table-bordered account-order-table">
            <thead>
            <tr>
                <th class="text-center">STT</th>
                <th class="text-center" width="100">Hình ảnh</th>
                <th class="text-center">Tên sản phẩm</th>
                <th class="text-center" width="100">Giá SP</th>
                <th class="text-center">Số lượng</th>
                <th class="text-center">Phụ thu</th>
                <th class="text-center">Phí vận chuyển</th>
                <th class="text-center" width="120">Tổng giá SP</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td class="text-center">{{$index}}</td>
                    <td class="text-center p-1">
                        @if($item->image_path != null)
                            <img src="{{$item->image_path}}"
                                 class="img-fluid mx-auto d-block" alt="">
                        @else
                            <img src="/assets/img/no-image-available.jpg" width="80" height="80"/>
                        @endif
                    </td>
                    <td>
                        <a href="{{$item->link}}" target="_blank">
                            {{$item->name}}
                        </a>
                        @if($item->variations != null)
                            @if(!empty($item->variations))
                                <br/>
                                <?php $item->variations = json_decode($item->variations, true);?>
                                @foreach($item->variations as $var)
                                    <strong>{{$var['name']}}</strong>: {{$var['value']}}<br/>
                                @endforeach
                            @endif
                        @endif
                    </td>
                    <td class="text-center">
                        {{$order->currency_symbol}}
                        {{number_format($item->price  + $item->web_shipping_fee, 2, '.', ',')}}
                    </td>
                    <td class="text-center">{{$item->quantity}}</td>
                    <td class="text-center">{{format_money($item->term_fee)}}</td>
                    <td class="text-center">{{format_money($item->item_world_shipping_fee)}}</td>
					<?php
					$item_total = $item->quantity * $item->price * $order->currency_rate +
						$item->term_fee * $item->quantity +
						$item->item_world_shipping_fee * $item->quantity;
					$tongiavnd += $item_total;
					?>
					<td class="text-center">{{format_money($item_total)}}</td>
                    <?php
                    /* $item_total = $item->quantity * $item->price * $order->currency_rate +
                        $item->term_fee * $item->quantity +
                        $item->quantity * $item->weight * 260000; 
                    <td class="text-center">{{format_money($item->sub_total + $item->shipping_fee)}}</td>*/
                    ?>
                </tr>
                <?php $index++; ?>
            @endforeach
       
			<tr>
				<td align="right" colspan="7"
					style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
					<strong>Giảm giá</strong>
				</td>
				<td style=" padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center"
					align="center">
					<strong>{{format_money($order->discount_amount)}}</strong>
				</td>
			</tr>
			<tr>
				<td align="right" colspan="7"
					style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
				   <strong>Phí dịch vụ {{$order->fee_percent}}%</strong>
					<?php 
					/*$percent_amount = round(100*$order->total_paid/$order->sub_total);
					if($percent_amount >= 50 && $percent_amount < 80){ ?>
					<strong>Phí dịch vụ (+4%)</strong>
					<?php } elseif($percent_amount >= 80 && $percent_amount < 100){ ?>
					<strong>Phí dịch vụ (+2%)</strong>
					<?php } else { ?>
					<strong>Phí dịch vụ</strong>
					<?php } */?>            
					</td>
				<td style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center"
					align="center">
					<strong>{{format_money(($tongiavnd * $order->fee_percent)/100)}}</strong>
				</td>
			</tr>
			<?php if (strpos($order->website, 'ebay') !== false) { ?>
			<tr>
				<td align="right" colspan="7"
					style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
					<strong>Phí vận chuyển quốc tế</strong>
				</td>
				<td style=" padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center"
					align="center">
					<strong>{{format_money($order->world_shipping_fee_total)}}</strong>
				</td>
			</tr>
			<?php } ?>
			
			<tr>
				<td align="right" colspan="7"
					style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
					<strong>Phí vận chuyển nội địa</strong>
				</td>
				<td style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center">
					<strong>{{format_money($order->inter_shipping_fee)}}</strong>
				</td>
			</tr>
			<tr>
				<td align="right" colspan="7"
					style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
					<strong>Tổng giá trị đơn hàng</strong>
				</td>
				<td style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center"
					align="center">
					<strong>{{format_money($order->sub_total)}}</strong>
				</td>
			</tr>
            </tbody>
        </table>
    </div>

    <!--
    <div class="table-responsive mb-4">
        <table class="table table-bordered account-order-table">
            <thead>
            <tr>
                <th class="text-center">STT</th>
                <th class="text-center" width="100">Hình ảnh</th>
                <th class="text-center">Tên sản phẩm</th>
                <th class="text-center" width="90">Giá SP</th>
                <th class="text-center">Số lượng</th>
                <th class="text-center" width="120">Tổng giá SP</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td class="text-center">{{$index}}</td>
                    <td class="text-center p-1">
                        @if($item->image_path != null)
                            <img src="{{$item->image_path}}"
                                 class="img-fluid mx-auto d-block" alt="">
                        @else
                            <img src="/assets/img/no-image-available.jpg" width="80" height="80"/>
                        @endif
                    </td>
                    <td>
                        <a href="{{$item->link}}" target="_blank">
                            {{$item->name}}
                        </a>
                    </td>
                    <td class="text-center">
                        {{$order->currency_symbol}}
                        {{number_format($item->price  + $item->web_shipping_fee, 2, '.', ',')}}
                    </td>
                    <td class="text-center">{{$item->quantity}}</td>
                    <?php
                    $item_total = $item->quantity * $item->price * $order->currency_rate + $item->quantity * $item->weight * 260000;
                    ?>
                    <td class="text-center">{{format_money($item_total + $item->quantity * $item->term_fee)}}</td>
                </tr>
                <?php $index++; ?>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-7">
            <h5 class="account-sub-name">Địa chỉ giao hàng</h5>
            <div class="address-li">
                <label>Họ tên:</label> {{$order->name}}
            </div>
            <div class="address-li">
                <label>Số điện thoại:</label> {{$order->phone}}
            </div>
            <div class="address-li">
                <label>Tỉnh thành:</label> {{$order->city}}
            </div>
            <div class="address-li">
                <label>Quận huyện:</label> {{$order->district}}
            </div>
            <div class="address-li">
                <label>Địa chỉ:</label> {{$order->shipping_address}}
            </div>
        </div>
        <div class="col-md-12 col-lg-5 md-space">
            <h5 class="account-sub-name">Đơn hàng</h5>
            <div class="address-li">
                <label>Tổng giá ngoại tệ:</label>
                {{$order->currency_symbol}}
                {{number_format($order->total, 2,'.',',')}}
            </div>
            <div class="address-li">
                <label>Tổng giá VNĐ:</label> {{format_money($order->currency_rate * $order->total)}}
            </div>
            <div class="address-li">
                <label>Phí vận chuyển quốc tế:</label>
                {{isset($order->world_shipping_fee) ? format_money($order->world_shipping_fee) : '-'}}
            </div>
            <div class="address-li">
                <label>Phí vận chuyển nội địa:</label> {{format_money($order->inter_shipping_fee)}}
            </div>
            <div class="address-li">
                <label>Chiết khấu giảm giá:</label> {{format_money(is_enull($order->discount_amount, 0))}}
            </div>
            <div class="address-li address-total">
                <label>Tổng cộng:</label> {{format_money($order->sub_total + $order->inter_shipping_fee + is_enull($order->discount_amount, 0))}}
            </div>
        </div>
    </div>
    -->
@endsection