<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('account_layout')
@section('right-content')

    @if($this->session->flashdata('error_msg'))
        <div class="alert alert-danger">
            {{$this->session->flashdata('error_msg')}}
        </div>
    @endif

    <h3 class="name">QUẢN LÝ ĐƠN HÀNG</h3>
    @if($this->input->get('id') == null)
        @include('partial/order_list')
    @else
        @include('partial/order_detail')
    @endif
@endsection

@section('head')
    <style>
        .order-page {
            min-height: 700px
        }
    </style>
@endsection