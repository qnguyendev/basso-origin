<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('account_layout')
@section('right-content')
    <form autocomplete="off" id="form-account">
        <h3 class="name">THÔNG TIN TÀI KHOẢN</h3>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">
                Họ tên
                <span class="text-danger">*</span><br/>
                <span class="validationMessage" data-bind="validationMessage: user.name"/>
            </label>
            <div class="col-md-8 col-lg-6">
                <input type="text" class="form-control" data-bind="value: user.name"/>
            </div>
        </div>
        <!--div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">Ngày sinh</label>
            <div class="col-md-8 col-lg-6">
                <input class="form-control" type="date"/>
            </div>
        </div-->
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">Giới tính</label>
            <div class="col-md-8 col-lg-6">
                <select class="form-control" data-bind="value: user.gender">
                    <option value="unknown">-</option>
                    <option value="male">Nam</option>
                    <option value="female">Nữ</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">
                Số điện thoại
                <span class="text-danger">*</span><br/>
                <span class="validationMessage" data-bind="validationMessage: user.name"/>
            </label>
            <div class="col-md-8 col-lg-6">
                <input type="text" class="form-control" data-bind="value: user.phone"/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">
                Email
                <span class="text-danger">*</span><br/>
                <span class="validationMessage" data-bind="validationMessage: user.name"/>
            </label>
            <div class="col-md-8 col-lg-6">
                <input type="text" class="form-control" data-bind="value: user.email" disabled/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">
                Địa Chỉ
                <span class="text-danger">*</span><br/>
                <span class="validationMessage" data-bind="validationMessage: user.name"/>
            </label>
            <div class="col-md-8 col-lg-6">
                <input type="text" class="form-control" data-bind="value: user.address" autocomplete="off"/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">Thành Phố</label>
            <div class="col-md-8 col-lg-6">
                <select class="form-control" name="city"
                        data-bind="options: data.cities, value: user.city_id, optionsText: 'name',
                        optionsValue: 'id', event: {change: function(){ $root.data.change_city() }}"></select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">Quận/Huyện</label>
            <div class="col-md-8 col-lg-6">
                <select class="form-control"
                        data-bind="options: data.districts, value: user.district_id, optionsText: 'name', optionsValue: 'id'"></select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">
                Công nợ phải trả (đ) 
            </label>
            <div class="col-md-8 col-lg-6">
                <input type="text" class="form-control" data-bind="value: user.debit" autocomplete="off" disabled/>
            </div>
        </div>
        <br>
        <h3 class="account-name">MẬT KHẨU</h3>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">Mật khẩu mới</label>
            <div class="col-md-8 col-lg-6">
                <input type="password" class="form-control" data-bind="value: user.new_pass" autocomplete="off"/>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label">Xác nhận mật khẩu mới</label>
            <div class="col-md-8 col-lg-6">
                <input type="password" class="form-control" data-bind="value: user.re_pass" autocomplete="off"/>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 col-lg-3 col-form-label d-none d-md-block">&nbsp;</label>
            <div class="col-md-8 col-lg-6">
                <button class="btn button" data-bind="click: update">
                    Cập nhật
                </button>
                <span class="text-danger">*</span> <i>thông tin bắt buộc</i>
            </div>
        </div>
    </form>
@endsection

@section('footer')
    <script src="{{theme_dir('js/account.js?v='.time())}}"></script>
@endsection

@section('head')
    <style>
        .order-page {
            min-height: 700px
        }
    </style>
@endsection