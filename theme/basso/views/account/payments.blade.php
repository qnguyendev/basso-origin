<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$valid_orders = array_filter($orders, function ($item) {
    return $item->status != CustomerOrderStatus::CANCELLED;
});

$total = array_sum(array_column($valid_orders, 'sub_total'));
$processing_orders = array_filter($valid_orders, function ($item) {
    return $item->status != CustomerOrderStatus::COMPLETED;
});
$processing_total = array_sum(array_column($processing_orders, 'sub_total'));
$processing_paid = array_sum(array_column($processing_orders, 'total_paid'));
?>
@layout('account_layout')
@section('right-content')
    <h3 class="name">LỊCH SỬ THANH TOÁN</h3>

    <table class="table border-vertical table-bordered">
        <tbody>
        <tr>
            <th class="text-right">Tổng tiền</th>
            <td>{{format_money($total)}}</td>
            <th class="text-right">Tổng tiền đơn GD</th>
            <td>{{format_money($processing_total)}}</td>
        </tr>
        <tr>
            <th class="text-right">Khách đã trả đơn đang GD</th>
            <td>{{format_money($processing_paid)}}</td>
            <th class="text-right">Còn thiếu</th>
            <td>{{format_money($processing_total - $processing_paid)}}</td>
        </tr>
        </tbody>
    </table>

    <div class="table-responsive">
        <table class="table table-bordered account-order-table">
            <thead class="text-center">
            <tr>
                <th>Ngày</th>
                <th>Đơn hàng</th>
                <th>Nội dung</th>
                <th>Số tiền</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach($payments as $item)
                @if($item->status == PaymentHistoryStatus::COMPLETED)
                    <tr>
                        <td>
                            {{date('d/m/Y', $item->created_time)}}
                        </td>
                        <td>
                            <a href="{{base_url('tai-khoan/don-hang?id='. $item->order_id)}}">
                                {{$item->order_code}}
                            </a>
                        </td>
                        <td>
                            {{$item->note}}
                        </td>
                        <td>
                            {{format_money($item->amount)}}
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

    @if(count($payments) > 0)
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <?php
                $attributes = [
                    'ul_attr' => ['class' => 'pagination'],
                    'li_attr' => ['class' => 'page-item'],
                    'anchor_attr' => ['class' => 'page-link']
                ];
                Pagination::display(base_url('tai-khoan/thanh-toan'), $pagination->current_page, $pagination->total_page, $attributes);
                ?>
            </div>
            <div class="col-sm-12 col-md-6 text-right">
                Trang {{$pagination->current_page}}/{{$pagination->total_page}},
                có {{format_number($pagination->total_item, 0)}} kết quả
            </div>
        </div>
    @endif
@endsection

@section('head')
    <style>
        .order-page {
            min-height: 700px
        }
    </style>
@endsection