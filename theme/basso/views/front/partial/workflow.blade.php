<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<section class="order-workflow" id="order-workflow">
    <div class="container">
        <h4 class="h2-title">QUY TRÌNH ĐẶT HÀNG</h4>
        <div class="row li">
            <div class="col-md-6">
                <h3 class="title">
                    <span>1/ ĐẶT HÀNG</span>
                </h3>
                <div class="content text1">
                    <?php
                    $widgets = get_widgets('flow-order', ['text']);
                    if (count($widgets) > 0) {
                        foreach ($widgets as $widget) {
                            show_widget($widget);
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="img1"><img src="{{theme_dir('images/noimg.png')}}"
                                       data-play="{{theme_dir('images/DatHang_00215.gif')}}"
                                       data-pause="{{theme_dir('images/noimg.png')}}" alt=""
                                       class="img-fluid mx-auto d-block"></div>
            </div>
        </div>
        <div class="row li">
            <div class="col-md-6 desktop">
                <div class="img2"><img src="{{theme_dir('images/noimg.png')}}"
                                       data-play="{{theme_dir('images/VanChuyen_00200.gif')}}"
                                       data-pause="{{theme_dir('images/noimg.png')}}" alt=""
                                       class="img-fluid mx-auto d-block"></div>
            </div>
            <div class="col-md-6">
                <h3 class="title">
                    <span>2/ VẬN CHUYỂN</span>
                </h3>
                <div class="content text2">
                    <?php
                    $widgets = get_widgets('flow-ship', ['text']);
                    if (count($widgets) > 0) {
                        foreach ($widgets as $widget) {
                            show_widget($widget);
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-6 mobile">
                <div class="img3"><img src="{{theme_dir('images/noimg.png')}}"
                                       data-play="{{theme_dir('images/VanChuyen_00200.gif')}}"
                                       data-pause="{{theme_dir('images/noimg.png')}}" alt=""
                                       class="img-fluid mx-auto d-block"></div>
            </div>
        </div>
        <div class="row li">
            <div class="col-md-6">
                <h3 class="title">
                    <span>3/ NHẬN HÀNG</span>
                </h3>
                <div class="content text3">
                    <?php
                    $widgets = get_widgets('flow-receive', ['text']);
                    if (count($widgets) > 0) {
                        foreach ($widgets as $widget) {
                            show_widget($widget);
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="img4"><img src="{{theme_dir('images/noimg.png')}}"
                                       data-play="{{theme_dir('images/NhanHang_00029.gif')}}"
                                       data-pause="{{theme_dir('images/noimg.png')}}" alt=""
                                       class="img-fluid mx-auto d-block"></div>
            </div>
        </div>
    </div>
</section>
