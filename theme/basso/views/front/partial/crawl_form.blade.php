<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$error_msg = null;
if ($this->session->flashdata('error') != null)
    $error_msg = $this->session->flashdata('error');
?>
<section class="auto-quotes" id="crawl-form">
    <div class="container">
        <h4 class="h2-title">BÁO GIÁ TỰ ĐỘNG</h4>
        <p class="p-intro">
            Nhập đường link các sản phẩm từ <b>Ebay</b> hoặc <b>Amazon</b> để được báo giá nhanh nhất từ Basso
        </p>
        @if($error_msg != null)
            <div class="alert alert-danger">{{$error_msg}}</div>
        @endif

        <form id="home_search_form" method="post" onsubmit="return submit_crawl()">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="www." name="url"/>
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                </div>
            </div>
            <div class="action">
                <button type="submit" class="btn button">XEM GIÁ</button>
            </div>
        </form>
    </div>
</section>
