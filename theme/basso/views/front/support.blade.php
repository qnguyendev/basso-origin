<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$widget_news = get_widgets('support-news', ['block_text']);
$widget_main = get_widgets('support-main', ['block_text']);
$widget_faq = get_widgets('support-faq', ['block_text']);

$widget = null;
$index = 0;

$widget = $this->widget_model->get_by_id($this->widget_id);
if ($widget != null) {
    $widget->data = [];
    $widget->show_title = $widget->show_title == 1;

    if ($widget->options != null)
        $widget->data = json_decode($widget->options, false);
}

?>
@layout('default_layout')
@section('content')
    <main class="help-page">
        <div class="w">
            <div class="container">
                <h2 class="h2-title">TRỢ GIÚP</h2>
                <div class="row">
                    <div class="col-md-4 col-lg-3">
                        @if(count($widget_news) > 0)
                            <ul class="help-nav">
                                <li class="parent">
                                    TIN TỨC
                                </li>
                                @foreach($widget_news as $t)
                                    <li class="{{$this->widget_id == $t->id ? 'active' : null}}">
                                        <a href="/tro-giup/{{$t->id}}">{{$t->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif

                        @if(count($widget_main) > 0)
                            <ul class="help-nav">
                                <li class="parent">
                                    TRỢ GIÚP
                                </li>
                                @foreach($widget_main as $t)
                                    <li class="{{$this->widget_id == $t->id ? 'active' : null}}">
                                        <a href="/tro-giup/{{$t->id}}">{{$t->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif

                        @if(count($widget_faq) > 0)
                            <ul class="help-nav">
                                <li class="parent">
                                    CÂU HỎI THƯỜNG GẶP
                                </li>
                                @foreach($widget_faq as $t)
                                    <li class="{{$this->widget_id == $t->id ? 'active' : null}}">
                                        <a href="/tro-giup/{{$t->id}}">{{$t->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="col-md-8 col-lg-9">

                        @if($widget != null)
                            <div class="accordion" id="accordionPayment">
                                @if(is_array($widget->data))
                                    @foreach($widget->data as $item)
                                        <div class="card">
                                            <div class="card-header p-0" id="heading-{{$item->id}}">
                                                <div class="custom-control custom-radio custom-control-inline d-block p-0" data-toggle="collapse"
                                                     data-target="#collapse-{{$item->id}}" aria-expanded="{{$index == 0 ? 'true' : 'false'}}"
                                                     aria-controls="collapse-{{$item->id}}">
                                                    <input type="radio" id="headship-{{$item->id}}" name="headship"
                                                           class="custom-control-input" {{$index == 0 ? 'checked' : null}}/>
                                                    <label class="custom-control-label d-block m-0 pt-3 pb-3 pl-4 pr-4" for="headship-{{$item->id}}">
                                                        {{$item->title}}
                                                    </label>
                                                </div>
                                            </div>

                                            <div id="collapse-{{$item->id}}" class="collapse {{$index == 0 ? 'show' : null}}"
                                                 aria-labelledby="heading-{{$item->id}}"
                                                 data-parent="#accordionPayment">
                                                <div class="card-body">
                                                    {{$item->content}}
                                                </div>
                                            </div>

                                            <?php $index++; ?>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection