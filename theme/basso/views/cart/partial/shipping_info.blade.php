<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
?>
<form class="order-contact">
    <h4 class="h4-title">
        <h4 class="h4-title">Thông tin người mua</h4>
    </h4>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Họ và tên (*)" required data-bind="value: shipping_info.name"/>
				<p class="validationMessage" data-bind="validationMessage: shipping_info.name" style="color: red"></p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Số điện thoại (*)" 
                       data-bind="value: shipping_info.phone" />
				<p class="validationMessage" data-bind="validationMessage: shipping_info.phone" style="color: red"></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email (*)" data-bind="value: shipping_info.email"/>
				<p class="validationMessage" data-bind="validationMessage: shipping_info.email" style="color: red"></p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group" >
                <input type="text" class="form-control"
                       placeholder="Địa chỉ ( Số nhà, đường, ...) (*)" data-bind="value: shipping_info.address"/>
				<p class="validationMessage" data-bind="validationMessage: shipping_info.address" style="color: red"></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <select class="form-control"
                        data-bind="options: cart.cities, value: shipping_info.city_id, event: {change: cart.change_city},
                        optionsText: 'name', optionsValue: 'id'">
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <select class="form-control"
                        data-bind="options: cart.districts, value: shipping_info.district_id,
                        optionsText: 'name', optionsValue: 'id'">
                </select>
            </div>
        </div>
    </div>
</form>
