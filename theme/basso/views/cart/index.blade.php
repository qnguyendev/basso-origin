
<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $cart;
$cart_total = array_sum(array_column($cart, 'total'));
?>
@layout('default_layout')
@section('content')

    <main class="order-page" id="cart-page">
        <div class="container">
            <h2 class="h2-title">Thanh toán đơn hàng</h2>
            <p class="p-intro"></p>

            @if(count($cart) > 0)
                <div data-bind="visible: items.list().length > 0">
                    @include('partial/shipping_info')

                    <h4 class="h4-title">Thông tin sản phẩm</h4>
                    @include('partial/items')

                    @include('partial/payments')
                </div>
            @else
                <div class="alert alert-info text-center" data-bind="visible: items.list().length == 0">
                    Không có sản phẩm trong đơn hàng. <a href="{{base_url('#crawl-form')}}">Click vào đây</a> để nhận
                    báo giá
                    nhanh nhất cho các sản phẩm từ Ebay và Amazon
                </div>
            @endif
        </div>
	    @include('partial/editor_modal')
        @include('partial/term_modal')
    </main>
@endsection

@section('footer')
    <script>
        var payment_limits = {{json_encode($payment_limits)}};
                @if($cart_total > 500)
        var discount_code = true;
        @endif
    </script>
    <script src='/assets/js/core/underscore.min.js'></script>
    <script src='/assets/js/core/underscore.ko.min.js'></script>
    <script src="{{theme_dir('js/cart.js?'.time())}}"></script>
@endsection