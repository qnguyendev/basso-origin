<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="app-figure" id="zoom-fig">
    <a id="zoomImg" class="MagicZoom" data-bind="attr: {href: thumbnail}">
        <img data-bind="attr: {src: thumbnail}"/>
    </a>

    <div class="selectors gallery" data-bind="foreach: images">
        <div>
            <a class="img mz-thumb-selected mz-thumb" data-zoom-id="zoomImg"
               data-bind="attr: {href: large}">
                <img height="80" data-bind="attr: {src: large, srcset: large}">
            </a>
        </div>
    </div>
</div>

