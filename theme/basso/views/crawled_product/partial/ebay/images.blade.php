<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="app-figure" id="zoom-fig">
    <a id="zoomImg" class="MagicZoom" href="{{$product->images[0]}}">
        <img src="{{$product->images[0]}}" class="img-fluid" alt="">
    </a>
    <div class="selectors gallery">
        @foreach($product->images as $key=>$value)
            <div>
                <a class="img mz-thumb-selected mz-thumb" data-zoom-id="zoomImg"
                   href="{{$value}}" data-image="{{$value}}">
                    <img src="{{$value}}" height="80"
                         srcset="{{$value}} 2x"
                         alt="">
                </a>
            </div>
        @endforeach
    </div>
</div>
