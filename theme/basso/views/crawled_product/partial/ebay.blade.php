<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="w" id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                @include('partial/ebay/images')
                <div class="clearfix"></div>
                <p class="pt-2">
                    <a href="{{$product->url}}" target="_blank">
                        Xem link gốc <i class="fa fa-arrow-right"></i>
                    </a>
                </p>
            </div>
            <div class="col-lg-7">
                <h1 class="product-head">
                    {{$product->title}}
                </h1>
                <!-- Reviews -->
                <div class="product-rating">
                    @if(strpos($product->website, 'amazon') !== false)
                        @for($i = 1; $i <= 5; $i++)
                            @if($product->rating - $i >= 0)
                                <i class="far fas fa-star"></i>
                            @elseif($product->rating - $i < 0 && $product->rating - $i > -1)
                                <i class="far fas fa-star-half-alt"></i>
                            @else
                                <i class="far fa-star"></i>
                            @endif
                        @endfor
                        {{isset($product->total_reviews) ? $product->total_reviews : 0}}
                        đánh giá từ <img src="{{theme_dir('images/amazon-logo.png')}}" height="15"/>
                    @endif
                </div>
                <!--/-->
                <div class="row">
                    <div class="col-lg-7">
                        <span class="product-price" data-bind="text: price">
                            {{format_money($product->price)}}
                        </span>
                        <a href="javascript:" class="view-price-detail">
                            (
                            Xem chi tiết)
                        </a>
                        <ul class="view-price-detail">
                            <li>
                                Giá sản phẩm trên Ebay: <b data-bind="text: origin_price"></b>
                            </li>
                            <li data-bind="visible: term_fee() > 0">
                                Giá phụ thu (nếu có): <b data-bind="text: parseInt(term_fee()).toMoney(0)"></b>
                            </li>
                            <li data-bind="visible: web_shipping_fee() > 0">
                                Phí ship web: $ <strong data-bind="text: parseFloat(web_shipping_fee()).toFixed(2)"></strong>
                            </li>
                        </ul>
                        @if($product->weight == 0)
                            <div class="product-meta highlight p-3">
                                Giá trên chưa bao gồm phí vận chuyển quốc tế. Basso sẽ thu thêm
                                <strong>250k/kg</strong>
                                theo cân
                                nặng thực tế khi hàng về
                            </div>
                        @else
                            <div class="product-meta highlight">
                                (Giá trọn gói về Việt Nam với trọng lượng ước tính
                                <b>{{format_number($product->weight, 2)}}
                                    kg</b>)
                            </div>
                        @endif
                        <div class="product-meta">
                            @if(strpos($product->website, 'amazon') !== false)
                            @else
                                <p>
                                    <strong> Bán bởi:</strong>
                                    <a href="{{$product->seller_url}}" target="_blank">{{$product->seller}}</a>
                                </p>
                            @endif
                        </div>
                        @include('partial/ebay/add_to_cart')
                        @if(!empty($product->category))
                            <div style="font-size: .97em">
                                <strong>Danh mục:</strong> {{join(', ', explode(':', $product->category))}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-5">
                        @include('partial/add_info')
                    </div>
                </div>
            </div>
        </div>
        <div class="product-content">
            <div class="product-content-title">
                THÔNG SỐ SẢN PHẨM (MÃ SP: {{$product->id}})
            </div>
            <div class="product-content-data">
                <table class="table table-striped table-bordered">
                    <tbody>
                    @foreach($product->attributes as $key=>$value)
                        <tr>
                            <td width="30%">{{$key}}</td>
                            <td>{{$value}}</td>
                        </tr>
                    @endforeach
                </table>

            </div>
            <div class="product-content-title">THÔNG TIN SẢN PHẨM</div>
            <div class="product-content-data p-0">
                <iframe src="/item/description/{{$product->id}}.html" class="border-0 d-block" width="100%"
                        height="600"></iframe>
            </div>
        </div>

    </div>
</div>
