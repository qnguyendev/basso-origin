<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="detail-more-info pt-0">
    <div style="background: #45a177; color: #FFF" class="p-2">
        Giảm 2% cho đơn hàng > $500
    </div>
    <ul class="benefit">
        <li><i class="icon icon-1"></i> <span>Vận chuyển nhanh</span></li>
        <li><i class="icon icon-2"></i> <span>Nguồn hàng uy tín</span></li>
        <li><i class="icon icon-3"></i> <span>Không lo thủ tục</span></li>
        <li><i class="icon icon-4"></i> <span>CSKH tư vấn 24/7</span></li>
    </ul>
    <div class="card-info"><label>Hỗ trợ thanh toán </label>
        <ul>
            <li><img src="{{theme_dir('images/icon-atm.png')}}" alt="ATM" title="ATM">
            </li>
            <li><img src="{{theme_dir('images/icon-visa.png')}}" alt="VISA"
                     title="VISA"></li>
            <li><img src="{{theme_dir('images/icon-master.png')}}" alt="MASTERCARD"
                     title="MASTERCARD"></li>
        </ul>
    </div>
    <div class="estimate-time">
        Sản phẩm sẽ giao vào khoảng ngày
        <span class="text-danger">{{date('d/m/Y', time() + 15 * 24 * 3600)}}</span>
        đến <span class="text-danger">{{date('d/m/Y', time() + 20 * 24 * 3600)}}</span>
        nếu quý khách thanh toán trong hôm nay
    </div>
    <p>
        <i class="fas fa-phone"></i>
        <span>Hotline hỗ trợ:</span>
        <span class="text-danger">096 568 7790</span>
    </p>
    <p>
        <a href="/chinh-sach-doi-tra">
            <i class="fas fa-redo-alt"></i>
            <span>Xem chính sách đổi trả</span>
        </a>
    </p>
</div>
