<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('default_layout')
@section('content')
    <main class="news">
        <div class="w">
            <div class="container">
                <h2 class="title text-uppercase">{{$term->name}}</h2>
                <div class="text">
                    {{$term->content}}
                </div>
            </div>
        </div>
        <div class="container items">
            <div class="row">
                @foreach($articles as $item)
                    <div class="col-md-6 col-lg-4">
                        <div class="li">
                            @if($item->image_path != null)
                                <a href="{{base_url($item->slug.'.html')}}">
                                    <img src="/timthumb.php?src={{$item->image_path}}&w=400&h=300" alt="{{$item->name}}"
                                         class="img-fluid mx-auto d-block"/>
                                </a>
                            @endif
                            <div class="g">
                                <h4 class="title">
                                    <a href="{{base_url($item->slug.'.html')}}">
                                        {{$item->name}}
                                    </a>
                                </h4>
                                <div class="intro">
                                    {{$item->short_content}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="paging">
                <nav>
                    <?php
                    $attributes = [
                        'ul_attr' => ['class' => 'pagination'],
                        'li_attr' => ['class' => 'page-item'],
                        'anchor_attr' => ['class' => 'page-link']
                    ];

                    Pagination::display(base_url($term->slug), $pagination->current_page, $pagination->total_page, $attributes);
                    ?>
                </nav>
            </div>
        </div>
    </main>
@endsection