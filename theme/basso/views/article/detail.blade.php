<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('default_layout')
@section('content')
    <main class="news news-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="wrap">
                        <h1 class="name">{{$article->name}}</h1>
                        <div class="intro">
                            {{$article->short_content}}
                        </div>
                        <div class="content">
                            {{$article->content}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <h3 class="name">TIN TỨC LIÊN QUAN</h3>
                    @foreach($related_articles as $item)
                        <div class="li">
                            <a href="{{base_url($item->slug.'.html')}}">
                                <img src="{{$item->image_path}}" alt="{{$item->name}}"
                                     class="img-fluid mx-auto d-block"/>
                            </a>
                            <div class="g">
                                <h3 class="title">
                                    <a href="{{base_url($item->slug.'.html')}}">
                                        {{$item->name}}
                                    </a>
                                </h3>
                                <div class="intro">
                                    {{$item->short_content}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 dfc-article-related">
                        <h3 class="name">BÀI VIẾT LIÊN QUAN</h3>
                        <ul>
                        @foreach($related_articles as $item)
                            <li class="li">
                                <a href="{{base_url($item->slug.'.html')}}">
                                    <img src="{{$item->image_path}}" alt="{{$item->name}}"
                                         class="img-fluid mx-auto d-block"/>
                                </a>
                                <div class="g">
                                    <h3 class="title">
                                        <a href="{{base_url($item->slug.'.html')}}">
                                            {{$item->name}}
                                        </a>
                                    </h3>
                                    <div class="intro">
                                        {{$item->short_content}}
                                    </div>
                                </div>
                            </li>
                        @endforeach
                        </ul>
                </div>
            </div>
        </div>
    </main>
@endsection
