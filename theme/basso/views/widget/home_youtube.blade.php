<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@if(count($widget->data) > 0)
    <a class="mfp-video" href="javascript:" data-toggle="modal" data-target="#modal-home-youtube">
        <img data-src="{{$widget->data[0]->image_path}}" alt="" class="img-fluid mx-auto d-block lazyload">
    </a>
@endif
