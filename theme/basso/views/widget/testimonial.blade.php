<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<section class="testinomial">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 first">
                <h4 class="title">{{$widget->title}}</h4>
                <div class="text">{{$widget->description}}</div>
            </div>
            <div class="col-lg-8 reviews">
                @foreach($widget->data as $item)
                    <div>
                        <div class="li">
                            <div class="w">
                                <div class="text">{{$item->content}}</div>
                                <h4 class="name">{{$item->name}}</h4>
                                <div class="intro">{{$item->position}}</div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
