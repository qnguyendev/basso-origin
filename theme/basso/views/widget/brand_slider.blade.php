<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<section class="brand">
    <div class="container">
        @if($widget->show_title)
            <h4 class="h2-title">{{$widget->title}}</h4>
        @endif
        <div class="partner">
            @foreach($widget->data as $item)
                <div>
                    <div class="li">
                        <a href="{{$item->link == null ? 'javascript:' : $item->link}}" rel="nofollow">
                            <img data-src="{{$item->image_path}}" alt="{{$item->title}}" class="img-fluid mx-auto d-block lazyload">
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
