<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$posts = $this->article_model->get_widget(null, 'newest', 3);
?>
@if(!isset($_GET['no_layout']))
    @layout('default_layout')
@section('content')
    <main class="news about">
        <div class="w">
            @if($term->image_path != null)
                <img src="{{$term->image_path}}" alt="" class="img-fluid mx-auto d-block">
            @endif
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="wrap">
                        <h1 class="name">‌{{$term->name}}</h1>
                        {{$term->content}}
                    </div>
                </div>
                <div class="col-lg-3">
                    <h3 class="name">TIN TỨC MỚI</h3>
                    @foreach($posts as $item)
                        <div class="li">
                            <a href="{{'/'.$item->term_slug.'/'.$item->slug.'.html'}}">
                                <img src="{{$item->image_path}}" alt="" class="img-fluid mx-auto d-block"
                                     alt="{{$item->name}}"/>
                            </a>
                            <div class="g">
                                <h4 class="title">
                                    <a href="{{'/'.$item->term_slug.'/'.$item->slug.'.html'}}">{{$item->name}}</a>
                                </h4>
                                <div class="created">
                                    <span class="date">Ngày {{date('d/m/Y', $item->created_time)}}</span>
                                </div>
                                <div class="intro">{{$item->short_content}}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </main>
@endsection
@else
    {{$term->content}}
@endif
