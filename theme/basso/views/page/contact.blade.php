<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$contact_info = null;
$config = $this->option_model->get('contact_info');
if ($config != null)
    $contact_info = $config->value;
?>
@layout('default_layout')
@section('content')
    <main class="contact-page">
        <div class="container">

            <h2 class="title">LIÊN HỆ</h2>
            <div class="row">
                <div class="col-lg-6">
                    {{$contact_info}}
                </div>
                <div class="col-lg-6">
                    <form method="post" onsubmit="return submit_contact_form()" id="contact-form">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Họ và tên" required name="name"
                                           style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Số điện thoại" name="phone"
                                           required/>
                                </div>
                            </div>
                        </div>
                        <!--div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Địa chỉ ( Số nhà, đường, ...)">
                                </div>
                            </div>
                        </div-->
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Nhập nội dung" rows="4" required
                                      name="content"></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn button" type="submit">GỬI</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12896.054631583267!2d105.79276117894385!3d20.997731374527056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acba74d914a3%3A0x6410b626c0c6ec03!2zVMOyYSBOaMOgIEhIMiBC4bqvYyBIw6AsIFThu5EgSOG7r3UsIFRydW5nIFbEg24sIFRoYW5oIFh1w6JuLCBIw6AgTuG7mWksIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1554223129738!5m2!1sen!2s"
                width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
@endsection

@section('footer')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script>
        function submit_contact_form() {
            let name = $('#contact-form input[name=name]').val();
            let phone = $('#contact-form input[name=phone]').val();
            let content = $('#contact-form textarea').val();

            if (name != '' && phone != '' && content != '') {
                let data = {
                    name: name,
                    phone: phone,
                    content: content
                };

                AJAX.post('/send_contact', data, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        $('#contact-form input[name=name]').val('');
                        $('#contact-form input[name=name]').attr('value', '');
                        $('#contact-form input[name=phone]').val('');
                        $('#contact-form input[name=phone]').attr('value', '');
                        $('#contact-form textarea').val('');
                        $('#contact-form textarea').attr('value', '');

                        Swal.fire({
                            type: 'success',
                            html: '<p><center>Basso đã nhận được tin nhắn. Chúng tôi sẽ liên hệ lại sớm nhất</p></center>'
                        });
                    }
                });
            }

            return false;
        }
    </script>
@endsection