<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
$_product_url = null;
if (isset($product_url))
    $_product_url = $product_url;

$controller = $this->router->fetch_class();
?><!doctype html>
<html lang="vi-VN">
<head>
    {{site_head()}}
    <!-- <link rel="stylesheet" href="{{theme_dir('libraries/bootstrap/css/bootstrap.min.css')}}"/> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{theme_dir('fonts/UTMAvoBold.woff')}}" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="{{theme_dir('libraries/fontawesome/css/all.css')}}"/>
    <link rel="stylesheet" href="{{theme_dir('libraries/font-awesome-4.7.0/css/font-awesome.min.css')}}"> -->
    <!-- <link rel="stylesheet" href="{{theme_dir('libraries/slick/slick.min.css')}}"/> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" />
    
    <!-- <link rel="stylesheet" href="{{theme_dir('libraries/magnific/dist/magnific-popup.css')}}"/> -->
    <!-- <link rel="stylesheet" href="{{theme_dir('libraries/magiczoom/magiczoom.css')}}"/> -->
    <link rel="stylesheet" href="{{theme_dir('libraries/g5plus/css/style.min.css')}}"/>
    <link rel="stylesheet" href="{{theme_dir('css/style.min.css?v='.time())}}"/>
    <link rel="stylesheet" href="{{theme_dir('css/responsive.min.css?v='.time())}}"/>

    <!-- <link rel="stylesheet" href="{{theme_dir('libraries/jquery-ui/jquery-ui.css')}}"> -->
    <link rel="stylesheet" href="{{theme_dir('css/search_complete.min.css')}}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
     <script src="//cdn.jsdelivr.net/npm/@shinsenter/defer.js/dist/defer_plus.min.js"></script>
    <link rel="icon" href="{{fav_icon_url()}}"/>
    
    {{tracking_scripts('head')}}
    @yield('head')
</head>
<body>
<script>deferiframe('iframe[data-src],[data-style]')</script>
<script>deferimg('img[data-src],picture,video,audio')</script>
<nav class="navdevice">
    @include('nav_mobile')
</nav>
<header>
    @include('header')
</header>
<section class="main-menu">
    <div class="container position-relative">
        <nav class="navbar navbar-expand-lg">
            <div class="collapse navbar-collapse" id="menu">
                @include('nav')
            </div>
        </nav>
        <div class="quotation">
            @if($controller == 'front')
                <a href="{{base_url('#crawl-form')}}">BÁO GIÁ NHANH</a>
            @else
                <a href="javascript:" rel="modal-quick-crawl">BÁO GIÁ NHANH</a>
            @endif
        </div>
        <div class="search-menu" id="crawl-form-head">
            <form method="post" onsubmit="return submit_crawl_head()">
                <div class="input-group" style="border-radius: 6px; overflow: hidden">
                    <input type="text" class="form-control" name="url" placeholder="Nhập link sản phẩm cần báo giá"
                           value="{{$_product_url}}"/>
                    <button type="submit" class="input-group-append border-0" style="background: white">
                        <div class="input-group-text"><i class="fas fa-search"></i></div>
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>
<?php if (base_url() !== current_url()): ?>
<div class="container breadcrumb-front">
    <nav class="breadcrumbs uppercase">
        <a href="<?php echo base_url()?>">Trang chủ</a>
        <?php if (isset($breadcrumbs_front)):
        foreach ($breadcrumbs_front as $value):?>
        <i class="fas fa-angle-right"></i> <a href="<?php echo $value["url"]?>"><?php echo $value["title"]?></a>
        <?php  endforeach; ?>
        <?php endif;?>
    </nav>

</div>
<?php endif ?>

@yield('content')
<section class="newsletter">
    <div class="w">
        <div class="container">
            <h4 class="title">ĐĂNG KÝ NHẬN TIN SALE</h4>
            <p class="text">từ hàng trăm thương hiệu hàng đầu thế giới</p>
            <form method="post" onsubmit="return subscribe()" id="subscribe-form">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" placeholder="Nhập email ...">
                    <div class="input-group-prepend">
                        <button type="submit" class="btn input-group-text"><i class="fas fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<section class="bottom">
    @include('footer')
</section>
<footer>
    <div class="container">

        <?php
        $widgets = get_widgets('bottom-footer', ['text']);
        foreach ($widgets as $widget)
            show_widget($widget, 'bottom_footer_text');
        ?>

            <div class="text-center social-icons">
                <a href="https://www.facebook.com/bassohangmy" target="_blank" rel="nofollow">
                    <i class="fa fa-facebook"></i>
                </a>
                <a href="https://twitter.com/bassovietnam" target="_blank" rel="nofollow">
                    <i class="fa fa-twitter"></i>
                </a>
                <a href="https://www.pinterest.com/bassovietnam" target="_blank" rel="nofollow">
                    <i class="fa fa-pinterest"></i>
                </a>
                <a href="https://www.linkedin.com/in/basso-amazon-ebay" target="_blank" rel="nofollow">
                    <i class="fa fa-linkedin"></i>
                </a>
                <a href="https://gab.com/bassovietnam" target="_blank" rel="nofollow">
                    <i class="fab fa-gofore"></i>
                </a>
                <a href="https://myspace.com/bassovietnam" target="_blank" rel="nofollow">
                    <i class="fas fa-users"></i>
                </a>
                <a href="https://bassovietnam.tumblr.com" target="_blank" rel="nofollow">
                    <i class="fa fa-tumblr"></i>
                </a>
                <a href="https://www.reddit.com/user/basso-amazon-ebay" target="_blank" rel="nofollow">
                    <i class="fa fa-reddit"></i>
                </a>
            </div>
    </div>
</footer>
<div id="countdown-bg">
    <div id="countdown">
        <div id="countdown-number"></div>
        <svg>
            <circle r="36" cx="40" cy="40"></circle>
        </svg>
    </div>
</div>
@include('modal_login')
@include('modal_register')
@include('modal_crawl_error')
@include('modal_no_support_sites')
@include('loader')
@if($controller != 'home')
    @include('modal_quick_crawl_form')
@endif
@yield('modal')

<div id="modal-quick-search" style="display: none;">
    <section class="auto-quotes" id="crawl-form" style="display: block;">
        <button class="close">
            x
        </button>
        <div class="container">
            <h4 class="h2-title">XEM THÔNG TIN ĐƠN HÀNG</h4>
            <form id="order_search" action="/don-hang" class="order_search">
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group hover-to-show">
                            <input type="text" class="form-control required" placeholder="Tìm đơn hàng ..." name="id"
                                   value="{{$this->input->get('id')}}"/>
                            <div class="input-group-prepend">
                                <button type="submit" class="input-group-text"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

{{tracking_scripts('body')}}
</body>
</html>

<!-- <script src="{{theme_dir('js/lazysizes.min.js')}}" async=""></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.2.2/lazysizes.min.js" integrity="sha512-TmDwFLhg3UA4ZG0Eb4MIyT1O1Mb+Oww5kFG0uHqXsdbyZz9DcvYQhKpGgNkamAI6h2lGGZq2X8ftOJvF/XjTUg==" crossorigin="anonymous"></script>
<!-- <script src="{{theme_dir('libraries/jquery.min.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha512-+NqPlbbtM1QqiK8ZAo4Yrj2c4lNQoGv8P79DPtKzj++l5jnN39rHA/xsqn8zE9l0uSoxaCdrOgFs6yjyfbBxSg==" crossorigin="anonymous"></script>
<!-- <script src="{{theme_dir('libraries/bootstrap/js/bootstrap.min.js')}}"></script> -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<!-- <script src="{{theme_dir('libraries/slick/slick.min.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A==" crossorigin="anonymous"></script>
<!-- <script src="{{theme_dir('libraries/magnific/dist/jquery.magnific-popup.js')}}"></script> -->
<!-- <script src="{{theme_dir('libraries/magiczoom/magiczoom.js')}}"></script> -->
<script src="{{theme_dir('libraries/jquery.cookie.min.js')}}"></script>
<script src="{{theme_dir('libraries/jquery.hoverIntent.minified.js')}}"></script>
<script src="{{theme_dir('libraries/jquery.dcjqaccordion.2.7.minified.js')}}"></script>
<!-- <script src="{{theme_dir('libraries/jquery.nicescroll.min.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.0/jquery.nicescroll.min.js" integrity="sha512-UNhXgGfBNK/HX/XNqxbToByWCenHAQRgJxcpXt5eE8BytOVON5xcNm4BSSYBYV0NwNm7EjEghcpKhqKKM792zA==" crossorigin="anonymous"></script>
<script src="{{theme_dir('libraries/g5plus/js/main.min.js')}}"></script>
<script src="{{theme_dir('libraries/g5plus/js/script.min.js')}}"></script>
<!-- <script src="/assets/js/core/moment.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js" integrity="sha512-O2Y8hD83PQtRf8vcr0N+yxwRtErIVaHJ4NOpojzq2yvUmhiJbQIT9OAYu27t+mVk814t+ongBVGx+YGylICVkQ==" crossorigin="anonymous"></script>
<!-- <script src="/assets/js/core/knockout-3.4.2.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js" integrity="sha512-7KKD9IIJL3eTtMFYDMg09ZvR+Vi2GyCvBawcXCBJlnbfuZtYv/z47wsWb6BIGFC/eLH09OVFARagNh1szpULNA==" crossorigin="anonymous"></script>
<!-- <script src="/assets/js/core/knockout.mapping-latest.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/knockout.mapping/2.4.1/knockout.mapping.min.js" integrity="sha512-1LyBPWtezbX0LO4X4QzhfZd9jSetVu5W0IFpMV9ecq4lJUQBR6hx8uiNfUI2Aoe7LCF4rK1NV3rY+uil4VJ3QQ==" crossorigin="anonymous"></script>
<!-- <script src="/assets/js/core/knockout.validation.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/knockout-validation/1.0.2/knockout.validation.min.js" integrity="sha512-WCEIXEAimdf34IPp4/ASCthr3xc2Nc9htamuSMirjCI5SyJaV/jMG/hyJOt1sI4/yUopClsWpXwzJoT2sLumMw==" crossorigin="anonymous"></script>
<script src="/assets/vendor/hullabaloo.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.min.js" integrity="sha512-hxM27B68Dk70CMshtrqU02mFUoSIJQOw4RwdNHicieNEn1PK+6HehfAaUEv3wxXsRyuNThXn0YLdvd8UiYm+PA==" crossorigin="anonymous"></script> -->
<script src="/assets/js/resources/functions.js?v={{time()}}"></script>

<script src="{{theme_dir('js/script.min.js?v='.time())}}"></script>
<script>
    jQuery(document).ready(function(){

        setTimeout(function() {
            jQuery('.testinomial').attr('style', "background: url({{theme_dir('images/testinomial.webp')}}) no-repeat center top;");
            jQuery('.pricing').attr('style', "background: url({{theme_dir('images/pricing-fade.webp')}}) no-repeat center center;");
        },2000);

    });
</script>
<script type="text/javascript">
deferstyle("{{theme_dir('libraries/fontawesome/css/all.min.css')}}", 'all-css', 2000);
deferstyle("{{theme_dir('libraries/font-awesome-4.7.0/css/font-awesome.min.css')}}", 'awe470-css', 2000);
</script>


@if(!$this->ion_auth->logged_in())
    <!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.19.0/sweetalert2.min.js" integrity="sha512-qTLwJB5LmeSb1mIHoxDhhRRP0rc9LEDNE8o9QV3PbxTySvgSMD6LPQGuPquR2448ErUTppi7Kb3v8/5tSn1S2A==" crossorigin="anonymous"></script>
    {{-- <script defer src="/assets/js/core/ko.extends.js"></script> --}}
    <script src="{{theme_dir('js/user_modal.min.js?v='.time())}}"></script>
    <script src="{{theme_dir('js/register.min.js?v='.time())}}"></script>
@endif
@yield('footer')
{{tracking_scripts('footer')}}


<!-- <script src="{{theme_dir('libraries/jquery-ui/jquery-ui.min.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#twotabsearchtextbox').autocomplete({
            source: function (Query, Response) {
                $.ajax({
                    url: "http://completion.amazon.com/search/complete",
                    dataType: "jsonp",
                    data: {
                        q: 'casc',
                        "search-alias": "aps",
                        client: "amazon-search-ui",
                        mkt: 1,
                    },
                    success: function (Data) {
                        if (Data) {
                            var DataArr = Data.toString().split(',');
                            var Queries = new Array();
                            for (var q = 1; q < (DataArr.length - 1); q++) {
                                DataArr[q] = $.trim(DataArr[q]);
                                if (0 < DataArr[q].length && DataArr[q] != '[object Object]' && !$.isNumeric(DataArr[q])) {
                                    Queries.push(DataArr[q]);
                                }
                            }
                            /* console.log("Response");
                            console.log(Data); */
                            if (0 < Queries.length) {
                                Response(Queries);
                            }
                        }
                    }
                });
            },
            delay: 1000,
            minLength: 2
        });


        $('.order_search').hover(function () {
            $(".hover-to-show input").addClass("hover-to-show-input-hover");
        }, function () {
            $(".hover-to-show input").removeClass("hover-to-show-input-hover");
        });

        $("#order_search").submit(function () {
            if ($('.order_search input.required').val() == '') {
                $('.order_search input.required').addClass('highlight');
            } else {
                $('.order_search input.required').removeClass('highlight');
            }

            if ($('.order_search input.required.highlight').size() > 0) {
                return false;
            }
            return true;
        });
    });
</script>
