<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <!-- created with Free Online Sitemap Generator www.xml-sitemaps.com -->

    <url>
        <loc>https://basso.eprtech.com/</loc>
        <lastmod>2019-08-16T02:32:32+00:00</lastmod>
        <priority>1.00</priority>
    </url>
    <url>
        <loc>https://basso.eprtech.com/lien-he</loc>
        <lastmod>2019-08-16T02:32:32+00:00</lastmod>
        <priority>0.64</priority>
    </url>
    <url>
        <loc>https://basso.eprtech.com/gio-hang</loc>
        <lastmod>2019-08-16T02:32:32+00:00</lastmod>
        <priority>0.64</priority>
    </url>
    <url>
        <loc>https://basso.eprtech.com/dang-ky</loc>
        <lastmod>2019-08-16T02:32:32+00:00</lastmod>
        <priority>0.64</priority>
    </url>
    <url>
        <loc>https://basso.eprtech.com/quen-mat-khau</loc>
        <lastmod>2019-08-16T02:32:32+00:00</lastmod>
        <priority>0.64</priority>
    </url>
    @foreach($this->post as $item)
        <url>
            <loc>{{base_url($item->slug .'.html')}}</loc>
            <lastmod>{{$item->updated_time == null ? date('Y-m-d', $item->created_time) : date('Y-m-d', $item->updated_time)}}</lastmod>
            <priority>0.64</priority>
        </url>
    @endforeach

    <!-- @foreach($this->page as $item)
        <url>
            <loc>{{base_url($item->slug)}}</loc>
            <lastmod>{{$item->updated_time == null ? date('Y-m-d', $item->created_time) : date('Y-m-d', $item->updated_time)}}</lastmod>
            <priority>
                @if(in_array($item->slug, ['mua-hang-my', 'mua-hang-ebay', 'mua-hang-amazon']))
                    0.9
                @else
                    0.64
                @endif
            </priority>
        </url>
    @endforeach -->

    @foreach($this->widget_news as $item)
        <url>
            <loc>{{base_url('/tro-giup/' . $item->id)}}</loc>
            <lastmod>2019-08-16T02:32:32+00:00</lastmod>
            <priority>0.64</priority>
        </url>
    @endforeach

    @foreach($this->widget_main as $item)
        <url>
            <loc>{{base_url('/tro-giup/' . $item->id)}}</loc>
            <lastmod>2019-08-16T02:32:32+00:00</lastmod>
            <priority>0.64</priority>
        </url>
    @endforeach

    @foreach($this->widget_faq as $item)
        <url>
            <loc>{{base_url('/tro-giup/' . $item->id)}}</loc>
            <lastmod>2019-08-16T02:32:32+00:00</lastmod>
            <priority>0.64</priority>
        </url>
    @endforeach
</urlset>
