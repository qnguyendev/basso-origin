<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$user = $this->ion_auth->user()->row();
$action = $this->router->fetch_method();
?>
@layout('default_layout')
@section('content')
    <main class="order-page">
        <div class="container">
            <h2 class="h2-title mb-5">Kết quả tìm kiếm với '<?php echo isset($_GET["url"])?strtolower($_GET["url"]):""; ?>'</h2>
            <div class="order-wrapper">
                <div class="">
                    @yield('right-content')
                </div>
            </div>
        </div>
    </main>
@endsection