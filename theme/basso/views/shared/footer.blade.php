<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="logo">
                <a href="/">
                    <img src="{{theme_dir('images/logo-white.png')}}" alt="" class="img-fluid">
                </a>
            </div>
				
            <?php
            $widgets = get_widgets('footer-1', ['text', 'script', 'links']);
            foreach ($widgets as $widget)
                show_widget($widget, "footer_{$widget->type}");
            ?>
        </div>
        <div class="col-md-4 mt30">
            <?php
            $widgets = get_widgets('footer-2', ['text', 'script', 'links']);
            foreach ($widgets as $widget)
                show_widget($widget, "footer_{$widget->type}");
            ?>
        </div>
        <div class="col-md-4 mt30">
            <?php
            $widgets = get_widgets('footer-3', ['text', 'script', 'links']);
            foreach ($widgets as $widget)
                show_widget($widget, "footer_{$widget->type}");
            ?>
        </div>
    </div>
</div>
