<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal" tabindex="-1" role="dialog" id="modal-crawl-error">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="text-center p-3" style="font-size: 1.4em">
                    Hệ thống đang quá tải, xin vui lòng thử lại, hoặc chat với CSKH để được báo giá nhanh nhất.
                </p>
                <p class="text-center">
                    <a class="btn button" href="javascript:void(Tawk_API.toggle())">
                        <i class="fa fa-comments"></i>
                        Chat với CSKH
                    </a>
                    <button class="btn button" onclick="retry_crawl()">
                        <i class="fa fa-redo"></i>
                        Thử lại
                    </button>
                </p>
            </div>
        </div>
    </div>
</div>