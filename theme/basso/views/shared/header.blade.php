<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
global $cart;
?>
<div class="l1"></div>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-4">
            <button type="button" class="navbar-toggle" id="navdevice">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="logo">
                <a href="{{base_url()}}">
                    <img src="{{logo_url()}}" alt="" class="img-fluid"/> 
                </a>
            </div>
            <div class="user-links">
                <a href="/"><i class="far fa-question-circle"></i></a>
                @if(!$this->ion_auth->logged_in())
                    <a href="javascript:" data-toggle="modal" data-target="#modal-login">
                        <i class="far fa-user-circle"></i>
                    </a>
                @else
                    <a href="{{base_url('tai-khoan/don-hang')}}"><i class="far fa-user-circle"></i></a>
                @endif
                <a href="{{base_url('gio-hang')}}"><i class="fas fa-shopping-cart"></i></a>
            </div>
        </div>
        <div class="col-md-12 col-lg-8">
            <ul class="links">
                <li class="phone">
                    HOTLINE<br>
                    096 568 7790
                </li>
                <li class="help">
                    <a href="/tro-giup" rel="nofollow">
                        TRỢ GIÚP
                    </a>
                </li>
                <li class="account">
                    @if(!$this->ion_auth->logged_in())
                        <a href="javascript:show_login_modal()" rel="nofollow">
                            ĐĂNG NHẬP
                        </a><br>
                        <a href="javascript:show_register_modal()" rel="nofollow">
                            ĐĂNG KÝ
                        </a>
                    @else
                        <a href="{{base_url('tai-khoan')}}" rel="nofollow">TÀI KHOẢN</a><br/>
                        <a href="{{base_url('auth/logout')}}" rel="nofollow">THOÁT</a>
                    @endif
                </li>
                <li class="cart">
                    <a href="{{base_url('gio-hang')}}" rel="nofollow">GIỎ HÀNG (<span
                                id="cart-total">{{array_sum(array_column($cart, 'qty'))}}</span>)</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="l2"></div>
