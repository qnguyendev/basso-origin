<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal" tabindex="-1" role="dialog" id="modal-login">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ĐĂNG NHẬP</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="login-form">
                    <form method="get" action="" data-bind="submit: login">
                        <div class="alert alert-danger"
                             data-bind="visible: error_msg() !== undefined, text: error_msg"></div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Email đăng nhập"
                                   data-bind="value: email"/>
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="far fa-envelope"></i></div>
                            </div>
                        </div>
                        <div class="input-group">
                            <input type="password" class="form-control" placeholder="Mật khẩu" data-bind="value: pass"/>
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-key"></i></div>
                            </div>
                        </div>
                        <div class="input-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="remember">
                                <label class="custom-control-label mt-0" for="remember">Tự động đăng nhập</label>
                            </div>
                            <a href="{{base_url('quen-mat-khau')}}" class="aforget">Quên mật khẩu</a>
                        </div>
                        <div class="input-group">
                            <button type="submit" class="btn button btn-login">Đăng nhập</button>
                        </div>
                        <div class="input-group">
                            Bạn chưa có tài khoản? &nbsp;
                            <a href="javascript:" onclick="javascript: show_register_modal()">Đăng ký</a>
                        </div>
                    </form>
                </div>
				<ul class="social-logins">
					<li class="social-login-google"><a href="{{$GLOBALS['google_login_url']}}" class="aforget">Đăng nhập với Google</a></li>
					<!--<li class="social-login-zalo"><a href="{{$GLOBALS['zalo_login_url']}}" class="aforget">Đăng nhập với Zalo</a></li>-->
				</ul>
            </div>
        </div>
    </div>
</div>