<div class="modal" tabindex="-1" role="dialog" id="modal-no-support_sites">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ERROR !</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center p-3" style="font-size: 1.4em">
                    <p>
                        Hiện tại Basso chỉ báo giá cho đường link sản phẩm. 
						Nếu quý khách muốn tìm kiếm sản phẩm xin vui lòng liên hệ với CSKH để được trợ giúp.
					</p>
                </div>
                <p class="text-center">
                    <a class="btn button" href="javascript:void(Tawk_API.toggle())" target="_blank">
                        <i class="fa fa-comments"></i>
                        Chat với CSKH
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>