<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class User
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_URI $uri
 * @property User_model $user_model
 * @property CI_Form_validation $form_validation
 * @property Customer_model $customer_model
 */
class User extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('management/customer_model');
    }

    public function index()
    {
        $roles = $this->ion_auth->groups()->result();

        $this->blade->set('breadcrumbs', [['text' => 'Quản lý thành viên']])
            ->set('roles', $roles)
            ->render();
    }

    public function GET_index()
    {
        //  Danh sách tài khoản
        if ($this->input->get('init') != null)
            json_success(null, ['data' => $this->user_model->get()]);

        //  Chi tiết tài khoản
        $id = $this->input->get('id');
        if ($id != null) {
            $user = $this->ion_auth->user($id)->row();
            $user->name = $user->first_name;
            $user->roles = array_column($this->ion_auth->get_users_groups($id)->result(), 'id');

            json_success(null, ['data' => $user]);
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('email', null, 'required|valid_email');
        $this->form_validation->set_rules('active', null, 'required|numeric');

        if (!$this->form_validation->run())
            json_error('Vui lòng nhập thông tin bắt buộc');

        $name = $this->input->post('name');
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $pass = $this->input->post('pass');
        $roles = $this->input->post('roles');
        $active = $this->input->post('active');
        $phone = $this->input->post('phone');

        if ($id == 0) {
            $this->form_validation->set_rules('pass', null, 'required');
            if (!$this->form_validation->run())
                json_error('Vui lòng nhập mật khẩu');
        }

        if ($id == 0) {
            if ($this->ion_auth->email_check($email)) {
                $user = $this->customer_model->get_by_email($email);
                if ($user != null) {

                    if ($user->id != null) {
                        $groups = $this->ion_auth->get_users_groups($user->id)->result();
                        if (!in_array(6, array_column($groups, 'id')))
                            json_error('Địa chỉ email đã tồn tại');
                        else {
                            $data = [
                                'first_name' => $name,
                                'active' => $active,
                                'phone' => $phone
                            ];

                            if ($pass != null) {
                                if (!empty($pass))
                                    $data['password'] = $pass;
                            }

                            $this->ion_auth->update($user->id, $data);
                            $this->ion_auth->remove_from_group(null, $user->id);

                            if ($roles != null) {
                                if (count($roles) > 0)
                                    $this->ion_auth->add_to_group($roles, $user->id);
                            }
                        }
                    }
                }
            } else {
                $data = ['first_name' => $name, 'active' => $active, 'phone' => $phone];
                $this->ion_auth->register($email, $pass, $email, $data, $roles);
            }
        } else {
            $data = [
                'first_name' => $name,
                'active' => $active,
                'phone' => $phone
            ];

            if ($pass != null) {
                if (!empty($pass))
                    $data['password'] = $pass;
            }

            $this->ion_auth->update($id, $data);
            $this->ion_auth->remove_from_group(null, $id);

            if ($roles != null) {
                if (count($roles) > 0)
                    $this->ion_auth->add_to_group($roles, $id);
            }
        }

        json_success($id == 0 ? 'Thêm tài khoản thành công' : 'Cập nhật thành công');
    }

    public function POST_delete()
    {
        $id = $this->input->post('id');
        if ($id != null) {
            if (is_numeric($id)) {
                $user = $this->ion_auth->user()->row();
                if ($user->id != $id) {
                    $this->ion_auth->delete_user($id);
                    json_success('Xóa tài khoản thành công');
                    die;
                } else {
                    json_error('Bạn không thể xóa tài khoản của chính mình');
                    die;
                }
            }
        }

        json_error('Tài khoản không tồn tại');
        die;
    }
}