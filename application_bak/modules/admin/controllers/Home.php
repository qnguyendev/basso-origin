<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class admin/Home
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Cache $cache
 * @property Home_model $home_model
 * @property CI_Form_validation $form_validation
 */
class Home extends \Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('status');
        $this->load->model('home_model');
    }

    public function index()
    {
        $breadcrumbs = [['text' => 'Trang chủ quản trị']];

        if (!$this->ion_auth->in_group('editor')) {
            $waiting = $this->home_model->orders('waiting', 0, time());
            return $this->blade
                ->set('waiting', $waiting)
                ->set('breadcrumbs', $breadcrumbs)
                ->render();
        }

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');

        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $time = $this->input->get('time');
        $from = $this->input->get('start');
        $to = $this->input->get('end');

        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $start = strtotime(date('Y-m-d 00:00:00', $day));
                $end = time();
                break;
            case 'month':
                $start = strtotime(date('Y-m-1 00:00:00'));
                $end = time();
                break;
            case 'custom':
                $start = strtotime("$from 00:00:00");
                $end = strtotime("$to 23:59:59");
                break;
            case 'all':
                $start = strtotime('2018-10-10');
                $end = time();
                break;
            default:
                $start = strtotime(date('Y-m-d 00:00:00'));
                $end = time();
                break;
        }

        $orders = $this->home_model->orders([
            OrderStatus::PROCESSING,
            OrderStatus::COMPLETED,
            OrderStatus::WAITING
        ], $start, $end);

        $charts = [];
        $success_orders = $orders;/*array_filter($orders, function ($item) {
            return $item->shipping_status == OrderStatus::COMPLETED || $item->shipping_status == OrderStatus::PROCESSING;
        });*/

        for ($i = $start; $i <= $end; $i += 24 * 3600) {
            $_orders = array_filter($success_orders, function ($item) use ($i) {
                return $item->created_time >= $i && $item->created_time <= $i + 24 * 3600 - 1;
            });

            if (array_sum(array_column($_orders, 'sub_total')) == 0)
                continue;

            array_push($charts, [
                'date' => date('d/m/Y', $i),
                'total' => array_sum(array_column($_orders, 'sub_total'))
            ]);
        }

        json_success(null, [
            'chart' => [
                'revenue' => array_column($charts, 'total'),
                'date' => array_column($charts, 'date')
            ],
            'total_order' => format_number(count($orders), 0),
            'revenue' => format_money(array_sum(array_column($success_orders, 'sub_total'))),
        ]);
    }

    public function clear_cache()
    {
        $this->cache->clean();
        return redirect(base_url());
    }

    /**
     * Thay đổi ngôn ngữ
     * @param $lang
     */
    public function lang($lang)
    {
        set_cookie('admin_lang', $lang, 30 * 24 * 3600, null, '/');
        if (isset($_SERVER["HTTP_REFERER"]))
            return redirect($_SERVER["HTTP_REFERER"]);

        return redirect(base_url('cpanel'));
    }
}