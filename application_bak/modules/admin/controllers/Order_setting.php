<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Order_Setting_model $order_setting_model
 * @property Option_model $option_model
 * @property Order_Label_model $order_label_model
 */
class Order_setting extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_setting_model');
        $this->load->model('order_label_model');
        $this->load->model('option_model');
    }

    public function payment()
    {
        $breadcrumbs = [['text' => 'Đơn hàng', 'url' => '/admin/orders'],
            ['text' => 'Cấu hình thanh toán']];

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render();
    }

    public function GET_payment()
    {
        $type = $this->input->get('type');
        if ($type == null) {
            $payments = $this->order_setting_model->get();
            json_success(null, ['data' => $payments]);
        }

        $banks = $this->order_setting_model->get_banks();
        json_success(null, ['banks' => $banks]);
    }

    public function POST_payment()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $des = $this->input->post('des');
        $type = $this->input->post('type');
        $app_key = $this->input->post('app_key');
        $app_secret = $this->input->post('app_secret');
        $bank_list = $this->input->post('bank_list');

        if ($id == null || $name == null || $type == null)
            json_error('Vui lòng nhập các thông tin bắt buộc');

        if (in_array($type, ['banking', 'visa'])) {
            if ($app_key == null || $app_secret == null) {
                json_error('Vui lòng nhập mã cho thanh toán online');
            }
        }

        $this->order_setting_model->update($id, $name, $des, $app_key, $app_secret);
        $this->order_setting_model->insert_banks($id, $bank_list);

        json_success('Cập nhật thành công');
    }

    public function POST_change_active()
    {
        $id = $this->input->post('id');
        $active = $this->input->post('active');
        $this->order_setting_model->change_active($id, $active);
        die;
    }

    public function index()
    {
        $data = ['order_send_list' => null,            //  Email chính nhận thông báo có đơn hàng
            'order_thank_you_content' => null,
            'order_email_footer_content' => null];

        foreach ($data as $key => $value) {
            $option = $this->option_model->get($key);
            if ($option != null)
                $data[$key] = $option->value;
        }

        $breadcrumbs = [['text' => 'Đơn hàng', 'url' => '/admin/orders'],
            ['text' => 'Cấu hình đặt hàng']];

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render($data);
    }

    public function POST_index()
    {
        $valid_keys = ['order_send_list', 'order_thank_you_content', 'order_email_footer_content'];

        foreach ($_POST as $key => $value) {
            if (in_array($key, $valid_keys))
                $this->option_model->save($key, $value);
        }

        $this->session->set_flashdata('success', 'Cập nhật thành công');
        return redirect('/admin/order_setting');
    }

    /**
     * Quản lý nhãn đơn hàng
     */
    public function label()
    {
        $breadcrumbs = [['text' => 'Đơn hàng', 'url' => '/admin/orders'],
            ['text' => 'Quản lý nhãn đơn hàng']];

        return $this->blade->set('breadcrumbs', $breadcrumbs)->render();
    }

    public function GET_label()
    {
        $id = $this->input->get('id');
        if ($id == null)
            json_success(null, ['data' => $this->order_label_model->get()]);

        if (is_numeric($id))
            json_error('Không tìm thấy nhãn theo yêu cầu');

        json_success(null, ['data' => $this->order_label_model->get_by_id($id)]);
    }

    public function POST_label()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $des = $this->input->post('des');
        $color = $this->input->post('color');

        if ($id == null || $name == null)
            json_error('Vui lòng nhập thông tin yêu cầu');

        $slug = url_title($name, '-', true);
        $label = $this->order_label_model->get_by_slug($slug);

        if ($label != null) {
            if ($label->id != $id) {
                json_error('Nhãn đã tồn tại trong hệ thống');
            }
        }

        $this->order_label_model->insert($id, $name, $slug, $des, $color);
        json_success($id == 0 ? 'Thêm nhãn thành công' : 'Cập nhật thành công');
    }

    public function POST_delete_label()
    {
        $id = $this->input->post('id');
        if ($id == null)
            json_error('Không tìm thấy nhãn');

        if (!is_numeric($id))
            json_error('Không tìm thấy nhãn');

        if ($this->order_label_model->get_by_id($id))
            json_error('Không tìm thấy nhãn');

        $this->order_label_model->delete($id);
        json_success(null);
    }
}