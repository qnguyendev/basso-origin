<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Brand
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Brand_model $brand_model
 * @property CI_Upload $upload
 * @property Image_model $image_model
 * @property CI_Form_validation $form_validation
 */
class Brand extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
            return redirect('/admin/auth/login');

        $this->load->model('image_model');
        $this->load->model('brand_model');
    }

    public function index()
    {
        $breadcrumbs = [['text' => 'Quản lý thương hiệu']];
        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->set('page_title', 'Quản lý thương hiệu')->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        if ($page != null) {
            if (is_numeric($page)) {
                $page = $page < 1 ? 1 : $page;
                $result = $this->brand_model->get($page);
                $total_item = $this->brand_model->count();
                $pagination = Pagination::calc($total_item, $page, $this->config->item('result_per_page'));
                json_success(null, ['data' => $result, 'pagination' => $pagination]);
            }
        }
        die;
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if ($this->form_validation->run() == false)
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $brand = $this->brand_model->get_by_id($id);
        if ($brand != null) {
            if ($brand->image_id != null)
                $this->image_model->set_active($brand->image_id, 0);

            $this->brand_model->delete($id);
            json_success('Xóa thương hiệu thành công');
        }

        json_error('Không tìm thấy thương hiệu');
    }

    public function create()
    {
        $breadcrumbs = [['text' => 'Quản lý thương hiệu', 'url' => '/admin/brand'],
            ['text' => 'Thêm thương hiệu']];

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->set('page_title', 'Thêm thương hiệu')->render('editor');
    }

    public function edit($id)
    {
        if ($id == null)
            return redirect('/admin/term');

        if (!is_numeric($id))
            return redirect('/admin/term');

        $breadcrumbs = [['text' => 'Quản lý thương hiệu', 'url' => '/admin/brand'],
            ['text' => 'Sửa thương hiệu']];

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->set('page_title', 'Cập nhật thương hiệu')->render('editor');
    }

    public function GET_edit($id)
    {
        $brand = $this->brand_model->get_by_id($id);
        if ($brand == null)
            json_error('Nhãn hiệu không tồn tại', ['url' => '/admin/brand']);
        json_success(null, ['data' => $brand]);
    }

    public function POST_save()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập thông tin bắt buộc');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $slug = $this->input->post('slug');
        $website = $this->input->post('website');
        $content = $this->input->post('content');
        $seo_title = $this->input->post('seo_title');
        $seo_des = $this->input->post('seo_des');
        $seo_index = $this->input->post('seo_index');
        $image = $this->input->post('image');

        $slug = $slug == null
            ? url_title($name, '-', true)
            : url_title($slug, '-', true);
        $slug = $this->brand_model->create_slug($id, $slug);

        $data = [
            'content' => $content,
            'website' => $website,
            'seo_title' => $seo_title,
            'seo_description' => $seo_des,
            'seo_index' => $seo_index,
            'slug' => $slug,
            'name' => $name,
            'image_id' => null,
            'image_path' => null
        ];

        //  Set các ảnh cũ chưa active
        if ($id > 0) {
            $brand = $this->brand_model->get_by_id($id);
            if ($brand->image_id != null)
                $this->image_model->set_active($brand->image_id, 0);
        }

        #region Hình ảnh upload
        if ($image != null) {
            $this->image_model->set_active($image);     //  Đánh dấu ảnh được sử dụng
            $img = $this->image_model->get($image);     //  Lấy thông tin ảnh
            if ($img != null) {
                $data['image_path'] = $img->path;
                $data['image_id'] = $img->id;
            }
        }
        #endregion

        $this->brand_model->insert($id, $data);

        success_msg($id == 0 ? 'Thêm thương hiệu thành công' : 'Cập nhật thương hiệu thành công');
        json_success(null, ['url' => '/admin/brand']);


    }

    public function POST_change_active()
    {
        $id = $this->input->post('id');
        $active = $this->input->post('active');
        if ($id != null && $active != null) {
            $this->brand_model->change_active($id, $active);
        }

        die;
    }
}