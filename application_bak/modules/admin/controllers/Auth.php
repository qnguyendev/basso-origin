<?php

if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Loader $load
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property CI_URI $uri
 * @property CI_Form_validation $form_validation
 * @property CI_Session $session
 * @property Email_model $email_model
 * @property Log_model $log_model
 * @property CI_Encryption $encryption
 */
class Auth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('common');
        $this->load->model('email_model');
        $this->load->library('encryption');
    }

    public function _remap($method)
    {
        $param_offset = 2;
        if ($this->input->method(true) != 'GET')
            $method = $this->input->method(true) . '_' . $method;
        else {
            if ($this->input->is_ajax_request())
                $method = 'GET_' . $method;
        }

        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        if (method_exists($this, $method))
            return call_user_func_array(array($this, $method), $params);

        show_error('Can not found method: ' . $method);
    }

    public function login()
    {
        if ($this->ion_auth->logged_in()) {
            $redirect_url = base_url('admin');
            if (get_cookie('prev_url') != null)
                $redirect_url = get_cookie('prev_url');

            return redirect($redirect_url);
        }

        $this->blade->render();
    }

    public function POST_login()
    {
        $this->form_validation->set_rules('email', null, 'required|valid_email');
        $this->form_validation->set_rules('pass', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $email = $this->input->post('email');
        $pass = $this->input->post('pass');

        $valid = $this->ion_auth->login($email, $pass, true);
        if ($valid) {
            $redirect_url = base_url('admin');
            if (get_cookie('prev_url') != null)
                $redirect_url = get_cookie('prev_url');

            json_success(null, ['url' => $redirect_url]);
        }

        json_error('Tài khoản không hợp lệ');
    }

    /**
     * Log the user out
     */
    public function logout()
    {
        $this->ion_auth->logout();
        $this->session->sess_destroy();
        redirect('admin/auth/login');
    }

    public function POST_change_pass()
    {
        if (!$this->ion_auth->logged_in()) {
            json_error('Vui lòng F5, sau đó đăng nhập lại');
            die;
        }

        $this->form_validation->set_rules('new_pass', null, 'required');
        $this->form_validation->set_rules('re_pass', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $new_pass = $this->input->post('new_pass');
        $re_pass = $this->input->post('re_pass');

        if ($re_pass != $new_pass)
            json_error('Xác nhận mật khẩu không khớp');

        $user = $this->ion_auth->user()->row();
        $this->load->model('log_model');
        $this->log_model->insert($user->id, 'admin/auth', 'change_pass', "$user->email đổi mật khẩu sang: $new_pass");

        $this->ion_auth->update($user->id, ['password' => $new_pass]);

        json_success('Đổi mật khẩu thành công');
    }

    public function reset()
    {
        return $this->blade->render();
    }

    public function POST_reset()
    {
        $this->form_validation->set_rules('email', null, 'required|valid_email');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập email hợp lệ');

        $email = $this->input->post('email');
        $email = strtolower(trim($email));

        if (!$this->ion_auth->email_check($email))
            json_error('Không tìm thấy email trong hệ thống');

        $user = $this->ion_auth->where('email', $email)->users()->row();
        if ($user == null)
            json_error('Không tìm thấy tài khoản trong hệ thống');

        $reset_url = base_url('admin/auth/reset_pass?token=' . urlencode($this->encryption->encrypt($email . '|' . $user->id . '|' . time())));
        $content = $this->blade->render('partial/forgot_pass_email', ['name' => $user->first_name, 'reset_url' => $reset_url], true);
        $subject = "Khôi phục mật khẩu quản trị";
        $this->email_model->insert($subject, $content, $email);

        json_success('Vui lòng kiểm tra email để thay đổi mật khẩu');
    }

    public function reset_pass()
    {
        if ($this->ion_auth->logged_in())
            return redirect(base_url('admin'));

        $token = $this->input->get('token');
        $valid = true;
        $data = ['token' => $token];

        if ($token == null)
            $valid = false;
        else {
            $str = $this->encryption->decrypt($token);

            if (!$str)
                $valid = false;
            else {
                $str = explode('|', $str);
                $data['email'] = $str[0];
                $data['id'] = $str[1];
                $data['time'] = $str[2];

                if (!is_numeric($data['time']))
                    $valid = false;
                else {
                    if ($data['time'] < time() - 6 * 3600)
                        $valid = false;
                }
            }
        }

        if (!$valid)
            return redirect(base_url('admin/auth/login'));

        return $this->blade->render($data);
    }

    public function POST_reset_pass()
    {
        $token = $this->input->get('token');
        $token = urlencode($token);

        $this->form_validation->set_rules('pass', null, 'required');
        $this->form_validation->set_rules('re_pass', null, 'required');
        $this->form_validation->set_rules('token', null, 'required');
        $this->form_validation->set_rules('email', null, 'required|valid_email');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error_msg', 'Vui lòng nhập đủ thông tin');
            return redirect(base_url('admin/auth/reset_pass?token=' . $token));
        }

        $pass = $this->input->post('pass');
        $email = strtolower(trim($this->input->post('email')));
        $re_pass = $this->input->post('re_pass');
        $post_token = $this->input->post('token');

        if ($token != urlencode($post_token)) {
            $this->session->set_flashdata('error_msg', 'Yêu cầu không hợp lệ');
            return redirect(base_url('admin/auth/reset_pass?token=' . $token));
        }

        if (!$this->ion_auth->identity_check($email)) {
            $this->session->set_flashdata('error_msg', 'Không tìm thấy Email của bạn');
            return redirect(base_url('admin/auth/reset_pass?token=' . $token));
        }

        $user = $user = $this->ion_auth->where('email', $email)->users()->row();
        if ($user == null) {
            $this->session->set_flashdata('error_msg', 'Tài khoản không tồn tại');
            return redirect(base_url('admin/auth/reset_pass?token=' . $token));
        }

        if ($pass != $re_pass) {
            $this->session->set_flashdata('error_msg', 'Xác nhận mật khẩu không khớp');
            return redirect(base_url('admin/auth/reset_pass?token=' . $token));
        }

        $str = $this->encryption->decrypt($post_token);
        $str = explode('|', $str);
        if ($email != $str[0]) {
            $this->session->set_flashdata('error_msg', 'Địa chỉ email không đúng với email yêu cầu khôi phục mật khẩu');
            return redirect(base_url('admin/auth/reset_pass?token=' . $token));
        }

        $this->ion_auth->update($user->id, ['password' => $pass]);
        $this->session->set_flashdata('success_msg', 'Thay đổi mật khẩu thành công');
        return redirect(base_url('admin/auth/login'));
    }
}