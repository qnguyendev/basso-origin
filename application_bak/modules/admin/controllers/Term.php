<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Term
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property Term_model $term_model
 * @property CI_Upload $upload
 * @property CI_Config $config
 * @property Minify $minify
 * @property Image_model $image_model
 * @property CI_Form_validation $form_validation
 */
class Term extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
            return redirect('admin/auth/login');

        $this->load->model('admin/term_model');
        $this->load->model('admin/image_model');
        $this->load->helper('term');
    }

    public function index()
    {
        $breadcrumbs = [['text' => 'Danh mục']];
        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        $type = $this->input->get('type');
        $key = $this->input->get('key');

        if ($page != null) {
            if (is_numeric($page)) {
                $page = $page < 1 ? 1 : $page;
                global $theme_config;

                $system_terms = array_filter($theme_config['term'], function ($item) {
                    return $item['active'];
                });
                $terms = array_filter($system_terms, function ($item) {
                    return $item['active'];
                });

                if ($type == null) {
                    $type = array_column($terms, 'id');
                }

                $result = $this->term_model->get($page, $type, $key);
                $pagination = Pagination::calc($this->term_model->count($type, $key), $page, PAGING_SIZE);

                for ($i = 0; $i < count($result); $i++) {
                    foreach ($system_terms as $term) {
                        if ($result[$i]->type == $term['id']) {
                            $result[$i]->type = $term['name'];
                            break;
                        }
                    }
                }

                echo json_encode(['data' => $result, 'pagination' => $pagination]);
            }
        }
        die;
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $term = $this->term_model->get_by_id($id);
        if ($term != null) {
            if ($term->image_id != null)
                $this->image_model->set_active($term->image_id, 0);
            $this->term_model->delete($id);

            json_success('Xóa danh mục thành công');
        }
        json_error('Không tìm thấy danh mục yêu cầu');
    }

    public function create()
    {
        $breadcrumbs = [['url' => '/admin/term', 'text' => 'Danh mục'], ['text' => 'Thêm danh mục']];

        $data['terms'] = $this->term_model->get();
        $this->blade->set('image_uploader', true);

        return $this->blade
            ->set('page_title', lang('terms_create_heading'))
            ->set('breadcrumbs', $breadcrumbs)
            ->render('editor', $data);
    }

    public function edit($id = null)
    {
        if ($id == null)
            return redirect('/admin/term');

        if (!is_numeric($id))
            return redirect('/admin/term');

        $breadcrumbs = [['url' => '/admin/term', 'text' => 'Danh mục'], ['text' => 'Cập nhật danh mục']];
        $data['terms'] = $this->term_model->get();

        $this->blade->set('image_uploader', true);
        return $this->blade
            ->set('page_title', 'Cập nhật danh mục')
            ->set('breadcrumbs', $breadcrumbs)
            ->set('id', $id)
            ->render('editor', $data);
    }

    public function GET_edit($id = null)
    {
        $term = $this->term_model->get_by_id($id);
        if ($term == null)
            json_error('Danh mục không tồn tại', ['url' => '/admin/term']);

        json_success(null, ['data' => $term]);
    }

    /**
     * Tạo mới/cập nhật danh mục
     */
    public function POST_save()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin yêu cầu');

        $id = $this->input->post('id');
        $parent_id = $this->input->post('parent_id');
        $name = $this->input->post('name');
        $slug = $this->input->post('slug');
        $type = $this->input->post('type');
        $external_url = $this->input->post('external_url');
        $des = $this->input->post('des');
        $content = $this->input->post('content', false);
        $order = $this->input->post('order');
        $new_tab = $this->input->post('new_tab');
        $active = $this->input->post('active');
        $seo_title = $this->input->post('seo_title');
        $seo_des = $this->input->post('seo_des');
        $seo_index = $this->input->post('seo_index');
        $image = $this->input->post('image');

        $slug = $slug == null
            ? url_title($name, '-', true)
            : url_title($slug, '-', true);
        $slug = $this->term_model->create_slug($id, $slug);

        if ($parent_id == 0)
            $parent_id = null;

        $data = ['parent_id' => $parent_id,
            'description' => $des,
            'content' => $content,
            'external_url' => $external_url,
            'active' => $active,
            'new_tab' => $new_tab,
            'order' => $order,
            'seo_title' => $seo_title,
            'seo_description' => $seo_des,
            'seo_index' => $seo_index,
            'slug' => $slug,
            'name' => $name,
            'image_path' => null,
            'image_id' => null];

        if ($id == 0)
            $data['type'] = $type;

        //  Set các ảnh cũ chưa active
        if ($id > 0) {
            $term = $this->term_model->get_by_id($id);
            if ($term->image_id != null)
                $this->image_model->set_active($term->image_id, 0);
        }

        #region Hình ảnh upload
        if ($image != null) {
            $this->image_model->set_active($image);     //  Đánh dấu ảnh được sử dụng
            $img = $this->image_model->get($image);     //  Lấy thông tin ảnh
            if ($img != null) {
                $data['image_path'] = $img->path;
                $data['image_id'] = $img->id;
            }
        }
        #endregion

        $msg = $id == 0 ? 'Thêm danh mục thành công' : 'Cập nhật danh mục thành công';
        $this->session->set_flashdata('success', $msg);

        $this->term_model->insert($id, $data);
        json_success(null, ['url' => '/admin/term']);
    }

    /**
     * Thay đổi trạng thái ẩn/hiện
     */
    public function POST_change_active()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('active', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $active = $this->input->post('active');

        $this->term_model->change_active($id, $active);
        json_success('Cập nhật thành công');
    }

    /**
     * Thay đổi thứ tự sắp xếp
     */
    public function POST_change_order()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('order', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $order = $this->input->post('order');

        $this->term_model->change_order($id, $order);
        json_success('Cập nhật thành công');
    }
}