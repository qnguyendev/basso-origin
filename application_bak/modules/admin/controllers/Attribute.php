<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Attribute
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Attribute_model $attribute_model
 * @property CI_Upload $upload
 * @property CI_Form_validation $form_validation
 */
class Attribute extends Cpanel_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
            return redirect('admin/auth/login');

        $this->load->model('attribute_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Đặc tính sản phẩm']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $id = $this->input->get('id');
        $page = $this->input->get('page');
        if ($id == 0) {
            $result['result'] = $this->attribute_model->get();
            $result['pagination'] = Pagination::calc($this->attribute_model->count(), $page,
                $this->config->item('result_per_page'));
            echo json_encode($result);
        } else {
            $result['result'] = $this->attribute_model->get_values($id);
            $result['pagination'] = Pagination::calc($this->attribute_model->count_values($id), $page,
                $this->config->item('result_per_page'));
            echo json_encode($result);
        }

        die;
    }

    public function POST_save()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('active', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('type', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin yêu cầu');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $type = $this->input->post('type');
        $active = $this->input->post('active');
        $des = $this->input->post('dec');

        $this->attribute_model->insert($id, $name, $des, $type, $active);
        json_success('Lưu thành công');
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if ($this->form_validation->run() == false)
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $this->attribute_model->delete($id);
        json_success('Xóa đặc tính thành công');
    }

    public function POST_save_value()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('attribute_id', null, 'required|numeric');
        $this->form_validation->set_rules('active', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('type', null, 'required');
        if ($this->form_validation->run() == false)
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $type = $this->input->post('type');
        $active = $this->input->post('active');
        $attribute_id = $this->input->post('attribute_id');
        $value = $this->input->post('value');

        if (!empty($_FILES) && $type == 'image') {
            create_folder();

            $config['file_name'] = uniqid();
            $config['upload_path'] = './uploads/' . date('Y/m/d');
            $config['allowed_types'] = 'jpg|jpeg';
            $this->load->library('upload', $config);

            $this->upload->do_upload('file');
            $image_data = $this->upload->data();
            $value = '/uploads/' . date('Y/m/d') . '/' . $image_data['file_name'];
        }

        $this->attribute_model->insert_value($attribute_id, $id, $name, $type, $value, $active);
        json_success('Thêm giá trị thành công');
    }

    public function POST_delete_value()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if ($this->form_validation->run() == false)
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $this->attribute_model->delete_value($id);
        json_success('Xóa giá trị thành công');
    }
}