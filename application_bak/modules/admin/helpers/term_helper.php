<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
function display_select_tree($terms)
{
    $parents = array_filter($terms, function ($item) {
        return $item->parent_id == null;
    });

    foreach ($parents as $item) {
        echo "<option value='{$item->id}'>{$item->name}</option>";
        get_child($terms, $item->id, 1);
    }
}

function get_child($terms, $parent_id, $deep)
{
    $temp = array_filter($terms, function ($item) use ($parent_id) {
        return $item->parent_id == $parent_id;
    });

    foreach ($temp as $item) {
        echo "<option value='{$item->id}'>";
        echo '|';
        for ($i = 0; $i < $deep; $i++)
            echo '- ';
        echo "{$item->name}</option>";

        $childs = array_filter($terms, function ($t) use ($item) {
            return $t->parent_id == $item->id;
        });

        if (count($childs) > 0) {
            $_depp = $deep + 1;
            get_child($terms, $item->id, $_depp);
        }
    }
}