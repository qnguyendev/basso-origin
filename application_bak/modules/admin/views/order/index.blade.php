<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <a class="float-right" data-toggle="modal" data-target="#create_order_modal" href="javascript:">
                    <i class="fa fa-plus"></i>
                    Tạo đơn hàng
                </a>
                Danh sách đơn hàng
            </div>
        </div>
        <form autocomplete="off" class="card-body">

            <?php if (count($labels) > 0) : ?>
            <div class="form-group">
                <label>Nhãn đơn hàng</label>
                @foreach($labels as $label)
                    <div class="checkbox c-checkbox mr-2 d-inline-block">
                        <label>
                            <input class="form-check-input" type="checkbox" value="{{$label->id}}"
                                   data-bind="checked: filter_labels">
                            <span class="fa fa-check mr-0"></span>
                            <span class="badge badge-info w-auto"
                                  style="background-color: {{$label->color}}; border-color: {{$label->color}}">
                                <i class="fa fa-tags"></i>
                                {{$label->name}}
                            </span>
                        </label>
                    </div>
                @endforeach
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="form-group col-12 col-sm-3">
                    <label>Thời gian</label>
                    <select class="form-control" data-bind="value: filter_time">
                        <option value="all">Tất cả</option>
                        <option value="today">Hôm nay</option>
                        <option value="yesterday">Hôm qua</option>
                        <option value="month">Trong tháng</option>
                        <option value="custom">Tùy chọn</option>
                    </select>
                </div>
                <div class="form-group col-12 col-sm-3">
                    <label>Tùy chọn ngày</label>
                    <input type="text" class="form-control"
                           data-bind="datePicker: filter_date, enable: filter_time() == 'custom'"/>
                </div>
                <div class="form-group col-12 col-sm-3">
                    <label>Trạng thái</label>
                    <select class="form-control" data-bind="value: filter_status">
                        <option value="all">Tất cả</option>
                        @foreach(OrderStatus::LIST as $key=>$value)
                            <option value="{{$key}}">{{$value['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-sm-3">
                    <label>Tìm kiếm</label>
                    <div class="input-group input-sm">
                        <input type="text" class="form-control" placeholder="Mã ĐH, tên, điện thoại"
                               data-bind="value: filter_key"/>
                        <div class="input-group-append">
                            <button class="btn btn-info btn-sm" data-bind="click: function(){$root.search(1)}">
                                <i class="fa fa-search"></i>
                                TÌM
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th width="80">Mã ĐH</th>
                    <th>Khách hàng</th>
                    <th>Tổng tiền</th>
                    <th width="140">Thời gian</th>
                    <th width="130">Trạng thái</th>
                    <th>Nhãn</th>
                    <th width="120"></th>
                </tr>
                </thead>
                <tbody data-bind="foreach: result" class="text-center">
                <tr>
                    <td class="font-weight-bold" data-label="Mã đơn hàng" data-bind="text: '#' + id()"></td>
                    <td class="text-left" data-label="Khách hàng">
                        <strong>N:</strong> <span data-bind="text: name"></span><br/>
                        <strong>T:</strong> <span data-label="Khách hàng" data-bind="text: phone"></span><br/>
                        <strong>E: </strong> <span data-bind="text: email"></span>
                    </td>
                    <td class="text-right" data-label="Tổng tiền">
                        <span data-bind="text: parseInt(sub_total()).toMoney(0)"></span><br/>
                        <i data-bind="text: payment"></i>
                        <!--ko if: payment_type() != 'cod'-->
                        <br/>
                        <!--ko if: payment_status() == 'waiting'-->
                        <b class="text-warning font-italic">Đợi thanh toán</b>
                        <!--/ko-->
                        <!--ko if: payment_status() == 'error'-->
                        <b class="text-danger font-italic">Lỗi thanh toán</b>
                        <!--/ko-->
                        <!--ko if: payment_status() == 'success'-->
                        <b class="text-success font-italic">Đã thanh toán</b>
                        <!--/ko-->
                        <!--/ko-->
                    </td>
                    <td data-label="Thời gian"
                        data-bind="text: moment.unix(created_time()).format('HH:mm DD/MM/YYYY')"></td>
                    <td data-label="Trạng thái">
                        @foreach(OrderStatus::LIST as $key=>$value)
                            <span data-bind="visible: shipping_status() == '{{$key}}'"
                                  class="text-{{$value['class']}}">{{$value['name']}}</span>
                        @endforeach
                    </td>
                    <td>
                        <!--ko foreach: labels()-->
                        <span class="badge badge-info"
                              data-bind="style: {'background-color': color, 'border-color': color}">
                            <i class="fa fa-tags"></i>
                            <span data-bind="text: name"></span>
                        </span>
                        <!--/ko-->
                        <!--/ko-->
                    </td>
                    <td>
                        <a class="text-primary font-weight-bold" href="javascript:"
                           data-bind="attr: {href: '/admin/order/detail/' + id()}">
                            <i class="icon-list"></i>
                            Chi tiết
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
            @include('pagination')
        </form>
    </div>
@endsection
@section('modal')
    @include('modal/create_order')
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection