<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="value_editor_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Giá trị của <span data-bind="text: values().attribute_name"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Tên giá trị
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: values().name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: values().name"/>
                </div>
                <div class="form-group">
                    <label>Kiểu giá trị</label>
                    <!--ko if: values().type() == 'text'-->
                    <input type="text" class="form-control" readonly value="Giá trị"/>
                    <!--/ko-->
                    <!--ko if: values().type() == 'color'-->
                    <input type="text" class="form-control" readonly value="Màu sắc"/>
                    <!--/ko-->
                    <!--ko if: values().type() == 'image'-->
                    <input type="text" class="form-control" readonly value="Có ảnh"/>
                    <!--/ko-->
                </div>
                <div class="form-group" data-bind="visible: values().type() == 'color'">
                    <label> Chọn màu </label>
                    <input type="text" data-bind="colorPicker: values().value, style: {background: values().value()}"
                           class="form-control"/>
                </div>
                <div class="form-group form-file-upload form-file-simple" id="preview_image"
                     data-bind="visible: values().type() == 'image'">
                    <label>Logo</label>
                    <input type="text" class="form-control inputFileVisible"
                           placeholder="Định dạng .jpg, .jpeg"/>
                    <input type="file" class="inputFileHidden"
                           data-bind="event: {change: $root.values().preview_image}">
                    <div class="card card-product" data-bind="visible: values().value() !== undefined">
                        <div class="card-img">
                            <button type="button" class="btn-close btn btn-danger"
                                    data-bind="click: $root.values().clear_image">
                                x
                            </button>
                            <img data-bind="attr: {src: values().value}">
                        </div>
                    </div>
                </div>
                <div class="form-gorup text-center">
                    <button class="btn btn-success" data-bind="click: $root.values().save">
                        <i class="now-ui-icons ui-1_check"></i>
                        Lưu lại
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
