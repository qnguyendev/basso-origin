<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $theme_config;
$system_terms = array_filter($theme_config['term'], function ($item) {
    return $item['active'];
});
?>
@layout('cpanel_layout')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <a href="{{base_url('admin/term/create')}}" class="float-right">
                            <i class="fa fa-plus"></i>
                            Thêm danh mục
                        </a>
                        Danh mục hệ thống
                    </div>
                </div>
                <form autocomplete="off" class="card-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Tìm tên danh mục"
                                   data-bind="value: filter_key"/>
                            <div class="input-group-append">
                                <select class="form-control border-left-0"
                                        tabindex="1" data-bind="value: filter_type">
                                    <option value="">Tất cả</option>
                                    @foreach($system_terms as $term)
                                        <?php if ($term['active']) : ?>
                                        <option value="{{$term['id']}}">{{$term['name']}}</option>
                                        <?php endif; ?>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-primary btn-sm"
                                        data-bind="click: function(){$root.search(1)}">
                                    <i class="now-ui-icons ui-1_zoom-bold"></i>
                                    TÌM
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 70px">Sắp xếp</th>
                                <th width="80">Ảnh</th>
                                <th>Tên danh mục</th>
                                <th>Phân loại</th>
                                <th>Ẩn/hiện</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: result">
                            <tr>
                                <td class="text-center">
                                    <input type="number" class="form-control text-center input-sm"
                                           data-bind="value: order, event: {change: $root.change_order}"/>
                                </td>
                                <td style="padding: 3px">
                                    <!--ko if: image_path() != null -->
                                    <img data-bind="attr: {src: '/timthumb.php?src=' + image_path() + '&w=50&h=50'}, visible: image_path() != null"/>
                                    <!--/ko-->
                                    <!--ko if: image_path() == null-->
                                    <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                                    <!--/ko-->
                                </td>
                                <td class="text-left">
                                    <a data-bind="attr: {href: '/admin/term/edit/' + id()}, text: name"
                                       class="font-weight-bold"></a>
                                </td>
                                <td data-bind="text: type"></td>
                                <td class="text-center">
                                    <!--ko if: active() == 1-->
                                    <a class="btn btn-xs btn-success" href="javascript:" data-bind="click: $root.change_active">
                                        <i class="fa fa-eye"></i>
                                        Hiện
                                    </a>
                                    <!--/ko-->
                                    <!--ko if: active() == 0-->
                                    <a class="btn btn-xs btn-danger" href="javascript:" data-bind="click: $root.change_active">
                                        <i class="fa fa-eye-slash"></i>
                                        Ẩn
                                    </a>
                                    <!--/ko-->
                                </td>
                                <td class="text-center">
                                    <a href="javascript:" class="text-danger text-bold" data-bind="click: $root.delete">
                                        <i class="icon-trash"></i>
                                        Xóa
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    @include('pagination')
                </form>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection