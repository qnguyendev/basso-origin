<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $current_user;
?>
<div class="modal fade" id="editor_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    data-bind="text: id() == 0 ? 'Thêm tài khoản' : 'Cập nhật tài khoản'"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="row">
                    <div class="form-group col-12 col-md-6">
                        <label>
                            Tên thành viên
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: name"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: name"/>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>
                            Email đăng nhập
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: email"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: email, enable: id() == 0"/>
                    </div>

                    <div class="form-group col-12 col-md-6">
                        <label>
                            Mật khẩu đăng nhập
                            <span class="text-danger" data-bind="visible: id() == 0">*</span>
                            <span class="validationMessage" data-bind="validationMessage: pass"></span>
                        </label>
                        <input type="password" class="form-control" data-bind="value: pass"/>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>Trạng thái</label>
                        <select class="form-control" data-bind="value: active">
                            <option value="1">Kích hoạt</option>
                            <option value="0">Không kích hoạt</option>
                        </select>
                    </div>

                    <div class="form-group col-12 col-md-6">
                        <label>Điện thoại</label>
                        <input type="text" class="form-control" data-bind="value: phone"/>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>
                            Phân quyền
                        </label>
                        <select class="form-control select2" multiple
                                data-bind="selectedOptions: roles, disable: id() == {{$current_user->id}}">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->description}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
            <div class="modal-footer text-right">
                <span class="float-right">
                    <span class="text-danger">*</span>
                    <i>thông tin bắt buộc</i>
                </span>
                <button class="btn btn-success" data-bind="click: $root.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>