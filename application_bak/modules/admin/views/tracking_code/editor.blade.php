<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <form autocomplete="off" class="row">
        <div class="col-md-12 col-lg-7 col-xl-6 offset-xl-1">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a class="float-right" href="#" data-bind="click: $root.save">
                            <i class="fa fa-check"></i>
                            Lưu lại
                        </a>
                        <span data-bind="text: id() == 0 ? 'Thêm mã tracking' : 'Cập nhật mã tracking'"></span>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>
                            Tên mã
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: name"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: name"/>
                    </div>
                    <div class="form-group">
                        <label>
                            Mã javascript
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: code"></span>
                        </label>
                        <textarea class="form-control" rows="15" cols="20" data-bind="value: code"
                                  style="height: 200px !important;padding: 10px; line-height: 1.2; font-family: monospace;"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-lg-5 col-xl-4">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Tùy chọn</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Kích hoạt</label>
                        <select class="form-control" data-bind="value: active">
                            <option value="1">Có</option>
                            <option value="0">Không</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Trang hiển thị</label>
                        <select class="form-control select2" multiple data-bind="selectedOptions: page">
                            <option value="-1">Trang chủ</option>
                            @foreach($terms as $term)
                                <option value="{{$term->id}}">{{$term->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Vị trí chèn</label>
                        <select class="form-control" data-bind="value: position">
                            <option value="head">Trong thẻ HEAD</option>
                            <option value="body">Trong thẻ BODY</option>
                            <option value="footer">Cuối trang web</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text text-muted">
                        Bỏ trống <b>Trang hiển thị</b> để hiển thị mã toàn bộ website
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('script')
    <script src="{{load_js('edit')}}"></script>
@endsection