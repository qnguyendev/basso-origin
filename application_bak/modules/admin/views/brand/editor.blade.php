<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-7 col-xl-6 offset-xl-1">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Thông tin thương hiệu</div>
                </div>
                <form autocomplete="off" class="card-body">
                    <div class="form-group">
                        <label>
                            Tên
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: name"></span>
                        </label>
                        <input type="text" class="form-control"
                               data-bind="value: name, event: {change: $root.preview_seo}"/>
                    </div>
                    <div class="form-group">
                        <label>Đường dẫn tĩnh</label>
                        <input type="text" class="form-control"
                               data-bind="value: slug, event: {change: $root.create_slug}"/>
                    </div>
                    <div class="form-group">
                        <label>Website</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    http://
                                </div>
                            </div>
                            <input type="text" class="form-control" data-bind="value: website"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Mô tả/giới thiêu</label>
                        <textarea data-bind="ckeditor: content" class="form-control" rows="5" cols="10"></textarea>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-12 col-lg-5 col-xl-4">
            <div class="card card-default">
                <div class="card-body text-center">
                    <a href="/admin/brand" class="btn btn-warning btn-sm">
                        <i class="fa fa-arrow-left"></i>
                        Hủy
                    </a>
                    <button class="btn btn-success btn-sm" data-bind="click: $root.save">
                        <i class="fa fa-save"></i>
                        Lưu lại
                    </button>
                </div>
            </div>
            @include('partial/single_image_upload')
            @include('partial/seo_editor')
        </div>
    </div>
@endsection
@section ('script')
    {{ckeditor()}}
    <script src="{{load_js('edit')}}"></script>
@endsection