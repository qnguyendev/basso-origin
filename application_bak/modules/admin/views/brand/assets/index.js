function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();

    self.search = function (page) {
        let data = {page: page};
        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.change_active = function (item) {
        let active = item.active() == 1 ? 0 : 1;
        AJAX.post('/admin/brand/change_active', {id: item.id(), active: active}, false, () => {
            item.active(active);
            NOTI.success('Cập nhật trạng thái thành công');
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa nhãn hiệu', null, function () {
            AJAX.post('/admin/brand/delete', {id: item.id()}, true, () => {
                NOTI.success('Xóa thành công nhãn hiệu');
                self.search(self.pagination().current_page);
            });
        });
    }
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'))