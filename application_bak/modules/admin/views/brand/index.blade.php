<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-10 offset-lg-1 col-xl-10 offset-xl-1">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a href="{{base_url('admin/brand/create')}}" class="float-right">
                            <i class="fa fa-plus"></i>
                            Thêm thương hiệu
                        </a>
                        Danh sách thương hiệu
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Logo</th>
                            <th>Tên</th>
                            <th>Website</th>
                            <th>SP</th>
                            <th>Ẩn/hiện</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <td style="padding: 3px">
                                <!--ko if: image_path() != null -->
                                <img data-bind="attr: {src: '/timthumb.php?src=' + image_path() + '&w=50&h=50'}"/>
                                <!--/ko-->
                                <!--ko if: image_path() == null-->
                                <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                                <!--/ko-->
                            </td>
                            <td class="text-left">
                                <a data-bind="attr: {href: '/admin/brand/edit/' + id()}, text: name"
                                   class="font-weight-bold"></a>
                            </td>
                            <td data-bind="text: website"></td>
                            <td data-bind="text: product_count"></td>
                            <td>
                                <!--ko if: active() == 1-->
                                <a class="btn btn-xs btn-success" href="javascript:"
                                   data-bind="click: $root.change_active">
                                    <i class="fa fa-eye"></i>
                                    Hiện
                                </a>
                                <!--/ko-->
                                <!--ko if: active() == 0-->
                                <a class="btn btn-xs btn-danger" href="javascript:"
                                   data-bind="click: $root.change_active">
                                    <i class="fa fa-eye-slash"></i>
                                    Ẩn
                                </a>
                                <!--/ko-->
                            </td>
                            <td>
                                <a data-bind="attr: {href: '/admin/brand/edit/' + id()}" class="btn btn-xs btn-info">
                                    <i class="fa fa-edit"></i>
                                    Sửa
                                </a>
                                <a href="javascript:" class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    @include('pagination')
                </div>
            </div>
        </div>
    </div>
@endsection
@section ('script')
    <script src="{{load_js('index')}}"></script>
@endsection