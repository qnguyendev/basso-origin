<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Kiểu hiển thị</label>
            <select class="form-control" data-bind="value: member().type">
                <option value="all">Tất cả thành viên</option>
                <option value="list">Theo ID lựa chọn</option>
            </select>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group" data-bind="visible: member().type() == 'all'">
            <label>Giới hạn số lượng</label>
            <input type="number" class="form-control"
                   data-bind="value: member().limit, enable: member().type() == 'all'"/>
        </div>
        <div class="form-group" data-bind="visible: member().type() == 'list'">
            <label>Danh sách ID</label>
            <input type="text" class="form-control"
                   data-bind="value: member().list, enable: member().type() == 'list'"/>
        </div>
    </div>
</div>
