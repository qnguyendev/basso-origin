<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="form-group">
    <button class="btn btn-primary btn-block" data-bind="click: links().add">
        <i class="fa fa-plus"></i>
        Thêm liên kết
    </button>
</div>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th width="40">STT</th>
        <th>Nội dung</th>
        <th>Liên kết</th>
        <th width="120">Tab mới</th>
        <th width="100">Follow</th>
        <th width="40"></th>
    </tr>
    </thead>
    <tbody data-bind="foreach: links().list" class="text-center">
    <tr>
        <th data-bind="text: $index() + 1"></th>
        <td>
            <input type="text" class="form-control input-sm" data-bind="value: text"/>
        </td>
        <td>
            <input type="text" class="form-control input-sm" data-bind="value: link"/>
        </td>
        <td>
            <select class="form-control input-sm" style="padding: 2px; height: 30px" data-bind="value: new_tab">
                <option value="0">Không</option>
                <option value="1">Có</option>
            </select>
        </td>

        <td>
            <select class="form-control input-sm" style="padding: 2px; height: 30px" data-bind="value: follow">
                <option value="dofollow">dofollow</option>
                <option value="nofollow">nofollow</option>
            </select>
        </td>
        <td>
            <button class="btn btn-xs btn-danger" data-bind="click: $root.links().delete">
                <i class="fa fa-trash"></i>
            </button>
        </td>
    </tr>
    </tbody>
</table>
