function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();

    self.filter = {
        position: ko.observable('all'),
        type: ko.observable('all'),
        active: ko.observable(-1),
        key: ko.observable()
    };

    self.search = function (page) {
        let data = {
            page: page,
            position: self.filter.position(),
            type: self.filter.type(),
            active: self.filter.active(),
            key: self.filter.key()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa widget', item.title(), function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    }
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));