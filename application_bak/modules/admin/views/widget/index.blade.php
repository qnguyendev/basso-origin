<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

?>
@layout('cpanel_layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <a class="float-right" href="{{base_url('admin/widget/create')}}">
                    <i class="fa fa-plus"></i>
                    Thêm widget
                </a>
                Danh sách widgets
            </div>
        </div>
        <div class="card-body">

            <form autocomplete="off" class="row">
                <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                    <div class="form-group">
                        <label>Vị trí</label>
                        <select class="form-control" data-bind="value: filter.position">
                            <option value="all">Tất cả</option>
                            @foreach($positions as $p)
                                <option value="{{$p['id']}}">{{$p['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                    <div class="form-group">
                        <label>Loại widget</label>
                        <select class="form-control" data-bind="value: filter.type">
                            <option value="all">Tất cả</option>
                            @foreach($types as $p)
                                @if($p['active'])
                                    <option value="{{$p['id']}}">{{$p['name']}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                    <div class="form-group">
                        <label>Trạng thái</label>
                        <select class="form-control" data-bind="value: filter.active">
                            <option value="-1">Tất cả</option>
                            <option value="1">Kích hoạt</option>
                            <option value="0">Không kích </option>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                    <div class="form-group">
                        <label>Tìm theo tên</label>
                        <input class="form-control" data-bind="value: filter.key"/>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3 col-xl-2">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button class="btn btn-primary btn-block" data-bind="click: function(){ $root.search(1); }">
                            TÌM
                        </button>
                    </div>
                </div>
            </form>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th width="80">STT</th>
                    <th class="text-left">Tên widget</th>
                    <th width="140">Hiện tiêu đề</th>
                    <th width="120">Trạng thái</th>
                    <th>Loại widget</th>
                    <th>Vị trí</th>
                    <th width="130"></th>
                </tr>
                </thead>
                <tbody data-bind="foreach: result" class="text-center">
                <tr>
                    <td class="font-weight-bold" data-bind="text: order"></td>
                    <td class="text-left">
                        <a class="font-weight-bold"
                           data-bind="text: title, attr: {href: '/admin/widget/edit/' + id()}"></a>
                    </td>
                    <td data-bind="text: show_title() == 1 ? 'Có' : 'Không'"></td>
                    <td data-bind="text: active() == 1 ? 'Kích hoạt' : 'Ẩn'"></td>
                    <td data-bind="text: type"></td>
                    <td data-bind="text: area"></td>
                    <td>
                        <a data-bind="attr: {href: '/admin/widget/edit/' + id()}" class="text-info text-bold">
                            <i class="fa fa-edit"></i>
                            Sửa
                        </a>
                        <a href="javascript:" class="text-danger text-bold ml-2" data-bind="click: $root.delete">
                            <i class="fa fa-trash"></i>
                            Xóa
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>

            @include('pagination')
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection
