<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a href="#" class="float-right" data-bind="click: add_voucher">
                            <i class="fa fa-plus"></i>
                            Thêm mã
                        </a>
                        Danh sách mã giảm giá
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Mã giảm giá</th>
                            <th>Từ ngày</th>
                            <th>Đến ngày</th>
                            <th>Kiểu giảm giá</th>
                            <th>Giá trị</th>
                            <th>Trạng thái</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: code"></th>
                            <td data-bind="text: moment.unix(valid_from()).format('DD/MM/YYYY')"></td>
                            <td data-bind="text: moment.unix(valid_to()).format('DD/MM/YYYY')"></td>
                            <td data-bind="text: discount_type() == 'percent' ? 'Phần trăm' : 'Giá trị'"></td>
                            <td>
                                <!--ko if: discount_type() == 'percent'-->
                                <span data-bind="text: discount_value()"></span>%
                                <!--/ko-->
                                <!--ko if: discount_type() == 'amount'-->
                                <span data-bind="text: parseInt(discount_value()).toMoney(0)"></span>
                                <!--/ko-->
                            </td>
                            <td>
                                <!--ko if: active() == 1-->
                                <button class="btn btn-xs btn-success" data-bind="click: $root.change_active">
                                    Kích hoạt
                                </button>
                                <!--/ko-->
                                <!--ko if: active() == 0-->
                                <button class="btn btn-xs btn-warning" data-bind="click: $root.change_active">
                                    Tạm dừng
                                </button>
                                <!--/ko-->
                            </td>
                            <td>
                                <button class="btn btn-xs btn-info" data-bind="click: $root.edit">
                                    <i class="fa fa-edit"></i>
                                    Sửa
                                </button>
                                <button class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('editor_modal')
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection
