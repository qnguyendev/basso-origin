<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <form autocomplete="off" class="row">
        <div class="col-md-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a class="float-right" data-bind="click: $root.create" href="#">
                            <i class="fa fa-plus"></i>
                            Thêm nhãn
                        </a>
                        Quản lý nhãn đơn hàng
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="60">#</th>
                            <th>Tên nhãn</th>
                            <th>Mô tả</th>
                            <th>Màu sắc</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: description"></td>
                            <td>
                                <div data-bind="style: {background: color}, visible: color() != null"
                                     class="attribute-value-color"></div>
                            </td>
                            <td>
                                <button class="btn btn-xs btn-info" data-bind="click: $root.edit">
                                    <i class="fa fa-edit"></i>
                                    Cập nhật
                                </button>
                                <button class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('modal')
    @include('partial/label_editor_modal')
@endsection
@section('script')
    <script src="/assets/vendor/colorpicker/js/colorpicker.js"></script>
    <script src="{{load_js('label')}}"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="/assets/vendor/colorpicker/css/colorpicker.css"/>
@endsection