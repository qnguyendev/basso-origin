<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-10 offset-lg-1">
            <div class="card card-default">
                <div class="card-header no-border">
                    <div class="card-title">
                        <a href="#" class="float-right" data-bind="click: $root.create">
                            <i class="fa fa-plus"></i>
                            Thêm Slide
                        </a>
                        Quản lý slide
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="80">STT</th>
                            <th width="300">Ảnh/Video</th>
                            <th>Tên ghi nhớ</th>
                            <th>Trạng thái</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <td class="text-center">
                                <input type="number" class="form-control text-center input-sm"
                                       data-bind="value: order, event: {change: $root.change_order}"/>
                            </td>
                            <td>
                                <!--ko if: image_path() !== null-->
                                <img data-bind="attr: {src: image_path}" class="w-100"/>
                                <!--/ko-->
                                <!--ko if: youtube_id() !== null-->
                                <img class="w-100"
                                     data-bind="attr: {src: 'https://img.youtube.com/vi/' + youtube_id() + '/default.jpg'}"/>
                                <!--/ko-->
                            </td>
                            <td data-bind="text: name"></td>
                            <td><!--ko if: active() == 1-->
                                <a class="btn btn-xs btn-success" href="javascript:"
                                   data-bind="click: $root.change_active">
                                    <i class="fa fa-eye"></i>
                                    Hiện
                                </a>
                                <!--/ko-->
                                <!--ko if: active() == 0-->
                                <a class="btn btn-xs btn-danger" href="javascript:"
                                   data-bind="click: $root.change_active">
                                    <i class="fa fa-eye-slash"></i>
                                    Ẩn
                                </a>
                                <!--/ko-->
                            </td>
                            <td class="text-center">
                                <a data-bind="click: $root.edit" class="btn btn-link" href="#">
                                    <i class="fa fa-edit"></i>
                                    Sửa
                                </a>
                                <a href="javascript:" class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('editor_modal')
@endsection

@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection