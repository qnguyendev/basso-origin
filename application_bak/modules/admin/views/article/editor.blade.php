<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <form autocomplete="off" class="row">
        <div class="col-lg-8 col-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Thông tin bài viết</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>
                            Tên
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: name"></span>
                        </label>
                        <input type="text" class="form-control"
                               data-bind="value: name, event: {change: seo_preview.change}"/>
                    </div>
                    <div class="form-group">
                        <label> Đường dẫn tĩnh </label>
                        <input type="text" class="form-control"
                               data-bind="value: slug, event: {change: $root.create_slug}"/>
                    </div>

                    <div class="form-group">
                        <label>Chọn danh mục</label>
                        <div class="row">
                            @foreach($terms as $term)
                                <div class="col-md-6 col-12 col-lg-4">
                                    <div class="checkbox c-checkbox w-100 mt-2">
                                        <label>
                                            <input class="form-check-input" type="checkbox" value="{{$term->id}}"
                                                   data-bind="checked: terms"/>
                                            <span class="fa fa-check"></span>
                                            {{$term->name}}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group">
                        <label>
                            Mô tả ngắn
                        </label>
                        <textarea class="form-control no-resize" rows="7"
                                  data-bind="value: des, event: {change: $root.preview_seo}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea data-bind="ckeditor: content"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-12">
            <div class="card card-default">
                <div class="card-body">
                    <div class="form-group">
                        <label>Thời gian đăng</label>
                        <input type="text" data-bind="dateTimePicker: created_time" class="form-control"/>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="c-checkbox checkbox w-100">
                                <label>
                                    <input class="form-check-input" type="checkbox" data-bind="checked: active"/>
                                    <span class="fa fa-check"></span>
                                    Hiển thị bài
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="c-checkbox checkbox w-100">
                                <label>
                                    <input class="form-check-input" type="checkbox" data-bind="checked: featured"/>
                                    <span class="fa fa-check"></span>
                                    Tin nổi bật
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <a class="btn btn-secondary" href="{{base_url('admin/article')}}">
                        <i class="fa fa-arrow-left"></i>
                        Hủy
                    </a>
                    <button class="btn btn-success" data-bind="click: save">
                        <i class="fa fa-check"></i>
                        Lưu lại
                    </button>
                </div>
            </div>

            @include('partial/single_image_upload')
            @include('partial/seo_editor')
        </div>
    </form>
@endsection
@section('script')
    {{ckeditor()}}
    <script src="{{load_js('edit')}}"></script>
@endsection