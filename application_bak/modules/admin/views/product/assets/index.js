function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.filter = ko.observable();

    self.search = function (page) {
        let data = {
            page: page,
            filter: self.filter()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa sản phẩm', null, () => {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    }
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));