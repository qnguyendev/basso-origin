<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('top')

@endsection
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                <a class="float-right" href="{{base_url('admin/product/create')}}">
                    <i class="fa fa-plus"></i>
                    Thêm sản phẩm
                </a>
                Danh sách sản phẩm
            </div>
        </div>

        <form autocomplete="off" class="card-body">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-sm btn-primary" style="margin: 0;">
                            <i class="now-ui-icons ui-1_zoom-bold"></i>
                            Tìm kiếm
                        </button>
                    </div>
                    <input type="text" class="form-control text-indent-20"
                           data-bind="value: filter, event: {change: function(){$root.search(0)}}"
                           placeholder="Theo tên, mã sản phẩm"/>
                </div>
            </div>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <!--th style="width: 40px"></th-->
                    <th style="width: 80px">Ảnh</th>
                    <th width="100">Mã</th>
                    <th>Sản phẩm</th>
                    <th width="130">Giá</th>
                    <th width="150">Giá sale</th>
                    <th>Tags</th>
                    <th></th>
                </tr>
                </thead>
                <tbody data-bind="foreach: result" class="text-center">
                <tr>
                    <!--td class="text-left">
                        <div class="form-check" style="padding-left: 0;">
                            <label class="form-check-label font-weight-normal">
                                <input class="form-check-input" type="checkbox" d/>
                                <span class="form-check-sign"></span>
                            </label>
                        </div>
                    </td-->
                    <td style="padding: 3px">
                        <!--ko if: image() == null-->
                        <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                        <!--/ko-->
                        <!--ko if: image() != null-->
                        <img data-bind="attr: {src: '/timthumb.php?src=' + image() + '&w=50&h=50'}"/>
                        <!--/ko-->
                    </td>
                    <td data-bind="text: sku"></td>
                    <td class="text-left">
                        <a data-bind="text: name, attr: {href: '/admin/product/edit/' + id()}"
                           class="font-weight-bold"> </a>
                    </td>
                    <td>
                        <!--ko if: price() != null-->
                        <span data-bind="text: parseInt(price()).toMoney(0)"></span>
                        <!--/ko-->
                    </td>
                    <td>
                        <!--ko if: sale_price() != null-->
                        <span data-bind="text: parseInt(sale_price()).toMoney(0)"></span>
                        <!--ko if: sale_from() != null || sale_to() != null-->
                        <br/>
                        <i class="text-info">(
                            <span data-bind="text: sale_from() != null ? moment.unix(sale_from()).format('DD/MM/YY') : '-'"></span>
                            <span data-bind="text: sale_to() != null ? moment.unix(sale_to()).format('DD/MM/YY') : '-'"></span>
                            )
                        </i>
                        <!--/ko-->
                        <!--/ko-->
                    </td>
                    <td>
                        <!--ko if: tags() != null-->
                        <!--ko foreach: tags().split(',')-->
                        <span href="javascript:" class="badge badge-secondary"
                           data-bind="text: $data"></span>
                        <!--/ko-->
                        <!--/ko-->
                    </td>
                    <td>
                        <a href="javascript:" class="text-danger text-bold" data-bind="click: $root.delete">
                            <i class="icon-trash"></i>
                            Xóa
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>

            @include('pagination')
        </form>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection