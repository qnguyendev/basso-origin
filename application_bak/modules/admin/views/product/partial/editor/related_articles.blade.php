<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card">
    <div class="card-header">
        <h6 class="card-title">
            Bài viết liên quan
            <a href="#" class="float-right text-primary" data-bind="click: related_articles.show">
                <i class="icon-plus"></i>
                Chọn
            </a>
        </h6>
    </div>
    <div class="card-body">
        <table class="table table-stripe table-bordered" data-bind="visible: related_articles.list().length > 0">
            <thead>
            <tr>
                <th></th>
                <th>Tiêu đề</th>
                <th>Ngày đăng</th>
                <th></th>
            </tr>
            </thead>
            <tbody data-bind="foreach: related_articles.list" class="text-center">
            <tr>
                <td>
                    <!--ko if: image_path() == null-->
                    <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                    <!--/ko-->
                    <!--ko if: image_path() != null-->
                    <img data-bind="attr: {src: '/timthumb.php?src=' + image_path() + '&w=50&h=50'}"/>
                    <!--/ko-->
                </td>
                <td data-bind="text: name"></td>
                <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                <td>
                    <a href="#" data-bind="click: $root.related_articles.remove" class="text-danger text-bold">
                        <i class="icon-trash"></i>
                        Xóa
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
