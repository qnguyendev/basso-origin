<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$success_msg = $this->session->flashdata('success_msg');
?><!DOCTYPE html>
<html lang="en">
@include('backend/head')
<body id="login-form">
<div class="wrapper">
    <div class="block-center mt-4 wd-xl">
        <!-- START card-->
        <div class="card card-flat">
            <div class="card-header text-center bg-dark">
                <a href="#">
                    <img class="block-center rounded" src="/assets/img/logo.png" alt="Image">
                </a>
            </div>
            <div class="card-body">
                @if(isset($success_msg))
                    <div class="alert alert-info">{{$success_msg}}</div>
                @endif
                <form class="mb-3">
                    <div class="form-group">
                        <div class="input-group with-focus">
                            <input type="email" class="form-control"
                                   placeholder="Email của bạn" data-bind="value: email"/>
                            <div class="input-group-append">
                                <span class="input-group-text text-muted bg-transparent border-left-0">
                                    <em class="fa fa-envelope"></em>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group with-focus">
                            <input type="password" placeholder="Mật khẩu của bạn"
                                   class="form-control" data-bind="value: pass"/>
                            <div class="input-group-append">
                                <span class="input-group-text text-muted bg-transparent border-left-0">
                                    <em class="fa fa-lock"></em>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="checkbox c-checkbox float-left mt-0">
                            <label>
                                <input type="checkbox" value="" name="remember">
                                <span class="fa fa-check"></span> Ghi nhớ</label>
                        </div>
                        <div class="float-right">
                            <a class="text-muted" href="{{base_url('admin/auth/reset')}}">Quên mật khẩu?</a>
                        </div>
                    </div>
                    <button data-bind="click: login" class="btn btn-block btn-primary mt-3">Đăng nhập</button>
                </form>
            </div>
        </div>
    </div>
</div>
@include('backend/loader')
</body>
</html>
<script src="/assets/vendor/modernizr/modernizr.custom.js"></script>
<script src="/assets/vendor/jquery/dist/jquery.js"></script>
<script src="/assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/assets/vendor/moment/min/moment-with-locales.js"></script>
<script src='/assets/js/core/knockout-3.4.2.js'></script>
<script src='/assets/js/core//knockout.validation.min.js'></script>
<script src='/assets/js/resources/string.js'></script>
<script src="/assets/vendor/sweetalert/dist/sweetalert.min.js"></script>
<script src='/assets/js/resources/functions.js'></script>
<script src='/assets/js/core/ko.extends.js'></script>
<script src="/assets/js/login.js"></script>