<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/fav.png">
    <title>Trung tâm cấp cứu 115 Hà Nội</title>
    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/assets/css/helper.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body style="background: url('/assets/images/3px-tile.png')">
<!-- Main wrapper  -->
<div class="error-page" id="wrapper">
    <div class="error-box">
        <div class="error-body text-center" style="padding-top: 50px;">
            <h1>404</h1>
            <h3 class="text-uppercase">Không tìm thấy trang yêu cấu</h3>
            <p class="text-muted m-t-30 m-b-30">Vui lòng thử lại</p>
            <a class="btn btn-success btn-rounded waves-effect waves-light m-b-40" href="/auth/login">ĐĂNG NHẬP</a>
        </div>
        <footer class="footer text-center">
            © 2018 by <a href="http://asiajsc.vn" target="_blank" class="text-info">Asia Global Technology JSC</a>.
            Rendered in {elapsed_time} second(s)
        </footer>
    </div>
</div>
</body>
</html>