function viewModel() {
    let self = this;
    self.id = ko.observable(0);
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.position = ko.observable().extend({
        required: {message: LABEL.required}
    });

    self.description = ko.observable();
    self.content = ko.observable();
    self.email = ko.observable();
    self.phone = ko.observable();
    self.facebook = ko.observable();
    self.twitter = ko.observable();
    self.google_plus = ko.observable();

    self.image_id = ko.observable();
    self.image_path = ko.observable();

    self.init = function () {
        if (window.location.toString().indexOf('/edit/') > 0) {
            self.id(data.id);
            self.name(data.name);
            self.position(data.position);
            self.image_path(data.image_path);
            self.image_id(data.image_id);
            self.email(data.email);
            self.phone(data.phone);
            self.facebook(data.facebook);
            self.twitter(data.twitter);
            self.google_plus(data.google_plus);
            self.description(data.description);
            self.content(data.content);
        }
    };

    //#region Xem trước ảnh upload
    self.images_preview = function (data, event) {
        if (event.target.files.length === 0)
            return;

        let file = event.target.files[0];
        if (file.size < 2048 * 1024) {
            if (file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/png") {
                let formData = new FormData();
                formData.append('files[]', file);

                AJAX.upload(URL.upload_image, formData, true, (res) => {
                    if (res.data.length == 1) {
                        self.image_id(res.data[0].id);
                        self.image_path(res.data[0].path);
                    }
                });
            } else {
                ALERT.error('Định dạng ảnh không hợp lệ.');
            }
        } else {
            ALERT.error('Ảnh phải nhỏ hơn 2MB');
        }
    };

    self.delete_image = function () {
        self.image_id(undefined);
        self.image_path(undefined);
    };
    //#endregion

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                name: self.name(),
                des: self.description(),
                content: self.content(),
                image_id: self.image_id(),
                position: self.position(),
                email: self.email(),
                phone: self.phone(),
                facebook: self.facebook(),
                twitter: self.twitter(),
                google_plus: self.google_plus()
            };

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    window.location = '/admin/member';
                }
            });
        }
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));