<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class User_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class User_model extends CI_Model
{
    public function get()
    {
        global $theme_config;
        $exclude_group = [];
        if (isset($theme_config['exclude_user_role'])) {
            if (is_array($theme_config['exclude_user_role']))
                $exclude_group = $theme_config['exclude_user_role'];
        }

        $this->db->select('t.id, t.first_name as name, t.email, t.active, d.id as role_id,  GROUP_CONCAT(d.description) as roles');
        $this->db->from('users t');
        $this->db->join('users_roles o', 't.id = o.user_id', 'left');
        $this->db->join('roles d', 'd.id = o.group_id', 'left');
        if (count($exclude_group) > 0)
            $this->db->where_not_in('o.group_id', $exclude_group);
        $this->db->group_by('t.id');

        return $this->db->get()->result();
    }
}