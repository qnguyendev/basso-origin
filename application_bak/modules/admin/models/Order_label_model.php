<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_Label_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Order_label_model extends CI_Model
{
    public function get()
    {
        return $this->db->get('order_labels')->result();
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('order_labels')->row();
    }

    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('order_labels')->row();
    }

    public function insert($id, $name, $slug, $des, $color)
    {
        $data = array(
            'name' => $name,
            'slug' => $slug,
            'description' => $des,
            'color' => $color
        );

        if ($id == 0) {
            $this->db->insert('order_labels', $data);
        } else {
            $this->db->where('id', $id);
            $this->db->update('order_labels', $data);
        }
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('order_labels');
    }
}