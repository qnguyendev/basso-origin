<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
//  Danh mục phiếu thu/chi tự động
define('AUTO_BILLING_TERM', 1);

//  Loại phiếu thu/chi
abstract class BillingType
{
    //  Phiếu thu
    const IN = 'in';

    //  Phiếu chi
    const OUT = 'out';
}