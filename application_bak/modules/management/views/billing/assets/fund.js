function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.total_in = ko.observable();
    self.total_out = ko.observable();

    self.filter = {
        time: ko.observable('all'),
        start: ko.observable(moment(new Date()).format('YYYY-MM-DD')),
        end: ko.observable(moment(new Date()).format('YYYY-MM-DD')),
        group: ko.observable('all')
    };

    self.search = function (page) {
        let data = {
            page: page,
            group: self.filter.group(),
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end()
        };

        AJAX.get(window.location, data, true, (res) => {
            if (!res.error) {
                ko.mapping.fromJS(res.result, self.result);
                self.pagination(res.pagination);
                self.total_in(res.total_in);
                self.total_out(res.total_out);
            } else {
                NOTI.danger(res.message);
            }
        });
    };

    self.export = function () {
        let data = {
            group: self.filter.group(),
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end()
        };

        window.location = '/management/billing/fund_export?' + $.param(data);
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));