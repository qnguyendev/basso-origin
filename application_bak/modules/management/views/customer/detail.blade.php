<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$valid_orders = array_filter($orders, function ($item) {
    return $item->status != CustomerOrderStatus::CANCELLED;
});

$total = array_sum(array_column($valid_orders, 'sub_total'));
$processing_orders = array_filter($valid_orders, function ($item) {
    return $item->status != CustomerOrderStatus::COMPLETED;
});
$processing_total = array_sum(array_column($processing_orders, 'sub_total'));
$processing_paid = array_sum(array_column($processing_orders, 'total_paid'));

?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                Khách hàng | {{$customer->name}}
                @if(count($orders) > 0)
                    <a class="btn btn-success btn-xs float-right mr-2" href="#" data-bind="click: $root.export">
                        <i class="fa fa-file-export"></i>
                        Xuất Excel
                    </a>
                @endif

                <a href="#" class="float-right mr-2" data-bind="click: customer_modal.show">
                    <i class="icon-pencil"></i>
                    Cập nhật
                </a>

                <a href="#" class="float-right text-danger mr-2" data-bind="click: $root.delete">
                    <i class="fa fa-trash"></i>
                    Xóa
                </a>
            </div>
        </div>
        <div class="card-wrapper">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 col-xl-4">
                        <table class="table border-vertical">
                            <tbody>
                            <tr>
                                <th class="text-right">Điện thoại</th>
                                <td>{{$customer->phone}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Email</th>
                                <td>
                                    @if(strpos($customer->email, '@no_email.com') === false)
                                        {{$customer->email}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">Ngày sinh</th>
                                <td>{{$customer->birthday != null ? date('d/m/Y', $customer->birthday) : ''}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Giới tính</th>
                                <td>{{$customer->gender != null ? ($customer->gender == 'male' ? 'Nam' : 'Nữ') : ''}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6 col-xl-4">
                        <table class="table border-vertical">
                            <tbody>
                            <tr>
                                <th class="text-right">Địa chỉ</th>
                                <td>{{$customer->address}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Quận/huyện</th>
                                <td>{{$customer->district}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Tỉnh/thành</th>
                                <td>{{$customer->city}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Nhóm khách</th>
                                <td>{{$customer->group}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6 col-xl-4">
                        <table class="table border-vertical">
                            <tbody>
                            <tr>
                                <th class="text-right">Tổng tiền</th>
                                <td>{{format_money($total)}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Tổng tiền đơn GD</th>
                                <td>{{format_money($processing_total)}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Khách đã trả đơn đang GD</th>
                                <td>{{format_money($processing_paid)}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Còn thiếu</th>
                                <td>{{format_money($processing_total - $processing_paid)}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-default">
        <div role="tabpanel" style="margin: 0 -1px -1px -1px">
            <!-- Nav tabs-->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" href="#orderlabel" aria-controls="orderlabel" role="tab"
                       data-toggle="tab">
                        Đơn hàng
                    </a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="#ordertracking" aria-controls="ordertracking" role="tab"
                       data-toggle="tab">
                        Lịch sử thanh toán
                    </a>
                </li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content">
                <div class="tab-pane active show" id="orderlabel" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="border-top-0 border-left-0">Ngày tạo đơn</th>
                                <th class="border-top-0" width="90">Mã ĐH</th>
                                <th class="border-top-0">Trạng thái</th>
                                <th class="border-top-0">Website</th>
                                <th class="border-top-0">Tổng tiền</th>
                                <th class="border-top-0">Tổng tiền (vnđ)</th>
                                <th class="border-top-0">Khách đã trả</th>
                                <th class="border-top-0">Còn lại</th>
                                <th class="border-top-0">Ghi chú</th>
                                <th class="border-top-0">NV tạo đơn</th>
                                <th class="border-top-0">NV duyệt đơn</th>
                                <th class="border-top-0"></th>
                            </tr>
                            </thead>
                            <tbody class="text-center">
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{date('d/m/Y', $order->created_time)}}</td>
                                    <td>
                                        <a href="#"class="order-expand">
                                            {{$order->order_code == null ? $order->id : $order->order_code}}
                                        </a>
                                    </td>
                                    <td>
                                        @foreach(CustomerOrderStatus::LIST as $key=>$value)
                                            @if($key == $order->status)
                                                <span class="badge badge-{{$value['class']}}">{{$value['name']}}</span>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$order->website}}</td>
                                    <td>{{$order->currency_symbol}} {{$order->total}}</td>
                                    <td>{{format_money($order->sub_total)}}</td>
                                    <td>{{format_money($order->total_paid)}}</td>
                                    <td>{{format_money($order->sub_total - $order->total_paid)}}</td>
                                    <td>{{$order->note}}</td>
                                    <td>{{$order->user}}</td>
                                    <td>{{$order->approved_user}}</td>
                                    <td>
                                        <a href="/basso/customer_order/detail/{{$order->id}}" target="_blank">
                                            Chi tiết
                                        </a>
                                    </td>
                                </tr>
                                <tr class="tr-collapse">
                                    <td colspan="12">
                                        <div class="item-detail">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Sản phẩm</th>
                                                    <th>Variant</th>
                                                    <th>SL</th>
                                                    <th>Giá</th>
                                                    <th>Tình trạng</th>
                                                    <th>Giá vnđ</th>
                                                    <th>Ghi chú</th>
                                                </tr>
                                                </thead>
                                                <?php
                                                $items = array_filter($order_items, function ($t) use ($order) {
                                                    return $order->id == $t->order_id;
                                                });
                                                ?>
                                                <tbody>
                                                @foreach($items as $item)
                                                    <tr>
                                                        <td>
                                                            <a href="{{$item->link}}" target="_blank">
                                                                {{$item->name}}
                                                            </a>
                                                        </td>
                                                        <td>
                                                            @if($item->variations != null)
                                                                @foreach($item->variations as $variant)
                                                                    <b>{{$variant->name}}</b>: {{$variant->value}}
                                                                @endforeach
                                                            @endif
                                                        </td>
                                                        <td>{{$item->quantity}}</td>
                                                        <td>
                                                            {{$order->currency_symbol}} {{$item->price}}
                                                        </td>
                                                        <td>
                                                            @foreach(CustomerOrderItemStatus::LIST as $key=>$value)
                                                                @if($item->status == $key)
                                                                    {{$value['name']}}
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                        <td>{{format_money($item->quantity * $item->price * $order->currency_rate)}}</td>
                                                        <td>{{$item->note}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="ordertracking" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Thời gian</th>
                                <th>Số tiền</th>
                                <th>Trạng thái</th>
                                <th>Nội dung</th>
                                <th>PTTT</th>
                            </tr>
                            </thead>
                            <tbody class="text-center">
                            @foreach($payment_histories as $item)
                                <tr>
                                    <th>{{date('d/m/Y', $item->created_time)}}</th>
                                    <td>{{format_money($item->amount)}}</td>
                                    <td>
                                        @foreach(PaymentHistoryStatus::LIST as $key=>$value)
                                            @if($key == $item->status)
                                                <span class="badge badge-{{$value['class']}}">{{$value['name']}}</span>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$item->note}}</td>
                                    <td>
                                        @foreach(PaymentType::LIST as $key=>$value)
                                            @if($key == $item->type)
                                                {{$value}}
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('partial/detail/customer_editor_modal')
@endsection

@section('script')
    <script src="{{load_js('detail')}}"></script>
@endsection