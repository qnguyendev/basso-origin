<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$action = strtolower($this->router->fetch_method());
?>
<div class="modal fade" id="customer-editor-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cập nhật thông tin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group">
                            <label>
                                Khách hàng
                                <span class="text-danger">*</span>
                                <span class="validationMessage"
                                      data-bind="validationMessage: customer_modal.info.name"></span>
                            </label>
                            <input type="text" class="form-control" data-bind="value: customer_modal.info.name"/>
                        </div>
                        <div class="form-group">
                            <label>Giới tính</label>
                            <select class="form-control" data-bind="value: customer_modal.info.gender">
                                <option value="male">Nam</option>
                                <option value="female">Nữ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Email </label>
                            <input type="text" class="form-control" data-bind="value: customer_modal.info.email"/>
                        </div>
                        <div class="form-group">
                            <label>
                                Điện thoại
                                <span class="text-danger">*</span>
                                <span class="validationMessage"
                                      data-bind="validationMessage: customer_modal.info.phone"></span>
                            </label>
                            <input type="text" class="form-control" data-bind="value: customer_modal.info.phone"/>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6">
                        <div class="form-group">
                            <label> Địa chỉ </label>
                            <input type="text" class="form-control" data-bind="value: customer_modal.info.address"/>
                        </div>
                        <div class="form-group">
                            <label>Tỉnh/thành</label>
                            <select class="form-control" name="city"
                                    data-bind="options: customer_modal.data.cities, optionsText: 'name', optionsValue: 'id',
                                    value: customer_modal.info.city_id, event:{change: customer_modal.data.change_city}"></select>
                        </div>
                        <div class="form-group">
                            <label>Quận/huyện</label>
                            <select class="form-control" name="district"
                                    data-bind="value: customer_modal.info.district_id">
                                <option>- Lựa chọn -</option>
                                <!--ko foreach: customer_modal.data.districts-->
                                <option data-bind="value: id, text: name"></option>
                                <!--/ko-->
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Phân nhóm</label>
                            <select class="form-control" data-bind="options: customer_modal.data.groups, value: customer_modal.info.group_id,
                                optionsText: 'name', optionsValue: 'id'"></select>
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <label>Ghi chú</label>
                        <input type="text" class="form-control" data-bind="value: customer_modal.info.note"/>
                    </div>
                </div>
            </form>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: customer_modal.save">
                    <i class="icon-check"></i>
                    CẬP NHẬT
                </button>
            </div>
        </div>
    </div>
</div>