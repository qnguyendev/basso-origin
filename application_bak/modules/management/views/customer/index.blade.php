<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                Danh sách khách hàng
                <a href="#" class="float-right text-success ml-3" data-bind="click: import_modal.show">
                    <i class="fa fa-file-import"></i>
                    Nhập khách cũ
                </a>

                <a href="#" class="float-right" data-bind="click: modal.show">
                    <i class="fa fa-plus"></i>
                    Thêm khách hàng
                </a>
            </div>
        </div>
        <div class="card-body">
            <form autocomplete="off" class="row">
                <div class="form-group col-sm-12 col-md-6 col-lg-3">
                    <table class="table border-vertical pt-0">
                        <tbody>
                        <tr>
                            <th width="100" class="text-right">Tổng chi</th>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-bind="moneyMask: filter.total_paid"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">đ</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group col-sm-12 col-md-6 col-lg-3">
                    <table class="table border-vertical pt-0">
                        <tbody>
                        <tr>
                            <th width="100" class="text-right">Nhóm khách</th>
                            <td>
                                <select class="form-control" data-bind="value: filter.group_id">
                                    <option value="0">Tất cả</option>
                                    <!--ko foreach: data.groups-->
                                    <option data-bind="value: id, text: name"></option>
                                    <!--/ko-->
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group col-sm-12 col-md-6 col-xl-3">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Tìm theo tên, mã, số điện thoại"
                               data-bind="value: filter.key"/>
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-primary" data-bind="click: function(){search(1);}">
                                <i class="fa fa-search"></i>
                                Tìm
                            </button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Khách hàng</th>
                        <th>Điện thoại</th>
                        <th>Tiền hàng</th>
                        <th>Đã thanh toán</th>
                        <th>Còn thiếu</th>
                        <th>Nhóm khách</th>
                    </tr>
                    </thead>
                    <tbody class="text-center" data-bind="foreach: result">
                    <tr>
                        <td>
                            <a class="text-info font-weight-bold"
                               data-bind="attr: {href: `/management/customer/detail/${id()}`}, text: name">
                                <i class="icon-pencil"></i>
                                Chi tiết
                            </a>
                        </td>
                        <td data-bind="text: phone"></td>
                        <td data-bind="text: parseInt(sub_total()).toMoney(0)"></td>
                        <td data-bind="text: parseInt(total_paid()).toMoney(0)"></td>
                        <td data-bind="text: parseInt(sub_total() - total_paid()).toMoney(0)"></td>
                        <td data-bind="text: group"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            @include('pagination')
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection
@section('modal')
    @include('partial/index/editor_modal')
    @include('partial/index/import_modal')
@endsection