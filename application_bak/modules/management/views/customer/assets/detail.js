window.onload = function () {
    $('a.order-expand').click(function () {
        let tbody = $(this).parent().parent().parent();
        let nextItems = $(this).parent().parent().next().find('.item-detail');
        tbody.find('.item-detail').each(function () {
            if (!$(this).is(nextItems))
                $(this).slideUp();
        });
        nextItems.slideToggle();
    });
};

function viewModel() {
    let self = this;

    self.customer_modal = {
        info: {
            name: ko.observable().extend({required: {message: LABEL.required}}),
            email: ko.observable(),
            phone: ko.observable().extend({required: {message: LABEL.required}}),
            address: ko.observable(),
            city_id: ko.observable(),
            district_id: ko.observable(),
            group_id: ko.observable(),
            gender: ko.observable(),
            note: ko.observable()
        },
        data: {
            cities: ko.mapping.fromJS([]),
            districts: ko.mapping.fromJS([]),
            groups: ko.mapping.fromJS([]),
            change_city: function () {
                if ($('#customer-editor-modal').is(':visible')) {
                    AJAX.get(window.location, {
                        action: 'change-city',
                        id: self.customer_modal.info.city_id()
                    }, false, (res) => {
                        ko.mapping.fromJS(res.data, self.customer_modal.data.districts);
                    });
                }
            },
            init: function () {
                AJAX.get(window.location, {action: 'init'}, true, (res) => {
                    ko.mapping.fromJS(res.cities, self.customer_modal.data.cities);
                    ko.mapping.fromJS(res.districts, self.customer_modal.data.districts);
                    ko.mapping.fromJS(res.groups, self.customer_modal.data.groups);

                    self.customer_modal.info.name(res.customer.name);
                    self.customer_modal.info.email(res.customer.email);
                    self.customer_modal.info.phone(res.customer.phone);
                    self.customer_modal.info.address(res.customer.address);
                    self.customer_modal.info.gender(res.customer.gender);
                    self.customer_modal.info.city_id(res.customer.city_id);
                    self.customer_modal.info.district_id(res.customer.district_id);
                    self.customer_modal.info.note(res.customer.note);
                    self.customer_modal.info.group_id(res.customer.group_id);

                    $('#customer-editor-modal select[name=city]').val(self.customer_modal.info.city_id());
                });
            }
        },
        show: function () {
            self.customer_modal.info.errors.showAllMessages(false);
            MODAL.show('#customer-editor-modal');
        },
        save: function () {
            if (!self.customer_modal.info.isValid())
                self.customer_modal.info.errors.showAllMessages();
            else {
                let data = {
                    name: self.customer_modal.info.name(),
                    phone: self.customer_modal.info.phone(),
                    address: self.customer_modal.info.address(),
                    city_id: self.customer_modal.info.city_id(),
                    district_id: self.customer_modal.info.district_id(),
                    gender: self.customer_modal.info.gender(),
                    note: self.customer_modal.info.note(),
                    group_id: self.customer_modal.info.group_id(),
                    action: 'update-customer',
                    email: self.customer_modal.info.email()
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        window.location.reload();
                    }
                });
            }
        }
    };

    self.export = function () {
        AJAX.post(window.location, {action: 'export'}, true, (res) => {
            if (res.error)
                NOTI.danger(res.message);
            else {
                if (res.redirect_url != null)
                    window.location = res.redirect_url;
            }
        });
    };

    self.delete = function () {
        ALERT.confirm('Xác nhận xóa khách hàng', null, function () {
            AJAX.delete(window.location, null, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else
                    window.location = res.redirect_url;
            });
        });
    };
}

let model = new viewModel();
model.customer_modal.data.init();
ko.validatedObservable(model.customer_modal.info);
ko.applyBindings(model, document.getElementById('main-content'));