<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Billing_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Billing_model extends CI_Model
{
    private $_billing_table = 'billings';
    private $_term_table = 'billing_terms';
    private $_user_table = 'users';
    private $_web_order_table = 'web_orders';

    /**
     * Thêm danh mục thu chi
     * @param int $id
     * @param string $name
     * @param string|null $des
     * @return bool
     */
    public function term_insert(int $id = 0, string $name, string $des = null)
    {
        $this->db->trans_begin();
        if ($id == 0) {
            $this->db->insert($this->_term_table, ['name' => $name, 'description' => $des]);
        } else {
            $this->db->where('id', $id);
            $this->db->where('editable', 1);
            $this->db->update($this->_term_table, ['name' => $name, 'description' => $des]);
        }
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function term_get(int $id = null)
    {
        if ($id == 0)
            return $this->db->get($this->_term_table)->result();

        $this->db->where('id', $id);
        $this->db->where('editable', 1);
        $this->db->get($this->_term_table)->row();
    }

    /**
     * Xóa danh mục
     * @param int $id
     * @return bool
     */
    public function term_delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->where('editable', 1);
        $this->db->delete($this->_term_table);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * @param int $time
     * @param string $type
     * @param int $term_id
     * @param string|null $name
     * @param int $amount
     * @param string|null $phone
     * @param string|null $des
     * @param int|null $user_id
     * @param string|null $group
     * @param string|null $payment_method
     * @param int|null $customer_order_id
     * @param int|null $web_order_id
     * @return bool
     */
    public function insert(int $time, string $type, int $term_id, string $name = null, int $amount, string $phone = null,
                           string $des = null, int $user_id = null, string $group = null, string $payment_method = null,
                           int $customer_order_id = null, int $web_order_id = null)
    {
        $this->db->trans_begin();
        $data = [
            'time' => $time,
            'created_time' => time(),
            'name' => $name,
            'amount' => $amount,
            'description' => $des,
            'phone' => $phone,
            'type' => $type,
            'term_id' => $term_id,
            'user_id' => $user_id,
            'payment_method' => $payment_method,
            'group' => $group,
            'customer_order_id' => $customer_order_id,
            'web_order_id' => $web_order_id
        ];

        $this->db->insert($this->_billing_table, $data);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Xóa phiếu thu/chi
     * @param int $id
     * @return bool
     */
    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete($this->_billing_table);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get_by_id(int $id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_billing_table)->row();
    }

    public function get(string $type = null, int $page = 1, int $start, int $end = 0, string $payment_method = 'all', $key = null)
    {
        $this->db->select('t.*, o.name as term, d.first_name as user');
        $this->db->from("$this->_billing_table t");
        $this->db->join("$this->_term_table o", 't.term_id = o.id', 'left');
        $this->db->join("$this->_user_table d", 't.user_id = d.id', 'left');
        $this->db->join("$this->_web_order_table k", 'k.id = t.web_order_id', 'left');

        if ($type != null)
            $this->db->where('t.type', $type);

        $this->db->where('t.created_time >=', $start);

        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($payment_method != 'all') {
            $this->db->where('t.payment_method', $payment_method);
        }

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(k.order_number like '%$key%' or t.id like '%$key%')");
            }
        }

        if ($page > 0) {
            $this->db->offset(($page - 1) * PAGING_SIZE);
            $this->db->limit(PAGING_SIZE);
        }

        $this->db->order_by('t.created_time', 'desc');
        return $this->db->get()->result();
    }

    public function count(string $type = null, int $start, int $end = 0, string $payment_method = 'all', string $key = null)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_billing_table t");
        $this->db->join("$this->_web_order_table k", 'k.id = t.web_order_id', 'left');

        $this->db->where('t.created_time >=', $start);

        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($payment_method != 'all') {
            $this->db->where('t.payment_method', $payment_method);
        }

        if ($type != null)
            $this->db->where('t.type', $type);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(k.order_number like '%$key%' or t.id like '%$key%')");
            }
        }

        return $this->db->count_all_results();
    }

    public function sum(string $type = null, int $start, int $end = 0, string $group = 'all')
    {
        $this->db->select_sum('amount');
        $this->db->from($this->_billing_table);
        $this->db->where('created_time >=', $start);

        if ($end > 0)
            $this->db->where('created_time <=', $end);
        if ($group != 'all') {
            if (in_array($group, array_keys(BillingGroup::LIST)))
                $this->db->where('group', $group);
        }
        if ($type != null)
            $this->db->where('type', $type);
        return intval($this->db->get()->row()->amount);
    }

    public function delete_by_web_order(int $web_order_id)
    {
        $this->db->trans_begin();
        $this->db->where('web_order_id', $web_order_id);
        $this->db->delete($this->_billing_table);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function update_by_web_order(int $web_order_id, string $type, int $amount)
    {
        $this->db->trans_begin();
        $this->db->where('type', $type);
        $this->db->where('web_order_id', $web_order_id);
        $this->db->update($this->_billing_table, ['amount' => $amount]);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}