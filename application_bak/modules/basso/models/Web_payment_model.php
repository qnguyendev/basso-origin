<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Web_payment_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Web_payment_model extends CI_Model
{
    private $_table = 'web_order_payments';

    public function insert(int $id = 0, string $name, int $rate, $note = null)
    {
        $this->db->trans_begin();
        if ($id == 0) {
            $this->db->insert($this->_table, [
                'name' => $name,
                'currency_rate' => $rate,
                'note' => $note
            ]);
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->_table, [
                'name' => $name,
                'currency_rate' => $rate,
                'note' => $note
            ]);
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get($id = null)
    {
        if ($id != null) {
            $this->db->where('id', $id);
            return $this->db->get($this->_table)->row();
        }

        $this->db->order_by('name', 'asc');
        return $this->db->get($this->_table)->result();
    }

    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete($this->_table);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}