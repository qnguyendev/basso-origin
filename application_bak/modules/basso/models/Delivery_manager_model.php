<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Delivery_manager_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Delivery_manager_model extends CI_Model
{
    private $_web_order_table = 'web_orders';
    private $_web_item_table = 'web_order_items';
    private $_cus_order_table = 'customer_orders';
    private $_cus_item_table = 'customer_order_items';
    private $_warehouse_table = 'warehouses';
    private $_excerp_item_status = [CustomerOrderItemStatus::IN_INVENTORY, CustomerOrderItemStatus::SHIPPED, CustomerOrderItemStatus::RETURNED];

    #region Chưa có mã tracking
    public function no_tracking(int $page = 1, int $day = 10, $warehouse_id = 0, $key = null)
    {
        $this->db->select('t.id, t.created_time as buy_date, o.customer_order_id, t.order_number, d.name, t.note, d.order_code');
        $this->db->select('t.sub_total, o.tracking_code, o.delivered_date, t.currency_symbol');
        $this->db->select('d.created_time, t.total, k.name as warehouse, l.imported_time, o.id as item_id');

        $this->db->from("$this->_web_order_table t");
        $this->db->join("$this->_web_item_table o", 't.id = o.web_order_id', 'left');
        $this->db->join("$this->_cus_order_table d", 'o.customer_order_id = d.id', 'left');
        $this->db->join("$this->_warehouse_table k", 'k.id = t.warehouse_id', 'left');
        $this->db->join("$this->_cus_item_table l", 'l.id = o.item_id', 'left');

        if ($warehouse_id != 0)
            $this->db->where('k.id', $warehouse_id);
        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.tracking_code like '%$key%' or d.name like '%$key%' or d.phone like '%$key%' 
                or d.order_code like '%$key%' or t.order_number like '%$key%')");
            }
        }
        $this->db->where('t.status !=', WebOrderStatus::CANCELLED);
        $this->db->where('d.status !=', CustomerOrderStatus::CANCELLED);
        $this->db->where_not_in('l.status', $this->_excerp_item_status);
        $this->db->where('t.created_time <=', time() - $day * 24 * 3600);
        $this->db->where('o.tracking_code is null');
        $this->db->order_by('t.created_time', 'desc');
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count_no_tracking(int $day = 10, $warehouse_id = 0, $key = null)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_web_order_table t");
        $this->db->join("$this->_web_item_table o", 't.id = o.web_order_id', 'left');
        $this->db->join("$this->_cus_order_table d", 'o.customer_order_id = d.id', 'left');
        $this->db->join("$this->_warehouse_table k", 'k.id = t.warehouse_id', 'left');
        $this->db->join("$this->_cus_item_table l", 'l.id = o.item_id', 'left');

        if ($warehouse_id != 0)
            $this->db->where('k.id', $warehouse_id);
        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.tracking_code like '%$key%' or d.name like '%$key%' or d.phone like '%$key%' 
                or d.order_code like '%$key%' or t.order_number like '%$key%')");
            }
        }

        $this->db->where('t.status !=', WebOrderStatus::CANCELLED);
        $this->db->where('d.status !=', CustomerOrderStatus::CANCELLED);
        $this->db->where_not_in('l.status', $this->_excerp_item_status);
        $this->db->where('t.created_time <=', time() - $day * 24 * 3600);
        $this->db->where('o.tracking_code is null');
        $this->db->group_by('t.id');
        return $this->db->count_all_results();
    }

    #endregion

    #region Chưa về warehouse
    public function not_delivered(int $page = 1, int $day = 10, $warehouse_id = 0, $key = null)
    {
        $this->db->select('t.id, t.created_time as buy_date, o.customer_order_id, t.order_number, d.name, t.note, d.order_code');
        $this->db->select('t.sub_total, o.tracking_code, o.delivered_date, t.currency_symbol');
        $this->db->select('d.created_time, t.total, k.name as warehouse, l.imported_time, o.id as item_id');

        $this->db->from("$this->_web_order_table t");
        $this->db->join("$this->_web_item_table o", 't.id = o.web_order_id', 'left');
        $this->db->join("$this->_cus_order_table d", 'o.customer_order_id = d.id', 'left');
        $this->db->join("$this->_warehouse_table k", 'k.id = t.warehouse_id', 'left');
        $this->db->join("$this->_cus_item_table l", 'l.id = o.item_id', 'left');

        if ($warehouse_id != 0)
            $this->db->where('k.id', $warehouse_id);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.tracking_code like '%$key%' or d.name like '%$key%' or d.phone like '%$key%' 
                or d.order_code like '%$key%' or t.order_number like '%$key%')");
            }
        }

        $this->db->where_not_in('l.status', $this->_excerp_item_status);
        $this->db->where('t.status !=', WebOrderStatus::CANCELLED);
        $this->db->where('d.status !=', CustomerOrderStatus::CANCELLED);
        $this->db->where('o.tracking_date <=', time() - $day * 24 * 3600);
        $this->db->where('(o.delivered_date is null or o.delivered_date = 0)');
        $this->db->order_by('t.created_time', 'desc');
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count_not_delivered(int $day = 10, $warehouse_id = 0, $key = null)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_web_order_table t");
        $this->db->join("$this->_web_item_table o", 't.id = o.web_order_id', 'left');
        $this->db->join("$this->_cus_order_table d", 'o.customer_order_id = d.id', 'left');
        $this->db->join("$this->_warehouse_table k", 'k.id = t.warehouse_id', 'left');
        $this->db->join("$this->_cus_item_table l", 'l.id = o.item_id', 'left');

        if ($warehouse_id != 0)
            $this->db->where('k.id', $warehouse_id);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.tracking_code like '%$key%' or d.name like '%$key%' or d.phone like '%$key%' 
                or d.order_code like '%$key%' or t.order_number like '%$key%')");
            }
        }

        $this->db->where_not_in('l.status', $this->_excerp_item_status);
        $this->db->where('t.status !=', WebOrderStatus::CANCELLED);
        $this->db->where('d.status !=', CustomerOrderStatus::CANCELLED);
        $this->db->where('(o.delivered_date is null or o.delivered_date = 0)');
        $this->db->where('o.tracking_date <=', time() - $day * 24 * 3600);
        //$this->db->group_by('t.id');
        return $this->db->count_all_results();
    }
    #endregion

    #region Chưa về VN
    public function not_inventory(int $page = 1, int $day = 10, $warehouse_id = 0, $key = null)
    {
        $this->db->select('t.id, t.created_time as buy_date, o.customer_order_id, t.order_number, d.name, t.note, d.order_code');
        $this->db->select('t.sub_total, o.tracking_code, o.delivered_date, t.currency_symbol');
        $this->db->select('d.created_time, t.total, m.name as warehouse, l.imported_time, o.id as item_id');

        $this->db->from("$this->_web_order_table t");
        $this->db->join("$this->_web_item_table o", 't.id = o.web_order_id', 'left');
        $this->db->join("$this->_cus_order_table d", 'o.customer_order_id = d.id', 'left');
        $this->db->join("$this->_cus_item_table k", 'k.id = o.item_id', 'left');
        $this->db->join("$this->_warehouse_table m", 'm.id = t.warehouse_id', 'left');
        $this->db->join("$this->_cus_item_table l", 'l.id = o.item_id', 'left');

        if ($warehouse_id != 0)
            $this->db->where('m.id', $warehouse_id);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.tracking_code like '%$key%' or d.name like '%$key%' or d.phone like '%$key%' 
                or d.order_code like '%$key%' or t.order_number like '%$key%')");
            }
        }

        //$this->db->join("$this->_warehouse_table l", 'l.id = t.warehouse_id');
        $this->db->where('t.status !=', WebOrderStatus::CANCELLED);
        $this->db->where('d.status !=', CustomerOrderStatus::CANCELLED);
        $this->db->where('o.delivered_date <=', time() - $day * 24 * 3600);
        $this->db->where('o.delivered_date >', 0);
        $this->db->where_not_in('k.status', $this->_excerp_item_status);
        $this->db->order_by('t.created_time', 'desc');
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count_not_inventory(int $day = 10, $warehouse_id = 0, $key = null)
    {
        $this->db->select('t.id');

        $this->db->from("$this->_web_order_table t");
        $this->db->join("$this->_web_item_table o", 't.id = o.web_order_id', 'left');
        $this->db->join("$this->_cus_order_table d", 'o.customer_order_id = d.id', 'left');
        $this->db->join("$this->_cus_item_table k", 'k.id = o.item_id', 'left');
        $this->db->join("$this->_warehouse_table m", 'm.id = t.warehouse_id', 'left');
        //$this->db->join("$this->_cus_item_table l", 'l.id = o.item_id', 'left');

        if ($warehouse_id != 0)
            $this->db->where('m.id', $warehouse_id);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.tracking_code like '%$key%' or d.name like '%$key%' or d.phone like '%$key%' 
                or d.order_code like '%$key%' or t.order_number like '%$key%')");
            }
        }

        $this->db->where('t.status !=', WebOrderStatus::CANCELLED);
        $this->db->where('d.status !=', CustomerOrderStatus::CANCELLED);
        $this->db->where('o.delivered_date <=', time() - $day * 24 * 3600);
        $this->db->where('o.delivered_date >', 0);
        $this->db->where_not_in('k.status', $this->_excerp_item_status);

        return $this->db->count_all_results();
    }
    #endregion

    #region Tất cả
    public function all(int $page = 1, $warehouse_id = 0, $key = null)
    {
        $this->db->select('t.id, t.created_time as buy_date, o.customer_order_id, t.order_number, d.name, t.note, d.order_code');
        $this->db->select('t.sub_total, o.tracking_code, o.delivered_date, t.currency_symbol');
        $this->db->select('d.created_time, t.total, k.name as warehouse, l.imported_time, o.id as item_id');

        $this->db->from("$this->_web_order_table t");
        $this->db->join("$this->_web_item_table o", 't.id = o.web_order_id', 'left');
        $this->db->join("$this->_cus_order_table d", 'o.customer_order_id = d.id', 'left');
        $this->db->join("$this->_warehouse_table k", 'k.id = t.warehouse_id', 'left');
        $this->db->join("$this->_cus_item_table l", 'l.id = o.item_id', 'left');

        if ($warehouse_id != 0)
            $this->db->where('k.id', $warehouse_id);
        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.tracking_code like '%$key%' or d.name like '%$key%' or d.phone like '%$key%' 
                or d.order_code like '%$key%' or t.order_number like '%$key%')");
            }
        }

        $this->db->where('t.status !=', WebOrderStatus::CANCELLED);
        $this->db->where('d.status !=', CustomerOrderStatus::CANCELLED);
        $this->db->order_by('t.created_time', 'desc');
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count_all($warehouse_id = 0, $key = null)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_web_order_table t");
        $this->db->join("$this->_web_item_table o", 't.id = o.web_order_id', 'left');
        $this->db->join("$this->_cus_order_table d", 'o.customer_order_id = d.id', 'left');
        $this->db->join("$this->_warehouse_table k", 'k.id = t.warehouse_id', 'left');

        if ($warehouse_id != 0)
            $this->db->where('k.id', $warehouse_id);
        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.tracking_code like '%$key%' or d.name like '%$key%' or d.phone like '%$key%' 
                or d.order_code like '%$key%' or t.order_number like '%$key%')");
            }
        }

        $this->db->where('t.status !=', WebOrderStatus::CANCELLED);
        $this->db->where('d.status !=', CustomerOrderStatus::CANCELLED);
        $this->db->group_by('t.id');
        return $this->db->count_all_results();
    }
    #endregion
}