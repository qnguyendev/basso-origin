<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Tab_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Order_tab_model extends CI_Model
{
    private $_table = 'customer_order_tabs';

    public function save(int $id = 0, array $data)
    {
        if ($id == 0)
            $data['created_time'] = time();

        $this->db->trans_begin();
        if ($id == 0) {
            $this->db->insert($this->_table, $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->_table, $data);
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return $id;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get_by_user(int $user_id)
    {
        $this->db->where('user_id', $user_id);
        return $this->db->get($this->_table)->result();
    }

    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete($this->_table);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}