<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_label_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Order_label_model extends CI_Model
{
    /**
     * Thêm nhóm nhãn đơn hàng
     * @param int $id
     * @param string $name
     * @param string|null $des
     * @return bool
     */
    public function group_insert(int $id, string $name, string $des = null)
    {
        $this->db->trans_begin();
        $data = ['name' => $name, 'des' => $des];
        if ($id == 0)
            $this->db->insert('order_label_groups', $data);
        else {
            $this->db->where('id', $id);
            $this->db->update('order_label_groups', $data);
        }
        if($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function group_get($id = null)
    {
        if ($id == null) {
            return $this->db->get('order_label_groups')->result();
        }

        if (is_array($id)) {
            $this->db->where_in('id', $id);
            $this->db->order_by('name', 'asc');
            return $this->db->get('order_label_groups')->result();
        }

        $this->db->where('id', $id);
        $this->db->order_by('name', 'asc');
        return $this->db->get('order_label_groups')->row();
    }

    /**
     * Xóa nhóm nhãn đơn hàng
     * @param int $id
     * @return bool
     */
    public function group_delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete('order_label_groups');

        if($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Thêm nhãn đơn hàng
     * @param int $id
     * @param int $group_id
     * @param string $name
     * @param string|null $des
     * @return bool
     */
    public function label_insert(int $id, int $group_id, string $name, string $des = null)
    {
        $this->db->trans_begin();
        $data = ['name' => $name, 'des' => $des];
        if ($id == 0) {
            $data['group_id'] = $group_id;
            $this->db->insert('order_labels', $data);
        } else {
            $this->db->where('id', $id);
            $this->db->update('order_labels', $data);
        }
        if($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function label_get($id = null, $group_id = null)
    {
        if ($id == null) {
            if ($group_id != null)
                $this->db->where('group_id', $group_id);
            return $this->db->get('order_labels')->result();
        }

        $this->db->where('id', $id);
        $this->db->order_by('name', 'asc');
        return $this->db->get('order_labels')->row();
    }

    /**
     * Xóa nhãn đơn hàng
     * @param int $id
     * @return bool
     */
    public function label_delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete('order_labels');
        if($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}