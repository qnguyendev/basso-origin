<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Return_order_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Return_order_model extends CI_Model
{
    private $_order_table = 'customer_orders_returned';
    private $_order_item_table = 'customer_order_returned_items';

    public function insert(array $data, array $items)
    {
        $data['created_time'] = time();
        $data['sub_total'] = array_sum(array_column($items, 'total'));

        $this->db->trans_begin();
        $this->db->insert($this->_order_table, $data);
        $order_id = $this->db->insert_id();

        if ($this->db->trans_status()) {
            if ($this->_insert_items($order_id, $items)) {
                $this->db->trans_commit();
                return true;
            }
        }

        $this->db->trans_rollback();
        return false;
    }

    public function _insert_items(int $order_return_id, array $items)
    {
        for ($i = 0; $i < count($items); $i++)
            $items[$i]['order_returned_id'] = $order_return_id;

        $this->db->trans_begin();
        $this->db->insert_batch($this->_order_item_table, $items);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get(int $page = 1, int $start = 0, int $end = 0, string $status = null, string $key = null)
    {
        $this->db->select('t.created_time, t.status, t.order_id, o.name, t.sub_total, t.note, t.id, o.order_code');
        $this->db->from("$this->_order_table t");
        $this->db->join("customer_orders o", 't.order_id = o.id', 'left');
        $this->db->join("$this->_order_item_table d", 'd.order_returned_id = t.id', 'left');
        $this->db->join('customer_order_items k', 'k.id = d.item_id', 'left');

        if ($status != null) {
            if (in_array($status, array_keys(CustomerOrdersReturnedStatus::LIST)))
                $this->db->where('t.status', $status);
        }

        $this->db->where('t.created_time >=', $start);
        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.name like '%$key%' or o.phone like '%$key%')");
            }
        }

        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->order_by('t.created_time', 'desc');
        $this->db->group_by('t.id');

        return $this->db->get()->result();
    }

    public function count(int $start = 0, int $end = 0, string $status = null, string $key = null)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_order_table t");
        $this->db->join("customer_orders o", 't.order_id = o.id', 'left');

        if ($status != null) {
            if (in_array($status, array_keys(CustomerOrdersReturnedStatus::LIST)))
                $this->db->where('t.status', $status);
        }

        $this->db->where('t.created_time >=', $start);
        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(o.name like '%$key%' or o.phone like '%$key%')");
            }
        }

        return $this->db->count_all_results();
    }

    public function get_items($return_order_id)
    {
        $this->db->select('t.price, t.quantity, d.path as image_path, o.name, t.order_returned_id, t.total');
        $this->db->from("$this->_order_item_table t");
        $this->db->join("customer_order_items o", 't.item_id = o.id', 'left');
        $this->db->join('images d', 'o.image_id = d.id', 'left');

        if (is_array($return_order_id))
            $this->db->where_in('t.order_returned_id', $return_order_id);
        else
            $this->db->where('t.order_returned_id', $return_order_id);

        return $this->db->get()->result();
    }
}