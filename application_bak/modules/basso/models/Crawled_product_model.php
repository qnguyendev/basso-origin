<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Crawled_product_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Crawled_product_model extends CI_Model
{
    private $_table_name = 'crawled_products';

    public function insert(string $id, array $data)
    {
        $this->db->trans_begin();

        $data['id'] = $id;
        $data['created_time'] = time();

        $this->db->insert($this->_table_name, $data);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get_by_id(string $id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_table_name)->row();
    }

    public function clean()
    {
        $this->db->where('created_time <=', time() - 3600 * 48);
        $this->db->delete($this->_table_name);
    }
}