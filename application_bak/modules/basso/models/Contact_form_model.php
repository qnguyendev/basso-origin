<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Contact_form_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Contact_form_model extends CI_Model
{
    private $_table = 'contact_forms';

    public function insert($name, $phone, $content)
    {
        $data = [
            'name' => $name,
            'created_time' => time(),
            'phone' => $phone,
            'content' => $content
        ];

        $this->db->insert($this->_table, $data);
    }

    public function update_note(int $id, string $note = null)
    {
        $this->db->where('id', $id);
        $this->db->update($this->_table, ['note' => $note]);
    }

    public function get_by_id(int $id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_table)->row();
    }

    public function get(int $page, string $key = null)
    {
        if ($key != null) {
            if (!empty($key))
                $this->db->where("(name like '%$key%' or phone like '%$key%')");
        }

        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->order_by('created_time', 'desc');

        return $this->db->get($this->_table)->result();
    }

    public function count(string $key = null)
    {
        if ($key != null) {
            if (!empty($key))
                $this->db->where("(name like '%$key%' or phone like '%$key%')");
        }

        return $this->db->count_all_results($this->_table);
    }
}