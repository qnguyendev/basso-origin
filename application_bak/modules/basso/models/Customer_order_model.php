<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Customer_order_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Customer_order_model extends CI_Model
{
    private $_order_table = 'customer_orders';
    private $_order_item_table = 'customer_order_items';
    private $_order_label_table = 'customer_order_labels';
    private $_web_order_table = 'web_orders';
    private $_web_order_items_table = 'web_order_items';
    private $_website_table = 'websites';
    private $_country_table = 'countries';
    private $_user_table = 'users';
    private $_image_table = 'images';

    /**
     * @param array $customer_info
     * @param array $order_detail
     * @param array $items
     * @return int|null
     */
    public function insert(array $customer_info, array $order_detail, array $items)
    {
        $this->db->trans_begin();
        $data = [
            'created_time' => $order_detail['created_time'],
            'branch' => $order_detail['branch'],
            'customer_id' => $customer_info['id'],
            'name' => $customer_info['name'],
            'phone' => $customer_info['phone'],
            'email' => $customer_info['email'],
            'shipping_address' => $customer_info['shipping_address'],
            'city_id' => $customer_info['city_id'],
            'district_id' => $customer_info['district_id'],
            'order_place_id' => $customer_info['order_place_id'],
            'country_id' => $order_detail['country_id'],
            'currency_rate' => $order_detail['currency_rate'],
            'currency_symbol' => $order_detail['currency_symbol'],
            'discount_code' => $order_detail['discount_code'],
            'discount_total' => $order_detail['discount_total'],
            'discount_type' => isset($order_detail['discount_type']) ? is_enull($order_detail['discount_type'], null) : null,
            'discount_max' => isset($order_detail['discount_max']) ? is_enull($order_detail['discount_max'], 0) : 0,
            'discount_amount' => isset($order_detail['discount_amount']) ? is_enull($order_detail['discount_amount'], 0) : 0,
            'web_shipping_fee' => $order_detail['web_shipping_fee'],
            'status' => $order_detail['status'],
            'total' => $order_detail['total'],
            'sub_total' => $order_detail['sub_total'],
            'website' => $order_detail['website'],
            'payment_status' => OrderPaymentStatus::NO_PAYMENT,
            'brand' => $order_detail['brand'],
            'shipping_status' => CustomerOrderShipStatus::NONE,
            'user_id' => is_enull($order_detail['user_id'], null, 'numeric'),
            'order_code' => $order_detail['order_code'],
            'approve_user_id' => null,
            'world_shipping_fee' => 0,
            'total_weight' => 0,
            'payment_method' => null,
            'quantity' => isset($order_detail['quantity']) ? is_enull($order_detail['quantity'], 0) : 0,
            'note' => isset($order_detail['note']) ? $order_detail['note'] : null
        ];

        if (isset($order_detail['fee_percent'])) {
            $data['fee_percent'] = is_enull($order_detail['fee_percent'], 0, 'numeric');
        }

        if (isset($order_detail['world_shipping_fee'])) {
            $data['world_shipping_fee'] = is_enull($order_detail['world_shipping_fee'], 0, 'numeric');
        }

        if (isset($order_detail['payment_method'])) {
            $data['payment_method'] = is_enull($order_detail['payment_method'], null);
        }

        if (isset($order_detail['total_weight'])) {
            $data['total_weight'] = is_enull($order_detail['total_weight'], 0, 'numeric');
        }

        if (isset($order_detail['approve_user_id']))
            $data['approve_user_id'] = is_enull($order_detail['approve_user_id'], null, 'numeric');

        $this->db->insert($this->_order_table, $data);
        if ($this->db->trans_status()) {
            $order_id = $this->db->insert_id();
            if ($this->insert_items($order_id, $items)) {
                if ($this->_add_website($data['website'])) {
                    $this->db->trans_commit();
                    return $order_id;
                }
            }
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Lưu sản phẩm đơn hàng của khách
     * @param int $order_id
     * @param array $items
     * @return bool
     */
    public function insert_items(int $order_id, array $items)
    {
        $data = [];
        $this->db->trans_begin();
        if (isset($items)) {
            if (is_array($items)) {
                foreach ($items as $item) {
                    if (!isset($item['weight']))
                        $item['weight'] = 0;

                    $_item = [
                        'created_time' => time(),
                        'order_id' => $order_id,
                        'name' => $item['name'],
                        'quantity' => $item['quantity'],
                        'price' => $item['price'],
                        'variations' => $item['variations'],
                        'note' => $item['note'],
                        'status' => $item['status'],
                        'term_id' => $item['term_id'],
                        'term_fee' => $item['term_fee'],
                        'link' => $item['link'],
                        'website' => $item['website'],
                        'requirement' => $item['requirement'],
                        'image_id' => $item['image_id'],
                        'weight' => is_enull($item['weight'], null),
                        'web_shipping_fee' => isset($item['web_shipping_fee']) ? is_enull($item['web_shipping_fee'], 0, 'numeric') : 0
                    ];

                    if (isset($item['image_id']))
                        $_item['image_id'] = $item['image_id'];

                    array_push($data, $_item);
                }

                $this->db->insert_batch($this->_order_item_table, $data);
                if ($this->db->affected_rows()) {
                    $this->db->trans_commit();
                    return true;
                }
            }
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Xóa sản phẩm khỏi đơn hàng
     * @param int $order_id
     * @param $item_id
     * @return bool
     */
    public function delete_items(int $order_id, $item_id)
    {
        if (isset($item_id)) {
            $this->db->trans_begin();
            $this->db->where('order_id', $order_id);
            if (!is_array($item_id))
                $this->db->where('id', $item_id);
            else
                $this->db->where_in('id', $item_id);

            $this->db->delete($this->_order_item_table);
            if ($this->db->affected_rows()) {
                $this->db->trans_commit();
                return true;
            }

            $this->db->trans_rollback();
            return false;
        }
    }

    /**
     * Chi tiết đơn hàng theo ID
     * @param int $id
     * @return mixed
     */
    public function get_by_id(int $id)
    {
        $this->db->select('t.*, o.name as country, d.name as order_place, l.name as district');
        $this->db->select('k.name as city, o.shipping_fee as country_shipping_fee');
        $this->db->select('m.first_name as approve_user');
        $this->db->from("{$this->_order_table} t");
        $this->db->join("$this->_country_table o", 't.country_id = o.id', 'left');
        $this->db->join('order_places d', 't.order_place_id = d.id', 'left');
        $this->db->join('cities k', 't.city_id = k.id', 'left');
        $this->db->join('districts l', 't.district_id = l.id', 'left');
        $this->db->join("$this->_user_table m", 'm.id = t.approve_user_id', 'left');
        $this->db->where('t.id', $id);
        return $this->db->get()->row();
    }

    public function get_by_code(string $code)
    {
        $this->db->select('t.*, o.name as country, d.name as order_place, l.name as district');
        $this->db->select('k.name as city, o.shipping_fee as country_shipping_fee');
        $this->db->select('m.first_name as approve_user');
        $this->db->from("{$this->_order_table} t");
        $this->db->join("$this->_country_table o", 't.country_id = o.id', 'left');
        $this->db->join('order_places d', 't.order_place_id = d.id', 'left');
        $this->db->join('cities k', 't.city_id = k.id', 'left');
        $this->db->join('districts l', 't.district_id = l.id', 'left');
        $this->db->join("$this->_user_table m", 'm.id = t.approve_user_id', 'left');
        $this->db->where('t.order_code', $code);
        return $this->db->get()->row();
    }

    /**
     * Cập nhật thông tin đơn hàng
     * @param int $order_id
     * @param array $data
     * @return bool
     */
    public function update(int $order_id, array $data)
    {
        $this->db->trans_begin();
        $allow_keys = ['status', 'name', 'email', 'shipping_address', 'updated_time', 'city_id', 'user_id', 'note', 'payment_status',
            'country_id', 'currency_rate', 'currency_symbol', 'order_place_id', 'total', 'sub_total', 'approve_user_id', 'order_code',
            'web_shipping_fee', 'inter_shipping_fee', 'discount_total', 'discount_code', 'brand', 'branch', 'world_shipping_fee',
            'discount_amount', 'discount_type'];

        foreach ($data as $key => $value) {
            if (!in_array($key, $allow_keys))
                unset($data[$key]);
        }

        $data['updated_time'] = time();
        $this->db->where('id', $order_id);
        $this->db->update($this->_order_table, $data);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function create_order_code(int $created_time, string $brand = null)
    {
        if ($brand == null)
            return null;

        $start = strtotime(date('Y-m-d 00:00:00', $created_time));
        $end = $start + 24 * 3600 - 1;

        $this->db->where('created_time >=', $start);
        $this->db->where('created_time <=', $end);
        $total = $this->db->count_all_results($this->_order_table) + 1;

        if ($brand == 'asale') {
            return 'AS' . date('dmy', $created_time) . ($total < 10 ? "0$total" : $total);
        }

        return 'BS' . date('dmy', $created_time) . ($total < 10 ? "0$total" : $total);
    }

    /**
     * Cập nhật sản phẩm
     * @param int $order_id
     * @param int $item_id
     * @param array $data
     * @return bool
     */
    public function update_item(int $order_id, int $item_id, array $data)
    {
        $this->db->trans_begin();
        $data['updated_time'] = time();
        $allow_keys = ['name', 'quantity', 'price', 'status', 'term_id', 'term_fee', 'weight',
            'tracking_code', 'imported_time', 'image_id', 'requirement', 'note'];

        foreach ($data as $key => $value) {
            if (!in_array($key, $allow_keys))
                unset($data[$key]);
        }

        $this->db->where('order_id', $order_id);
        $this->db->where('id', $item_id);
        $this->db->update($this->_order_item_table, $data);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function update_web_order_tracking_code(int $item_id, string $tracking_code)
    {
        $this->db->where('item_id', $item_id);
        $this->db->update('web_order_items', ['tracking_code' => $tracking_code]);
    }

    /**
     * Danh sách sản phẩm của đơn hàng
     * @param int|array $order_id
     * @param array|null $status
     * @param string|null $requirement
     * @param bool $get_order_number
     * @return array
     */
    public function get_items($order_id, $status = null, $requirement = null, bool $get_order_number = false)
    {
        $this->db->select('t.*, o.path as image_path, d.delivered_date');
        if ($get_order_number)
            $this->db->select('l.order_number');

        $this->db->from("$this->_order_item_table t");
        $this->db->join("$this->_image_table o", 't.image_id = o.id', 'left');
        $this->db->join("$this->_web_order_items_table d", 'd.item_id = t.id', 'left');
        if ($get_order_number) {
            $this->db->join("$this->_web_order_table l", 'l.id = d.web_order_id', 'left');
            /*$this->db->where('(l.status != "' . WebOrderStatus::CANCELLED . '" or l.status is null)');*/
        }

        if ($status != null) {
            if (is_array($status))
                $this->db->where_in('t.status', $status);
            else
                $this->db->where('t.status', $status);
        }

        if ($requirement != null) {
            if (is_array($status))
                $this->db->where_in('t.requirement', $requirement);
            else
                $this->db->where('t.requirement', $requirement);
        }

        if (!is_array($order_id))
            $this->db->where('t.order_id', $order_id);
        else if (count($order_id) > 0)
            $this->db->where_in('t.order_id', $order_id);

        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    /**
     * Lấy sản phẩm theo ID
     * @param $id
     * @param null $status
     * @return array|mixed
     */
    public function get_items_by_id($id, $status = null)
    {
        $this->db->select('t.*, o.path as image_path');
        $this->db->from("$this->_order_item_table t");
        $this->db->join("$this->_image_table o", 't.image_id = o.id', 'left');

        if ($status != null)
            $this->db->where('t.status', $status);

        if (is_array($id)) {
            $this->db->where_in('t.id', $id);
            $this->db->group_by('t.id');
            return $this->db->get()->result();
        }

        $this->db->where('t.id', $id);
        $this->db->group_by('t.id');
        return $this->db->get()->row();
    }

    /**
     * Cập nhật nhãn đơn hàng
     * @param int $order_id
     * @param array $labels
     * @return bool
     */
    public function update_labels(int $order_id, array $labels)
    {
        $this->db->trans_begin();
        $this->db->where('order_id', $order_id);
        $this->db->delete('customer_order_labels');

        for ($i = 0; $i < count($labels); $i++)
            $labels[$i]['order_id'] = $order_id;

        $this->db->insert_batch('customer_order_labels', $labels);
        if ($this->db->affected_rows()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Danh sách nhãn đơn hàng
     * @param int $order_id
     * @return array
     */
    public function get_labels(int $order_id)
    {
        $this->db->select('group_id, label_id');
        $this->db->where('order_id', $order_id);
        return $this->db->get('customer_order_labels')->result();
    }


    /**
     * @param int $page
     * @param int $start
     * @param int $end
     * @param null $status
     * @param null $payment_status
     * @param null $item_status
     * @param null $label_id
     * @param null $staff_id
     * @param null $shipping_status
     * @param null $tracking_status
     * @param null $item_requirement
     * @param null $search
     * @return array
     */
    public function get(int $page = 1, int $start = 0, int $end = 0, $status = null, $payment_status = null, $item_status = null,
                        $label_id = null, $staff_id = null, $shipping_status = null, $tracking_status = null,
                        $item_requirement = null, $search = null)
    {
        $this->db->select('t.*, o.first_name as user, l.first_name as approved_user, m.name as country, x.first_name as customer_name');
        $this->db->from("{$this->_order_table} t");
        $this->db->join("{$this->_user_table} o", 't.user_id = o.id', 'left');
        $this->db->join("{$this->_user_table} l", 't.approve_user_id = l.id', 'left');
        $this->db->join("{$this->_order_item_table} d", 'd.order_id = t.id', 'left');
        $this->db->join("{$this->_order_label_table} k", 'k.order_id = t.id', 'left');
        $this->db->join("{$this->_country_table} m", 'm.id = t.country_id', 'left');
        $this->db->join("{$this->_user_table} x", 'x.id = t.customer_id', 'left');

        $this->db->where('t.created_time >=', $start);
        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($tracking_status != null) {
            if (is_array($tracking_status)) {
                $condi = [];
                if (in_array(CustomerOrderItemTracking::NONE, $tracking_status))
                    array_push($condi, 'd.tracking_code IS NULL');
                if (in_array(CustomerOrderItemTracking::HAS, $tracking_status))
                    array_push($condi, 'NOT (d.tracking_code <=> NULL)');

                $condi = join('or', $condi);
                $this->db->where("($condi)");
            }
        }

        if ($shipping_status != null) {
            if (is_array($shipping_status))
                $this->db->where_in('t.shipping_status', $shipping_status);
        }

        if ($item_requirement != null) {
            if (is_array($item_requirement))
                $this->db->where_in('d.requirement', $item_requirement);
        }

        if ($status != null) {
            if (is_array($status))
                $this->db->where_in('t.status', $status);
            else
                $this->db->where('t.status', $status);
        }

        if ($payment_status != null) {
            if (is_array($payment_status)) {
                if (!in_array(OrderPaymentStatus::COMPLETED, $payment_status) && !in_array(OrderPaymentStatus::UNCOMPLETED, $payment_status))
                    $this->db->where_in('t.payment_status', $payment_status);
                else {
                    if (in_array(OrderPaymentStatus::COMPLETED, $payment_status))
                        $this->db->where('t.total_paid = t.sub_total');
                    else if (in_array(OrderPaymentStatus::UNCOMPLETED, $payment_status))
                        $this->db->where('t.total_paid < t.sub_total');
                }
            }
        }

        if ($item_status != null) {
            if (is_array($item_status)) {
                $this->db->where_in('d.status', $item_status);
            }
        }

        if ($label_id != null) {
            if (is_array($label_id)) {
                $this->db->where_in('k.label_id', $label_id);
            }
        }

        if ($staff_id != null) {
            if (is_array($staff_id)) {
                $this->db->where_in('t.	approve_user_id', $staff_id);
            }
        }

        if ($search != null) {
            $this->db->where("(t.name like '%$search%' or t.email like '%$search%' or t.phone like '%$search%' or t.website = '$search' or t.order_code like '%$search%')");
        }

        $this->db->order_by('t.id', 'desc');
        $this->db->group_by('t.id');

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }
        return $this->db->get()->result();
    }

    public function count(int $start = 0, int $end = 0, $status = null, $payment_status = null, $item_status = null,
                          $label_id = null, $staff_id = null, $shipping_status = null, $tracking_status = null,
                          $item_requirement = null, $search = null)
    {
        $this->db->select('t.id');
        $this->db->from("{$this->_order_table} t");
        $this->db->join('users o', 't.user_id = o.id', 'left');
        $this->db->join("{$this->_order_item_table} d", 'd.order_id = t.id', 'left');
        $this->db->join('customer_order_labels k', 'k.order_id = t.id', 'left');

        $this->db->where('t.created_time >=', $start);
        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($tracking_status != null) {
            if (is_array($tracking_status)) {
                $condi = [];
                if (in_array(CustomerOrderItemTracking::NONE, $tracking_status))
                    array_push($condi, 'd.tracking_code is null');
                if (in_array(CustomerOrderItemTracking::HAS, $tracking_status))
                    array_push($condi, 'd.tracking_code is not null');

                $condi = join('or', $condi);
                $this->db->where("($condi)");
            }
        }

        if ($item_requirement != null) {
            if (is_array($item_requirement))
                $this->db->where_in('d.requirement', $item_requirement);
        }

        if ($shipping_status != null) {
            if (is_array($shipping_status))
                $this->db->where_in('t.shipping_status', $shipping_status);
        }

        if ($payment_status != null) {
            if (is_array($payment_status)) {
                if (!in_array(OrderPaymentStatus::COMPLETED, $payment_status) && !in_array(OrderPaymentStatus::UNCOMPLETED, $payment_status))
                    $this->db->where_in('t.payment_status', $payment_status);
                else {
                    if (in_array(OrderPaymentStatus::COMPLETED, $payment_status))
                        $this->db->where('t.total_paid = t.sub_total');
                    else if (in_array(OrderPaymentStatus::UNCOMPLETED, $payment_status))
                        $this->db->where('t.total_paid < t.sub_total');
                }
            }
        }

        if ($status != null) {
            if (is_array($status))
                $this->db->where_in('t.status', $status);
            else
                $this->db->where('t.status', $status);
        }

        if ($item_status != null) {
            if (is_array($item_status)) {
                $this->db->where_in('d.status', $item_status);
            }
        }

        if ($label_id != null) {
            if (is_array($label_id)) {
                $this->db->where_in('k.label_id', $label_id);
            }
        }

        if ($staff_id != null) {
            if (is_array($staff_id)) {
                $this->db->where_in('t.	approve_user_id', $staff_id);
            }
        }

        if ($search != null) {
            $this->db->where("(t.name like '%$search%' or t.email like '%$search%' or t.phone like '%$search%' or t.website = '$search' or t.order_code like '%$search%')");
        }

        $this->db->group_by('t.id');
        return $this->db->count_all_results();
    }

    /**
     * Lấy đơn hàng cho export
     * @param int $start
     * @param int $end
     * @param null $status
     * @param null $search
     * @param array|null $user_id
     * @return array
     */
    public function get_export(int $start = 0, int $end = 0, $status = null, $search = null, array $user_id = null)
    {
        $this->db->select('t.*, o.first_name as user, l.first_name as approved_user, m.name as country');
        $this->db->from("{$this->_order_table} t");
        $this->db->join("{$this->_user_table} o", 't.user_id = o.id', 'left');
        $this->db->join("{$this->_user_table} l", 't.approve_user_id = l.id', 'left');
        $this->db->join("{$this->_order_item_table} d", 'd.order_id = t.id', 'left');
        $this->db->join("{$this->_order_label_table} k", 'k.order_id = t.id', 'left');
        $this->db->join("{$this->_country_table} m", 'm.id = t.country_id', 'left');

        $this->db->where('t.created_time >=', $start);
        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($status != null)
            if ($status != 'all')
                $this->db->where('t.status', $status);

        if ($search != null) {
            $this->db->where("(t.name like '%$search%' or t.phone like '%$search%' or t.website = '$search' or t.order_code like '%$search%')");
        }

        if ($user_id != null) {
            if (is_array($user_id))
                $this->db->where_in('t.approve_user_id', $user_id);
            else
                $this->db->where('t.approve_user_id', $user_id);
        }

        $this->db->order_by('t.created_time', 'desc');
        $this->db->group_by('t.id');

        return $this->db->get()->result();
    }

    /**
     * Xóa đơn hàng
     * @param int $order_id
     * @return bool
     */
    public function delete(int $order_id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $order_id);
        $this->db->delete($this->_order_table);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Lấy mã tracking của sản phẩm đã mua nhưng chưa về kho VC
     * @return array
     */
    public function get_tracking()
    {
        $this->db->distinct();
        $this->db->select('d.tracking_code');
        $this->db->from("{$this->_order_table} t");
        $this->db->join("{$this->_order_item_table} d", 'd.order_id = t.id', 'left');
        $this->db->where('d.tracking_code !=', null);
        $this->db->where('d.status', CustomerOrderItemStatus::BOUGHT);

        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    private function _add_website($website)
    {
        $this->db->trans_begin();
        $this->db->where('name', $website);
        if ($this->db->count_all_results($this->_website_table) == 0) {
            $this->db->insert($this->_website_table, ['name' => $website]);
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Danh sách các website đặt mua
     * @return array
     */
    public function get_websites()
    {
        $this->db->order_by('name', 'asc');
        return $this->db->get($this->_website_table)->result();
    }

    /**
     * Tìm sản phẩm, đơn hàng để order trên website
     * @param null $website
     * @param int $page
     * @param null $key
     * @return array
     */
    public function get_web_orders(int $page = 0, $website = null, $key = null)
    {
        if ($key != null) {
            if (!empty($key))
                $key = trim($key);
        }

        $this->db->select('t.*, k.first_name as user, o.first_name as customer_name');
        $this->db->from("{$this->_order_table} t");
        $this->db->join("{$this->_order_item_table} d", 'd.order_id = t.id', 'left');
        $this->db->join("{$this->_user_table} o", 'o.id = t.customer_id', 'left');
        $this->db->join("{$this->_user_table} k", 'k.id = t.user_id', 'left');

        if ($website != null)
            $this->db->where('t.website', $website);

        $this->db->where('d.requirement', CustomerOrderItemRequirement::BUY_NOW);
        $this->db->where('d.status', CustomerOrderItemStatus::PENDING);
        $this->db->where('t.status', CustomerOrderStatus::PROCESSING);

        if ($key != null) {
            $this->db->where("(t.name like '%$key%' or t.phone like '%$key%' or t.order_code like '%$key%')");
        }

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }

        $this->db->order_by('t.id', 'desc');
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count_web_orders($website = null, $key = null)
    {
        $this->db->select('t.id');
        $this->db->from("{$this->_order_table} t");
        $this->db->join("{$this->_order_item_table} d", 'd.order_id = t.id', 'left');

        if ($website != null)
            $this->db->where('t.website', $website);

        $this->db->where('d.requirement', CustomerOrderItemRequirement::BUY_NOW);
        $this->db->where('d.status', CustomerOrderItemStatus::PENDING);
        $this->db->where('t.status', CustomerOrderStatus::PROCESSING);

        if ($key != null) {
            $this->db->where("(t.name like '%$key%' or t.phone like '%$key%')");
        }

        $this->db->group_by('t.id');
        return $this->db->count_all_results();
    }

    /**
     * Danh sách sản phẩm tròng đơn hàng theo khách hàng
     * @param int $customer_id
     * @return array
     */
    public function get_items_by_customer(int $customer_id)
    {
        $this->db->select('t.*, o.currency_symbol');
        $this->db->from("$this->_order_item_table t");
        $this->db->join("$this->_order_table o", 't.order_id = o.id', 'left');
        $this->db->where('o.customer_id', $customer_id);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    /**
     * Danh sách đơn hàng theo ID khách hàng
     * @param int $customer_id
     * @param int $page
     * @param string|null $status
     * @param string|null $key
     * @return array
     */
    public function get_orders_by_customer(int $page = 1, int $customer_id, string $status = null, string $key = null)
    {
        $this->db->select('t.*, o.first_name as user, l.first_name as approved_user, m.name as country');
        $this->db->from("{$this->_order_table} t");
        $this->db->join("{$this->_user_table} o", 't.user_id = o.id', 'left');
        $this->db->join("{$this->_user_table} l", 't.approve_user_id = l.id', 'left');
        $this->db->join("{$this->_order_item_table} d", 'd.order_id = t.id', 'left');
        $this->db->join("{$this->_order_label_table} k", 'k.order_id = t.id', 'left');
        $this->db->join("{$this->_country_table} m", 'm.id = t.country_id', 'left');

        if ($status != null) {
            if ($status == CustomerAccountOrderTab::COMPLETED) {
                $this->db->where('t.status', CustomerOrderStatus::COMPLETED);
            } else if ($status == CustomerAccountOrderTab::PROCESSING) {
                $this->db->where('t.status', CustomerOrderStatus::PROCESSING);
            } else if ($status == CustomerAccountOrderTab::UNPAID) {
                $this->db->where('t.status', CustomerOrderStatus::PENDING);
                $this->db->where('t.total_paid', 0);
            }
        }

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(t.name like '%$key%' or t.phone like '%$key%' or t.email like '%$key%')");
            }
        }

        $this->db->where('t.customer_id', $customer_id);
        $this->db->order_by('t.created_time', 'desc');
        $this->db->group_by('t.id');

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }
        return $this->db->get($this->_order_table)->result();
    }

    public function count_orders_by_customer(int $customer_id, string $status = null, string $key = null)
    {
        $this->db->where('customer_id', $customer_id);
        if ($status != null) {
            if ($status == CustomerAccountOrderTab::COMPLETED) {
                $this->db->where('status', CustomerOrderStatus::COMPLETED);
            } else if ($status == CustomerAccountOrderTab::PROCESSING) {
                $this->db->where('status', CustomerOrderStatus::PROCESSING);
            } else if ($status == CustomerAccountOrderTab::UNPAID) {
                $this->db->where('status', CustomerOrderStatus::PENDING);
                $this->db->where('total_paid', 0);
            }
        }

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(name like '%$key%' or phone like '%$key%' or email like '%$key%')");
            }
        }

        return $this->db->count_all_results($this->_order_table);
    }

    public function total_revenue_by_customer(int $customer_id, string $status = null, string $key = null)
    {
        $this->db->select_sum('sub_total');
        $this->db->where('customer_id', $customer_id);
        if ($status != null) {
            if ($status == CustomerAccountOrderTab::COMPLETED) {
                $this->db->where('status', CustomerOrderStatus::COMPLETED);
            } else if ($status == CustomerAccountOrderTab::PROCESSING) {
                $this->db->where('status', CustomerOrderStatus::PROCESSING);
            } else if ($status == CustomerAccountOrderTab::UNPAID) {
                $this->db->where('status', CustomerOrderStatus::PENDING);
                $this->db->where('total_paid', 0);
            }
        }

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(name like '%$key%' or phone like '%$key%' or email like '%$key%')");
            }
        }

        return $this->db->get($this->_order_table)->row();
    }

    /**
     * Đánh dấu đã gửi email báo hàng về
     * @param $item_id
     * @return bool
     */
    public function set_sent_email($item_id)
    {
        $this->db->trans_begin();
        $this->db->where('status', CustomerOrderItemStatus::IN_INVENTORY);
        if (is_array($item_id))
            $this->db->where_in('id', $item_id);
        else
            $this->db->where('id', $item_id);

        $this->db->update($this->_order_item_table, ['sent_email' => 1]);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật cân nặng tổng đơn hàng, tổng tiền VNĐ
     * @param int $order_id
     * @return bool
     */
    public function update_order_total(int $order_id)
    {
        $this->db->trans_begin();
        $this->db->query("update customer_orders l,
                (select sum(ifnull(o.weight,0) * o.quantity) as total_weight, ifnull(t.total, 0) as total, ifnull(t.currency_rate,0) as currency_rate, t.discount_total, 
                ifnull(t.web_shipping_fee,0) as web_shipping_fee, ifnull(t.inter_shipping_fee, 0) as inter_shipping_fee, sum(o.quantity) as quantity, 
                sum(ifnull(o.term_fee, 0) * o.quantity) as term_fee, count(o.id) as total_item
                from customer_orders t
                left join customer_order_items o on t.id = o.order_id
                where t.id = $order_id) m 
                    set l.total_weight = m.total_weight,
                    l.sub_total = (((m.total + m.web_shipping_fee) * m.currency_rate + m.inter_shipping_fee + 
                    m.total_weight * l.world_shipping_fee) + term_fee - l.discount_amount) * (l.fee_percent + 100) / 100,
                    l.quantity = m.quantity, l.total_item = m.total_item
                where l.id = $order_id");
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Kiểm tra đơn hàng hoàn thành
     * @param int $order_id
     * @return bool
     */
    public function check_order_completed(int $order_id)
    {
        $this->db->select('status, id');
        $this->db->where('order_id', $order_id);
        $items = $this->db->get($this->_order_item_table)->result();
        if (count($items) == 0)
            return true;

        $items_shipped = array_filter($items, function ($t) {
            return $t->status == CustomerOrderItemStatus::SHIPPED;
        });

        if (count($items_shipped) < count($items))
            return true;

        $this->db->trans_begin();
        $this->db->where('id', $order_id);
        $this->db->where('sub_total <= total_paid');
        $this->db->update($this->_order_table, ['status' => CustomerOrderStatus::COMPLETED]);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get_null_order_code()
    {
        $this->db->where('order_code is null');
        return $this->db->get($this->_order_table)->result();
    }

    public function get_all_order()
    {
        return $this->db->get($this->_order_table)->result();
    }

    public function get_pending_orders()
    {
        $this->db->select('t.order_code, o.id as item_id, t.id, t.sub_total, t.total_paid, count(o.id) as total_item');
        $this->db->from("$this->_order_table t");
        $this->db->join("$this->_order_item_table o", 't.id = o.order_id', 'left');

        $this->db->where('t.status', CustomerOrderStatus::PROCESSING);
        $this->db->group_by('t.id');

        return $this->db->get()->result();
    }

    public function get_shipped_order(int $order_id)
    {
        $this->db->select('count(o.order_item_id) as total_items, t.order_id');
        $this->db->from('order_shippings t');
        $this->db->join('order_shipping_items o', 't.id = o.order_shipping_id', 'left');
        $this->db->where('t.status', ShippingOrderStatus::COMPLETED);
        $this->db->where('t.order_id', $order_id);
        $this->db->group_by('t.order_id');

        return $this->db->get()->row();
    }
}