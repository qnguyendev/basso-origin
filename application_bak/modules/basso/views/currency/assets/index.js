function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.modal = {
        id: ko.observable(0),
        name: ko.observable().extend({required: {message: LABEL.required}}),
        symbol: ko.observable().extend({required: {message: LABEL.required}}),
        show: function () {
            self.modal.id(0);
            self.modal.name(undefined);
            self.modal.symbol(undefined);
            self.modal.errors.showAllMessages(false);
            MODAL.show('#editor-modal');
        },
        edit: function (item) {
            self.modal.id(item.id());
            self.modal.name(item.name());
            self.modal.symbol(item.symbol());
            MODAL.show('#editor-modal');
        },
        save: function () {
            if (!self.modal.isValid())
                self.modal.errors.showAllMessages();
            else {
                let data = {
                    id: self.modal.id(),
                    name: self.modal.name(),
                    symbol: self.modal.symbol()
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        NOTI.success(res.message);
                        MODAL.hide('#editor-modal');
                        self.init();
                    }
                });
            }
        }
    };

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa', `Ngoại tệ ${item.name()}`, () => {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    self.init();
                }
            });
        });
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model.modal);
ko.applyBindings(model, document.getElementById('main-content'));