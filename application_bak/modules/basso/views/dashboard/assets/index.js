function viewModel() {
    let self = this;
    self.filter = {
        time: ko.observable('month'),
        start: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        end: ko.observable(moment(new Date()).format('DD-MM-YYYY'))
    };

    self.summary = {
        total_orders: ko.observable(),
        revenue: ko.observable(),
        total_customers: ko.observable(),
        new_customers: ko.observable()
    };

    self.chart = null;

    self.search = function () {
        let data = {
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end()
        };

        AJAX.get(window.location, data, true, (res) => {
                if (!res.error) {
                    self.summary.total_orders(res.total_orders);
                    self.summary.revenue(res.revenue);
                    self.summary.total_customers(moneyFormat(res.total_customers));
                    self.summary.new_customers(res.new_customers);

                    if (self.chart != null) {
                        self.chart.destroy();
                        self.chart = null;
                    }

                    //#region Biểu đồ
                    if (self.chart == null) {
                        let chartData = [];
                        let ctx = document.getElementById('chart').getContext("2d");

                        chartData.push({
                            backgroundColor: '#2f80e7',
                            borderColor: '#2f80e7',
                            label: 'Số tiền',
                            data: res.payments,
                            fill: false,
                            type: 'bar'
                        });

                        self.chart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: res.dates,
                                datasets: chartData
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: 'Biểu đồ doanh thu'
                                },
                                tooltips: {
                                    mode: 'index',
                                    intersect: true,
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + tooltipItem.yLabel.toMoney(0);
                                        }
                                    }
                                },
                                hover: {
                                    mode: 'nearest',
                                    intersect: true
                                },
                                responsive: true,
                                maintainAspectRatio: false,
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {display: false},
                                        stacked: true,
                                    }],
                                    yAxes: [{
                                        display: true,
                                        scaleLabel: {display: false},
                                        stacked: true,
                                        ticks: {
                                            // Include a dollar sign in the ticks
                                            callback: function (value, index, values) {
                                                return value.toMoney(0);
                                            }
                                        }
                                    }]
                                }
                            }
                        });
                        //#endregion
                    }
                } else
                    ALERT.error(res.message);
            }
        );
    };
}

let model = new viewModel();
model.search();
ko.applyBindings(model, document.getElementById('main-content'));