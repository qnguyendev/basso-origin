<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-6 form-group col-lg-3 col-xl-2">
            <label>Thời gian</label>
            <select class="form-control" data-bind="value: filter.time">
                <option value="today">Hôm nay</option>
                <option value="week">Trong tuần</option>
                <option value="month">Trong tháng</option>
                <option value="all">Tất cả</option>
                <option value="custom">Tùy chọn</option>
            </select>
        </div>
        <div class="col-md-6 form-group col-lg-3 col-xl-2">
            <label>Từ ngày</label>
            <input type="text" class="form-control"
                   data-bind="datePicker: filter.start, enable: filter.time() == 'custom'"/>
        </div>
        <div class="col-md-6 form-group col-lg-3 col-xl-2">
            <label>đến ngày</label>
            <input type="text" class="form-control"
                   data-bind="datePicker: filter.end, enable: filter.time() == 'custom'"/>
        </div>
        <div class="col-md-6 form-group col-lg-3 col-xl-2">
            <label>&nbsp;</label>
            <button class="btn btn-dark btn-block" data-bind="click: search">
                <i class="fa fa-search"></i>
                THÔNG KÊ
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3 col-md-6">
            <!-- START card-->
            <div class="card flex-row align-items-center align-items-stretch border-0">
                <div class="col-4 d-flex align-items-center bg-primary-dark justify-content-center rounded-left">
                    <em class="icon-pie-chart fa-3x"></em>
                </div>
                <div class="col-8 py-3 rounded-right">
                    <div class="h2 mt-0" data-bind="text: summary.revenue"></div>
                    <div class="text-uppercase">tổng Doanh thu</div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-lg-6 col-md-12">
            <!-- START date widget-->
            <div class="card flex-row align-items-center align-items-stretch border-0">
                <div class="col-4 d-flex align-items-center bg-gray-dark justify-content-center rounded-left">
                    <em class="icon-basket-loaded fa-3x"></em>
                </div>
                <div class="col-8 py-3 rounded-right">
                    <div class="h2 mt-0" data-bind="text: summary.total_orders"></div>
                    <div class="text-uppercase">đơn hàng</div>
                </div>
            </div>
            <!-- END date widget-->
        </div>

        <div class="col-xl-3 col-lg-6 col-md-12">
            <!-- START date widget-->
            <div class="card flex-row align-items-center align-items-stretch border-0">
                <div class="col-4 d-flex align-items-center bg-info-dark justify-content-center rounded-left">
                    <em class="icon-people fa-3x"></em>
                </div>
                <div class="col-8 py-3 rounded-right">
                    <div class="h2 mt-0" data-bind="text: summary.total_customers"></div>
                    <div class="text-uppercase">tổng khách hàng</div>
                </div>
            </div>
            <!-- END date widget-->
        </div>

        <div class="col-xl-3 col-lg-6 col-md-12">
            <!-- START date widget-->
            <div class="card flex-row align-items-center align-items-stretch border-0">
                <div class="col-4 d-flex align-items-center bg-success-dark justify-content-center rounded-left">
                    <em class="icon-user-follow fa-3x"></em>
                </div>
                <div class="col-8 py-3 rounded-right">
                    <div class="h2 mt-0" data-bind="text: summary.new_customers"></div>
                    <div class="text-uppercase">khách mới</div>
                </div>
            </div>
            <!-- END date widget-->
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <canvas id="chart" style="display: block; width: 100%; height: 550px"></canvas>
        </div>
    </div>
@endsection

@section('script')
    <script src="/assets/vendor/chart.js/dist/Chart.js"></script>
    <script src="{{load_js('index')}}"></script>
@endsection