<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                <a href="javascript:" class="float-right" data-bind="click: modal.show">
                    <i class="fa fa-plus"></i>
                    Thêm quốc gia
                </a>
                Quản lý quốc gia
            </div>
        </div>
        <div class="card-wrapper">
            <div class="card-body">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th width="60">STT</th>
                        <th>Tên</th>
                        <th>Ngoại tệ</th>
                        <th>Tỷ giá khách</th>
                        <th>Tỷ giá mua</th>
                        <th>Giá ship</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="text-center" data-bind="foreach: result">
                    <tr>
                        <th data-bind="text: $index() + 1"></th>
                        <td data-bind="text: name"></td>
                        <td data-bind="text: currency_name() + ' - ' + currency_symbol()"></td>
                        <td data-bind="text: parseInt(customer_rate()).toMoney(0)"></td>
                        <td data-bind="text: parseInt(buy_rate()).toMoney(0)"></td>
                        <td data-bind="text: parseInt(shipping_fee()).toMoney(0)"></td>
                        <td>
                            <button data-bind="click: $root.modal.edit" class="btn btn-xs btn-link">
                                <i class="fa fa-edit"></i>
                                Cập nhật
                            </button>
                            <button class="btn btn-xs btn-primary"
                                    data-bind="click: $root.user_group.show">
                                <i class="fa fa-list"></i>
                                Chi tiết
                            </button>
                            <button class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                <i class="fa fa-trash"></i>
                                Xóa
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/editor_modal')
    @include('partial/user_group_modal')
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection