<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="user-group-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Tỷ giá nhóm khách hàng
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered">
                    <thead data-bind="with: user_group.warehouse">
                    <tr>
                        <th class="text-left border-top-0" data-bind="text: name"></th>
                        <th width="160" class="text-right border-top-0"
                            data-bind="text: parseInt(customer_rate()).toMoney(0)"></th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: user_group.list">
                    <tr>
                        <td data-bind="text: name"></td>
                        <td>
                            <div class="input-group">
                                <input type="text" class="text-right form-control input-sm"
                                       data-bind="moneyMask: rate"/>
                                <div class="input-group-append">
                                    <span class="input-group-text text-sm">₫</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: $root.user_group.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>