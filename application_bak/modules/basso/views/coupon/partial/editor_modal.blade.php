<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$action = $this->router->fetch_method();
?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-8 col-xl-6 offset-xl-1">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        {{$action == 'create' ? 'Thêm mã giảm giá' : 'Cập nhật mã giá'}}
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>
                                    Mã giảm giá
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: code"></span>
                                </label>
                                <input type="text" class="form-control text-uppercase" data-bind="value: code"/>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>Kiểu giảm giá</label>
                                <select class="form-control" data-bind="value: type">
                                    <option value="rate">Tỷ giá</option>
                                    <option value="amount">Số tiền</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>
                                    Giá trị
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: amount"></span>
                                </label>
                                <div class="input-group">
                                    <input type="text" class="form-control text-right" data-bind="moneyMask: amount"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">đ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select class="form-control" data-bind="value: actived">
                                    <option value="1">Kích hoạt</option>
                                    <option value="0">Tạm dừng</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>
                                    Từ ngày
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: from"></span>
                                </label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-bind="datePicker: from"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>
                                    Đến ngày
                                    <span class="text-danger">*</span>
                                    <span class="validationMessage" data-bind="validationMessage: to"></span>
                                </label>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-bind="datePicker: to"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <i class="float-left">
                        <span class="text-danger">*</span> thông tin bắt buộc
                    </i>
                    <button class="btn btn-sm btn-success" data-bind="click: save">
                        <i class="fa fa-check"></i>
                        Lưu lại
                    </button>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-lg-4 col-xl-4">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Tỷ giá ngoại tệ
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="text-left">Ngoại tệ</th>
                            <th>Tỷ giá khách</th>
                            <th width="100">Tỷ giá KM</th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: currencies_rates">
                        <tr>
                            <td data-bind="text: currency_name"></td>
                            <td data-bind="text: parseInt(customer_rate()).toMoney(0)" class="text-center"></td>
                            <td>
                                <input type="text" class="form-control input-sm text-right" data-bind="moneyMask: rate"/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        let warehouses = {{json_encode($warehouses)}};
    </script>
    <script src="{{load_js('edit')}}"></script>
@endsection