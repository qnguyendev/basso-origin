function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.filter = {
        shipping_id: ko.observable(0),
        status: ko.observable('all'),
        key: ko.observable()
    };

    self.current_tab = ko.observable();
    self.set_current = function (tab) {
        if (tab != self.current_tab()) {
            self.current_tab(tab);
            self.search(1);
        }
    };

    self.selected_items = ko.observableArray([]);

    self.check_all = function (data, event) {
        if ($(event.currentTarget).is(':checked')) {
            self.result.each((x) => {
                if (!self.selected_items().includes(x.id()))
                    self.selected_items.push(x.id());
            });
        } else
            self.selected_items([]);
    };

    self.search = function (page) {
        self.selected_items([]);
        let data = {
            page: page,
            shipping_id: self.filter.shipping_id(),
            status: self.filter.status(),
            key: self.filter.key(),
            branch: self.current_tab()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.print_invoice = function () {
        if (self.selected_items().length > 0) {
            let id = self.selected_items().join(',');
            window.open(`/basso/invoice?id=${id}`, '_blank');
        }
    };

    self.mark_shipped = function (item) {
        ALERT.confirm('Xác nhận đơn đã được giao', null, function () {
            AJAX.post(window.location, {id: item.id()}, true, (res) => {
                ALERT.success(res.message);
                item.status(res.status);
            });
        });
    };

    self.multi_shipped = function () {
        ALERT.confirm('Xác nhận đơn đã được giao', null, function () {
            AJAX.post(window.location, {id: self.selected_items()}, true, (res) => {
                ALERT.success(res.message);
                self.result.each((x) => {
                    if (self.selected_items().includes(x.id()))
                        x.status(res.status);
                });

                self.selected_items([]);
            });
        });
    };

    self.toggle_row = function (data, event) {
        toggle_row(event.currentTarget);
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));