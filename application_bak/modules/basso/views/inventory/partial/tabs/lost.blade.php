<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="border-top-0">STT</th>
        <th class="border-top-0">Ngày nhập kho</th>
        <th class="border-top-0">Warehouse</th>
        <th class="border-top-0">Tracking VC</th>
        <th class="border-top-0">Tracking website</th>
        <th class="border-top-0">Sản phẩm</th>
        <th class="border-top-0">Cân nặng (kg)</th>
        <th class="border-top-0">Trạng thái</th>
        <th class="border-top-0">Nhân viên</th>
        <th class="border-top-0" width="110">Chi nhánh</th>
        <th class="border-top-0"></th>
    </tr>
    </thead>
    <tbody class="text-center" data-bind="foreach: result">
    <tr>
        <th data-bind="text: $index() + 1"></th>
        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
        <td data-bind="text: warehouse"></td>
        <td data-bind="text: real_tracking"></td>
        <td data-bind="text: web_tracking"></td>
        <td data-bind="text: name"></td>
        <td data-bind="text: total_weight"></td>
        <td>
        <!--ko if: status() === '{{InventoryItemStatus::LOST}}'-->
            <div class="btn-group">
                <button data-bind="css: lost_pending() == 1 ? 'btn btn-xs btn-warning dropdown-toggle' : 'btn btn-xs btn-success dropdown-toggle' "
                        type="button" data-toggle="dropdown"
                        aria-expanded="false">
                    <!--ko if: lost_pending() == 1-->
                    <span>pending</span>
                    <!--/ko-->
                    <!--ko if: lost_pending() == 0-->
                    <span>done</span>
                    <!--/ko-->
                </button>
                <div class="dropdown-menu" role="menu" x-placement="top-start">
                    <!--ko if: lost_pending() == 0-->
                    <a class="dropdown-item" data-bind="click: $root.update_pending_status">pending</a>
                    <!--/ko-->
                    <!--ko if: lost_pending() == 1-->
                    <a class="dropdown-item" data-bind="click: $root.update_pending_status">done</a>
                    <!--/ko-->
                </div>
            </div>
            <!--/ko-->
        </td>
        <td data-bind="text: user"></td>
        <td>
        @foreach(Branch::LIST as $key=>$value)
            <!--ko if: branch() == '{{$key}}'-->
            {{$value}}
            <!--/ko-->
            @endforeach
        </td>
        <td>
            <a class="order-expand text-primary" href="#" data-bind="click: $root.toggle_row">
                <i class="fa fa-eye"></i>
                Chi tiết
            </a>
        </td>
    </tr>
    <tr class="tr-collapse">
        <td colspan="11">
            <div class="item-detail">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div data-bind="class: items().length == 0 ? 'col-12' : 'col-md-12 col-lg-5'">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row" data-bind="visible: items().length == 0">
                                            <div class="col-md-6 col-lg-4">
                                                <table class="table border-vertical bg-transparent table-borderless">
                                                    <tr>
                                                        <th class="text-right">Tracking VC</th>
                                                        <td>
                                                            <input type="text" class="form-control input-sm"
                                                                   data-bind="value: real_tracking"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-right">Tracking website</th>
                                                        <td>
                                                            <input type="text" class="form-control input-sm"
                                                                   data-bind="value: web_tracking"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <table class="table border-vertical bg-transparent table-borderless">
                                                    <tr>
                                                        <th class="text-right">Cân nặng (kg)</th>
                                                        <td>
                                                            <input type="number" class="form-control input-sm"
                                                                   step="0.01"
                                                                   min="0"
                                                                   data-bind="value: total_weight"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-right">Mã đơn hàng</th>
                                                        <td class="text-left">
                                                            <div data-bind="visible: customer_orders().length > 0">
                                                                <!--ko foreach: customer_orders-->
                                                                <a data-bind="text: order_code() != null ? order_code() : order_id(),
                                                        attr: {href: `/basso/customer_order/detail/${order_id()}`}"
                                                                   target="_blank" class="badge badge-secondary"></a>
                                                                <!--/ko-->
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <table class="table border-vertical bg-transparent table-borderless">
                                                    <tr>
                                                        <th class="text-right">Tên kiện (sp)</th>
                                                        <td>
                                                            <input type="text" class="form-control input-sm"
                                                                   data-bind="value: name"/>
                                                        </td>
                                                    </tr>
                                                    <tr data-bind="visible: status() === '{{InventoryItemStatus::LOST}}'">
                                                        <th class="text-right">Trạng thái</th>
                                                        <td>
                                                            <select class="form-control input-sm"
                                                                    data-bind="value: lost_pending">
                                                                <option value="0">cancelled</option>
                                                                <option value="1">pending</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="row text-left" data-bind="visible: items().length > 0">
                                            <div class="col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label>Tracking VC</label>
                                                    <input type="text" class="form-control input-sm"
                                                           data-bind="value: real_tracking"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tracking website</label>
                                                    <input type="text" class="form-control input-sm"
                                                           data-bind="value: web_tracking"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Mã đơn hàng</label>
                                                    <div data-bind="visible: customer_orders().length > 0">
                                                        <!--ko foreach: customer_orders-->
                                                        <a data-bind="text: order_code() != null ? order_code() : order_id(),
                                                attr: {href: `/basso/customer_order/detail/${order_id()}`}"
                                                           target="_blank" class="badge badge-secondary"></a>
                                                        <!--/ko-->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label>Tên kiện (sp)</label>
                                                    <input type="text" class="form-control input-sm"
                                                           data-bind="value: name"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Cân nặng (kg)</label>
                                                    <input type="text" class="form-control input-sm" min="0" step="0.01"
                                                           data-bind="value: total_weight"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Chi nhánh</label>
                                                    <select class="form-control input-sm" data-bind="value: branch">
                                                        <option>-- Chọn --</option>
                                                        @foreach(Branch::LIST as $key=>$value)
                                                            <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-center">
                                            <button class="btn btn-sm btn-primary"
                                                    data-bind="click: $root.update_package">
                                                <i class="fa fa-check"></i>
                                                CẬP NHẬT
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-7" data-bind="visible: items().length > 0">
                                <div class="card">
                                    <div class="card-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th width="80"></th>
                                                <th>Tên</th>
                                                <th>Variant</th>
                                                <th>Cân nặng</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody data-bind="foreach: items" class="text-center">
                                            <tr>
                                                <td class="text-center p-1">
                                                    @include('partial/item_image_upload')
                                                </td>
                                                <td data-bind="text: name" class="text-left"></td>
                                                <td>
                                                    <!--ko foreach: variations-->
                                                    <b data-bind="text: name"></b>: <span
                                                            data-bind="text: value"></span><br/>
                                                    <!--/ko-->
                                                </td>
                                                <td>
                                                    <input type="number" min="0" step="0.01"
                                                           data-bind="value: weight"
                                                           class="input-sm form-control m-0 m-auto"
                                                           style="width: 80px"/>
                                                </td>
                                                <td>
                                                    <button class="btn btn-primary text-uppercase btn-sm"
                                                            data-bind="click: $root.items.update">
                                                        <i class="fa fa-check"></i>
                                                        Cập nhật
                                                    </button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>
