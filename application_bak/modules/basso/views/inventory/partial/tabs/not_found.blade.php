<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="border-top-0">STT</th>
        <th class="border-top-0">Ngày nhập kho</th>
        <th class="border-top-0">Warehouse</th>
        <th class="border-top-0">Tracking VC</th>
        <th class="border-top-0">Tracking website</th>
        <th class="border-top-0">Sản phẩm</th>
        <th class="border-top-0">Cân nặng (kg)</th>
        <th class="border-top-0">Nhân viên</th>
        <th class="border-top-0"></th>
    </tr>
    </thead>
    <tbody class="text-center" data-bind="foreach: result">
    <tr>
        <th data-bind="text: $index() + 1"></th>
        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
        <td data-bind="text: warehouse"></td>
        <td data-bind="text: real_tracking"></td>
        <td data-bind="text: web_tracking"></td>
        <td data-bind="text: name"></td>
        <td data-bind="text: total_weight"></td>
        <td data-bind="text: user"></td>
        <td>
            <a class="order-expand text-primary" href="#" data-bind="click: $root.toggle_row">
                <i class="fa fa-eye"></i>
                Chi tiết
            </a>
        </td>
    </tr>
    <tr class="tr-collapse">
        <td colspan="10">
            <div class="item-detail">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <table class="table border-vertical bg-transparent table-borderless">
                                    <tr>
                                        <th class="text-right">Tracking VC</th>
                                        <td>
                                            <input type="text" class="form-control input-sm"
                                                   data-bind="value: real_tracking"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Tracking website</th>
                                        <td>
                                            <input type="text" class="form-control input-sm"
                                                   data-bind="value: web_tracking"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <table class="table border-vertical bg-transparent table-borderless">
                                    <tr>
                                        <th class="text-right">Cân nặng (kg)</th>
                                        <td>
                                            <input type="number" class="form-control input-sm" step="0.01" min="0"
                                                   data-bind="value: total_weight"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Mã đơn hàng</th>
                                        <td class="text-left">
                                            <div data-bind="visible: customer_orders().length > 0">
                                                <!--ko foreach: customer_orders-->
                                                <a data-bind="text: $data, attr: {href: `/basso/customer_order/detail/${$data}`}"
                                                   target="_blank" class="badge badge-secondary"></a>
                                                <!--/ko-->
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6 col-lg-4">
                                <table class="table border-vertical bg-transparent table-borderless">
                                    <tr>
                                        <th class="text-right">Ngày nhập kho</th>
                                        <td class="text-left"
                                            data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-center">
                            <button class="btn btn-sm btn-primary" data-bind="click: $root.update_package">
                                <i class="fa fa-check"></i>
                                CẬP NHẬT
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    </tbody>
</table>