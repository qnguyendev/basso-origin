<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-xl-8">
            @include('partial/create/customer_info')
            @include('partial/create/items')
        </div>

        <div class="col-sm-12 col-xl-4">
            @include('partial/create/detail')
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/create/add_item_modal')
@endsection
@section('script')
    <script src="{{load_js('create')}}"></script>
@endsection