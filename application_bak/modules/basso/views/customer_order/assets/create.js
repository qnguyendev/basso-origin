function viewModel() {
    let self = this;
    ko.utils.extend(self, new imageUploadModel());
    self.prefix = 'customer_order_create_';
    self.customer_info = {
        id: ko.observable(0),
        name: ko.observable().extend({required: {message: LABEL.required}}),
        phone: ko.observable().extend({required: {message: LABEL.required}}),
        shipping_address: ko.observable().extend({required: {message: LABEL.required}}),
        email: ko.observable(),
        note: ko.observable(),
        order_place_id: ko.observable(),
        city_id: ko.observable(),
        district_id: ko.observable()
    };

    self.find_customer = {
        result: ko.mapping.fromJS([]),
        find: function () {
            let key = null;
            if (self.customer_info.name() != undefined)
                if (self.customer_info.name() != null)
                    if (self.customer_info.name().length > 0)
                        key = self.customer_info.name();

            if (self.customer_info.id() == 0 || self.customer_info.id() == undefined) {
                if (key != null) {
                    AJAX.get(window.location, {
                        search: 'find-customer', key: self.customer_info.name()
                    }, false, (res) => {
                        if (!res.error) {
                            ko.mapping.fromJS(res.data, self.find_customer.result);
                            if (res.data.length > 0)
                                $('.card-result').slideDown(300);
                        }
                    });
                } else {
                    $('.card-result').hide();
                }
            }
        },
        select: function (item) {
            self.customer_info.id(item.id());
            self.customer_info.name(item.name());
            self.customer_info.phone(item.phone());
            self.customer_info.phone(item.phone());
            self.customer_info.city_id(item.city_id());
            self.customer_info.district_id(item.district_id());
            self.customer_info.email(item.email());
            self.customer_info.shipping_address(item.address());
            self.find_customer.close();
        },
        close: function () {
            $('.card-result').slideUp(300, function () {
                ko.mapping.fromJS([], self.find_customer.result);
                self.customer_info.errors.showAllMessages(false);
            });
        },
        clear: function () {
            self.customer_info.id(0);
            self.customer_info.name(undefined);
            self.customer_info.phone(undefined);
            self.customer_info.email(undefined);
            self.customer_info.shipping_address(undefined);
            self.customer_info.errors.showAllMessages(false);
        }
    };

    self.data = {
        item_status: ko.mapping.fromJS([]),
        cities: ko.mapping.fromJS([]),
        districts: ko.mapping.fromJS([]),
        countries: ko.mapping.fromJS([]),
        order_places: ko.mapping.fromJS([]),
        order_terms: ko.mapping.fromJS([]),
        banks: ko.mapping.fromJS([]),
        init: function () {
            AJAX.get(window.location, {init: true}, true, (res) => {
                ko.mapping.fromJS(res.cities, self.data.cities);
                ko.mapping.fromJS(res.countries, self.data.countries);
                ko.mapping.fromJS(res.order_places, self.data.order_places);
                ko.mapping.fromJS(res.order_terms, self.data.order_terms);
                ko.mapping.fromJS(res.item_status, self.data.item_status);
                ko.mapping.fromJS(res.banks, self.data.banks);

                if (res.countries.length > 0)
                    self.order.country_id(res.countries[0].id);

                if (self.customer_info.city_id() !== undefined) {
                    $('select[name=city]').val(self.customer_info.city_id()).trigger('change');
                }

                //  Đặt phương thức thanh toán mặc định
                if ($('select[name=payment_type] option').length > 0) {
                    let val = $('select[name=payment_type] option').first().val();
                    self.payment.type(val);
                }

                //  Đặt trạng thái đơn hàng mặc định
                if ($('select[name=order_status] option').length > 0) {
                    let val = $('select[name=order_status] option').first().val();
                    self.order.status(val);
                }

                self.order.errors.showAllMessages(false);
            });
        },
        //  Lấy danh sách quận huyện khi thay đổi tỉnh thành
        change_city: function () {
            AJAX.get(window.location, {city_id: self.customer_info.city_id()}, true, (res) => {
                ko.mapping.fromJS(res.data, self.data.districts);

                if (self.customer_info.district_id() !== undefined) {
                    $('select[name=district]').val(self.customer_info.district_id()).trigger('change');
                }
            });
        },
        //  Thay đổi warehouse
        change_country: function () {
            for (let i = 0; i < self.data.countries().length; i++) {
                let item = self.data.countries()[i];
                if (item.id() === self.order.country_id()) {
                    self.order.customer_rate(moneyFormat(item.customer_rate()));
                    self.order.currency_symbol(item.currency_symbol());
                    break;
                }
            }

            window.setTimeout(self.calc_total, 100);
        }
    };

    self.order = {
        country_id: ko.observable(),
        shipping_fee: ko.observable(0),
        total: ko.observable(0),
        sub_total: ko.observable(0),
        created_time: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        status: ko.observable(),
        currency_symbol: ko.observable(),
        customer_rate: ko.observable(),
        brand: ko.observable().extend({required: {message: LABEL.required}}),
        branch: ko.observable().extend({required: {message: LABEL.required}}),
        website: ko.observable()
    };

    self.payment = {
        created_time: ko.observable(moment(new Date()).format('DD-MM-YYYY HH:mm')),
        amount: ko.observable(),
        note: ko.observable(),
        type: ko.observable(),
        bank_id: ko.observable(),
        check_valid_amount: function () {
            if (toNumber(self.payment.amount()) > self.order.sub_total()) {
                self.payment.amount(self.order.sub_total());
                NOTI.danger(`Số tiền không được lớn hơn ${parseInt(self.order.sub_total()).toMoney(0)}`);
            }
        }
    };

    self.items = {
        list: ko.mapping.fromJS([]),
        change_term: function (item, term_id) {
            let fee = 0, total = self.item_modal.price();
            let valid_step = 0;

            for (let i = 0; i < self.data.order_terms().length; i++) {
                let x = self.data.order_terms()[i];
                if (x.id() == term_id) {

                    for (let j = 0; j < x.steps().length; j++) {
                        let t = x.steps()[j];
                        if (total >= parseInt(t.step()) && parseInt(t.step()) >= valid_step) {
                            valid_step = parseInt(t.step());
                            fee = parseFloat(t.amount());
                            if (t.type() == 'percent')
                                fee = parseFloat(t.amount()) * total / 100 * toNumber(self.order.customer_rate());
                        }
                    }

                    break;
                }
            }

            item.term_id(term_id);
            item.term_fee(fee);
            self.calc_total();
        },
        //  Thay đổi link sản phẩm
        change_link: function (item) {
            if (!self.check_valid_url(item.link())) {
                NOTI.danger('Link sản phẩm không hợp lệ');
                return;
            }

            let website = self.get_website(item.link());
            if (!self.item_modal.check_exist_website(website)) {
                let allow_website = self.items.list()[0].website();
                ALERT.error(`Bạn chỉ được thêm sản phẩm từ ${allow_website}`);
                return;
            }

            item.website(website);
            self.order.website(item.website());
        },
        remove: function (item) {
            ALERT.confirm(`Vui lòng xác nhận`, `Xóa sản phẩm ${item.name()}`, function () {
                self.items.list.remove(item);
                self.calc_total();
                NOTI.success(`Đã xóa sản phẩm ${item.name()}`);

                if (self.items.list().length == 0)
                    self.order.website(undefined);
            });
        }, //  Up ảnh đại diện sản phẩm
        upload_image: function (item) {
            self.image_uploader.show(true);
            self.image_uploader.save = function () {
                item.image_id(self.image_uploader.selected_images[0].id);
                item.image_path(self.image_uploader.selected_images[0].path);
                self.image_uploader.close();

                let data = {action: 'upload-image', item_id: item.id(), image_id: item.image_id()};
                AJAX.post(window.location, data, false, (res) => {
                    if (!res.error)
                        NOTI.success(res.message);
                });
            };
        },
        //  Xóa ảnh sản phẩm
        remove_image: function (item) {
            let data = {action: 'remove-image', item_id: item.id(), image_id: item.image_id()};
            AJAX.post(window.location, data, false, (res) => {
                if (!res.error) {
                    item.image_path(undefined);
                    NOTI.success(res.message);
                }
            });
        }
    };

    //  Modal thêm sản phẩm
    self.item_modal = {
        name: ko.observable().extend({required: {message: LABEL.required}}),
        link: ko.observable().extend({required: {message: LABEL.required}}),
        image_path: ko.observable(undefined),
        image_id: ko.observable(undefined),
        quantity: ko.observable(1),
        price: ko.observable(),
        term_id: ko.observable().extend({required: {message: LABEL.required}}),
        term_fee: ko.observable(0),
        note: ko.observable(),
        variations: ko.observableArray([]),
        variation_editable: ko.observable(true),
        //  Thay đổi danh mục phụ thu
        change_term: function () {
            let fee = 0, total = self.item_modal.price();
            let valid_step = 0;

            for (let i = 0; i < self.data.order_terms().length; i++) {
                let x = self.data.order_terms()[i];
                if (x.id() == self.item_modal.term_id()) {

                    for (let j = 0; j < x.steps().length; j++) {
                        let t = x.steps()[j];
                        if (total >= parseInt(t.step()) && parseInt(t.step()) >= valid_step) {
                            valid_step = parseInt(t.step());
                            fee = parseFloat(t.amount());
                            if (t.type() == 'percent') {
                                let amount_fee = parseFloat(t.amount()) * total / 100;
                                if (amount_fee < x.min_fee())
                                    amount_fee = x.min_fee();
                                fee = amount_fee * toNumber(self.order.customer_rate());
                            }
                        }
                    }

                    break;
                }
            }

            self.item_modal.term_fee(fee * parseInt(self.item_modal.quantity()));
        },
        //  Hiện modal
        show: function () {
            self.item_modal.name(undefined);
            self.item_modal.link(undefined);
            self.item_modal.price(0);
            self.item_modal.quantity(1);
            self.item_modal.term_id(undefined);
            self.item_modal.term_fee(0);
            self.item_modal.note(undefined);
            self.item_modal.variations([]);
            self.item_modal.variation_editable(true);
            self.item_modal.image_id(undefined);
            self.item_modal.image_path(undefined);

            self.item_modal.change_term();
            self.item_modal.errors.showAllMessages(false);
            MODAL.show('#add-item-modal');
        },
        add: function () {
            if (!self.item_modal.isValid())
                self.item_modal.errors.showAllMessages();
            else {
                if (!self.check_valid_url(self.item_modal.link())) {
                    ALERT.error('Link web không hợp lệ');
                    return;
                }

                let item = {
                    name: ko.observable(self.item_modal.name()),
                    quantity: ko.observable(),
                    link: ko.observable(self.item_modal.link()),
                    website: ko.observable(self.get_website(self.item_modal.link())),
                    variations: ko.observableArray([]),
                    note: ko.observable(self.item_modal.note()),
                    tracking_code: ko.observable(),
                    term_id: ko.observable(self.item_modal.term_id()),
                    term_fee: ko.observable(self.item_modal.term_fee()),
                    price: ko.observable(self.item_modal.price()),
                    status: ko.observable(),
                    requirement: ko.observable(),
                    image_path: ko.observable(self.item_modal.image_path()),
                    image_id: ko.observable(self.item_modal.image_id()),
                    order_number: ko.observable()
                };

                self.item_modal.variations.each((x) => {
                    if (x.name() != undefined && x.value() != undefined) {
                        if (x.name() != null && x.value() != null) {
                            if (x.name() != '' && x.value() != '') {
                                item.variations.push({
                                    name: ko.observable(x.name()),
                                    value: ko.observable(x.value())
                                });
                            }
                        }
                    }
                });

                if (self.item_modal.quantity() !== undefined) {
                    if (self.item_modal.quantity() !== null) {
                        item.quantity(self.item_modal.quantity());
                    }
                }

                if (!self.item_modal.check_exist_website(item.website())) {
                    let allow_website = self.items.list()[0].website();
                    ALERT.error(`Bạn chỉ được thêm sản phẩm từ ${allow_website}`);
                    return;
                }

                if (self.data.item_status().length > 0)
                    item.status(self.data.item_status()[0].id());

                self.order.website(item.website());
                self.items.list.push(item);
                self.calc_total();
                MODAL.hide('#add-item-modal');
                NOTI.success(`Đã thêm sản phẩm ${item.name()}`);
            }
        },
        //  Kiểm tra web sản phẩm đã có trong danh sách
        check_exist_website: function (web) {
            if (self.items.list().length === 0)
                return true;

            let existing = false;
            for (let i = 0; i < self.items.list().length; i++) {
                let item = self.items.list()[i];
                if (item.website() === web) {
                    existing = true;
                    break;
                }
            }

            return existing;
        },
        add_variation: function () {
            if (self.item_modal.variation_editable()) {
                self.item_modal.variations.push({
                    name: ko.observable(),
                    value: ko.observable(),
                    sources: ko.observableArray([])
                });
            }
        },
        delete_variation: function (t) {
            if (self.item_modal.variation_editable())
                self.item_modal.variations.remove(t);
        },
        crawl: function () {
            //#region Verify url
            if (self.item_modal.link() == undefined) {
                NOTI.danger('Chưa nhập link sản phẩm');
                return;
            }

            if (self.item_modal.link() == null) {
                NOTI.danger('Chưa nhập link sản phẩm');
                return;
            }

            if (self.item_modal.link() == '') {
                NOTI.danger('Chưa nhập link sản phẩm');
                return;
            }

            if (!self.check_valid_url(self.item_modal.link())) {
                ALERT.error('Link web không hợp lệ');
                return;
            }
            //#endregion

            AJAX.post('/basso/customer_order/crawl', {url: self.item_modal.link()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    self.item_modal.variation_editable(false);
                    self.item_modal.name(res.data.title);
                    self.item_modal.price(res.data.price);
                    self.item_modal.variations([]);
                    self.item_modal.image_path(res.data.image_path);
                    self.item_modal.image_id(res.data.image_id);

                    for (let i = 0; i < res.data.variations.length; i++) {
                        self.item_modal.variations.push({
                            name: ko.observable(res.data.variations[i].name),
                            value: ko.observable(res.data.variations[i].value),
                            sources: ko.observableArray(res.data.variations[i].sources)
                        });
                    }
                }
            });
        }
    };

    //  Tính tổng tiền đơn hàng
    self.calc_total = function () {
        let total = 0.0;
        let sub_total = 0.0;

        self.items.list.each((item) => {
            if (item.quantity() !== undefined) {
                if (item.quantity() !== null) {
                    //  Kiểm tra nếu số lượng sản phẩm không phải là số
                    if (isNaN(item.quantity())) {
                        item.quantity(toNumber(item.quantity().toString()));
                        item.quantity(toNumber(item.quantity().toString()));
                    }

                    total += item.quantity() * item.price();
                    sub_total += parseFloat(item.term_fee()) * item.quantity();
                }
            }
        });

        self.order.total(parseFloat(total.toFixed(2)));
        self.order.sub_total(sub_total + (parseFloat(self.order.shipping_fee()) + total) * parseFloat(toNumber(self.order.customer_rate())));
    };

    self.toggle_row = function (data, event) {
        toggle_row(event.currentTarget);
    };

    //  Lấy tên website từ link sản phẩm
    self.get_website = function (str) {
        let a = document.createElement('a');
        a.href = str;
        return a.hostname.replace('www.', '');
    };

    //  Kiểm tra url có hợp lệ hay không
    self.check_valid_url = function (str) {
        let regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if (regexp.test(str))
            return true;
        return false;
    };

    self.create_order = function (order_status) {
        if (!self.customer_info.isValid())
            self.customer_info.errors.showAllMessages();
        else if (!self.order.isValid())
            self.order.errors.showAllMessages();
        else {
            if (self.data.countries().length === 0) {
                NOTI.danger('Bạn chưa chọn quốc gia');
                return;
            }

            if (self.items.list().length === 0) {
                NOTI.danger('Bạn chưa chọn sản phẩm');
                return;
            }

            let allow_website = self.items.list()[0].website();
            let valid_items = self.items.list.filter((x) => {
                return x.website() == allow_website;
            });

            if (valid_items.length < self.items.list().length) {
                ALERT.error(`Bạn chỉ được thêm sản phẩm từ ${allow_website}`);
                return;
            }

            ALERT.confirm('Xác nhận tạo đơn hàng', null, function () {
                self.order.customer_rate(toNumber(self.order.customer_rate()));
                self.order.status(order_status);

                let items = [];
                self.items.list.each((x) => {
                    items.push({
                        name: x.name(),
                        quantity: x.quantity(),
                        link: x.link(),
                        website: x.website(),
                        variations: ko.toJSON(x.variations()),
                        note: x.note(),
                        tracking_code: x.tracking_code(),
                        term_id: x.term_id(),
                        term_fee: x.term_fee(),
                        price: x.price(),
                        status: x.status(),
                        requirement: x.requirement(),
                        image_path: x.image_path(),
                        image_id: x.image_id()
                    });
                });

                let data = {
                    customer: self.customer_info,
                    order: self.order,
                    payment: self.payment,
                    items: items
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error) {
                        ALERT.error(res.message);
                    } else {
                        if (res.redirect_url) {
                            window.location = res.redirect_url;
                        }
                    }
                });
            });
        }
    };

    self.to_number = function (str) {
        return toNumber(str);
    };

    self.money_format = function (num) {
        return moneyFormat(num);
    }
}

let model = new viewModel();
ko.validatedObservable(model.customer_info);
ko.validatedObservable(model.item_modal);
ko.validatedObservable(model.order);
model.data.init();
model.customer_info.name.subscribe(model.find_customer.find);
ko.applyBindings(model, document.getElementById('main-content'));