<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card card-default">
    <div class="card-header no-border">
        <div class="card-title">
            <a href="javascript:" class="float-right"
               data-bind="click: shipping_modal.show, visible: order.info.status() != '{{CustomerOrderStatus::CANCELLED}}'
                                        && order.info.status() != '{{CustomerOrderStatus::COMPLETED}}'">
                <i class="fa fa-plus"></i>
                Thêm ship
            </a>
            Vận chuyển
        </div>
    </div>
    <div class="card-body">
        <table class="table table-striped" data-bind="visible: shipping.histories().length > 0">
            <thead>
            <tr>
                <th></th>
                <th class="text-left">Mã vận đơn</th>
                <th>Ngày tạo</th>
                <th>Đơn vị ship</th>
                <th>Phí ship</th>
                <th>Tiền thu COD</th>
                <th>Trạng thái</th>
                <th></th>
            </tr>
            </thead>
            <tbody data-bind="foreach: shipping.histories" class="text-center">
            <tr>
                <th class="text-left">
                    <a href="javascript:" class="order-expand" data-bind="click: $root.toggle_row">
                        <i class="fa fa-eye"></i>
                    </a>
                </th>
                <td data-bind="text: code"></td>
                <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                <td data-bind="text: shipping"></td>
                <td data-bind="text: parseInt(real_shipping_fee()).toMoney(0)"></td>
                <td data-bind="text: parseInt(cod_amount()).toMoney(0)"></td>
                <td>
                    @foreach(ShippingOrderStatus::LIST as $key=>$value)
                        <span data-bind="visible: status() == '{{$key}}'"
                              class="badge badge-{{$value['class']}}">{{$value['name']}}</span>
                    @endforeach
                </td>
                <td>
                    <a class="btn btn-xs btn-info" data-bind="attr: {href: `/basso/invoice?id=${id()}`}"
                       target="_blank">
                        <i class="fa fa-print"></i>
                        In phiếu
                    </a>
                </td>
            </tr>
            <tr class="tr-collapse">
                <td colspan="8">
                    <div class="item-detail pl-0">
                        <div class="row">
                            <div class="col-md-12 col-lg-5">
                                <table class="table border-vertical">
                                    <tbody>
                                    <tr>
                                        <th class="text-right" width="120">Người nhận</th>
                                        <td class="text-left" data-bind="text: name"></td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Điện thoại</th>
                                        <td class="text-left" data-bind="text: phone"></td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Địa chỉ</th>
                                        <td class="text-left">
                                            <span data-bind="text: address"></span>,
                                            <span data-bind="text: district"></span>,
                                            <span data-bind="text: city"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Ghi chú</th>
                                        <td class="text-left" data-bind="text: note"></td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Ngày lấy hàng</th>
                                        <td class="text-left" data-bind="text: shipped_time() != null
                                            ? moment.unix(shipped_time()).format('DD/MM/YYYY') : ''"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-lg-7">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Sản phẩm</th>
                                        <th>SL</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody class="text-center" data-bind="foreach: items">
                                    <tr>
                                        <td class="text-left" data-bind="text: name"></td>
                                        <td data-bind="text: quantity"></td>
                                        <td data-bind="text: parseInt(total()).toMoney(0)"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="pt-2 pb-1 text-primary text-center" data-bind="visible: shipping.histories().length === 0">
            <H4>KHÔNG CÓ ĐƠN VẬN CHUYỂN</H4>
        </div>
    </div>
</div>