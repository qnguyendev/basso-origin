<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="send-email-modal" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    Gửi email báo hàng về
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered mb-3">
                    <thead>
                    <tr>
                        <th width="40">
                            <div class="checkbox c-checkbox w-100 mt-2">
                                <label>
                                    <input type="checkbox"
                                           data-bind="checked: send_email_modal.selected_items().length === send_email_modal.items().length
                                           && send_email_modal.selected_items().length > 0,
                                           event: {change: send_email_modal.check_all}"/>
                                    <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </th>
                        <th>Sản phẩm</th>
                        <th>Variants</th>
                    </tr>
                    </thead>
                    <tbody class="text-center" data-bind="foreach: send_email_modal.items">
                    <tr data-bind="visible: status() === '{{CustomerOrderItemStatus::IN_INVENTORY}}'">
                        <td>
                            <div class="checkbox c-checkbox w-100 mt-2">
                                <label>
                                    <input type="checkbox"
                                           data-bind="checked: $root.send_email_modal.selected_items, value: id"/>
                                    <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </td>
                        <td data-bind="text: name"></td>
                        <td class="text-left">
                            <!--ko foreach: variations-->
                            <b data-bind="text: name"></b>: <span data-bind="text: value"></span><br/>
                            <!--/ko-->
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <label>
                        Tiêu đề
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: send_email_modal.email_subject"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: send_email_modal.email_subject"/>
                </div>
                <div class="form-group">
                    <label>
                        Nội dung
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: send_email_modal.email_content"></span>
                    </label>
                    <textarea class="form-control" cols="15" rows="7" data-bind="value: send_email_modal.email_content"></textarea>
                </div>
                <div class="form-group">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-success" data-bind="click: send_email_modal.send">
                    <i class="icon-cursor"></i>
                    Gửi email
                </button>
            </div>
        </div>
    </div>
</div>