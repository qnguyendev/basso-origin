<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="payment-edit-modal" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    Thay đổi số tiền tham chiếu
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
               <div class="form-group">
                   <label>Nhập số tiền</label>
                   <div class="input-group">
                       <input type="text" class="form-control" data-bind="moneyMask: payment.edit_modal.amount">
                       <div class="input-group-append">
                           <span class="input-group-text">đ</span>
                       </div>
                   </div>
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-success" data-bind="click: payment.edit_modal.save">
                    <i class="fa fa-check"></i>
                    Cập nhật
                </button>
            </div>
        </div>
    </div>
</div>