<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card card-default">
    <div class="card-header">
        <div class="card-title">
            <a href="javascript:" class="float-right"
               data-bind="click: customer_info.edit, visible: $root.order.info.status() != '{{CustomerOrderStatus::CANCELLED}}'
                                        && $root.order.info.status() != '{{CustomerOrderStatus::COMPLETED}}'">
                <i class="icon-pencil"></i>
                Thay đổi
            </a>
            Đơn hàng #<span data-bind="text: order.info.code"></span>
            @foreach(CustomerOrderStatus::LIST as $key=>$value)
                <span class="badge badge-{{$value['class']}}"
                      data-bind="visible: order.info.status() == '{{$key}}'">{{$value['name']}}</span>
            @endforeach
        </div>
    </div>
    <div class="card-wrapper">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <table class="table border-vertical">
                        <tbody>
                        <tr>
                            <th class="text-right">Khách hàng</th>
                            <td data-bind="text: customer_info.name"></td>
                        </tr>
                        <tr>
                            <th class="text-right">Điện thoại</th>
                            <td data-bind="text: customer_info.phone"></td>
                        </tr>
                        <tr>
                            <th class="text-right">Email</th>
                            <td data-bind="text: customer_info.email"></td>
                        </tr>
                        <tr>
                            <th class="text-right">Thời gian</th>
                            <td data-bind="text: moment.unix(order.info.created_time()).format('DD/MM/YYYY')"></td>
                        </tr>
                        <tr>
                            <th class="text-right">Nơi chốt đơn</th>
                            <td data-bind="text: order.info.order_place"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-12 col-md-6">
                    <table class="table border-vertical">
                        <tbody>
                        <tr>
                            <th class="text-right">Đ/c giao hàng</th>
                            <td data-bind="text: customer_info.shipping_address"></td>
                        </tr>
                        <tr>
                            <th class="text-right">Tỉnh/thành</th>
                            <td data-bind="text: customer_info.city"></td>
                        </tr>
                        <tr>
                            <th class="text-right">Brand</th>
                            <td>
                                @foreach(BassoBrand::LIST as $key=>$value)
                                    <span data-bind="visible: order.info.brand() === '{{$key}}'">{{$value}}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">NV duyệt đơn</th>
                            <td>{{$order->approve_user}}</td>
                        </tr>
                        <tr>
                            <th class="text-right">Ghi chú</th>
                            <td>
                                <div class="input-group">
                                    <input class="form-control input-sm" data-bind="value: customer_info.note"/>
                                    <div class="input-group-append">
                                        <button class="btn btn-xs" data-bind="click: $root.order.save_note">Lưu lại</button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>