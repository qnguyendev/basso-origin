<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $current_user, $user_roles;
?>
<!-- Tab panes-->
<div class="tab-content p-0">
    <div class="tab-pane active" id="home" role="tabpanel">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="border-top-0 border-left-0" width="105">Ngày tạo đơn</th>
                    <th class="border-top-0" width="90">Mã ĐH</th>
                    <th class="border-top-0">Khách hàng</th>
                    <th class="border-top-0">Trạng thái</th>
                    <th class="border-top-0">Website</th>
                    <th class="border-top-0" width="90">Tổng tiền</th>
                    <th class="border-top-0" width="110">Tổng tiền (vnđ)</th>
                    <th class="border-top-0" width="105">Khách đã trả</th>
                    <th class="border-top-0" width="105">Còn lại</th>
                    <th class="border-top-0">Ghi chú</th>
                    <th class="border-top-0" width="120">NV tạo đơn</th>
                    <th class="border-top-0">NV duyệt đơn</th>
                </tr>
                </thead>
                <tbody data-bind="foreach: result" class="text-center">
                <tr style="cursor: pointer">
                    <td class="border-left-0"
                        data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY'),
                    click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <th>
                        <a href="#" class="order-expand"
                           data-bind="text: order_code() == null ? id() : order_code(), click: $root.list_items"></a>
                    </th>
                    <td data-bind="text: customer_name() == null ? name()  : customer_name() , click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <td data-bind="click: function(){window.location = `/basso/customer_order/detail/${id()}`}">
                        @foreach(CustomerOrderStatus::LIST as $key=>$value)
                            <span class="badge badge-{{$value['class']}}"
                                  data-bind="visible: status() == '{{$key}}'">{{$value['name']}}</span>
                        @endforeach
                    </td>
                    <td data-bind="text: website, click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <td data-bind="text: `${currency_symbol()} ${parseFloat(total()).toFixed(2)}`,
                        click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <td data-bind="text: parseInt(sub_total()).toMoney(0), click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <td data-bind="text: parseInt(total_paid()).toMoney(0), click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <td data-bind="text: parseInt((sub_total() - total_paid()) <= 5 ? 0  : (sub_total() - total_paid())).toMoney(0), click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <td style="max-width: 300px" data-bind="text: note, click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <td data-bind="text: user() !== null ? user() : 'Website', click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                    <td data-bind="text: approved_user, click: function(){window.location = `/basso/customer_order/detail/${id()}`}"></td>
                </tr>
                <tr class="tr-collapse">
                    <td colspan="12">
                        <div class="item-detail">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th colspan="2">Sản phẩm</th>
                                    <th>Variant</th>
                                    <th>SL</th>
                                    <th>Giá</th>
                                    <th>Tình trạng</th>
                                    <th>Giá vnđ</th>
                                    <th>Ghi chú</th>
                                </tr>
                                </thead>
                                <tbody class="text-center" data-bind="foreach: $root.order_items">
                                <tr>
                                    <th data-bind="text: $index() + 1"></th>
                                    <td width="80">
                                        <img data-bind="attr: {src: `/timthumb.php?src=${image_path()}&w=200&h=200`}"
                                             class="img-fluid ie-fix-flex"/>
                                    </td>
                                    <td class="text-left">
                                        <a href="#" data-bind="attr: {href: link}, text: name" target="_blank">
                                        </a>
                                    </td>
                                    <td class="text-left">
                                        <!--ko foreach: variations-->
                                        <b data-bind="text: name"></b>: <span data-bind="text: value"></span><br/>
                                        <!--/ko-->
                                    </td>
                                    <td data-bind="text: quantity"></td>
                                    <td>
                                        <span data-bind="text: $parent.currency_symbol"></span>
                                        <span data-bind="text: price"></span>
                                    </td>
                                    <td>
                                        @foreach(CustomerOrderItemStatus::LIST as $key=>$value)
                                            <span data-bind="visible: status() == '{{$key}}'">{{$value['name']}}</span>
                                        @endforeach
                                    </td>
                                    <td data-bind="text: parseInt(price() * quantity() * $parent.currency_rate()).toMoney(0)"></td>
                                    <td data-bind="text: note"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="pt-4 pb-3 text-primary text-center" data-bind="visible: result().length === 0">
            <H4>KHÔNG TÌM THẤY ĐƠN HÀNG</H4>
        </div>
    </div>
</div>
@include('pagination')