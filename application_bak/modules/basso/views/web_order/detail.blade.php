<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default" data-bind="with: order">
        <div class="card-header">
            <div class="card-title">
                Đơn hàng <span data-bind="text: order_number"></span>
            </div>
        </div>
        <form autocomplete="off" class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <table class="table border-vertical">
                        <tbody>
                        <tr>
                            <th class="text-right">Chi nhánh</th>
                            <td>
                            @foreach(Branch::LIST as $key=>$value)
                                <!--ko if: branch == '{{$key}}'-->
                                {{$value}}
                                <!--/ko-->
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Thời gian</th>
                            <td data-bind="text: moment.unix(created_time).format('DD/MM/YYYY')"></td>
                        </tr>
                        <tr>
                            <th class="text-right">Order Number</th>
                            <td>
                                <input type="text" class="form-control input-sm" data-bind="value: $root.order_info.order_number"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Người mua</th>
                            <td>
                                <select class="form-control input-sm" style="max-width: 150px" data-bind="value: buyer_id, options: $root.data.buyers,
                                    optionsText: 'name', optionsValue: 'id'"></select>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Quốc gia</th>
                            <td>
                                <select class="form-control input-sm" style="max-width: 150px" data-bind="value: country_id, options: $root.data.countries,
                                    optionsText: 'name', optionsValue: 'id', event: {change: $root.data.change_country}"></select>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Warehouse</th>
                            <td>
                                <select class="form-control input-sm" style="max-width: 150px" data-bind="value: warehouse_id, options: $root.data.warehouses,
                                    optionsText: 'name', optionsValue: 'id'"></select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <table class="table border-vertical">
                        <tr>
                            <th class="text-right">SL sản phẩm</th>
                            <td data-bind="text: $root.items().length"></td>
                        </tr>
                        <tr>
                            <th class="text-right">Trạng thái</th>
                            <td>
                                @foreach(WebOrderStatus::LIST as $key=>$value)
                                    <span data-bind="visible: status == '{{$key}}'">{{$value}}</span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Tỷ giá mua</th>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text text-sm">₫</span>
                                    </div>
                                    <input type="text"
                                           data-bind="moneyMask: $root.order_info.buy_rate, event: {change: $root.data.calc}"
                                           class="form-control input-sm text-right" style="max-width: 100px"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Tổng tiền mua</th>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span data-bind="text: $root.order_info.currency_symbol"
                                              class="input-group-text text-sm"></span>
                                    </div>
                                    <input type="number" step="0.01" min="0"
                                           data-bind="value: $root.order_info.total, event: {change: $root.data.calc}"
                                           class="form-control input-sm text-right" style="max-width: 100px"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Phí ship web</th>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span data-bind="text: $root.order_info.currency_symbol"
                                              class="input-group-text text-sm"></span>
                                    </div>
                                    <input type="number" step="0.01" min="0"
                                           data-bind="value: $root.order_info.ship_fee, event: {change: $root.data.calc}"
                                           class="form-control input-sm text-right" style="max-width: 100px"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Tổng tiền VNĐ</th>
                            <td data-bind="text: parseInt($root.order_info.sub_total()).toMoney(0)"></td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <table class="table border-vertical">
                            <tbody>
                            <tr>
                                <th class="text-right">Trạng thái</th>
                                <td>
                                    <select class="form-control input-sm" data-bind="value: status">
                                        @foreach(WebOrderStatus::LIST as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr data-bind="visible: $root.items.filter((x)=>{ return x.tracking_code() == null; }).length > 0">
                                <th class="text-right">Mã tracking</th>
                                <td>
                                    <input type="text" class="form-control input-sm" autocomplete="off"
                                           data-bind="value: $root.default_tracking"/>
                                </td>
                            </tr>
                            <tr data-bind="visible: $root.items.filter((x)=>{ return x.delivered_date() == null; }).length > 0">
                                <th class="text-right">Ngày về warehouse</th>
                                <td><input type="text" class="form-control input-sm" autocomplete="off"
                                           data-bind="datePicker: $root.default_delivered_date"/></td>
                            </tr>
                            <tr data-bind="visible: note != null">
                                <th class="text-right">Ghi chú</th>
                                <td data-bind="text: note"></td>
                            </tr>
                            <tr>
                                <th class="border-right-0"></th>
                                <td>
                                    <button class="btn btn-success text-uppercase btn-sm"
                                            data-bind="click: $root.update_all">
                                        <i class="fa fa-check"></i>
                                        Cập nhật
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                Sản phẩm
            </div>
        </div>
        <form class="card-body">
            <div class="table-responsive-lg">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Mã ĐH</th>
                        <th>Sản phẩm</th>
                        <th>Variant</th>
                        <th>SL</th>
                        <th>Giá mua</th>
                        <th width="200">Mã tracking</th>
                        <th width="180">Ngày về warehouse</th>
                        <th width="80"></th>
                    </tr>
                    </thead>
                    <tbody class="text-center" data-bind="foreach: items">
                    <tr>
                        <td>
                            <a target="_blank"
                               data-bind="text: order_code() == null ? customer_order_id() : order_code(),
                               attr: {href: `/basso/customer_order/detail/${customer_order_id()}`}"></a>
                        </td>
                        <td data-bind="text: name"></td>
                        <td class="text-left">
                            <!--ko foreach: variations-->
                            <b data-bind="text: name"></b>: <span data-bind="text: value"></span><br/>
                            <!--/ko-->
                        </td>
                        <td data-bind="text: quantity"></td>
                        <td data-bind="text: $root.order_info.currency_symbol() + ' ' + buy_price()"></td>
                        <td>
                            <input type="text" class="form-control input-sm" autocomplete="off"
                                   data-bind="value: tracking_code"/>
                        </td>
                        <td>
                            <input type="text" class="form-control input-sm" autocomplete="off"
                                   data-bind="datePicker: delivered_date"/>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-xs text-uppercase"
                                    data-bind="click: $root.update_item">
                                <i class="fa fa-check"></i>
                                Cập nhật
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script src="{{load_js('detail')}}"></script>
@endsection