<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-xl-6 offset-xl-3 col-lg-8 offset-lg-2">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Phương thức thanh toán
                        <a href="#" class="float-right" data-bind="click: modal.show">
                            <i class="fa fa-plus"></i>
                            Thêm PTTT
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên PTTT</th>
                            <th>Tỷ giá</th>
                            <th>Ghi chú</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: parseInt(currency_rate()).toMoney(0)"></td>
                            <td data-bind="text: note"></td>
                            <td>
                                <a href="#" data-bind="click: $root.modal.edit" class="text-primary font-weight-bold">
                                    <i class="icon-pencil"></i>
                                    Sửa
                                </a>
                                <a href="#" data-bind="click: $root.delete"
                                   class="text-danger font-weight-bold ml-1">
                                    <i class="icon-trash"></i>
                                    Xóa
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/payment/editor_modal')
@endsection
@section('script')
    <script src="{{load_js('payment')}}"></script>
@endsection