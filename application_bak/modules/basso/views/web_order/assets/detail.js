function viewModel() {
    let self = this;
    self.order = ko.observable();
    self.items = ko.mapping.fromJS([]);
    self.default_tracking = ko.observable();
    self.default_delivered_date = ko.observable();

    self.order_info = {
        order_number: ko.observable(),
        buy_rate: ko.observable(),
        currency_symbol: ko.observable(),
        total: ko.observable(),
        ship_fee: ko.observable(),
        sub_total: ko.observable()
    };

    self.data = {
        warehouses: ko.mapping.fromJS([]),
        buyers: ko.mapping.fromJS([]),
        countries: ko.mapping.fromJS([]),
        init: function () {
            localStorage.setItem('prev_url', 'web_order_detail');
            AJAX.get(window.location, null, true, (res) => {
                ko.mapping.fromJS(res.warehouses, self.data.warehouses);
                ko.mapping.fromJS(res.buyers, self.data.buyers);
                ko.mapping.fromJS(res.countries, self.data.countries);

                res.order.total = parseFloat(res.order.total).toFixed(2);
                res.order.ship_fee = parseFloat(res.order.ship_fee).toFixed(2);

                self.order_info.buy_rate(moneyFormat(parseInt(res.order.buy_rate)));
                self.order_info.currency_symbol(res.order.currency_symbol);
                self.order_info.total(res.order.total);
                self.order_info.ship_fee(res.order.ship_fee);
                self.order_info.sub_total(res.order.sub_total);
                self.order_info.order_number(res.order.order_number);

                self.order(res.order);
                ko.mapping.fromJS(res.items, self.items);

                self.items.each((x)=>{
                   if(x.delivered_date() != null){
                       x.delivered_date(moment.unix(x.delivered_date()).format('DD-MM-YYYY'));
                   }
                });
            });
        },
        change_country: function (t) {
            self.data.countries.each((x) => {
                if (x.id() == parseInt(t.country_id)) {
                    console.log(x);
                    self.order_info.currency_symbol(x.currency_symbol());
                }
            });
        },
        calc: function () {
            let buy_rate = toNumber(self.order_info.buy_rate());
            let total = buy_rate * (parseFloat(self.order_info.total()) + parseFloat(self.order_info.ship_fee()));
            self.order_info.sub_total(total);
        }
    };

    self.update_all = function () {
        let data = {
            status: self.order().status,
            tracking_code: self.default_tracking(),
            delivered_date: self.default_delivered_date(),
            order: {
                buy_rate: toNumber(self.order_info.buy_rate()),
                warehouse_id: self.order().warehouse_id,
                country_id: self.order().country_id,
                buyer_id: self.order().buyer_id,
                total: self.order_info.total(),
                sub_total: self.order_info.sub_total(),
                ship_fee: self.order_info.ship_fee(),
                currency_symbol: self.order_info.currency_symbol(),
                order_number: self.order_info.order_number()
            }
        };

        AJAX.post(window.location, data, true, (res) => {
            if (res.error)
                ALERT.error(res.message);
            else {
                ALERT.success(res.message);
                self.items.each((x) => {
                    if (self.default_tracking() !== undefined) {
                        if (self.default_tracking() !== null) {
                            if (self.default_tracking().length > 0)
                                x.tracking_code(self.default_tracking());
                        }
                    }

                    if (self.default_delivered_date() !== undefined) {
                        if (self.default_delivered_date() !== null) {
                            if (self.default_delivered_date().length > 0)
                                x.delivered_date(self.default_delivered_date());
                        }
                    }
                });
            }
        });
    };

    self.update_item = function (item) {
        let data = {
            action: 'update-item',
            id: item.id(),
            order_id: item.web_order_id(),
            tracking_code: item.tracking_code(),
            delivered_date: item.delivered_date()
        };
        AJAX.post('/basso/web_order/', data, true, (res) => {
            if (res.error)
                ALERT.error(res.message);
            else {
                NOTI.success(res.message);
            }
        });
    }
}

let model = new viewModel();
model.data.init();
ko.applyBindings(model, document.getElementById('main-content'));