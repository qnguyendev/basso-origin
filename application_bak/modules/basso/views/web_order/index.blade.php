<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                <button class="float-right btn btn-xs btn-info" href="#" data-bind="click: update_tracking.show">
                    <i class="fa fa-mail-bulk"></i>
                    Cập nhật tracking
                </button>
                <button class="float-right btn-primary btn-xs mr-1 btn" data-bind="click: $root.export">
                    <i class="fa fa-download"></i>
                    Xuất Excel
                </button>
                <button class="btn btn-success btn-xs float-right mr-1" data-bind="click: export_tracking">
                    <i class="fa fa-file-export"></i>
                    Xuất mã tracking
                </button>
                Danh sách mua hàng
            </div>
        </div>
        <form class="card-body" autocomplete="off">
            <table class="table border-vertical mb-3">
                <tr>
                    <th class="text-right">Warehouse</th>
                    <td>
                        <select class="form-control" data-bind="value: filter.warehouse_id">
                            <option value="0">Tất cả</option>
                            <!--ko foreach: data.warehouses-->
                            <option data-bind="value: id, text: name"></option>
                            <!--/ko-->
                        </select>
                    </td>
                    <th class="text-right">Người mua</th>
                    <td>
                        <select class="form-control" data-bind="value: filter.buyer_id">
                            <option value="0">Tất cả</option>
                            <!--ko foreach: data.buyers-->
                            <option data-bind="value: id, text: name"></option>
                            <!--/ko-->
                        </select>
                    </td>
                    <th class="text-right">PTTT</th>
                    <td>
                        <select class="form-control" data-bind="value: filter.payment_id">
                            <option value="0">Tất cả</option>
                            <!--ko foreach: data.payments-->
                            <option data-bind="value: id, text: name"></option>
                            <!--/ko-->
                        </select>
                    </td>
                    <th class="text-right">Ngày mua</th>
                    <td>
                        <input type="text" class="form-control" data-bind="datePicker: filter.date"/>
                    </td>
                </tr>
                <tr>
                    <th class="text-right">Trạng thái</th>
                    <td>
                        <select class="form-control" data-bind="value: filter.status">
                            <option value="all">Tất cả</option>
                            @foreach(WebOrderStatus::LIST as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </td>
                    <th class="text-right">Chi nhánh</th>
                    <td>
                        <select class="form-control" data-bind="value: filter.branch">
                            <option value="all">Tất cả</option>
                            @foreach(Branch::LIST as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </td>
                    <td></td>
                    <td>
                        <input type="text" class="form-control" data-bind="value: filter.key"
                               placeholder="Tìm theo mã order, tracking">
                    </td>
                    <td></td>
                    <td>
                        <button class="btn btn-sm btn-primary btn-block" data-bind="click: function(){ search(1) }">
                            <i class="fa fa-search"></i>
                            Tìm
                        </button>
                    </td>
                </tr>
            </table>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Order Number</th>
                        <th>Website</th>
                        <th>Chi nhánh</th>
                        <th>Ngày mua</th>
                        <th>Trạng thái</th>
                        <th>Tổng tiền mua</th>
                        <th>Người mua</th>
                        <th>Warehouse</th>
                        <th>PTTT</th>
                        <th width="180">Thao tác</th>
                    </tr>
                    </thead>
                    <tbody class="text-center" data-bind="foreach: result">
                    <tr>
                        <th>
                            <a href="#" class="order-expand" data-bind="click: $root.toggle_row">
                                <i class="fa fa-eye"></i>
                            </a>
                        </th>
                        <td style="max-width: 230px" data-bind="text: order_number"></td>
                        <td data-bind="text: website"></td>
                        <td>
                        @foreach(Branch::LIST as $key=>$value)
                            <!--ko if: branch() == '{{$key}}'-->
                            {{$value}}
                            <!--/ko-->
                            @endforeach
                        </td>
                        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                        <td>
                            @foreach(WebOrderStatus::LIST as $key=>$value)
                                <span data-bind="visible: status() == '{{$key}}'">{{$value}}</span>
                            @endforeach
                        </td>
                        <td data-bind="text: `${currency_symbol()} ${(parseFloat(ship_fee()) + parseFloat(total())).toFixed(2)}`"></td>
                        <td data-bind="text: buyer"></td>
                        <td data-bind="text: warehouse"></td>
                        <td data-bind="text: payment"></td>
                        <td class="text-center">
                            <a class="text-primary mr-1 font-weight-bold"
                               data-bind="attr: {href: `/basso/web_order/detail/${id()}`}">
                                <i class="icon-pencil"></i>
                                Chi tiết
                            </a>
                            <a href="#" class="btn-link text-danger font-weight-bold">
                                <i class="icon-trash"></i>
                                Xóa
                            </a>
                        </td>
                    </tr>
                    <tr class="tr-collapse">
                        <td colspan="11">
                            <div class="item-detail">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Sản phẩm</th>
                                        <th>SL</th>
                                        <th>Giá mua</th>
                                        <th width="200">Mã tracking</th>
                                        <th width="140">Ngày về kho VC</th>
                                        <th width="70"></th>
                                    </tr>
                                    </thead>
                                    <tbody class="text-center" data-bind="foreach: items">
                                    <tr>
                                        <td>
                                            <a href="#" data-bind="attr: {href: link}, text: name" target="_blank">
                                            </a>
                                        </td>
                                        <td data-bind="text: quantity"></td>
                                        <td data-bind="text: `${$parent.currency_symbol()} ${buy_price()}`"></td>
                                        <td>
                                            <input type="text" class="form-control input-sm text-center"
                                                   data-bind="value: tracking_code"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm text-center"
                                                   data-bind="datePicker: delivered_date"/>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-xs text-uppercase"
                                                    data-bind="click: $root.update_item">
                                                <i class="fa fa-check"></i>
                                                Cập nhật
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            @include('pagination')
        </form>
    </div>
@endsection
@section('head')
    <style>
        #tracking-code-modal textarea {
            width: 100% !important;
            padding-left: 60px;
        }

        #tracking-code-modal .modal-body div textarea {
            position: absolute;
            width: 60px !important;
            top: 22px;
            bottom: 22px;
            background: transparent;
            border: 0;
            left: 15px;
            z-index: 99;
            padding-left: 5px;
            margin: 0 !important;
            color: #777;
            resize: none;
        }
    </style>
@endsection
@section('script')
    <script src="{{load_js('line-number')}}"></script>
    <script src="{{load_js('index')}}"></script>
@endsection
@section('modal')
    @include('partial/index/tracking_code_modal');
    @include('partial/index/update_tracking_code_modal');
@endsection