<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th colspan="2">Sản phẩm</th>
        <th>Variant</th>
        <th width="50">SL</th>
        <th width="140">Giá mua</th>
        <th>Tổng giá mua</th>
        <th>Ghi chú</th>
        <th></th>
    </tr>
    </thead>
    <tbody data-bind="foreach: buy_list.items" class="text-center">
    <tr>
        <td width="80">
            <img data-bind="attr: {src: `/timthumb.php?src=${image_path()}&w=200&h=200`}"
                 class="img-fluid ie-fix-flex"/>
        </td>
        <td>
            <a href="#" data-bind="attr: {href: link}, text: name" target="_blank">
            </a>
        </td>
        <td class="text-left">
            <!--ko foreach: variations-->
            <b data-bind="text: name"></b>: <span data-bind="text: value"></span><br/>
            <!--/ko-->
        </td>
        <td data-bind="text: quantity"></td>
        <td>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text text-sm" data-bind="text: $root.order_info.currency_symbol"></span>
                </div>
                <input type="number" min="0" step="0.01" class="form-control input-sm text-center"
                       data-bind="value: price, event: {change: $root.order_info.calc_total}"
                       style="width: 30px"/>
            </div>
        </td>
        <td data-bind="text: `${$root.order_info.currency_symbol()} ${(price() * quantity())}`"></td>
        <td data-bind="text: note"></td>
        <td class="text-center">
            <a class="btn btn-link text-danger font-weight-bold" href="#" data-bind="click: $root.buy_list.delete">
                <i class="icon-trash"></i>
                Xóa
            </a>
        </td>
    </tr>
    </tbody>
</table>

<br>
<div class="row pb-3">

    <div class="col-md-12 col-lg-5 offset-lg-1 col-xl-4 offset-xl-2">
        <table class="table border-vertical">
            <tbody>
            <tr>
                <th class="text-right">
                    <span class="text-danger">*</span>
                    Thời gian order
                </th>
                <td><input type="text" class="form-control input-sm" data-bind="datePicker: order_info.created_time">
                </td>
            </tr>
            <tr>
                <th class="text-right">Quốc gia</th>
                <td>
                    <select class="form-control input-sm"
                            data-bind="options: data.countries, optionsText: 'name', optionsValue: 'id',
                            value: order_info.country_id, event: {change: data.change_country}">
                    </select>
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    <span class="text-danger">*</span>
                    Chi nhánh
                </th>
                <td>
                    <select class="form-control input-sm" name="branch" data-bind="value: order_info.branch">
                        <option value="">- Lựa chọn -</option>
                        @foreach(Branch::LIST as $key=>$value)
                            <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <th class="text-right">
                    <span class="text-danger">*</span>
                    Warehouse</th>
                <td>
                    <select class="form-control input-sm"
                            data-bind="value: order_info.warehouse_id">
                        <option value="">- Lựa chọn -</option>
                        <!--ko foreach: data.warehouses-->
                        <option data-bind="value: id, text: name"></option>
                        <!--/ko-->
                    </select>
                </td>
            </tr>
            <tr>
                <th class="text-right">Order number</th>
                <td><input type="text" class="form-control input-sm" data-bind="value: order_info.order_number"/></td>
            </tr>
            <tr>
                <th class="text-right">
                    <span class="text-danger">*</span>
                    Người mua</th>
                <td>
                    <select class="form-control input-sm" data-bind="value: order_info.buyer_id" name="buyer">
                        @foreach($buyers as $t)
                            <option value="{{$t->id}}">{{$t->name}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <th class="text-right">Ghi chú</th>
                <td><input type="text" class="form-control input-sm" data-bind="value: order_info.note"/></td>
            </tr>
            <tr>
                <th class="text-right"></th>
                <td>
                    <div class="checkbox c-checkbox w-100">
                        <label>
                            <input type="checkbox" data-bind="checked: order_info.create_billing"/>
                            <span class="fa fa-check"></span>
                            Tạo phiếu chi
                        </label>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-md-12 col-lg-5 col-xl-4">
        <table class="table border-vertical">
            <tbody>
            <tr>
                <th class="text-right">
                    <span class="text-danger">*</span>
                    PTTT</th>
                <td>
                    <select class="form-control input-sm"
                            data-bind="value: order_info.payment_id, event: {change: data.change_payment}">
                        <option value="">- Lựa chọn -</option>
                        <!--ko foreach: data.payments-->
                        <option data-bind="value: id, text: name"></option>
                        <!--/ko-->
                    </select>
                </td>
            </tr>
            <tr>
                <th class="text-right">Tổng tiền hàng</th>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm" style="width: 50px"
                                  data-bind="text: order_info.currency_symbol"></span>
                        </div>
                        <input type="number" step="0.01" min="0" class="form-control input-sm text-right"
                               data-bind="value: order_info.total" disabled/>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">Ship web</th>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm" style="width: 50px"
                                  data-bind="text: order_info.currency_symbol"></span>
                        </div>
                        <input type="number" step="0.01" min="0" class="form-control input-sm text-right"
                               data-bind="value: order_info.ship_fee, event: {change: order_info.calc_total}">
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">Tổng tiền mua</th>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm" style="width: 50px"
                                  data-bind="text: order_info.currency_symbol"></span>
                        </div>
                        <input type="number" disabled step="0.01" min="0" class="form-control input-sm text-right"
                               data-bind="value: order_info.sub_total">
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">Tỷ giá mua</th>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm" style="width: 50px">đ</span>
                        </div>
                        <input type="text" class="form-control input-sm text-right"
                               data-bind="moneyMask: order_info.buy_rate, event: {change: order_info.calc_total}"/>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">Tổng tiền mua</th>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm" style="width: 50px">đ</span>
                        </div>
                        <input type="text" disabled class="form-control input-sm text-right"
                               data-bind="value: order_info.total_paid">
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="text-center">
    <button class="btn btn-success text-uppercase"
            data-bind="enable: buy_list.items().length > 0, click: function(){ $root.order_info.save() }">
        <i class="fa fa-check"></i>
        Tạo đơn hàng
    </button>
</div>
