function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();

    self.filter = {
        time: ko.observable('all'),
        start: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        end: ko.observable(moment(new Date()).format('DD-MM-YYYY'))
    };

    self.search = function (page) {
        let data = {
            page: page,
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.export_list = function () {
        window.location = `/basso/subscribe/export/?time=${self.filter.time()}&start=${self.filter.start()}&end=${self.filter.end()}`;
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));