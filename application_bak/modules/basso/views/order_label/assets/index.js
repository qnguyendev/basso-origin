function viewModel() {
    let self = this;

    self.group = ko.observable({
        id: ko.observable(),
        name: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        des: ko.observable(),
        result: ko.mapping.fromJS([]),
        add: function () {
            self.group().id(0);
            self.group().name(undefined);
            self.group().des(undefined);
            self.group().errors.showAllMessages(false);
            MODAL.show('#label-group-modal');
        },
        init: function () {
            AJAX.get(window.location, null, true, (res) => {
                ko.mapping.fromJS(res.data, self.group().result);
            });
        },
        edit: function (item) {
            self.group().id(item.id());
            self.group().name(item.name());
            self.group().des(item.des());
            MODAL.show('#label-group-modal');
        },
        delete: function (item) {
            ALERT.confirm('Xác nhận xóa nhóm', null, function () {
                AJAX.delete(window.location, {group_id: item.id()}, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        ALERT.success(res.message);
                        self.group().result.remove(item);
                    }
                });
            });
        },
        save: function () {
            if (!self.group().isValid())
                self.group().errors.showAllMessages();
            else {
                let data = {
                    id: self.group().id(),
                    name: self.group().name(),
                    des: self.group().des(),
                    type: 'group'
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        ALERT.success(res.message);
                        MODAL.hide('#label-group-modal');
                        self.group().init();
                    }
                });
            }
        },
        view: function (item) {
            self.value().group_id(item.id());
            self.value().group_name(item.name());
            self.value().init();
        }
    });

    self.value = ko.observable({
        id: ko.observable(),
        name: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        des: ko.observable(),
        result: ko.mapping.fromJS([]),
        group_id: ko.observable(),
        group_name: ko.observable(),
        add: function () {
            self.value().id(0);
            self.value().name(undefined);
            self.value().des(undefined);
            self.value().errors.showAllMessages(false);
            MODAL.show('#label-value-modal');
        },
        init: function () {
            AJAX.get(window.location, {group_id: self.value().group_id()}, true, (res) => {
                ko.mapping.fromJS(res.data, self.value().result);
            });
        },
        edit: function (item) {
            self.value().id(item.id());
            self.value().name(item.name());
            self.value().des(item.des());
            MODAL.show('#label-value-modal');
        },
        delete: function (item) {
            ALERT.confirm('Xác nhận xóa nhãn', null, function () {
                AJAX.delete(window.location, {label_id: item.id()}, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        ALERT.success(res.message);
                        self.value().result.remove(item);
                    }
                });
            });
        },
        save: function () {
            if (!self.value().isValid())
                self.value().errors.showAllMessages();
            else {
                let data = {
                    id: self.value().id(),
                    name: self.value().name(),
                    des: self.value().des(),
                    group_id: self.value().group_id(),
                    type: 'label'
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        ALERT.success(res.message);
                        MODAL.hide('#label-value-modal');
                        self.value().init();
                    }
                });
            }
        }
    });
}

let model = new viewModel();
model.group().init();
ko.validatedObservable(model.group());
ko.validatedObservable(model.value());
ko.applyBindings(model, document.getElementById('main-content'));