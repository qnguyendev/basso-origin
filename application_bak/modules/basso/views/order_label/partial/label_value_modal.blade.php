<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="label-value-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    <span data-bind="text: value().id() == 0 ? 'Thêm nhãn' : 'Cập nhật nhãn'"></span>
                    |
                    <span data-bind="text: value().group_name"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Tên nhãn
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: value().name"/>
                    </label>
                    <input type="text" class="form-control" data-bind="value: value().name"/>
                </div>
                <div class="form-group">
                    <label>Ghi chú</label>
                    <input type="text" class="form-control" data-bind="value: value().des"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-success" data-bind="click: value().save">
                    <em class="fa fa-check"></em>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>
