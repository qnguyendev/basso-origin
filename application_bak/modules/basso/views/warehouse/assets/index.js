function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);

    self.modal = {
        id: ko.observable(),
        name: ko.observable().extend({required: {message: LABEL.required}}),
        note: ko.observable(),
        show: function () {
            self.modal.id(0);
            self.modal.name(undefined);
            self.modal.note(undefined);
            self.modal.errors.showAllMessages(false);
            MODAL.show('#editor-modal');
        },
        init: function (item) {
            self.modal.id(item.id());
            self.modal.name(item.name());
            self.modal.note(item.note());
            MODAL.show('#editor-modal');
        },
        save: function () {
            if (!self.modal.isValid())
                self.modal.errors.showAllMessages();
            else {
                let data = {
                    id: self.modal.id(),
                    name: self.modal.name(),
                    note: self.modal.note()
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        MODAL.hide('#editor-modal');
                        NOTI.success(res.message);
                        self.init_data();
                    }
                });
            }
        }
    };

    self.init_data = function () {
        AJAX.get(window.location, null, false, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa warehouse?', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    NOTI.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    };
}

let model = new viewModel();
model.init_data();
ko.validatedObservable(model.modal);
ko.applyBindings(model, document.getElementById('main-content'));