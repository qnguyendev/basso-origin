<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Danh sách form liên hệ
                <a class="float-right" href="#s" data-bind="click: contact_info_modal.show">
                    <i class="fa fa-edit"></i>
                    Sửa thông tin
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <th>#</th>
                    <th>Tên</th>
                    <th>Số điện thoại</th>
                    <th>Thời gian</th>
                    <th>Ghi chú</th>
                    <th></th>
                    </thead>
                    <tbody data-bind="foreach: result" class="text-center">
                    <tr>
                        <th data-bind="text: $index() + 1"></th>
                        <td data-bind="text: name"></td>
                        <td data-bind="text: phone"></td>
                        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY HH:mm')"></td>
                        <td data-bind="text: note"></td>
                        <td>
                            <a href="#" data-bind="click: $root.detail_modal.show">
                                <i class="fa fa-eye"></i>
                                Chi tiết
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            @include('pagination')
        </div>
    </div>
@endsection

@section('script')
    {{ckeditor()}}
    <script src="{{load_js('index')}}"></script>
@endsection

@section('modal')
    @include('partial/contact_info_modal')
    @include('partial/contact_detail_modal')
@endsection