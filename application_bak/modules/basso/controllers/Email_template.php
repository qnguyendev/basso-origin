<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Email_template
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 */
class Email_template extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Email template']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $parts = ['footer', 'before_order', 'after_order', 'before_footer'];
        $prefix = $this->input->get('prefix');
        $data = [];

        foreach ($parts as $key) {
            $data[$key] = null;

            $option = $this->option_model->get($prefix . '_' . $key);
            if ($option != null)
                $data[$key] = $option->value;
        }

        json_success(null, $data);
    }

    public function POST_index()
    {
        $prefix = $this->input->post('prefix');
        $parts = ['footer', 'before_order', 'after_order', 'before_footer'];

        foreach ($parts as $key) {
            $data = $this->input->post($key, false);
            if ($data != null)
                $this->option_model->save($prefix . '_' . $key, $data);
        }

        json_success(null);
    }
}