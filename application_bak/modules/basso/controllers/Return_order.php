<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Return_order
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Return_order_model $return_order_model
 * @property Customer_order_model $customer_order_model
 * @property Bank_model $bank_model
 * @property Payment_history_model $payment_history_model
 * @property Billing_model $billing_model
 */
class Return_order extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('return_order_model');
        $this->load->model('customer_order_model');
        $this->load->model('bank_model');
        $this->load->model('payment_history_model');
        $this->load->model('management/billing_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Đơn hoàn trả']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $action = $this->input->get('action');
        switch ($action) {
            case 'modal-search':
                $this->_search_orders();
                break;
            case 'search':
                $this->_search();
                break;
            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    private function _search()
    {
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');
        $this->form_validation->set_rules('status', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $page = $this->input->get('page');
        $time = $this->input->get('time');
        $start_date = $this->input->get('start');
        $end_date = $this->input->get('end');
        $status = $this->input->get('status');
        $key = $this->input->get('key');
        $status = $status == 'all' ? null : $status;

        $start = strtotime("$start_date 00:00:00");
        $end = strtotime("$end_date 23:59:59");

        switch ($time) {
            //  all time
            case 'all':
                $start = 0;
                $end = 0;
                break;
            case 'today':
                $start = strtotime(date('Y-m-d 00:00:00'));
                $end = 0;
                break;
            //  in week
            case 'week':
                $day = strtotime('this week', time());
                $start = strtotime(date('Y-m-d 00:00:00', $day));
                $end = 0;
                break;
            //  trong tháng
            case 'month':
                $start = strtotime(date('Y-m-1'));
                $end = 0;
                break;
        }

        $result = $this->return_order_model->get($page, $start, $end, $status, $key);
        $total_item = $this->return_order_model->count($start, $end, $status, $key);

        if (count($result) > 0)
            $items = $this->return_order_model->get_items(array_column($result, 'id'));
        else
            $items = [];

        for ($i = 0; $i < count($result); $i++) {
            $result[$i]->items = array_filter($items, function ($t) use ($result, $i) {
                return $t->order_returned_id == $result[$i]->id;
            });
        }

        json_success(null, [
            'data' => $result,
            'pagination' => Pagination::calc($total_item, $page)
        ]);
    }

    private function _search_orders()
    {
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $page = $this->input->get('page');
        $start = $this->input->get('start');
        $end = $this->input->get('end');
        $key = $this->input->get('key');

        $start = strtotime("$start 00:00:00");
        $end = strtotime("$end 23:59:59");

        $orders = $this->customer_order_model->get($page, $start, $end, CustomerOrderStatus::COMPLETED,
            null, null, null, null, null, null, $key);
        $total_item = $this->customer_order_model->count($start, $end, CustomerOrderStatus::COMPLETED,
            null, null, null, null, null, null, $key);
        $pagination = Pagination::calc($total_item, $page);

        json_success(null, ['data' => $orders, 'pagination' => $pagination]);
    }

    public function create($id)
    {
        if (!isset($id)) {
            error_msg('Yêu cầu không hợp lệ', 'error');
            return redirect(base_url('basso/return_order'));
        }

        $order = $this->customer_order_model->get_by_id($id);
        if ($order == null) {
            error_msg('Không tìm thấy đơn hàng #' . $id, 'error');
            return redirect(base_url('basso/return_order'));
        }

        if ($order->status != CustomerOrderStatus::COMPLETED) {
            error_msg('Đơn hàng chưa hoàn thành', 'error');
            return redirect(base_url('basso/return_order'));
        }

        $items = $this->customer_order_model->get_items($id);
        if (count($items) == 0) {
            error_msg('Đơn hàng không có sản phẩm', 'error');
            return redirect(base_url('basso/return_order'));
        }

        for ($i = 0; $i < count($items); $i++) {
            $items[$i]->formatted_price = format_number($items[$i]->price * $order->currency_rate);
            $items[$i]->formatted_price = str_replace('.', ',', $items[$i]->formatted_price);
            $items[$i]->price = $items[$i]->price * $order->currency_rate;
        }

        $this->blade->set('breadcrumbs', [
            ['text' => 'Đơn hoàn trả', 'url' => base_url('basso/return_order')],
            ['text' => 'Tạo đơn trả hàng']
        ]);
        $this->blade->set('order', $order);
        $this->blade->set('items', $items);
        $this->blade->set('banks', $this->bank_model->get());

        return $this->blade->render();
    }

    public function POST_save($id)
    {
        $items = $this->input->post('items');
        $payment = $this->input->post('payment');
        $note = $this->input->post('note');

        if (!isset($items) || !isset($payment))
            json_error('Yêu cầu không hợp lệ');

        if (!is_array($items) || !is_array($payment))
            json_error('Yêu cầu không hợp lệ');

        $order = $this->customer_order_model->get_by_id($id);

        #region Hoàn tiền
        if (isset($payment['amount'])) {
            /*$this->payment_history_model->insert(strtotime($payment['time']), $payment['amount'], $payment['type'],
                isset($payment['note']) ? $payment['note'] : null, null, PaymentHistoryStatus::PENDING, null,
                $payment['bank_id'] == 0 ? null : $payment['bank_id']);*/

            //  Tạo phiếu chi
            $this->billing_model->insert(strtotime($payment['time']), BillingType::OUT, AUTO_BILLING_TERM,
                $order->name, $payment['amount'], "Hoàn tiền đơn hàng #$id");
        }
        #endregion

        for ($i = 0; $i < count($items); $i++) {
            $items[$i]['total'] = $items[$i]['quantity'] * $items[$i]['price'];
        }

        $return_order_id = $this->return_order_model->insert([
            'order_id' => $id,
            'note' => $note,
            'total_refund' => $payment['amount'],
            'status' => CustomerOrdersReturnedStatus::PROCESSING
        ], $items
        );

        if ($return_order_id) {
            success_msg("Tạo đơn hoàn trả cho đơn hàng #$id thành công");
            json_success('Đã tạo đơn hoàn trả', ['redirect_url' => base_url('basso/return_order')]);
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}