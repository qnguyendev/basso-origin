<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Shipping_order
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Order_shipping_model $order_shipping_model
 * @property Shipping_model $shipping_model
 * @property Customer_order_model $customer_order_model
 * @property Billing_model $billing_model
 * @property Payment_history_model $payment_history_model
 */
class Shipping_order extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_shipping_model');
        $this->load->model('customer_order_model');
        $this->load->model('management/shipping_model');
        $this->load->model('payment_history_model');
        $this->load->model('management/billing_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Quản lý giao hàng']]);
        $this->blade->set('shippings', $this->shipping_model->get());
        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('shipping_id', null, 'required|numeric');
        $this->form_validation->set_rules('status', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $page = $this->input->get('page');
        $shipping_id = $this->input->get('shipping_id');
        $status = $this->input->get('status');
        $key = $this->input->get('key');
        $branch = $this->input->get('branch');

        $shipping_id = $shipping_id == 0 ? null : $shipping_id;
        $status = $status == 'all' ? null : $status;

        $result = $this->order_shipping_model->get($page, $branch, $shipping_id, $status, $key);
        for ($i = 0; $i < count($result); $i++) {
            $result[$i]->items = $this->order_shipping_model->get_items($result[$i]->id);
        }

        $total_item = $this->order_shipping_model->count($branch, $shipping_id, $status, $key);
        $pagination = Pagination::calc($total_item, $page);

        json_success(null, ['data' => $result, 'pagination' => $pagination]);
    }

    public function POST_index()
    {
        $id = $this->input->post('id');
        if ($id != null) {
            $order = $this->order_shipping_model->get_by_id($id);
            if ($order != null) {
                $this->order_shipping_model->update_shipped_time($id, time());
                $this->customer_order_model->check_order_completed($order->order_id);

                if ($order->status != ShippingOrderStatus::COMPLETED) {
                    $customer_order = $this->customer_order_model->get_by_id($order->order_id);

                    if ($customer_order != null) {
                        //  Tạo phiếu thu tự động
                        $this->payment_history_model->insert(time(), $order->cod_amount, PaymentType::COD,
                            'Tiền thu COD đơn hàng ' . $customer_order->order_code,
                            $customer_order->id, PaymentHistoryStatus::COMPLETED);

                        $this->billing_model->insert(time(), BillingType::IN, AUTO_BILLING_TERM, $customer_order->name,
                            $order->cod_amount, null, 'Tiền thu COD đơn hàng ' . $customer_order->order_code, null,
                            BillingGroup::SHIPPING, PaymentType::COD, $customer_order->id, null);

                        $this->payment_history_model->update_order_total_paid($customer_order->id);
                        $this->customer_order_model->check_order_completed($customer_order->id);
                    }
                }
            }

            json_success('Cập nhật thành công', ['status' => ShippingOrderStatus::COMPLETED]);
        }

        json_error('Yêu cầu không hợp lệ');
    }
}