<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Invoice
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Order_shipping_model $order_shipping_model
 * @property Customer_order_model $customer_order_model
 * @property Payment_history_model $payment_history_model
 * @property Billing_model $billing_model
 */
class Invoice extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_shipping_model');
        $this->load->model('customer_order_model');
        $this->load->model('payment_history_model');
        $this->load->model('management/billing_model');
    }

    public function index()
    {
        $id = $this->input->get('id');
        if ($id == null)
            return redirect(base_url('basso/dashboard'));

        $id = explode(',', $id);
        $orders = $this->order_shipping_model->get_by_id($id);
        if (count($orders) > 0) {
            $items = $this->order_shipping_model->get_items(array_column($orders, 'id'));

            /*
            $this->order_shipping_model->update_shipped_time($id, time());

            foreach ($orders as $order) {
                //$this->customer_order_model->check_order_completed($order->order_id);
                if ($order->status != ShippingOrderStatus::COMPLETED) {
                    $customer_order = $this->customer_order_model->get_by_id($order->order_id);

                    if ($customer_order != null) {
                        //  Tạo phiếu thu tự động
                        $this->payment_history_model->insert(time(), $order->cod_amount, PaymentType::COD,
                            'Tiền thu COD đơn hàng ' . $customer_order->order_code,
                            $customer_order->id, PaymentHistoryStatus::COMPLETED);

                        $this->billing_model->insert(time(), BillingType::IN, AUTO_BILLING_TERM, $customer_order->name,
                            $order->cod_amount, null, 'Tiền thu COD đơn hàng ' . $customer_order->order_code, null,
                            BillingGroup::SHIPPING, PaymentType::COD, $customer_order->id, null);

                        $this->payment_history_model->update_order_total_paid($customer_order->id);
                    }
                }
            }
            */

            $this->blade->set('orders', $orders);
            $this->blade->set('items', $items);
            return $this->blade->render();
        }

        error_msg('Không tìm thấy đơn vận chuyển', 'error');
        return redirect(base_url('basso/dashboard'));
    }
}