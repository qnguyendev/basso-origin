<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Deliver_manager
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Delivery_manager_model $delivery_manager_model
 * @property Web_order_model $web_order_model
 * @property Warehouse_model $warehouse_model
 * @property Customer_order_model $customer_order_model
 */
class Delivery_manager extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('delivery_manager_model');
        $this->load->model('web_order_model');
        $this->load->model('warehouse_model');
        $this->load->model('customer_order_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Quản lý vận chuyển']]);
        $this->blade->set('warehouses', $this->warehouse_model->get());

        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('action', null, 'required');
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('warehouse_id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $action = $this->input->get('action');
        $page = $this->input->get('page');
        $warehouse_id = $this->input->get('warehouse_id');
        $key = $this->input->get('key');

        switch ($action) {
            case DeliveryManager::NO_TRACKING:
                $result = $this->delivery_manager_model->no_tracking($page, 5, $warehouse_id, $key);
                $total = $this->delivery_manager_model->count_no_tracking(5, $warehouse_id, $key);
                $pagination = Pagination::calc($total, $page);
                json_success(null, ['data' => $result, 'pagination' => $pagination]);
                break;

            case DeliveryManager::NO_DELIVERED:
                $result = $this->delivery_manager_model->not_delivered($page, 10, $warehouse_id, $key);
                $total = $this->delivery_manager_model->count_not_delivered(10, $warehouse_id, $key);
                $pagination = Pagination::calc($total, $page);
                json_success(null, ['data' => $result, 'pagination' => $pagination]);
                break;

            case DeliveryManager::NO_INVENTORY:
                $result = $this->delivery_manager_model->not_inventory($page, 15, $warehouse_id, $key);
                for ($i = 0; $i < count($result); $i++) {
                    if ($result[$i]->delivered_date != null) {
                        if (intval($result[$i]->delivered_date) > 0)
                            $result[$i]->delivered_date = date('d-m-Y', $result[$i]->delivered_date);
                        else
                            $result[$i]->delivered_date = null;
                    }
                }

                $total = $this->delivery_manager_model->count_not_inventory(15, $warehouse_id, $key);
                $pagination = Pagination::calc($total, $page);
                json_success(null, ['data' => $result, 'pagination' => $pagination]);
                break;

            case DeliveryManager::ALL:
                $result = $this->delivery_manager_model->all($page, $warehouse_id, $key);
                for ($i = 0; $i < count($result); $i++) {
                    if ($result[$i]->delivered_date != null) {
                        if (intval($result[$i]->delivered_date) > 0)
                            $result[$i]->delivered_date = date('d-m-Y', $result[$i]->delivered_date);
                        else
                            $result[$i]->delivered_date = null;
                    }
                }

                $total = $this->delivery_manager_model->count_all($warehouse_id, $key);
                $pagination = Pagination::calc($total, $page);
                json_success(null, ['data' => $result, 'pagination' => $pagination]);
                break;

            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('order_id', null, 'required|numeric');
        $this->form_validation->set_rules('item_id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $order_id = $this->input->post('order_id');
        $item_id = $this->input->post('item_id');
        $note = $this->input->post('note');
        $delivered_date = $this->input->post('delivered_date');

        $data = ['note' => $note];
        if ($delivered_date != null) {
            if (!empty($delivered_date)) {
                try {
                    $data['delivered_date'] = strtotime($delivered_date);
                } catch (Exception $exception) {
                }
            }
        }

        if (is_enull($note, null) != null)
            $this->web_order_model->update($order_id, ['note' => $note]);

        $result = $this->web_order_model->update_item($order_id, $item_id, $data);
        if ($result)
            json_success('Cập nhật thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}