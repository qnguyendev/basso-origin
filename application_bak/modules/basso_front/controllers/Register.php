<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Register
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Customer_model $customer_model
 * @property Email_model $email_model
 * @property CI_Encryption $encryption
 */
class Register extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('management/customer_model');
        $this->load->model('email_model');
    }

    public function index()
    {
        if ($this->ion_auth->logged_in())
            return redirect(base_url());

        set_head_title('Basso.vn - Đăng ký');
        set_head_index(false);
        return $this->blade->render();
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('email', null, 'required');
        $this->form_validation->set_rules('phone', null, 'required');
        $this->form_validation->set_rules('pass', null, 'required');
        $this->form_validation->set_rules('re_pass', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập thông tin bắt buộc');

        $name = $this->input->post('name');
        $email = strtolower($this->input->post('email'));
        $phone = $this->input->post('phone');
        $pass = $this->input->post('pass');
        $re_pass = $this->input->post('re_pass');

        if ($pass != $re_pass) {
            json_error('Xác nhận mật khẩu không khớp');
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            json_error('Địa chỉ email không hợp lệ');

        if ($this->ion_auth->email_check($email)) {
            json_error('Địa chỉ email đã được sử dụng');
        }

        $user = $this->customer_model->get_by_phone($phone);
        if ($user != null) {
            if (strpos('@no_email.com', $user->email) == false) {
                json_error('Số điện thoại đã được sử dụng');
            }
        }

        $this->blade->set('name', $name);
        $this->blade->set('email', $email);
        $this->blade->set('pass', $pass);
        $content = $this->blade->render('registered_success_email', null, true);
        $subject = "[Basso] - Đăng ký tài khoản thành công";
        $this->email_model->insert($subject, $content, $email);

        $this->ion_auth->register($email, $pass, $email, [
            'first_name' => $name,
            'phone' => $phone
        ], [6]);

        json_success('Tạo tài khoản thành công');
    }

    public function forgot_pass()
    {
        if ($this->ion_auth->logged_in())
            return redirect(base_url());

        set_head_title('Quên mật khẩu | Basso');
        set_head_index(false);

        return $this->blade->render();
    }

    public function POST_forgot_pass()
    {
        if ($this->ion_auth->logged_in())
            return redirect(base_url());

        $this->form_validation->set_rules('email', null, 'required|valid_email');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error_msg', 'Địa chỉ email không hợp lệ');
            return redirect(base_url('quen-mat-khau'));
        }

        $email = trim(strtolower($this->input->post('email')));
        if (!$this->ion_auth->identity_check($email)) {
            $this->session->set_flashdata('error_msg', 'Không tìm thấy Email của bạn');
            return redirect(base_url('quen-mat-khau'));
        }

        $this->load->model('email_model');
        $user = $this->customer_model->get_by_email($email);

        $reset_url = base_url('reset?token=' . md5($email . $user->id));
        $content = $this->blade->render('forgot_pass_email', ['name' => $user->name, 'reset_url' => $reset_url], true);
        $subject = "[Basso] khôi phục mật khẩu";
        $this->email_model->insert($subject, $content, $email);

        $this->session->set_flashdata('success_msg', 'Không tìm thấy Email của bạn');
        return redirect(base_url('quen-mat-khau'));
    }

    public function reset_pass()
    {
        if ($this->ion_auth->logged_in())
            return redirect(base_url());

        $token = $this->input->get('token');
        if ($token == null)
            return $this->blade->render('no_token');

        set_head_index(false);
        set_head_title('Basso - Khôi phục mật khẩu');

        return $this->blade->render();
    }

    public function POST_reset_pass()
    {
        if ($this->ion_auth->logged_in())
            return redirect(base_url());

        $token = $this->input->get('token');

        $this->form_validation->set_rules('pass', null, 'required');
        $this->form_validation->set_rules('re_pass', null, 'required');
        $this->form_validation->set_rules('email', null, 'required|valid_email');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error_msg', 'Vui lòng nhập đủ thông tin');
            return redirect(base_url('reset?token=' . $token));
        }

        $pass = $this->input->post('pass');
        $email = strtolower(trim($this->input->post('email')));
        $re_pass = $this->input->post('re_pass');

        if (!$this->ion_auth->identity_check($email)) {
            $this->session->set_flashdata('error_msg', 'Không tìm thấy Email của bạn');
            return redirect(base_url('reset?token=' . $token));
        }

        $user = $this->customer_model->get_by_email($email);
        if ($pass != $re_pass) {
            $this->session->set_flashdata('error_msg', 'Xác nhận mật khẩu không đúng');
            return redirect(base_url('reset?token=' . $token));
        }

        if (md5($email . $user->id) != $token) {
            $this->session->set_flashdata('error_msg', 'Địa chỉ email không đúng với email yêu cầu khôi phục mật khẩu');
            return redirect(base_url('reset?token=' . $token));
        }

        $this->ion_auth->update($user->id, ['password' => $pass]);
        $this->session->set_flashdata('reset_pass_success', true);
        return redirect(base_url('reset/success'));
    }

    public function reset_pass_success()
    {
        if ($this->ion_auth->logged_in())
            return redirect(base_url());

        if ($this->session->flashdata('reset_pass_success') == null)
            return redirect(base_url());

        return $this->blade->render();
    }
}