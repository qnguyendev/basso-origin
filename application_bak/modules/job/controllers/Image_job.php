<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Cron
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Image_model $image_model
 *
 */
class Image_job extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->input->is_cli_request() or exit("Execute via command line: php index.php migrate");
        $this->load->model('admin/image_model');
    }

    /**
     * Xóa ảnh hết hạn
     */
    public function clean()
    {
        $time = time() - 15 * 60;
        $images = $this->image_model->get_older($time, 0);
        foreach ($images as $img) {
            try {
                $path = ".{$img->path}";
                if (is_file($path)) {
                    unlink($path);
                    echo "Deleted {$img->path}<br/>";
                }
            } catch (Exception $exception) {
            }
        }

        if (count($images) > 0)
            $this->image_model->delete(array_column($images, 'id'));
    }
}