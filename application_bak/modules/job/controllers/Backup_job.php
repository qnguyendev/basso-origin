<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Backup
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Dropbox $dropbox
 * @property Option_model $option_model
 * @property Backup_model $backup_model
 * @property CI_DB_utility $dbutil
 */
class Backup_job extends MY_Controller
{
    private $dropbox_config;

    public function __construct()
    {
        parent::__construct();
        $this->input->is_cli_request() or exit("Execute via command line: php index.php migrate");

        $config_file = "./theme/config.php";
        if (!is_file($config_file))
            return;

        require "./theme/config.php";
        if (!isset($theme_config))
            $theme_config = [];

        $this->dropbox_config = $theme_config['dropbox'];

        $app = [
            'app_key' => $this->dropbox_config['app_key'],
            'app_secret' => $this->dropbox_config['app_secret'],
            'app_full_access' => true
        ];

        $this->load->library('dropbox', $app);
        $this->load->helper('file');
        $this->load->model('system/backup_model');
    }

    public function run()
    {
        $app = [
            'app_key' => $this->dropbox_config['app_key'],
            'app_secret' => $this->dropbox_config['app_secret'],
            'app_full_access' => true
        ];

        $dropbox_access_token = null;
        $dropbox_account_id = null;
        $dropbox_save_path = null;

        #region lấy cấu hình dropbox
        $config = $this->option_model->get('dropbox_access_token');
        if ($config != null) {
            if ($config->value != null) {
                if (!empty($config->value)) {
                    $dropbox_access_token = $config->value;
                }
            }
        }

        $config = $this->option_model->get('dropbox_account_id');
        if ($config != null) {
            if ($config->value != null) {
                if (!empty($config->value)) {
                    $dropbox_account_id = $config->value;
                }
            }
        }

        $config = $this->option_model->get('dropbox_path');
        if ($config != null) {
            if ($config->value != null) {
                if (!empty($config->value)) {
                    $dropbox_save_path = $config->value;
                }
            }
        }
        #endregion

        $this->load->library('dropbox', $app);
        $this->dropbox->SetBearerToken([
            't' => $dropbox_access_token,
            'account_id' => $dropbox_account_id
        ]);

        //  if authorized dropbox
        if ($this->dropbox->IsAuthorized()) {
            $this->load->dbutil();
            $prefs = [
                'format' => 'zip',                       // gzip, zip, txt
                'filename' => 'backup_db.sql'              // File name - NEEDED ONLY WITH ZIP FILES
            ];
            $backup = $this->dbutil->backup($prefs);
            $file_name = 'backup-' . date('H-i-s-d-m-Y') . '.zip';
            $file_path = "./uploads/$file_name";
            write_file($file_path, $backup);

            $upload = $this->dropbox->UploadFile($file_path, "$dropbox_save_path$file_name");
            $this->backup_model->save_history($file_name, $upload->size);
            unlink($file_path);
        }
    }

    /**
     * Xóa các file backup cũ
     * @throws DropboxException
     */
    public function clean()
    {
        $dropbox_access_token = null;
        $dropbox_account_id = null;
        $dropbox_save_path = null;

        #region lấy cấu hình dropbox
        $config = $this->option_model->get('dropbox_access_token');
        if ($config != null) {
            if ($config->value != null) {
                if (!empty($config->value)) {
                    $dropbox_access_token = $config->value;
                }
            }
        }

        $config = $this->option_model->get('dropbox_account_id');
        if ($config != null) {
            if ($config->value != null) {
                if (!empty($config->value)) {
                    $dropbox_account_id = $config->value;
                }
            }
        }

        $config = $this->option_model->get('dropbox_path');
        if ($config != null) {
            if ($config->value != null) {
                if (!empty($config->value)) {
                    $dropbox_save_path = $config->value;
                }
            }
        }
        #endregion

        $this->dropbox->SetBearerToken(array(
            't' => $dropbox_access_token,
            'account_id' => $dropbox_account_id
        ));

        if ($this->dropbox->IsAuthorized()) {
            $backups = $this->backup_model->get_older_backups(30);
            foreach ($backups as $backup) {
                $files = $this->dropbox->Search($dropbox_save_path, $backup->file_name);
                $this->backup_model->delete($backup->id);

                if (count($files) == 1)
                    $this->dropbox->Delete($files[0]->path);

                if (is_file('./uploads/' . $backup->file_name)) {
                    unlink('./uploads/' . $backup->file_name);
                }
            }
        }
    }
}