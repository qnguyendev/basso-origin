<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Pushover_job
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Pushover $pushover
 */
class Noti_job extends Worker_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pushover');
    }

    public function run()
    {
        if ($this->worker_enable) {
            while ($this->worker_status) {
                $data = $this->reverse_job('noti');
                if (is_array($data)) {
                    $noti_data = [];
                    foreach ($data as $key => $value)
                        $noti_data[$key] = $value;

                    $this->send($noti_data);
                }
            }
        }
    }

    private function send(array $data)
    {
        $this->pushover->set_message($data);
        $this->pushover->send();
    }
}