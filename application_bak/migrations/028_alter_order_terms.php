<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_order_terms * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_order_terms extends CI_Migration
{
    protected $_table_name = "order_terms";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'terms' => ['type' => 'text', 'null' => true]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}