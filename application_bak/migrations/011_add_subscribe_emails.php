<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_add_subscribe_emails * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_add_subscribe_emails extends CI_Migration
{
    protected $_table_name = "subscribe_emails";

    public function up()
    {
        $this->dbforge->add_field([
            'email' => ['type' => 'nvarchar', 'constraint' => 256],
            'created_time' => ['type' => 'int']
        ]);
        $this->dbforge->add_key('email', true);
        $this->dbforge->create_table($this->_table_name, TRUE);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}