<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_system_logs * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_system_logs extends CI_Migration
{
    protected $_table_name = "system_logs";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'object_id' => ['type' => 'varchar', 'constraint' => 64, 'null' => true]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}