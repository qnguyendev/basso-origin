<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_customer_order_tabs * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_customer_order_tabs extends CI_Migration
{
    protected $_table_name = "customer_order_tabs";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name,
            [
                'order_status' => ['type' => 'text', 'null' => true],
                'item_requirement' => ['type' => 'text', 'null' => true]
            ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}