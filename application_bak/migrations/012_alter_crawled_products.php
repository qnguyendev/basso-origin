<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_crawled_products * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_crawled_products extends CI_Migration
{
    protected $_table_name = "crawled_products";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'web_shipping_fee' => ['type' => 'float', 'default' => 0, 'null' => true],
            'raw_json' => ['type' => 'longtext', 'null' => true]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}