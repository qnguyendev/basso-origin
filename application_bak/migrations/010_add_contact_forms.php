<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_add_contact_forms * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_add_contact_forms extends CI_Migration
{
    protected $_table_name = "contact_forms";

    public function up()
    {
        $this->dbforge->add_field([
            'id' => ['type' => 'int', 'auto_increment' => true],
            'created_time' => ['type' => 'int'],
            'name' => ['type' => 'varchar', 'constraint' => 256],
            'phone' => ['type' => 'varchar', 'constraint' => 256, 'null' => true],
            'email' => ['type' => 'varchar', 'constraint' => 256, 'null' => true],
            'address' => ['type' => 'varchar', 'constraint' => 256, 'null' => true],
            'content' => ['type' => 'text'],
            'note' => ['type' => 'text', 'null' => true]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->_table_name, TRUE);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}