<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
//
$lang['required'] = 'Vui lòng nhập thông tin bắt buộc';
$lang['add_button_label'] = 'Thêm';
$lang['show_button_label'] = 'Hiện';
$lang['hide_button_label'] = 'Ẩn';
$lang['delete_button_label'] = 'Xóa';
$lang['save_button_label'] = 'Hoàn thành';

//  Quản lý danh mục
$lang['terms_function_name'] = 'Quản lý danh mục';
$lang['terms_create_heading'] = 'Thêm danh mục';
$lang['terms_edit_heading'] = 'Cập nhật danh mục';
$lang['terms_no_exist'] = 'Danh mục không tồn tại';
$lang['terms_parent_label'] = 'Danh mục cha';
$lang['terms_name_label'] = 'Tên danh mục';
$lang['terms_slug_label'] = 'Url hiển thị';
$lang['terms_external_url_label'] = 'Liên kết tùy chỉnh';
$lang['terms_type_label'] = 'Phân loại';