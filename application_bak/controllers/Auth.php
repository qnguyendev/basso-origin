<?php defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * s Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation $form_validation The form validation library
 * @property Blade blade template
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Auth extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * login
     */
    public function login()
    {
        if ($this->input->method() == 'post') {
            if ($this->ion_auth->login($this->input->post('identity'),
                $this->input->post('password'), true)) {

                if ($this->ion_auth->in_group('supporters'))
                    return redirect('lich-hen');

                return redirect('/');
            }

            $this->session->set_flashdata('error', 'Tài khoản không hợp lệ');
        }

        return $this->blade->render();
    }

    public function POST_login()
    {
        $this->form_validation->set_rules('email', null, 'required|valid_email');
        $this->form_validation->set_rules('pass', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng thông tin đăng nhập');

        $email = trim(strtolower($this->input->post('email')));
        $pass = $this->input->post('pass');
        if (!$this->ion_auth->login($email, $pass, true))
            json_error('Sai email đăng nhập/mật khẩu');

        json_success(null);
    }


    /**
     * Log the user out
     */
    public function logout()
    {
        $this->ion_auth->logout();
        redirect(base_url());
    }

    public function profile()
    {
        if (!$this->ion_auth->logged_in())
            redirect('/');

        $data['user'] = $this->ion_auth->user()->row();
        $data['page_title'] = 'Thông tin cá nhân';

        if ($this->input->is_ajax_request()) {

            if ($this->input->get('init') != null) {
                echo json_encode($data['user']);
            }

            if ($this->input->method() == 'post') {
                $first_name = $this->input->post('first_name');
                $last_name = $this->input->post('last_name');
                $phone = $this->input->post('phone');
                $pass = $this->input->post('pass');
                $re_pass = $this->input->post('re_pass');

                if ($first_name == null || $last_name == null || $phone == null) {
                    echo json_encode(array('error' => true, 'message' => 'Vui lòng nhập các thông tin bắt buộc'));
                    die;
                }

                if ($pass != null) {
                    if (strlen($pass) < 6 || $pass != $re_pass) {
                        echo json_encode(array('error' => true, 'message' => 'Xác nhận mật khẩu không đúng'));
                        die;
                    }
                }

                $update = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $phone
                );

                if ($pass != null)
                    $update['password'] = $pass;
                $this->ion_auth->update($data['user']->id, $update);

                echo json_encode(array('error' => false));
            }

            die;
        }
        $this->blade->render('auth/profile', $data);
    }
}
