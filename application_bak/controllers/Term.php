<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Term
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Term_model $term_model
 * @property Email_model $email_model
 */
class Term extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($slug, $page = 1)
    {
        $term = $this->term_model->get_by_slug($slug);
        if ($term == null) {
            set_head_title('Error 404');
            set_head_index(true);
            return $this->blade->render('errors/index');
        }

        $GLOBALS['current_term'] = $term;
        $page = $page < 1 ? 1 : $page;

        $title = $term->seo_title == null ? $term->name : $term->seo_title;
        if ($page > 1)
            $title .= ' - trang ' . $page;

        set_head_title($title);
        set_head_des($term->seo_description == null ? $term->description : $term->seo_description);
        set_head_index($term->seo_index);
        set_head_image($term->image_path == null ? null : base_url($term->image_path));

        //  Trang nội dung
        if ($term->type == 'page')
            return $this->blade->set('term', $term)->render('page/index');

        if ($term->type == 'contact')
            return $this->blade->set('term', $term)->render('page/contact');

        //  Danh mục sản phẩm
        if ($term->type == 'product' || $term->type == 'product_page') {
            require APPPATH . 'controllers/Product.php';
            $product = new Product();
            return $product->category($term, $page);
        }

        //  Thành viên
        if ($term->type == 'member') {
            require APPPATH . 'controllers/Team.php';
            $product = new Team();
            return $product->category($term);
        }

        //  Danh mục bài viết
        if ($term->type == 'post' || $term->type == 'post_page') {
            require APPPATH . 'controllers/Article.php';
            $article = new Article();
            return $article->category($term, $page);
        }

        if ($term->type == 'project') {
            require APPPATH . 'controllers/Project.php';
            $project = new Project();
            return $project->category($term, $page);
        }

        #region Danh mục menu tùy chọn theo theme
        global $theme_config;
        if (!isset($theme_config['term']))
            $theme_config['term'] = [];

        foreach ($theme_config['term'] as $item) {
            if ($term->type == $item['id']) {
                if (isset($item['route'])) {
                    $route = $item['route'];

                    if (isset($route['module']) && isset($route['controller'])) {

                        $controller = ucfirst($route['controller']);
                        $file_path = APPPATH . "modules/{$route['module']}/controllers/{$controller}.php";
                        if (is_file($file_path)) {
                            require $file_path;

                            $controller = new $controller();
                            $action = isset($route['action']) ? $route['action'] : 'index';
                            return $controller->$action($term, $page);
                        }
                    }
                }
            }
        }
        #endregion

        return redirect(base_url(), 'location', 301);
    }

    /**
     * Khi có POST submit: form liên hệ
     * @param $slug
     * @return string
     */
    public function POST_index($slug)
    {
        $term = $this->term_model->get_by_slug($slug);
        if ($term == null)
            return $this->blade->render('errors/index');

        if ($term->type == 'contact') {
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
            $email = $this->input->post('email');
            $message = $this->input->post('message');

            if ($name == null || $phone == null || $email == null || $message == null)
                $this->session->set_flashdata('error', 'Vui lòng nhập đủ thông tin liên hệ');
            else {
                $this->load->model('email_model');
                $this->session->set_flashdata('success',
                    'Cảm ơn bạn đã liên hệ. Chúng tôi sẽ email lại cho bạn sớm nhất!.');
            }
        }

        return redirect(current_url(), 'location', 301);
    }
}