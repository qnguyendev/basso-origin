<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Script_model
 * Mã tracking tùy biến (GA, pixel cho các page)
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Script_model extends CI_Model
{
    public function get($position, $page = null)
    {
        $this->db->select('t.code, t.note');
        $this->db->from('tracking_codes t');
        $this->db->join('tracking_code_page o', 't.id = o.tracking_id', 'left');
        $this->db->where('t.active', 1);
        $this->db->where('t.position', $position);

        if ($page != null) {
            if (is_numeric($page))
            {
                $this->db->where("(o.page_id = $page or o.page_id is null)");
                $this->db->where('t.note is null');
            }
        } else
            $this->db->where('o.page_id is null');

        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }
}