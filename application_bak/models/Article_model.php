<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Article_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Article_model extends CI_Model
{
    /**
     * Danh sách bài viết cho sitemap
     */
    public function get_sitemap()
    {
        $this->db->select('t.name, t.slug, t.created_time, t.updated_time');
        $this->db->from('articles t');
        //$this->db->join('articles_terms d', 't.id = d.article_id', 'left');
        //$this->db->join('terms o', 'o.id = d.term_id');
        $this->db->where('t.active', 1);
        return $this->db->get()->result();
    }

    public function get_by_term($term_id = 0, $page = 1, $limit = 15)
    {
        $offset = ($page - 1) * $limit;
        $offset = $offset < 0 ? 0 : $offset;

        $this->db->select('t.*');
        $this->db->from('articles t');
        $this->db->join('articles_terms o', 't.id = o.article_id', 'left');

        if ($term_id != 0)
            $this->db->where('o.term_id', $term_id);

        $this->db->where('t.active', 1);
        $this->db->offset($offset);
        $this->db->limit($limit);

        $this->db->order_by('t.created_time', 'desc');
        return $this->db->get()->result();
    }

    public function count_by_term($term_id = 0)
    {
        $this->db->from('articles t');
        $this->db->join('articles_terms o', 't.id = o.article_id', 'left');

        if ($term_id != 0)
            $this->db->where('o.term_id', $term_id);

        $this->db->where('t.active', 1);
        return $this->db->count_all_results();
    }

    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('articles')->row();
    }

    /**
     * Lấy bài viết cho widget
     * @param $terms
     * @param $order
     * @param $limit
     * @return array
     */
    public function get_widget($terms = null, $order, $limit)
    {
        $this->db->select('t.*, ifnull(d.slug, "blog") as term_slug, d.name as term');
        $this->db->from('articles t');
        $this->db->join('articles_terms o', 't.id = o.article_id', 'left');
        $this->db->join('terms d', 'o.term_id = d.id', 'left');

        if ($terms != null) {
            if (!empty($terms)) {
                $terms = explode(',', $terms);
                $this->db->where_in('o.term_id', $terms);
            }
        }

        $this->db->where('t.active', 1);
        if ($order == 'newest')
            $this->db->order_by('t.created_time', 'desc');
        elseif ($order == 'featured') {
            $this->db->where('t.featured', 1);
            $this->db->order_by('t.created_time', 'desc');
        } elseif ($order == 'views')
            $this->db->order_by('t.views', 'desc');

        $this->db->group_by('t.id');
        $this->db->limit($limit);
        return $this->db->get()->result();
    }

    /**
     * Lấy bài viết cùng danh mục
     * @param int $limit giới hạn số lượng bài viết lấy
     * @param $current_article_id
     * @param $term_id
     * @return array
     */
    public function get_related($term_id = 0, $current_article_id, $limit = 5)
    {
        $this->db->select('t.name, t.slug, t.image_path, t.created_time, t.short_content');
        $this->db->from('articles t');

        if ($term_id != 0) {
            if (is_numeric($term_id)) {
                $this->db->join('articles_terms o', 't.id = o.article_id', 'left');
                $this->db->where('o.term_id', $term_id);
            }
        }

        $this->db->where('t.active', 1);
        $this->db->where('t.id !=', $current_article_id);
        $this->db->limit($limit);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function update_view(int $id)
    {
        $sql = "UPDATE articles SET views = views+1 WHERE id = $id";
        $this->db->query($sql);
    }
}