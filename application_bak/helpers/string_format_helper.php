<?php
if (!defined('BASEPATH'))
    exit('No direct access script allowed');

/**
 * format number to vietnamese currency
 * @param $num
 * @return string
 */
function format_money($num)
{
    return number_format($num, 0, '.', ',') . ' ₫';
}

function format_number($num, $length = 0)
{
    #$num = floatval($num);
    if (floor($num) == $num)
        return $num;
    return number_format($num, $length, '.', '.');
}
