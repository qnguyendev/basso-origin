<?php echo '<?php'  ?> defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_{{$file_name}}
 * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_{{$file_name}} extends CI_Migration
{
    protected $_table_name = "{{$table_name}}";

    public function up()
    {
        $this->dbforge->add_field([
            'id' => ['type' => 'int', 'auto_increment' => true],
            'created_time' => ['type' => 'int']
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->_table_name, TRUE);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}