<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="widget-{{$widget->area}} widget-{{$widget->type}}">
    <?php if ($widget->show_title) : ?>
    <div class="widget-{{$widget->id}}">
        <h4 class="widget-title">{{$widget->title}}</h4>
    </div>
    <?php endif; ?>
    <div class="widget-content">
        @foreach($widget->data as $item)
            <div class="widget-photo-item">
                <a href="{{$item->link == null ? 'javascript:' : $item->link}}">
                    <h4 class="widget-photo-title">{{$item->title}}</h4>
                    <?php if ($item->des != null) : ?>
                    <p class="widget-photo-des">{{$item->des}}</p>
                    <?php endif; ?>
                    <img src="{{$item->image_path}}" alt="{{$item->title}}"/>
                </a>
            </div>
        @endforeach
    </div>
</div>