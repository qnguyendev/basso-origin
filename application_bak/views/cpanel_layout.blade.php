<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$error_msg = $this->session->flashdata('error');
$success_msg = $this->session->flashdata('success');
?><!DOCTYPE html>
<html lang="en">
@include('backend/head')
<body class="layout-fixed">
<div id="main-content">
    <div class="wrapper">
        @include('backend/header')
        @include('backend/sidebar')
        <section class="section-container">
            <div class="content-wrapper">
                <?php if(isset($breadcrumbs)) : ?>
                <div class="row">
                    <div class="col-12 col-sm-7">
                        <ol class="breadcrumb p-0">
                            <li class="breadcrumb-item">
                                <a>
                                    <i class="icon-tag"></i>
                                </a>
                            </li>
                            @foreach($breadcrumbs as $item)
                                <li class="breadcrumb-item">
                                    <?php if (isset($item['url'])) : ?>
                                    <a href="{{$item['url']}}">{{$item['text']}}</a>
                                    <?php else : ?>
                                    <a>{{$item['text']}}</a>
                                    <?php endif; ?>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                    <div class="col-12 col-sm-5 text-right">
                        @yield('top')
                    </div>
                </div>
                <?php endif; ?>
                @if($error_msg != null)
                    <div class="alert alert-warning">
                        {{$error_msg}}
                    </div>
                @endif

                @if ($success_msg != null)
                    <div class="alert alert-success">
                        {{$success_msg}}
                    </div>
                @endif
                @yield('content')
            </div>
        </section>
        <footer class="footer-container">
            <span>&copy; {{date('y')}} by BCE. Rendered in {{$this->benchmark->elapsed_time()}} second(s)</span>
        </footer>
    </div>
    @if(isset($image_uploader))
        @include('backend/image_uploader_modal')
    @endif
    @yield('modal')
    @include('backend/loader')
</div>
@include('backend/change_pass_modal')
</body>
</html>
@include('backend/footer')