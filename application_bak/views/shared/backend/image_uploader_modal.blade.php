<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="image-upload-modal" role="dialog" aria-labelledby="myModalLabel" style="z-index: 999999">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Image uploader
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#paste-image-area"
                           role="tab" aria-controls="paste-image-area" aria-selected="false">
                            <i class="fa fa-copy"></i>
                            Copy & paste
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                           aria-controls="profile" aria-selected="false">
                            <i class="fa fa-external-link-alt"></i>
                            Link bên ngoài
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                           aria-controls="home" aria-selected="true">
                            <i class="fa fa-upload"></i>
                            Từ máy tính
                        </a>
                    </li>
                </ul>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="form-group">
                            <label class="btn btn-block btn-info btn-block" for="image-uploader-file">
                                <i class="fa fa-file-import"></i>
                                Chọn ảnh
                            </label>
                            <!--ko if: image_uploader.is_single()-->
                            <input class="hidden" id="image-uploader-file" type="file"
                                   style="display: none; visibility: hidden;"
                                   data-bind="event: {change: image_uploader.upload.do}"
                                   accept="image/jpg, image/jpeg, image/png, image/gif"/>
                            <!--/ko-->
                            <!--ko if: !image_uploader.is_single()-->
                            <input class="hidden" id="image-uploader-file" type="file"
                                   style="display: none; visibility: hidden;" multiple
                                   data-bind="event: {change: image_uploader.upload.do}"
                                   accept="image/jpg, image/jpeg, image/png, image/gif"/>
                            <!--/ko-->
                        </div>
                    </div>

                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control"
                                       data-bind="value: $root.image_uploader.external.url"
                                       placeholder="Link ảnh https://abc.com/image.jpg..."/>
                                <div class="input-group-append">
                                    <button class="btn btn-sm btn-primary"
                                            data-bind="click: $root.image_uploader.external.add">
                                        <i class="fa fa-plus"></i>
                                        Thêm ảnh
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade show active" id="paste-image-area" role="tabpanel"
                         aria-labelledby="contact-tab">
                        <div style="height: 100px; border: dashed 2px #CCC; margin: 0 -4px" class="mb-4">
                            <h4 class="m-auto text-dark text-center" style="line-height: 100px">
                                PASTE IMAGE HERE
                            </h4>
                        </div>
                    </div>

                    <h4>
                        <i data-bind="visible: image_uploader.is_single()">Bạn chỉ được chọn một ảnh</i>
                        <i data-bind="visible: !image_uploader.is_single()">Bạn có thể chọn nhiều ảnh</i>
                    </h4>

                    <div class="row" data-bind="foreach: $root.image_uploader.result">
                        <div class="col-12 col-sm-4 col-md-3">
                            <div data-bind="class: $root.image_uploader.selected_id.indexOf(id) >= 0 ? 'img-container active' : 'img-container'">
                                <img data-bind="attr: {src: `/timthumb.php?src=${path}&w=200&h=200`}, click: $root.image_uploader.select"
                                     class="img-fluid ie-fix-flex"/>
                                <div class="checkbox c-checkbox">
                                    <label>
                                        <input class="form-check-input" type="checkbox"
                                               data-bind="checked: $root.image_uploader.selected_id,
                                               event: {change: $root.image_uploader.select}, value: id"/>
                                        <span class="fa fa-check"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="card-footer text-left">
                <button class="btn btn-success m-auto" data-bind="click: $root.image_uploader.save">
                    <i class="fa fa-check"></i>
                    LỰA CHỌN
                </button>
            </div>
        </div>
    </div>
</div>