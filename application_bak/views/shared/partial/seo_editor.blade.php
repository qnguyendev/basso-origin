<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card card-default">
    <div class="card-header">
        <div class="card-title">Tối ưu SEO</div>
    </div>
    <div class="card-body" id="seo-editor">
        <div class="form-group">
            <label>Tiêu đề trang</label>
            <input class="form-control" data-bind="value: seo_title, event: {change: seo_preview.change}"/>
        </div>
        <div class="form-group">
            <label>Mô tả trang</label>
            <textarea class="form-control no-resize" rows="5"
                      data-bind="value: seo_description, event: {change: seo_preview.change}"></textarea>
        </div>
        <div class="form-group">
            <label>Cho phép bot Google truy cập</label>
            <select class="form-control" data-bind="value: seo_index">
                <option value="1">Có</option>
                <option value="0">Không</option>
            </select>
        </div>
        <div class="form-group seo-preview" data-bind="with: seo_preview">
            <label>Xem trước SEO</label>
            <div class="seo-title" data-bind="text: title">Tiêu đề hiển thị tìm kiếm SEO</div>
            <div class="seo-link">{{base_url()}}<span data-bind="text: slug"></span></div>
            <div class="seo-des" data-bind="text: des">Nội dung mô tả tóm tắt hiển thị tìm kiếm SEO. Không quá 256 ký
                tự
            </div>
        </div>
    </div>
</div>
