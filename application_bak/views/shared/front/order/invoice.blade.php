<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<table data-bgcolor="BG Color 01" cellpadding="0" cellspacing="0" border="0" align="center">
    <tbody>
    <tr>
        <td valign="top">
            <!-- START order title -->
            <table data-bgcolor="BG Color 03" width="600" height="134" cellpadding="0" cellspacing="0" border="0"
                   bgcolor="#ffffff" align="center">
                <tbody>
                <tr>
                    <td>
                        <table width="520" cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td data-color="Order 01" data-size="Order 01" data-min="5" data-max="50"
                                    class="td-pad10-wide" align="center"
                                    style="font-weight:300; font-size:24px; letter-spacing:0.000em; line-height:26px; color:#1c1c1c; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;"
                                    contenteditable="true">
                                    Số hóa đơn #{{$order->id}}
                                    <br>
                                    <span data-color="Order 02" data-size="Order 02" data-min="5" data-max="50"
                                          style="font-weight:300; font-size:14px; letter-spacing:0.000em; line-height:22px; color:#4c4c4c; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;"
                                          contenteditable="true" class="editable">
												Ngày tạo hóa đơn {{date('H:i d/m/Y', $order->created_time)}}
												</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- END order title -->
            <!-- START address - info -->
            <table data-bgcolor="BG Color 04" style="width: 100%" height="240" cellpadding="0" cellspacing="0"
                   border="0"
                   bgcolor="#203442" align="center">
                <tbody>
                <tr>
                    <td>
                        <table style="width: 90%" cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <!-- START address -->
                                <td class="header-center-pad10" width="270" valign="middle">
												<span data-color="Address 01" data-size="Address 01" data-min="5"
                                                      data-max="50"
                                                      style="font-weight:500; font-size:15px; letter-spacing:0.025em; line-height:22px; color:#ffffff; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
												Người nhận:<br>
                                                    {{$order->name}}
												</span>
                                    <!-- START space -->
                                    <div style="line-height:18px;"> &nbsp;</div>
                                    <!-- END space -->
                                    <span data-color="Address 02" data-size="Address 02" data-min="5" data-max="50"
                                          style="font-weight:500; font-size:13px; letter-spacing:0.025em; line-height:17px; color:#ffffff; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
												Địa chỉ:<br>
												</span>
                                    <span data-color="Address 03" data-size="Address 03" data-min="5" data-max="50"
                                          style="font-weight:400; font-size:12px; letter-spacing:0.025em; line-height:17px; color:#ffffff; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
												{{$order->address}},<br>
                                        {{$order->district}}, {{$order->city}}
												</span>
                                    <!-- START space -->
                                    <div style="line-height:14px;"> &nbsp;</div>
                                    <!-- END space -->
                                    <span data-color="Address 02" data-size="Address 02" data-min="5" data-max="50"
                                          style="font-weight:500; font-size:13px; letter-spacing:0.025em; line-height:17px; color:#ffffff; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
												Điện thoại:
												</span>
                                    <span data-color="Address 03" data-size="Address 03" data-min="5" data-max="50"
                                          style="font-weight:400; font-size:12px; letter-spacing:0.025em; line-height:17px; color:#ffffff; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
												{{$order->phone}}
												</span>
                                    <br>
                                    <span data-color="Address 02" data-size="Address 02" data-min="5" data-max="50"
                                          style="font-weight:500; font-size:13px; letter-spacing:0.025em; line-height:17px; color:#ffffff; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
												Email:
												</span>
                                    <span data-color="Address 03" data-size="Address 03" data-min="5" data-max="50"
                                          style="font-weight:400; font-size:12px; letter-spacing:0.025em; line-height:17px; color:#ffffff; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
												{{$order->email}}
												</span>
                                </td>
                                <!-- END address -->
                                <!-- START info -->
                                <td data-bgcolor="BG Color 05" class="header-center-pad10" width="250" height="191"
                                    bgcolor="#ffffff">
                                    <!-- START info details -->
                                    <table width="208" cellpadding="0" cellspacing="0" border="0" align="center">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <span data-color="Address 04" data-size="Address 04" data-min="5"
                                                      data-max="50"
                                                      style="font-weight:500; font-size:13px; letter-spacing:0.025em; line-height:24px; color:#2d2d2d; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
															{{$payment->name}}
															</span>
                                                <br>
                                                <span data-color="Address 04" data-size="Address 04" data-min="5"
                                                      data-max="50"
                                                      style="font-weight:500; font-size:13px; letter-spacing:0.025em; line-height:24px; color:#2d2d2d; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
															Trạng thái:
															</span>
                                                <span data-color="Address 05" data-size="Address 05" data-min="5"
                                                      data-max="50"
                                                      style="font-weight:400; font-size:13px; letter-spacing:0.025em; line-height:24px; color:#4d4d4d; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
															<?php if ($payment->type != 'cod') : ?>
                                                    {{$order->payment_status == 'waiting' ? 'Chưa thanh toán' : 'Đã thanh toán'}}
                                                    <?php else : ?>
                                                    Đã thanh toán
                                                    <?php endif; ?>
															</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- END info details -->
                                    <!-- START 19px space -->
                                    <table width="220" height="19" cellpadding="0" cellspacing="0" border="0"
                                           align="center">
                                        <tbody>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- END 19px space -->
                                    <!-- START space -->
                                    <table data-bgcolor="Line Color 01" class="table-button220" width="220" height="1"
                                           cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#d1d1d1">
                                        <tbody>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- END 23px space -->
                                    <table width="220" height="23" cellpadding="0" cellspacing="0" border="0"
                                           align="center">
                                        <tbody>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- END 23px space -->
                                    <!-- START amount -->
                                    <table width="208" cellpadding="0" cellspacing="0" border="0" align="center">
                                        <tbody>
                                        <tr>
                                            <td data-color="Address 06" data-size="Address 06" data-min="5"
                                                data-max="50"
                                                style="font-weight:500; font-size:20px; letter-spacing:0.000em; line-height:22px; color:#2d2d2d; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
                                                TỔNG: {{format_money($order->sub_total)}}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- END amount -->
                                </td>
                                <!-- END info -->
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- END address - info -->
            <!-- START item details -->
            <table data-bgcolor="BG Color 06" style="width: 90%; margin-bottom: 30px" cellpadding="0" cellspacing="0"
                   border="0"
                   bgcolor="#ffffff" align="center">
                <tbody>
                <tr>
                    <td valign="top">
                        <!-- START space -->
                        <table width="480" height="42" cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- END space -->
                        <!-- START details title -->
                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td data-color="Details 01" data-size="Details 01" data-min="5" data-max="50"
                                    class="td-pad10-wide" width="335" align="left"
                                    style="font-weight:bold; font-size:15px; letter-spacing:0.000em; line-height:26px; color:#2d2d2d; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
                                    SẢN PHẨM
                                </td>
                                <td data-color="Details 01" data-size="Details 01" data-min="5" data-max="50" width="70"
                                    align="left"
                                    style="font-weight:bold; font-size:15px; letter-spacing:0.000em; line-height:26px; color:#2d2d2d; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
                                    SL
                                </td>
                                <td data-color="Details 01" data-size="Details 01" data-min="5" data-max="50"
                                    class="td-pad10-wide" width="100" align="right"
                                    style="font-weight:bold; font-size:15px; letter-spacing:0.000em; line-height:26px; color:#2d2d2d; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
                                    THÀNH TIỀN
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- END details title -->
                        <!-- START space -->
                        <table height="15" cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- END space -->
                        <table style="width: 100%" cellpadding="0" cellspacing="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td data-bgcolor="Line Color 02" height="1" bgcolor="#d1d1d1"></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- START space -->
                        <table style="width: 100%" height="20" cellpadding="0" cellspacing="0" border="0"
                               align="center">
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- END space -->
                    @foreach($products as $item)
                        <!-- START item 01 -->
                            <table style="width: 100%" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tbody>
                                <tr>
                                    <td data-color="Details 02" data-size="Details 02" data-min="5" data-max="50"
                                        class="td-pad10-wide" width="339" align="left"
                                        style="font-weight:500; font-size:15px; letter-spacing:0.010em; line-height:22px; color:#7c7c7c; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
                                        {{$item->name}}
                                    </td>
                                    <td data-color="Details 02" data-size="Details 02" data-min="5" data-max="50"
                                        width="67"
                                        align="left" valign="top"
                                        style="font-weight:500; font-size:15px; letter-spacing:0.010em; line-height:22px; color:#7c7c7c; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
                                        {{format_number($item->quantity, 0)}}
                                    </td>
                                    <td data-color="Details 02" data-size="Details 02" data-min="5" data-max="50"
                                        class="td-pad10-wide" width="74" align="right" valign="top"
                                        style="font-weight:500; font-size:15px; letter-spacing:0.010em; line-height:22px; color:#7c7c7c; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
                                        {{format_money($item->sub_total)}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- END item 01 -->
                    @endforeach
                    <!-- START space -->
                    </td>
                </tr>
                </tbody>
            </table>
            <?php if (isset($banks)) : if (count($banks) > 0) : ?>
            @foreach($banks as $bank)
                <table style="width: 96%; margin: 0 auto">
                    <tr>
                        <td data-color="Order 01" height="50" data-size="Order 01" colspan="2" data-min="5"
                            data-max="50"
                            class="td-pad10-wide" align="center"
                            style="font-weight:300; font-size:20px; letter-spacing:0.000em; line-height:26px; color:#000; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;"
                            contenteditable="true">
                            {{$bank->bank_name}}
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 14px; border-bottom: solid 1px #CCC;font-family:'Poppins', sans-serif;"
                            height="30">
                            Chủ tài khoản
                        </td>
                        <td style=" border-bottom: solid 1px #CCC;font-family:'Poppins', sans-serif; font-size: 14px"
                            height="30">
                            {{$bank->bank_account}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 14px; border-bottom: solid 1px #CCC;font-family:'Poppins', sans-serif;"
                            height="30">
                            Số tài khoản
                        </td>
                        <td style=" border-bottom: solid 1px #CCC;font-family:'Poppins', sans-serif; font-size: 14px"
                            height="30">
                            {{$bank->bank_number}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 14px; border-bottom: solid 1px #CCC;font-family:'Poppins', sans-serif;"
                            height="30">
                            Chi nhánh
                        </td>
                        <td style=" border-bottom: solid 1px #CCC;font-family:'Poppins', sans-serif; font-size: 14px"
                            height="30">
                            {{$bank->bank_branch}}</td>
                    </tr>
                </table>
            @endforeach
            <?php endif; endif; ?>
        </td>
    </tr>
    <?php if (isset($order_email_footer)) : ?>
    <table data-bgcolor="BG Color 08" width="620" height="176" cellpadding="0" cellspacing="0" border="0"
           bgcolor="#ffffff" align="center">
        <tbody>
        <tr>
            <td>
                <table style="width: 100%" cellpadding="0" cellspacing="0" border="0" align="center">
                    <tbody>
                    <tr>
                        <td data-color="Terms 02" data-size="Terms 02" data-min="5" data-max="50" class="td-pad10-wide"
                            align="left"
                            style="font-weight:300; font-size:12px; letter-spacing:0.025em; line-height:22px; color:#6e6e6e; font-family:'Poppins', sans-serif; mso-line-height-rule: exactly;">
                            {{$order_email_footer}}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <?php endif; ?>
    </tbody>
</table>
