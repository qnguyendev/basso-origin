<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<meta charset="utf-8"/>
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{$title}}</title>
<meta name="description" content="{{$des}}"/>
<meta property="og:title" content="{{$title}}">
<meta property="og:description" content="{{$des}}"/>
<meta property="og:image" content="{{$image}}"/>
<meta property="og:image:url" content="/timthumb.php?src={{$image}}&=600&h=315"/>
<meta property="og:image:width" content="600"/>
<meta property="og:image:height" content="315"/>
@if(is_ssl())
    <meta property="og:image:secure_url" content="/timthumb.php?src={{$image}}&=600&h=315"/>
@endif
<meta property="og:url" content="{{current_url()}}"/>
<meta name="twitter:title" content="{{$title}}"/>
<meta name="twitter:description" content="{{$des}}"/>
<meta name="twitter:image" content="{{$image}}"/>
<meta name="twitter:card" content="summary_large_image"/>
<?php if (!$no_index) : ?>
<meta name="robots" content="noindex, follow"/>
<?php endif; ?>
<link rel="canonical" href="{{current_url()}}"/>
<meta name="viewport"
      content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<?php if(is_array($custom)) : ?>
@foreach($custom as $item)
<meta {{$item['type']}}="{{$item['type_content']}}" content="{{$item['content']}}"/>
@endforeach
<?php endif; ?>