<?php

if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Site_Controller
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Term_model $term_model
 * @property CI_Cart $cart
 * @property Product_model $product_model
 * @property CI_URI $uri
 * @property Option_model $option_model
 */
class Site_Controller extends \MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $GLOBALS['ci'] =& get_instance();
        $this->load->helper('common');
        load_site_config();

        $this->load->library('cart');
        $this->load->helper('site');
        $this->load->helper('display');
        $this->load->helper('widget');
        $this->load->helper('text');
        $this->load->helper('string_format');

        $this->load->model('term_model');
        $this->load->model('product_model');
        $this->load->model('article_model');
        $this->load->model('widget_model');
        $this->load->model('team_model');
        $this->load->model('project_model');
        $this->load->model('script_model');

        //  Danh sách danh mục (cho menu)
        $GLOBALS['terms'] = $this->term_model->get(1);

        #region Giỏ hàng
        $cart = array();
        foreach ($this->cart->contents() as $item) {
            $object = new stdClass();
            foreach ($item as $key => $value)
                $object->$key = $value;

            $object->name = $object->_name;
            array_push($cart, $object);
        }

        $GLOBALS['cart'] = $cart;
        #endregion

        #region Thêm item vào giỏ hàng ($_GET)
        if ($this->input->get('cart') != null) {
            switch ($this->input->get('cart')) {

                //  Xóa sản phẩm khỏi giỏ hàng
                case 'remove':
                    $rowid = $this->input->get('rowid');
                    $this->cart->remove($rowid);
                    break;

                //  Thêm sp vào giỏ hàng
                case 'add':
                    $qty = $this->input->get('qty');
                    $id = $this->input->get('id');
                    $prod = $this->product_model->get_by_id($id);

                    if ($prod != null) {
                        $data = array(
                            'id' => $prod->id,
                            'qty' => intval($qty),
                            'price' => $prod->price,
                            'sale_price' => null,
                            'name' => $prod->name,
                            'slug' => $prod->slug,
                            'image' => null
                        );

                        $images = $this->product_model->get_images($prod->id);
                        if (count($images) > 0)
                            $data['image'] = $images[0]->path;

                        if (has_sale_price($prod))
                            $data['sale_price'] = $prod->sale_price;

                        $attribute = $this->input->get('attribute');
                        if ($attribute != null) {
                            $values = $this->product_model->get_attribute_values($attribute);
                            usort($values, function ($item1, $item2) {
                                return $item1->id <=> $item2->id;
                            });

                            $data['attributes'] = $values;
                            $data['id'] = $prod->id . '-' . join('-', array_column($values, 'id'));
                        }

                        $this->cart->insert($data);
                        $this->session->set_flashdata('added_to_cart', "Đã thêm sản phẩm <b>{$prod->name}</b> vào giỏ hàng");
                    }
                    break;
            }

            //  Nếu thêm vào giỏ hàng bằng ajax
            if ($this->input->is_ajax_request()) {
                $redirect = false;
                $config = $this->option_model->get('redirect_to_cart');
                if ($config != null)
                    $redirect = boolval($config->value);

                echo json_encode(array('redirect' => $redirect, 'url' => CART_URL));
                die;
            }

            return redirect(current_url(), 'location', 301);
        }
        #endregion

        #region Mã utm
        foreach ($_GET as $key => $value) {
            if (strpos($key, 'utm_') !== false)
                setcookie($key, $value, time() + 30 * 24 * 3600, '/');
        }
        #endregion
    }

    public function _remap($method)
    {
        $param_offset = 2;
        if ($this->input->method(true) != 'GET') {
            if (!$this->input->is_cli_request())
                $method = $this->input->method(true) . '_' . $method;
        } else {
            if ($this->input->is_ajax_request())
                $method = 'GET_' . $method;
        }

        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        if (method_exists($this, $method))
            return call_user_func_array(array($this, $method), $params);

        show_error("$method can not found");
    }
}