<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Created by PhpStorm.
 * User: sodaubai
 * Date: 12/19/2018
 * Time: 12:02 PM
 */

use LeonardoTeixeira\Pushover\Client;
use LeonardoTeixeira\Pushover\Message;
use LeonardoTeixeira\Pushover\Exceptions\PushoverException;
use \LeonardoTeixeira\Pushover\Sound;
use \LeonardoTeixeira\Pushover\Priority;

class Pushover
{
    private $client;
    private $message;
    private $callback_url;
    private $enable = false;

    public function __construct()
    {
        require './theme/config.php';
        if (!isset($theme_config))
            $theme_config = [];

        if (isset($theme_config['pushover'])) {
            $pushover = $theme_config['pushover'];
            $this->client = new Client($pushover['user_key'], $pushover['api_token']);

            if (isset($pushover['callback_url'])) {
                if ($pushover['callback_url'] != null)
                    $this->callback_url = $pushover['callback_url'];
            }

            if (isset($pushover['enable'])) {
                if (is_bool($pushover['enable'])) {
                    $this->enable = $pushover['enable'];
                }
            }
        }
    }

    public function set_message(array $data = null)
    {
        $this->message = new Message();
        $this->message->setPriority(Priority::HIGH);
        $this->message->setSound(Sound::CASHREGISTER);
        $this->message->setHtml(true);
        $this->message->setDate(new DateTime());

        if ($this->callback_url != null)
            $this->message->setCallback($this->callback_url);

        if (isset($data['message']))
            $this->message->setMessage($data['message']);

        if (isset($data['title']))
            $this->message->setTitle($data['title']);

        if (isset($data['url']))
            $this->message->setUrl($data['url']);
    }

    public function send()
    {
        if ($this->message == null) return;
        if (!$this->enable) return;

        try {
            $this->client->push($this->message);
        } catch (PushoverException $e) {
            echo $e->getMessage();
        }
    }
}