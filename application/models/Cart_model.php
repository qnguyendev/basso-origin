<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Cart_model extends CI_Model
{
    private $_table = 'carts';
    /**
     * Thêm đơn hàng
     * @param $name
     * @param $phone
     * @param $email
     * @param $note
     * @param $address
     * @param $city_id
     * @param $district_id
     * @param $payment_id
     * @param $payment_type
     * @param $cart biến giỏ hàng của CI
     * @return int
     */
    public function create($id,$rowid,$content,$customer_id)
    {
        $data = [
            'id' => $id,
            'rowid' => $rowid,
            'content' => $content,
			'customer_id' => $customer_id
        ];

        $this->db->insert($this->_table, $data);

    }


    public function get_by_customer($customer_id)
    {
        $this->db->select('t.*');
        $this->db->from('carts t');
        $this->db->where('t.customer_id', $customer_id);
        return $this->db->get()->result();
    }

    public function delete_by_customer($customer_id)
    {
		$this->db->trans_begin();
        $this->db->where('customer_id', $customer_id);
        $this->db->delete($this->_table);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}