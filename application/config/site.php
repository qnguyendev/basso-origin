<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
$config['theme_config'] = [
    'name' => 'default',
    'sidebars' => [
        [
            'name' => 'Trang chủ CRM',
            'icon' => 'now-ui-icons design_app',
            'module' => 'admin',
            'controller' => 'Dashboard',
            'action' => 'index',
            'roles' => ['admin', 'editor', 'shop_manager', 'order_manager'],
            'active' => true
        ],
        [
            'name' => 'Danh mục',
            'icon' => 'now-ui-icons design_bullet-list-67',
            'module' => 'admin',
            'controller' => 'term',
            'action' => ['index', 'create', 'edit'],
            'roles' => ['admin', 'editor'],
            'active' => true
        ],
        [
            'name' => 'Bài viết',
            'icon' => 'now-ui-icons design-2_ruler-pencil',
            'module' => 'admin',
            'controller' => 'article',
            'action' => ['index', 'create', 'edit'],
            'roles' => ['admin', 'editor'],
            'active' => true
        ],
        [
            'name' => 'Thành viên',
            'icon' => 'now-ui-icons users_single-02',
            'module' => 'admin',
            'controller' => 'member',
            'action' => ['index', 'create', 'edit'],
            'roles' => ['admin', 'editor'],
            'active' => false
        ],
        [
            'name' => 'Dự án',
            'icon' => 'now-ui-icons ui-1_calendar-60',
            'module' => 'admin',
            'controller' => 'project',
            'action' => ['index', 'create', 'edit'],
            'roles' => ['admin', 'editor'],
            'active' => false
        ],
        [
            'name' => 'Sản phẩm',
            'icon' => 'now-ui-icons design_app',
            'roles' => ['admin', 'shop_manager'],
            'active' => true,
            'items' => [
                [
                    'name' => 'Tất cả sản phẩm',
                    'module' => 'admin',
                    'controller' => 'product',
                    'action' => ['index', 'edit'],
                    'roles' => ['admin', 'shop_manager'],
                    'active' => true
                ],
                [
                    'name' => 'Thêm sản phẩm',
                    'module' => 'admin',
                    'controller' => 'product',
                    'action' => 'create',
                    'roles' => ['admin', 'shop_manager'],
                    'active' => true
                ],
                [
                    'name' => 'Đặc tính sản phẩm',
                    'module' => 'admin',
                    'controller' => 'attribute',
                    'action' => 'index',
                    'roles' => ['admin', 'shop_manager'],
                    'active' => true
                ],
                [
                    'name' => 'Thương hiệu',
                    'module' => 'admin',
                    'controller' => 'brand',
                    'action' => 'index',
                    'roles' => ['admin', 'shop_manager'],
                    'active' => true
                ]
            ]
        ],
        [
            'name' => 'Đơn hàng',
            'icon' => 'now-ui-icons design_vector',
            'roles' => ['admin', 'order_manager'],
            'active' => true,
            'items' => [
                [
                    'name' => 'Tất cả đơn hàng',
                    'module' => 'admin',
                    'controller' => 'order',
                    'action' => ['index', 'detail'],
                    'roles' => ['admin', 'order_manager'],
                    'active' => true
                ],
                [
                    'name' => 'Quản lý nhãn',
                    'module' => 'admin',
                    'controller' => 'order_setting',
                    'action' => 'label',
                    'roles' => ['admin', 'order_manager'],
                    'active' => true
                ],
                [
                    'name' => 'Hình thức thanh toán',
                    'module' => 'admin',
                    'controller' => 'order_setting',
                    'action' => 'payment',
                    'roles' => ['admin'],
                    'active' => true
                ],
                [
                    'name' => 'Cấu hình đặt hàng',
                    'module' => 'admin',
                    'controller' => 'order_setting',
                    'action' => 'index',
                    'roles' => ['admin'],
                    'active' => true
                ]
            ]
        ],
        [
            'name' => 'Quản trị website',
            'icon' => 'now-ui-icons business_globe',
            'roles' => ['admin'],
            'active' => true,
            'items' => [
                [
                    'name' => 'Slide trang chủ',
                    'module' => 'admin',
                    'controller' => 'slider',
                    'action' => 'index',
                    'roles' => ['admin'],
                    'active' => true
                ],
                [
                    'name' => 'Cấu hình website',
                    'url' => 'config',
                    'module' => 'admin',
                    'controller' => 'config',
                    'action' => 'index',
                    'roles' => ['admin'],
                    'active' => true
                ],
                [
                    'name' => 'Cấu hình SEO',
                    'module' => 'admin',
                    'controller' => 'config',
                    'action' => 'seo',
                    'roles' => ['admin'],
                    'active' => true
                ],
                [
                    'name' => 'Widgets',
                    'module' => 'admin',
                    'controller' => 'widget',
                    'action' => ['index', 'edit', 'create'],
                    'roles' => ['admin'],
                    'active' => true
                ],
                [
                    'name' => 'Mã javascript',
                    'module' => 'admin',
                    'controller' => 'tracking_code',
                    'action' => ['index', 'edit', 'save'],
                    'roles' => ['admin'],
                    'active' => true
                ]
            ]
        ],
        [
            'name' => 'Tài khoản',
            'icon' => 'now-ui-icons users_circle-08',
            'module' => 'admin',
            'controller' => 'user',
            'action' => 'index',
            'roles' => ['admin'],
            'active' => true
        ]
    ],
    'terms' => [
        [
            'name' => 'Trang chủ',
            'id' => 'Dashboard',
            'active' => true
        ],
        [
            'name' => 'Bài viết',
            'id' => 'post',
            'active' => true
        ],
        [
            'name' => 'Trang nội dung',
            'id' => 'page',
            'active' => true
        ],
        [
            'name' => 'Sản phẩm',
            'id' => 'product',
            'active' => true
        ],
        [
            'name' => 'Trang liên hệ',
            'id' => 'contact',
            'active' => true
        ],
        [
            'name' => 'Liên kết tùy chỉnh',
            'id' => 'external_link',
            'active' => true
        ],
        [
            'name' => 'Dự án',
            'id' => 'project',
            'active' => false
        ]
    ],
    'widgets' => [
        [
            'name' => 'Danh mục bài viết',
            'view' => 'article_terms',
            'id' => 'article_terms',
            'active' => true
        ],
        [
            'name' => 'Bài viết',
            'view' => 'articles',
            'id' => 'articles',
            'active' => true
        ],
        [
            'name' => 'Liên kết',
            'view' => 'links',
            'id' => 'links',
            'active' => true
        ],
        [
            'name' => 'Ảnh (banner/slide)',
            'view' => 'photos',
            'id' => 'photos',
            'active' => true
        ],
        [
            'name' => 'Danh mục sản phẩm',
            'view' => 'product_terms',
            'id' => 'product_terms',
            'active' => true
        ],
        [
            'name' => 'Sản phẩm',
            'view' => 'products',
            'id' => 'products',
            'active' => true
        ],
        [
            'name' => 'Mã code (javascript/html)',
            'view' => 'script',
            'id' => 'script',
            'active' => true
        ],
        [
            'name' => 'Văn bản',
            'view' => 'text',
            'id' => 'text',
            'active' => true
        ],
        [
            'name' => 'Thành viên',
            'view' => 'member',
            'id' => 'member',
            'active' => false
        ],
        [
            'name' => '',
            'id' => 'icons',
            'view' => 'icons',
            'active' => false
        ]
    ],
    'widget_positions' => [
        [
            'id' => 'top',
            'name' => 'Hiển thị đầu website'
        ],
        [
            'id' => 'Dashboard',
            'name' => 'Trang chủ'
        ],
        [
            'id' => 'footer-1',
            'name' => 'Footer cột 1',
            'description' => 'Chân website, cột số 1'
        ],
        [
            'id' => 'footer-2',
            'name' => 'Footer cột 2',
            'description' => 'Chân website, cột số 2'
        ],
        [
            'id' => 'footer-3',
            'name' => 'Footer cột 3',
            'description' => 'Chân website, cột số 3'
        ],
        [
            'id' => 'footer-4',
            'name' => 'Footer cột 4',
            'description' => 'Chân website, cột số 4'
        ],
        [
            'id' => 'pre-footer',
            'name' => 'Bên trên footer',
            'description' => 'Trên form đăng ký email'
        ]
    ]
];