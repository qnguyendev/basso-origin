<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="widget-{{$widget->area}} widget-{{$widget->type}}">
    <?php if ($widget->show_title) : ?>
    <div class="widget-{{$widget->id}}">
        <h4 class="widget-title">{{$widget->title}}</h4>
    </div>
    <?php endif; ?>
    <div class="widget-content">
        <ul>
            @foreach($widget->data as $item)
                <li>
                    <a href="{{$item->link}}" {{$item->follow == 'nofollow' ? 'rel="nofollow"' : ''}}
                            {{$item->new_tab == 1 ? 'target="_blank"' : ''}}>
                        {{$item->text}}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>