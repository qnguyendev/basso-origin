<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($data as $item)
        <url>
            <loc>{{base_url($item->slug)}}</loc>
            <priority>0.3</priority>
        </url>
    @endforeach
</urlset>
