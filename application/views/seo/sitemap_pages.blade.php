<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($data as $item)
        <url>
            <loc>{{base_url($item->slug)}}</loc>
            <priority>
                @if(in_array($item->slug, ['mua-hang-my', 'mua-hang-ebay', 'mua-hang-amazon']))
                    0.9
                @else
                    0.3
                @endif
            </priority>
        </url>
    @endforeach
</urlset>
