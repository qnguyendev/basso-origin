<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('amzsearch_layout')

@section('right-content')

<?php if(isset($_GET['url']) && $_GET['url'] != "") { ?>

<?php
/* $search_text = str_replace(" ", "+", trim($_GET['field-keywords'])); */
$search_text = trim($_GET['url']);

// kvstore API url
$url = 'https://amzproductapi.com/apiv5/';

// Collection object
/* $data = [
  'ItemIds' => ['B07K1RZWMC'],
  'Resources' => ["Images.Primary.Large", "ItemInfo.Title", "Offers.Summaries.LowestPrice", "Offers.Summaries.HighestPrice", "Offers.Listings.Price"],
  'Operation' => 'GetItems', //GetBrowseNodes,GetVariations,GetItems,SearchItems
]; */

$data = [
	"Keywords" => $search_text,  //"water bottle",
	'Resources' => ["Images.Primary.Large", "ItemInfo.Title", "Offers.Summaries.LowestPrice", "Offers.Summaries.HighestPrice", "Offers.Listings.Price", "ItemInfo.Features"],
	"Operation" => "SearchItems"
];
 
// Initializes a new cURL session
$curl = curl_init($url);
// Set the CURLOPT_RETURNTRANSFER option to true
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// Set the CURLOPT_POST option to true for POST request
curl_setopt($curl, CURLOPT_POST, true);
// Set the request data as JSON using json_encode function
curl_setopt($curl, CURLOPT_POSTFIELDS,  json_encode($data));
// Set custom headers for RapidAPI Auth and Content-Type header
curl_setopt($curl, CURLOPT_HTTPHEADER, [
  'Authorization: key-aa1cc26a1a4349d8858ab688',
  'Content-Type: application/json'
]);
// Execute cURL request with all previous settings
$response = curl_exec($curl);
// Close cURL session
curl_close($curl);

$result = json_decode($response);
$product_foundProductDetails = $result->SearchResult->Items;

?>

<?php for($i=0; $i<count($product_foundProductDetails); $i++) { ?>
<div class="row basso_search_result">
	<?php  if(isset($product_foundProductDetails[$i]->Images->Primary->Large)) { ?>
	<div class="col-md-12 col-lg-3">
		<a href="https://www.amazon.com/dp/<?php echo $product_foundProductDetails[$i]->ASIN; ?>" class="s_image_fixed" target="_blank"><img src = "<?php echo $product_foundProductDetails[$i]->Images->Primary->Large->URL; ?>" class="s_image" /></a>
	</div>
	<?php } ?>
	
	<div class="col-md-12 col-lg-9">
		<!-- <a class="a-link-normal a-text-normal s_title" href="https://www.amazon.com/dp/<?php //echo $product_foundProductDetails[$i]->ASIN; ?>" target="_blank"> -->
		<a class="a-link-normal a-text-normal s_title" onClick='getPrice("#btn_<?php echo $product_foundProductDetails[$i]->ASIN; ?>"); return false;' target="_blank">
			<b style="color: #00354E;"><?php echo $product_foundProductDetails[$i]->ItemInfo->Title->DisplayValue; ?></b>
		</a>
		
		
		<div class="s_stars">
			<br />
			<?php 
				if(isset($product_foundProductDetails[$i]->ItemInfo->Features)) {
					$feature_arr = $product_foundProductDetails[$i]->ItemInfo->Features->DisplayValues;
					for($j=0; $j<count($feature_arr); $j++) {
						echo "- ".$feature_arr[$j]."<br />";
						if($j == 4) {
							echo "...";
							break;
						}
					}
				}
			?>
			<!--
			<div class="a-row a-size-small">
				<span aria-label="3.7 out of 5 stars">
					<span class="a-declarative" data-action="a-popover" data-a-popover="{&quot;max-width&quot;:&quot;700&quot;,&quot;closeButton&quot;:false,&quot;position&quot;:&quot;triggerBottom&quot;,&quot;url&quot;:&quot;/review/widgets/average-customer-review/popover/ref=acr_search__popover?ie=UTF8&amp;asin=B01N4R20RS&amp;ref=acr_search__popover&amp;contextId=search&quot;}">
						<a href="javascript:void(0)" class="a-popover-trigger a-declarative"><i class="a-icon a-icon-star-small a-star-small-3-5 aok-align-bottom">
							<span class="a-icon-alt"><?php echo $product_foundProductDetails[$i]->productRating; ?></span></i><i class="a-icon a-icon-popover"></i>
						</a>
					</span>
				</span>
				<span aria-label="<?php echo $product_foundProductDetails[$i]->countReview; ?>">
					<a class="a-link-normal" href="/Apple-iPhone-Unlocked-Quad-Core-Smartphone/dp/B01N4R20RS/ref=sr_1_1?keywords=iphone&amp;qid=1579252958&amp;sr=8-1#customerReviews">
						<span class="a-size-base" dir="auto"><?php echo $product_foundProductDetails[$i]->countReview; ?></span>
					</a>
				</span>
			</div>
			-->
		</div>
		
		<?php  if(isset($product_foundProductDetails[$i]->Offers->Listings[0])) { ?>
		<div class="s_price" style="padding-bottom: 10px;"><span class="a-price-symbol"></span><?php echo $product_foundProductDetails[$i]->Offers->Listings[0]->Price->DisplayAmount; ?></div>
		<?php } ?>
		
		<form method="post" onsubmit="return submit_crawl_search('https://www.amazon.com/dp/<?php echo $product_foundProductDetails[$i]->ASIN; ?>?ref_=nav_em_T1_0_4_9_2__k_ods_tab_kk')">
			<div class="action">
				<button id="btn_<?php echo $product_foundProductDetails[$i]->ASIN; ?>" type="submit" class="btn button">XEM BÁO GIÁ</button>
			</div>
		</form>
	</div>
</div>
<?php } ?>
<?php } else { ?>
	<p>Không có sản phẩm nào được tìm thấy</p>
<?php }?>


@endsection

@section('head')
    <style>
        .order-page {
            min-height: 700px 
        }
    </style>
@endsection

<script>
function getPrice(id) {
	jQuery(id).trigger("click");
}
</script>