<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<meta charset="utf-8"/>
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{$title}}</title>
<meta name="description" content="{{$des}}"/>
<meta property="og:title" content="{{$title}}">
<meta property="og:description" content="{{$des}}"/>
<meta property="og:image" content="{{$image}}"/>
<meta property="og:image:url" content="/timthumb.php?src={{$image}}&=600&h=315"/>
<meta property="og:image:width" content="600"/>
<meta property="og:image:height" content="315"/>
@if(is_ssl())
    <meta property="og:image:secure_url" content="/timthumb.php?src={{$image}}&=600&h=315"/>
@endif
<meta property="og:url" content="{{current_url()}}"/>
<meta name="twitter:title" content="{{$title}}"/>
<meta name="twitter:description" content="{{$des}}"/>
<meta name="twitter:image" content="{{$image}}"/>
<meta name="twitter:card" content="summary_large_image"/>
<?php if (!$no_index) : ?>
<meta name="robots" content="noindex, follow"/>
<?php endif; ?>
<?php
$CI =& get_instance();
$rsegments_arr = $CI->uri->rsegment_array();
//echo "<pre>";
//print_r( $CI->uri);
//echo "</pre>";
//        echo "<pre>";
//        print_r($pagination);
//        echo "</pre>";

if (isset($rsegments_arr[1]) && ($rsegments_arr[1] == "term") && isset($rsegments_arr[4])) :?>
<link rel="canonical" href="<?php echo $CI->config->site_url($rsegments_arr[3]);?>"/>
<?php if ($rsegments_arr[4] > 1): ?>
<link rel="prev" href="<?php echo $CI->config->site_url($rsegments_arr[3]."/".($rsegments_arr[4]-1));?>"/>
<?php endif; ?>
<?php if ($rsegments_arr[4] < $pagination->total_page): ?>
<link rel="next" href="<?php echo $CI->config->site_url($rsegments_arr[3]."/".($rsegments_arr[4]+1));?>"/>
<?php endif; ?>
<?php else: ?>
<link rel="canonical" href="{{current_url()}}"/>
<?php endif;?>
<meta name="viewport"
      content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<?php if(is_array($custom)) : ?>
@foreach($custom as $item)
    <meta {{$item['type']}}="{{$item['type_content']}}" content="{{$item['content']}}"/>
@endforeach
<?php endif; ?>
