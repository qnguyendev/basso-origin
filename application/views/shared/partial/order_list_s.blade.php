<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$index = 1;
?>
@layout('search_layout')
@section('right-content')
    <div class="row">
        <div class="col-md-6 col-12">
            <table class="table table-striped table-bordered">
                <tr>
                    <th width="200">Mã đơn hàng</th>
                    <td>{{$order->order_code}}</td>
                </tr>
                <tr>
                    <th>Ngày đặt hàng</th>
                    <td>{{date('d/m/Y', $order->created_time)}}</td>
                </tr>
                <tr>
                    <th>Tổng giá trị đơn hàng</th>
                    <td>{{format_money($order->sub_total + is_enull($order->discount_amount, 0))}}</td>
                </tr>
                <tr>
                    <th>Trạng thái đơn hàng</th>
                    <td>
                        @if($order->status == CustomerOrderStatus::COMPLETED)
                            Đã hoàn tất
                        @elseif($order->status == CustomerOrderStatus::CANCELLED)
                            Đã hủy
                        @else
                            Đã tiếp nhận
                        @endif
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-6 col-12">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Hình thức thanh toán</th>
                    <td>{{$order->payment_method}}</td>
                </tr>
                <tr>
                    <th>Trạng thái thanh toán</th>
                    <td>{{$order->total_paid == 0 ? 'Chưa đặt cọc' : 'Đã đặt cọc'}}</td>
                </tr>
                <tr>
                    <th> Số tiền đã thanh toán</th>
                    <td>{{format_money($order->total_paid)}}</td>
                </tr>
                <tr>
                    <th>Số tiền còn lại</th>
                    <td>{{format_money($order->sub_total - $order->total_paid)}}</td>
                </tr>
            </table>
        </div>
    </div>

   
                    
    <div class="table-responsive mb-4">
        <table class="table table-bordered account-order-table">
            <thead>
            <tr>
                <th class="text-center">STT</th>
                <th class="text-center" width="100">Hình ảnh</th>
                <th class="text-center">Tên sản phẩm</th>
                <th class="text-center" width="100">Giá SP</th>
                <th class="text-center">Số lượng</th>
                <th class="text-center">Phụ thu</th>
                <th class="text-center">Phí vận chuyển</th>
                <th class="text-center" width="120">Tổng giá SP</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td class="text-center">{{$index}}</td>
                    <td class="text-center p-1">
                        @if($item->image_path != null)
                            <img src="{{$item->image_path}}"
                                 class="img-fluid mx-auto d-block" alt="">
                        @else
                            <img src="/assets/img/no-image-available.jpg" width="80" height="80"/>
                        @endif
                    </td>
                    <td>
                        <a href="{{$item->link}}" target="_blank">
                            {{$item->name}}
                        </a>
                        @if($item->variations != null)
                            @if(!empty($item->variations))
                                <br/>
                                <?php $item->variations = json_decode($item->variations, true);?>
                                @foreach($item->variations as $var)
                                    <strong>{{$var['name']}}</strong>: {{$var['value']}}<br/>
                                @endforeach
                            @endif
                        @endif
                    </td>
                    <td class="text-center">
                        {{$order->currency_symbol}}
                        {{number_format($item->price  + $item->web_shipping_fee, 2, '.', ',')}}
                    </td>
                    <td class="text-center">{{$item->quantity}}</td>
                    <td class="text-center">{{format_money($item->term_fee)}}</td>
                    <td class="text-center">{{format_money($item->shipping_fee)}}</td>
					<?php
					$item_total = $item->quantity * $item->price * $order->currency_rate +
						$item->term_fee * $item->quantity +
						$item->shipping_fee * $item->quantity;
					?>
					<td class="text-center">{{format_money($item_total)}}</td>
                    <?php
                    /* $item_total = $item->quantity * $item->price * $order->currency_rate +
                        $item->term_fee * $item->quantity +
                        $item->quantity * $item->weight * 260000; 
                    <td class="text-center">{{format_money($item->sub_total + $item->shipping_fee)}}</td>*/
                    ?>
                </tr>
                <?php $index++; ?>
            @endforeach
        <tr>
            <td align="right" colspan="7"
                style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
                <strong>Giảm giá</strong>
            </td>
            <td style=" padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center"
                align="center">
                <strong>{{format_money($order->discount_amount)}}</strong>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="7"
                style=" padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
                <?php 
				$percent_amount = round(100*$order->total_paid/$order->sub_total);
				if($percent_amount >= 50 && $percent_amount < 80){ ?>
				<strong>Phí dịch vụ (+4%)</strong>
				<?php } elseif($percent_amount >= 80 && $percent_amount < 100){ ?>
				<strong>Phí dịch vụ (+2%)</strong>
				<?php } else { ?>
				<strong>Phí dịch vụ</strong>
				<?php } ?>
            </td>
            <td style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center"
                align="center">
                <strong>{{format_money($order->discount_amount)}}</strong>
            </td>
        </tr>
		<?php if (strpos($order->website, 'ebay') !== false) { ?>
		<tr>
			<td align="right" colspan="7"
				style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
				<strong>Phí vận chuyển quốc tế</strong>
			</td>
			<td style=" padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center"
				align="center">
				<strong>{{format_money($order->world_shipping_fee)}}/kg</strong>
			</td>
		</tr>
		<?php } ?>
		
        <tr>
            <td align="right" colspan="7"
                style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
                <strong>Phí vận chuyển nội địa</strong>
            </td>
            <td style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center">
                <strong>-</strong>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="7"
                style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-top: none">
                <strong>Tổng giá trị đơn hàng</strong>
            </td>
            <td style="padding: 7px; color: #333333; border: solid 1px #dee2e6; border-left: none; border-top: none; text-align: center"
                align="center">
                <strong>{{format_money($order->sub_total)}}</strong>
            </td>
        </tr>
            </tbody>
        </table>
    </div>
@endsection