<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <title>BCE Digital Marketing Solutions CMS</title>
    <link rel="stylesheet" href="/assets/vendor/@fortawesome/fontawesome-free/css/brands.css"/>
    <link rel="stylesheet" href="/assets/vendor/@fortawesome/fontawesome-free/css/regular.css"/>
    <link rel="stylesheet" href="/assets/vendor/@fortawesome/fontawesome-free/css/solid.css"/>
    <link rel="stylesheet" href="/assets/vendor/@fortawesome/fontawesome-free/css/fontawesome.css"/>
    <link rel="stylesheet" href="/assets/vendor/simple-line-icons/css/simple-line-icons.css"/>
    <link rel="stylesheet" href="/assets/css/bootstrap.css"/>
    <link rel="stylesheet" href="/assets/vendor/chosen-js/chosen.css"/>
    <link rel="stylesheet" href="/assets/css/app.css" id="maincss"/>
    <link rel="stylesheet" href="/assets/css/theme-e.css"/>
    <link rel="stylesheet" href="/assets/vendor/datetimepicker-master/jquery.datetimepicker.css"/>
    <link rel="stylesheet" href="/assets/vendor/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css"/>
    <link rel="stylesheet" href="/assets/vendor/select2/dist/css/select2.css"/>
    <link rel="stylesheet" href="/assets/css/style.css"/>
    @yield('css')
    @yield('head')
</head>
