<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">Lỗi - 404</div>
        </div>
        <div class="card-body text-center text-danger">
            <h3>Bạn không có quyền truy cập chức năng này</h3>
        </div>
    </div>
@endsection