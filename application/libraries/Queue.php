<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

use \Pheanstalk\Pheanstalk;

class Queue
{
    private $pheanstalk;
    private $tube_prefix;
    private $enable = false;

    const NOTI = 'noti';
    const EMAIL = 'email';
    const SMS = 'sms';

    public function __construct()
    {
        require './theme/config.php';
        if (!isset($theme_config))
            $theme_config = [];

        $server = null;
        $port = 0;

        if (isset($theme_config['pheanstalk'])) {
            $pheanstalk = $theme_config['pheanstalk'];
            $server = $pheanstalk['server'];
            $port = $pheanstalk['port'];

            if (isset($pheanstalk['tube_prefix']))
                $this->tube_prefix = $pheanstalk['tube_prefix'];

            if (isset($pheanstalk['enable'])) {
                if (is_bool($pheanstalk['enable']))
                    $this->enable = $pheanstalk['enable'];
            }
        }

        if ($this->enable) {
            try {
                $this->pheanstalk = new Pheanstalk($server, $port);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

    public function tube($name = 'email')
    {
        if ($this->enable) {
            try {
                $this->pheanstalk->useTube($this->tube_prefix . $name);
                return $this;
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        return null;
    }

    public function add_job($data = null)
    {
        if ($this->enable) {
            if ($data != null) {
                if (!is_string($data))
                    $data = json_encode($data);

                try {
                    $this->pheanstalk->put($data);
                    return $this;
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                }
            }
        }
    }
}