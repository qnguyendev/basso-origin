<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_crawled_amz_variations * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_crawled_amz_variations extends CI_Migration
{
    protected $_table_name = "crawled_amz_variations";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'condition' => ['null' => true, 'type' => 'varchar', 'constraint' => 64]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}