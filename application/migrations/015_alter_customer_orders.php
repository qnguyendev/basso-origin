<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_customer_orders * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_customer_orders extends CI_Migration
{
    protected $_table_name = "customer_orders";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'payment_method' => ['type' => 'varchar', 'constraint' => 32, 'null' => true]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}