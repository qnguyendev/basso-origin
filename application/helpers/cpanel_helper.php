<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Số kết quản phân trang
 */
define('LOGIN_URL', base_url('admin/auth/login'));
define('LOGOUT_URL', base_url('admin/auth/logout'));

if (!function_exists('layout_file')) {
    /**
     * thư mục chứa theme
     * @return string
     */
    function layout_file()
    {
        global $ci;
        $theme = $ci->config->item('theme');
        return './theme' . DIRECTORY_SEPARATOR . $theme . DIRECTORY_SEPARATOR . 'layout.json';
    }
}

if (!function_exists('has_sale_price')) {
    /**
     * Kiểm tra sản phẩm giảm giá hay không
     * @param $prod
     * @return bool
     */
    function has_sale_price($prod)
    {
        if ($prod->sale_price != null) {
            if ($prod->sale_from != null || $prod->sale_to != null) {
                $prod->sale_from = $prod->sale_from == null ? 0 : $prod->sale_from;
                $prod->sale_to = $prod->sale_to == null ? time() + 1 : $prod->sale_to;

                return $prod->sale_from <= time() && $prod->sale_to >= time();
            }

            return true;
        }

        return false;
    }
}

if (!function_exists('system_roles')) {
    /**
     * Phân quyền hệ thống
     * @return array
     */
    function system_roles()
    {
        $roles = [
            'system' => [
                'backup' => [
                    'index' => ['admin']
                ],
                'email' => [
                    'index' => ['admin']
                ]
            ]
        ];

        if (is_file('./theme/role.php')) {
            require './theme/role.php';
            if (isset($role_config)) {
                if (is_array($role_config))
                    $roles = array_merge($roles, $role_config);
            }
        }

        return $roles;
    }
}

if (!function_exists('has_role')) {
    function has_role($sidebar_item)
    {
        global $user_roles;
        $roles = system_roles();

        if (isset($sidebar_item['items'])) {
            $valid_permission = false;
            foreach ($sidebar_item['items'] as $item) {
                $valid_permission = has_role($item);
                if ($valid_permission)
                    break;
            }

            return $valid_permission;
        } else {

            $module = strtolower($sidebar_item['module']);
            $controller = strtolower($sidebar_item['controller']);
            $actions = $sidebar_item['action'];
            if (!is_array($actions))
                $actions = [$actions];

            //  Có module
            if (array_key_exists($sidebar_item['module'], $roles)) {
                $_module = $roles[$module];

                //  Có controller
                if (array_key_exists($controller, $_module)) {
                    $_controller = $_module[$controller];
                    if (count($_controller) > 0) {
                        if (is_numeric(array_keys($_controller)[0])) {
                            return count(array_intersect($user_roles, $_controller)) > 0;
                        }
                    }

                    $common_actions = array_unique(array_intersect(array_keys($_controller), $actions));
                    foreach ($common_actions as $_action)
                        return count(array_intersect($user_roles, $_controller[$_action])) > 0;
                }
            }
        }

        return false;
    }
}

if (!function_exists('ckeditor')) {
    function ckeditor()
    {
        return '<script src="/assets/ckeditor/ckeditor.js"></script>' .
            '<script src="/assets/ckfinder/ckfinder.js"></script>' .
            '<script src="/assets/js/resources/ko.ckeditor.js"></script>';
    }
}

if (!function_exists('load_js')) {
    function load_js(string $file_name, bool $version = true)
    {
        global $ci;
        $module = strtolower($ci->router->fetch_module());
        $controller = strtolower($ci->router->fetch_class());

        $file_path = "/application/modules/$module/views/$controller/assets/$file_name.js";
        if (file_exists('.' . $file_path)) {
            if ($version)
                $file_path .= '?v=' . time();

            return $file_path;
        }
        return null;
    }
}

if (!function_exists('get_lang')) {
    function get_lang()
    {
        if (get_cookie('lang') == null) {
            global $ci;
            return $ci->config->item('main_language');
        }
        return get_cookie('lang');
    }
}

if (!function_exists('get_lang')) {
    function get_lang()
    {
        global $theme_config;
        if (isset($theme_config['languages'])) {
            if (is_array($theme_config['languages'])) {
                if (count($theme_config['languages']) > 1) {
                    $lang_cookie = get_cookie('admin_lang');
                    $current_lang = null;

                    if (isset($lang_cookie)) {
                        if ($lang_cookie != null) {
                            $current_lang = $lang_cookie;
                        }
                    }

                    if ($current_lang == null) {
                        $current_lang = array_keys($theme_config['languages'])[0];
                    }

                    return $current_lang;
                }
            }
        }

        return null;
    }
}