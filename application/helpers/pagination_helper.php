<?php
if (!defined('BASEPATH'))
    exit('No direct access script allowed');

class Pagination
{
    public static function calc($total_item, $current_page, $per_page = 20)
    {
        $paging = new stdClass();
        $paging->limit = $per_page;
        $paging->total_item = $total_item;
        $paging->current_page = $current_page;
        $paging->show_from = 0;
        $paging->show_to = $current_page * $per_page;

        if ($current_page * $per_page > $total_item)
            $paging->show_to = $total_item;

        $paging->show_from = $paging->show_to - $per_page + 1;
        $paging->show_from = $paging->show_from < 1 ? 1 : $paging->show_from;

        $paging->total_page = intval($total_item) % $per_page == 0
            ? intval($total_item) / $per_page
            : floor(intval($total_item) / $per_page) + 1;

        $paging->current_page = $paging->current_page > $paging->total_page
            ? $paging->total_page
            : $paging->current_page;
        return $paging;
    }

    public static function display($reload_url, $current_page, $total_page, $attributes = null)
    {
        $adjacents = 1;
        $prevlabel = "&lsaquo; Trước";
        $nextlabel = "Sau &rsaquo;";
        $ul_attr = [];
        $li_attr = [];
        $anchor_attr = [];

        if ($attributes != null) {
            if (is_array($attributes)) {
                if (array_key_exists('ul_attr', $attributes))
                    $ul_attr = $attributes['ul_attr'];

                if (array_key_exists('li_attr', $attributes))
                    $li_attr = $attributes['li_attr'];

                if (array_key_exists('anchor_attr', $attributes))
                    $anchor_attr = $attributes['anchor_attr'];
            }
        }

        $out = '<ul>';
        if (count($ul_attr) > 0) {
            $out = '<ul';
            foreach ($ul_attr as $key => $value) {
                $out .= " $key='$value'";
            }

            $out .= '>';
        }

        $queries = array();
        $query = '';
        foreach ($_GET as $key => $value)
            array_push($queries, "$key=$value");

        if (count($queries) > 0)
            $query = '?' . join('&', $queries);

        // previous
        if ($current_page >= 3) {
            $out .= Pagination::get_li_attributes($li_attr);
            $out .= Pagination::get_anchor_attributes($anchor_attr);
            $out .= " href=" . $reload_url . "/" . ($current_page - 1) . "$query>" . $prevlabel . "</a></li>";
        }

        $pmin = ($current_page > $adjacents) ? ($current_page - $adjacents) : 1;
        $pmax = ($current_page < ($total_page - $adjacents)) ? ($current_page + $adjacents) : $total_page;

        if ($current_page >= 5) {
            for ($i = 1; $i <= 2; $i++) {
                $out .= Pagination::get_li_attributes($li_attr);
                $out .= Pagination::get_anchor_attributes($anchor_attr);
                $out .= " href=" . $reload_url . "$query>" . $i . "</a></li>";
            }

            $out .= Pagination::get_li_attributes($li_attr);
            $out .= Pagination::get_anchor_attributes($anchor_attr);
            $out .= ' href="javascript:">...</a></li>';
        }

        for ($i = $pmin; $i <= $pmax; $i++) {
            if ($i == $current_page) {
                $out .= Pagination::get_li_attributes($li_attr, true);
                $out .= Pagination::get_anchor_attributes($anchor_attr);
                $out .= " href='javascript:'>" . $i . "</a></li>";
            } elseif ($i == 1) {
                $out .= Pagination::get_li_attributes($li_attr);
                $out .= Pagination::get_anchor_attributes($anchor_attr);
                $out .= " href=" . $reload_url . "$query>" . $i . "</a></li>";
            } else {
                $out .= Pagination::get_li_attributes($li_attr);
                $out .= Pagination::get_anchor_attributes($anchor_attr);
                $out .= " href=" . $reload_url . "/" . $i . "$query>" . $i . "</a></li>";
            }
        }


        if ($current_page < ($total_page - $adjacents)) {
            $out .= Pagination::get_li_attributes($li_attr);
            $out .= Pagination::get_anchor_attributes($anchor_attr);
            $out .= ' href="javascript:">...</a></li>';

            $out .= Pagination::get_li_attributes($li_attr);
            $out .= Pagination::get_anchor_attributes($anchor_attr);
            $out .= " href=" . $reload_url . "/" . $total_page . "$query>" . $total_page . "</a></li>";
        }

        if ($current_page < $total_page - 2) {
            $out .= Pagination::get_li_attributes($li_attr);
            $out .= Pagination::get_anchor_attributes($anchor_attr);
            $out .= " href=" . $reload_url . "/" . ($current_page + 1) . "$query>" . $nextlabel . "</a></li>";
        }

        echo $out . '</ul>';
    }

    /**
     * @param null $anchor_attr
     * @return string
     */
    private static function get_anchor_attributes($anchor_attr = null)
    {
        if ($anchor_attr == null)
            return '<a';

        if (!is_array($anchor_attr))
            return '<a';

        if (count($anchor_attr) == 0)
            return "<a";

        $out = '<a';
        foreach ($anchor_attr as $key => $value) {
            if ($key != 'href') {
                $out .= " $key='$value'";
            }
        }

        return $out;
    }

    /**
     * @param null $li_attr
     * @param bool $active
     * @return string
     */
    private static function get_li_attributes($li_attr = null, $active = false)
    {
        if ($li_attr == null)
            return '<li>';

        if (!is_array($li_attr))
            return '<li>';

        if (count($li_attr) == 0)
            return '<li>';

        $out = '<li';
        foreach ($li_attr as $key => $value) {
            if ($active && $key == 'class') {
                $out .= " $key='$value active'";
            } else
                $out .= " $key='$value'";
        }

        $out .= '>';
        return $out;
    }
}