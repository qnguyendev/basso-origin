<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

if (!function_exists('create_bank_transaction')) {
    /**
     * Tạo thanh toán chuyển khoản internet banking
     * @param $order_id
     * @param $order_info
     * @param $amount
     * @param $access_key
     * @param $secret
     * @return mixed
     */
    function create_bank_transaction($order_id, $order_info, $amount, $access_key, $secret)
    {
        $data = "access_key=$access_key&amount=$amount" .
            "&command=request_transaction&order_id=$order_id" .
            "&order_info=$order_info&return_url=" . CONFIRM_TRANSACTION_URL;

        $signature = hash_hmac("sha256", $data, $secret);
        $data = "access_key=" . $access_key .
            "&amount=" . $amount .
            "&command=request_transaction" .
            "&order_id=" . urlencode($order_id) .
            "&order_info=" . urlencode($order_info) .
            "&return_url=" . urlencode(CONFIRM_TRANSACTION_URL);

        $data .= "&signature=" . $signature;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.pay.truemoney.com.vn/bank-charging/service/v2');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }
}

if (!function_exists('check_bank_transaction')) {
    function check_bank_transaction($trans_ref, $access_key, $app_secret)
    {
        $data = "access_key=$access_key&command=get_transaction_detail&trans_ref=$trans_ref";
        $signature = hash_hmac("sha256", $data, $app_secret);
        $data .= "&signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.pay.truemoney.com.vn/bank-charging/service/v2');
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, false);
    }
}