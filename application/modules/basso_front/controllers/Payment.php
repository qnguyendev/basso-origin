<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Payment
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Payment_model $payment_model
 * @property Payment_limit_model $payment_limit_model
 * @property Common_model $common_model
 * @property Customer_model $customer_model
 * @property Voucher_model $voucher_model
 * @property Customer_order_model $customer_order_model
 * @property Payment_history_model $payment_history_model
 * @property Country_model $country_model
 * @property Order_term_model $order_term_model
 * @property Email_model $email_model
 */
class Payment extends Site_Controller
{
    private $_vn_pay;

    public function __construct()
    {
        parent::__construct();
        global $theme_config;
        $this->_vn_pay = $theme_config['vn_pay'];

        $this->load->model('admin/image_model');
        $this->load->model('payment_model');
        $this->load->model('basso/payment_limit_model');
        $this->load->model('basso/customer_order_model');
        $this->load->model('basso/payment_history_model');
        $this->load->model('basso/country_model');
        $this->load->model('basso/order_term_model');
        $this->load->model('common_model');
        $this->load->model('management/customer_model');
        $this->load->model('admin/voucher_model');
        $this->load->helper('basso/crawler');
        $this->load->model('email_model');
    }

    public function confirm()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('vnp_Amount', null, 'required');
        $this->form_validation->set_rules('vnp_ResponseCode', null, 'required');
        $this->form_validation->set_rules('vnp_TxnRef', null, 'required');
        $this->form_validation->set_rules('vnp_SecureHash', null, 'required');
		
		/* print_r("<pre>Test Confirm");
		print_r($_GET);
		print_r("</pre>"); */

        /* if (!$this->form_validation->run()) {
            return redirect(base_url());
        } */

        $res_code = $this->input->get('vnp_ResponseCode');
        $payment_id = intval($this->input->get('vnp_TxnRef'));

        $vnp_SecureHash = $_GET['vnp_SecureHash'];
        $inputData = array();
        foreach ($_GET as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }
		
		/* print_r("<pre>");
		print_r($inputData);
		print_r("</pre>");
		die; */
		
        unset($inputData['vnp_SecureHashType']);
        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . $key . "=" . $value;
            } else {
                $hashData = $hashData . $key . "=" . $value;
                $i = 1;
            }
        }

        /* $secureHash = hash('sha256', $this->_vn_pay['vnp_HashSecret'] . $hashData); */
		$secureHash = hash('sha256', $this->_vn_pay['vnp_HashSecret'] . $hashData);
		
		/* $this->ipn(); */
		
        if ($secureHash == $vnp_SecureHash) {
            $payment = $this->payment_history_model->get_by_id($payment_id);
            if ($payment == null) {
                return redirect(base_url());
            }

            if ($res_code == '00') {
				/* $order = $this->customer_order_model->get_by_id($payment->order_id);
                $this->payment_history_model->update_status($payment_id, PaymentHistoryStatus::COMPLETED, $order); */
                $this->session->set_flashdata('success', true);
            } else {
                $this->session->set_flashdata('error', true);
            }
            return redirect(base_url('tai-khoan/don-hang?id=' . $payment->order_id));
        }

        return redirect(base_url());
    }

    public function ipn()
    {
        $inputData = [];
        $returnData = [];

        $data = $_REQUEST;
        foreach ($data as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }

        $vnp_SecureHash = $inputData['vnp_SecureHash'];
		
		/* print_r("<pre>");
		print_r($data);
		print_r("</pre>");
		
		print_r("<pre>");
		print_r($inputData);
		print_r("</pre>");
		die; */
		
		
        unset($inputData['vnp_SecureHashType']);
        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . $key . "=" . $value;
            } else {
                $hashData = $hashData . $key . "=" . $value;
                $i = 1;
            }
        }

        /* $secureHash = hash('sha256', $this->_vn_pay['vnp_HashSecret'] . $hashData); */
		$secureHash = hash('sha256', $this->_vn_pay['vnp_HashSecret'] . $hashData);
		
		/* print_r("<pre>");
		print_r($hashData);
		print_r("</pre>");
		
		print_r("<pre>");
		print_r($vnp_SecureHash);
		print_r("</pre>");
		
		print_r("<pre>");
		print_r($secureHash);
		print_r("</pre>");
		
		die; */
		
        try {
            if ($secureHash == $vnp_SecureHash) {
                $payment_id = $inputData['vnp_TxnRef'];
                $payment = $this->payment_history_model->get_by_id($payment_id);
		
                if ($payment != null) {
                    if ($payment->status != PaymentHistoryStatus::COMPLETED) {
                        if ($inputData['vnp_ResponseCode'] == '00') {
							$order = $this->customer_order_model->get_by_id($payment->order_id);
                            $this->payment_history_model->update_status($payment_id, PaymentHistoryStatus::COMPLETED, $order);
						}
                        $returnData['RspCode'] = '00';
                        $returnData['Message'] = 'Confirm Success';
                    } else {
                        $returnData['RspCode'] = '02';
                        $returnData['Message'] = 'Order already confirmed';
                    }
					
					if ($payment->amount != ($inputData['vnp_Amount']/100)) {
						$returnData['RspCode'] = '04';
                        $returnData['Message'] = 'Invalid amount';
					}

                } else {
                    $returnData['RspCode'] = '01';
                    $returnData['Message'] = 'Order not found';
                }
            } else {
                $returnData['RspCode'] = '97';
                $returnData['Message'] = 'Invalid signature';
            }
        } catch (Exception $e) {
            $returnData['RspCode'] = '99';
            $returnData['Message'] = 'Unknow error';
        }
		
		/* print_r("<pre>");
		print_r(json_encode($returnData));
		print_r("</pre>");
		die; */
		
		header('Content-Type: application/json');
        echo json_encode($returnData);
    }
}