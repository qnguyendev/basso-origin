<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Tracking_order
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Customer_order_model $customer_order_model
 */
class Tracking_order extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('basso/customer_order_model');
    }

    public function index()
    {

    }
}