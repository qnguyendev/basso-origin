<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Site_map
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property CI_Output $output
 * @property Article_model $article_model
 */
class Site_map extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->output->set_header("Content-Type: text/xml;charset=utf-8");
        $data = [
            'post' => $this->article_model->get_sitemap(),
            'page' => $this->term_model->get_by_type('page'),
            'widget_news' => get_widgets('support-news', ['block_text']),
            'widget_main' => get_widgets('support-main', ['block_text']),
            'widget_faq' => get_widgets('support-faq', ['block_text'])
        ];

        return $this->blade->render($data);
    }
}