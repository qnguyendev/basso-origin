<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Front
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Slider_model $slider_model
 * @property Crawled_product_model $crawled_product_model
 * @property Amazon_crawler_model $amazon_crawler_model
 * @property Amazon_product_model $amazon_product_model
 * @property CI_Cart $cart
 * @property Crawl_history_model $crawl_history_model
 * @property Subscribe_model $subscribe_model
 * @property Contact_form_model $contact_form_model
 * @property Widget_model $widget_model
 */
class Front extends Site_Controller
{
    private $_cart;

    public function __construct()
    {
        parent::__construct();
        global $cart;
        $this->_cart = $cart;
        $this->load->model('admin/slider_model');
        $this->load->model('basso/crawled_product_model');
        $this->load->helper('basso/crawler');
        $this->load->model('basso/amazon_crawler_model');
        $this->load->model('basso/amazon_product_model');
        $this->load->model('basso/crawl_history_model');
        $this->load->library('email');
    }

    public function index()
    {
        $term = $this->term_model->get_by_type('home');
        if (count($term) > 0) {
            $term = $term[0];
            $GLOBALS['current_term'] = $term;

            set_head_title($term->seo_title == null ? $term->name : $term->seo_title);
            set_head_des($term->seo_description == null ? $term->description : $term->seo_description);
            set_head_index($term->seo_index);
            set_head_image($term->image_path == null ? null : base_url($term->image_path));
        }

        $this->load->model('admin/slider_model');
        return $this->blade
            ->set('sliders', $this->slider_model->get(1))
            ->render();
    }

    public function GET_index()
    {
        $action = $this->input->get('action');
        if ($action == null)
            json_error(null);

        if ($action == 'cart') {
            json_success(null, ['data' => array_sum(array_column($this->_cart, 'qty'))]);
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('url', null, 'required|trim');
        if ($this->form_validation->run()) {

            $url = $this->input->post('url');
            if (strpos($url, 'ebay') || strpos($url, 'amazon')) {
                try {
                    $uri = parse_url($url);
                    $id = $source = $slug = null;
                    if (strpos($uri['host'], 'ebay') !== false) {
                        $id = explode('/', $uri['path'])[3];
                        $slug = explode('/', $uri['path'])[2];
                        $source = 'ebay';
                    }

                    if (strpos($uri['host'], 'amazon')) {
                        if (strpos($uri['path'], 'gp/product')) {
                            $id = explode('/', $uri['path'])[3];
                        } else {
                            if (explode('/', $uri['path'])[2] == 'dp') {
                                $slug = explode('/', $uri['path'])[1];
                                $id = explode('/', $uri['path'])[3];
                            } else {
                                $id = explode('/', $uri['path'])[2];
                            }
                        }

                        $source = 'amazon';
                    }

                    if ($slug != null) {
                        $slug = url_title($slug);
                        $redirect_url = 'item/' . $slug . '-' . $id . '.html';
                    } else
                        $redirect_url = 'item/' . $id . '.html';

                    $crawl_result = $this->_crawl_product($source, $id);
                    $this->crawl_history_model->insert($url, true);

                    if ($source == 'amazon') {
                        $product = $this->amazon_product_model->get_variant($id);
                        if ($product == null) {
                            if ($crawl_result != null) {
                                $variant = $crawl_result['variations'][0];
                                if (isset($variant))
                                    $redirect_url = 'item/' . $slug . '-' . $variant['id'] . '.html';
                            }
                        }
                    }

                    json_success(null, ['redirect_url' => base_url($redirect_url)]);
                } catch (Exception $ex) {

                    $this->crawl_history_model->insert($url, false);
                    json_error('Vui lòng thử lại sau 5 giây nữa');
                }
            }

            $this->crawl_history_model->insert($url, false);
            json_error('Hiện tại hệ thống chỉ hỗ trợ Ebay và Amazon', ['error_sites' => true]);
        }

        json_error('Vui lòng nhập link sản phẩm');
    }

    private function _crawl_product($source, $id)
    {
        switch ($source) {
            case 'ebay':
                $product = $this->crawled_product_model->get_by_id($id);
                if ($product == null) {
                    $product = crawl_ebay($id);
                    if ($product != null) {
                        $this->crawled_product_model->insert($id, object_to_array($product));
                    }
                }

                return null;
                break;
            case 'amazon':
                $product = $this->amazon_product_model->get_parent_product($id);
                if ($product == null) {
                    $result = $this->amazon_crawler_model->get($id);
                    if ($result) {
                        $this->amazon_product_model->insert($result['product'], $result['variations']);
                        return $result;
                    }
                }

                return null;
                break;
        }
    }

    public function support($id = null)
    {
        set_head_title('Basso - Trợ giúp');
        set_head_index(false);

        $widget_news = get_widgets('support-news', ['block_text']);
        $widget_main = get_widgets('support-main', ['block_text']);
        $widget_faq = get_widgets('support-faq', ['block_text']);

        if ($id == null) {
            if (count($widget_news) > 0)
                $id = $widget_news[0]->id;
            else if (count($widget_main) > 0)
                $id = $widget_main[0]->id;
            else if (count($widget_faq) > 0)
                $id = $widget_faq[0]->id;

            if ($id != null)
                return redirect(base_url('/tro-giup/' . $id));
        } else {
            $this->load->model('widget_model');
            $widget = $this->widget_model->get_by_id($id);
            if ($widget != null) {
                set_head_title('Basso - Trợ giúp | '. $widget->title);
            }
        }

        return $this->blade->render(['widget_id' => $id]);
    }

    public function test()
    {
        $user = $this->ion_auth->users('quanghuytotdong@gmail.com')->row();
        $this->ion_auth->update(1, ['password' => 'Bongma199)']);
    }

    public function POST_subscribe()
    {
        $this->form_validation->set_rules('email', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập email của bạn');

        $email = $this->input->post('email');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            json_error('Địa chỉ email không hợp lệ');

        $this->subscribe_model->insert($email);
        json_success('');
    }

    public function POST_contact()
    {
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('phone', null, 'required');
        $this->form_validation->set_rules('content', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đầy đủ thông tin');

        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $content = $this->input->post('content');

        $this->load->model('basso/contact_form_model');
        $this->contact_form_model->insert($name, $phone, $content);
		
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		$mail_to = "nguyengiap1467@gmail.com";
		$subject = "Thông tin liên hệ từ Basso";
		$mailcontent = "<p>Họ và tên:&nbsp;".$name."</p>".
					"<p>Số điện thoại:&nbsp;".$phone."</p>".
					"<p>Nội dung:&nbsp;".$content."</p>";
	
		//mail ($mail_to, $subject, $mailcontent, $headers);
		//$from_email = "email@example.com";
        //$to_email = $this->input->post('email');
        //Load email library
        $this->email->from($mail_to, $name);
        $this->email->to($mail_to);
        $this->email->subject($subject);
        $this->email->message($mailcontent);
        //Send mail
        if($this->email->send())
			json_success('');
        else
            json_error('Xảy ra lỗi quá trình gửi Email.');
		
    }
}