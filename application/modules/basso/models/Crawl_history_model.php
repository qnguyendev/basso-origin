<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Crawl_history_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 * @property CI_Input $input
 */
class Crawl_history_model extends CI_Model
{
    private $_table = 'crawl_histories';

    public function insert(string $url, bool $is_success)
    {
        $data = [
            'created_time' => time(),
            'url' => $url,
            'is_success' => $is_success,
            'ipaddress' => $this->input->ip_address()
        ];

        $this->db->insert($this->_table, $data);
    }

    public function get(int $page, int $start, int $end)
    {
        $this->db->where('created_time >=', $start);
        $this->db->where('created_time <=', $end);

        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);

        $this->db->order_by('created_time', 'desc');
        return $this->db->get($this->_table)->result();
    }

    public function count(int $start, int $end)
    {
        $this->db->where('created_time >=', $start);
        $this->db->where('created_time <=', $end);

        return $this->db->count_all_results($this->_table);
    }
	
	public function count_success_fail(int $status) 
    {
        $this->db->where('is_success =', $status);

        return $this->db->count_all_results($this->_table);
    }
	
	public function count_keyword_weblink() 
    {
        $this->db->where("(url like 'http://%') or (url like '%www.%') or (url like '%.%')");

        return $this->db->count_all_results($this->_table);
    }
	
	public function count_keyword_not_weblink($string_search) 
    {
        $this->db->where("url like '%$string_search%'");

        return $this->db->count_all_results($this->_table);
    }
}