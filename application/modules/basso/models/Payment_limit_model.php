<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Payment_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Payment_limit_model extends CI_Model
{
    public function insert(int $id = 0, int $limit, int $fee, string $des = null)
    {
        $this->db->trans_begin();
        $data = [
            'limit' => $limit,
            'fee' => $fee,
            'description' => $des
        ];

        if ($id == 0)
            $this->db->insert('payment_limits', $data);
        else {
            $this->db->where('id', $id);
            $this->db->update('payment_limits', $data);
        }
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get(int $id = null)
    {
        if ($id == null) {
            $this->db->order_by('limit', 'desc');
            return $this->db->get('payment_limits')->result();
        }

        $this->db->where('id', $id);
        return $this->db->get('payment_limits')->row();
    }

    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete('payment_limits');
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}