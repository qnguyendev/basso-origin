<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Dashboard_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Dashboard_model extends CI_Model
{
    private $_cus_order_table = 'customer_orders';
    private $_payment_table = 'payment_histories';
    private $_user_table = 'users';
    private $_user_role_table = 'users_roles';
    private $_user_info_table = 'users_info';

    /**
     * Số lượng đơn hàng
     * @param int $start
     * @param int $end
     * @return int
     */
    public function total_orders(int $start, int $end)
    {
        $this->db->select('id');

        $this->db->where('created_time >=', $start);
        $this->db->where('created_time <=', $end);
        $this->db->where_in('status', [CustomerOrderStatus::COMPLETED, CustomerOrderStatus::PROCESSING]);

        return $this->db->count_all_results($this->_cus_order_table);
    }

    public function revenue(int $start, int $end)
    {
        $this->db->select('created_time, amount');

        $this->db->where('created_time >=', $start);
        $this->db->where('created_time <=', $end);
        $this->db->where('status', PaymentHistoryStatus::COMPLETED);
        return $this->db->get($this->_payment_table)->result();
    }

    /**
     * Tổng số khách hàng
     * @param int $start
     * @param int $end
     * @return int
     */
    public function total_customers(int $start, int $end)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_user_table t");
        $this->db->join("$this->_user_role_table o", 't.id = o.user_id', 'left');
        $this->db->where('t.created_on >=', $start);
        $this->db->where('t.created_on <=', $end);
        $this->db->where('o.group_id', 6);
        $this->db->group_by('t.id');
        return $this->db->count_all_results();
    }

    public function new_customers(int $start, int $end)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_user_table t");
        $this->db->join("$this->_user_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_cus_order_table d", 't.id = d.customer_id', 'left');
        $this->db->where('t.created_on >=', $start);
        $this->db->where('t.created_on <=', $end);
        $this->db->where('o.group_id', 6);
		
		/* fixcode */
        /* $this->db->having('count(d.id)', 1); */
        $this->db->group_by('t.id');

        return $this->db->count_all_results();
    }
}