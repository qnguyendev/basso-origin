<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Currency_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Currency_model extends CI_Model
{
    private $_table = 'currencies';

    public function get()
    {
        return $this->db->get($this->_table)->result();
    }

    public function insert($id = 0, $name, $symbol)
    {
        $this->db->trans_begin();
        if ($id == 0) {
            $this->db->insert($this->_table, ['name' => $name, 'symbol' => $symbol]);
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->_table, ['name' => $name, 'symbol' => $symbol]);
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete($this->_table);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}