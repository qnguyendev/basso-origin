<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Inventory_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Inventory_model extends CI_Model
{
    private $_inventory_table = 'inventory_packages';
    private $_inventory_item_table = 'inventory_package_items';
    private $_image_table = 'images';
    private $_customer_item_table = 'customer_order_items';
    private $_warehouse_table = 'warehouses';
    private $_cus_order_table = 'customer_orders';
    private $_user_table = 'users';
    private $_web_orders_table = 'web_orders';
    private $_web_order_items_table = 'web_order_items';
	
    private $_order_shippings_table = 'order_shippings';

    #region danh sách nhập kho
    public function get(int $page = 1, string $status = 'all', int $warehouse_id = 0, int $start = 0, int $end = 0, $key = null, $branch = null)
    {
        $this->db->select('t.id, t.created_time, t.real_tracking, t.web_tracking, d.name as warehouse, x.name as customer_name, codt.quantity as total_quantity, t.image_id, img.path as image_path');
        $this->db->select('t.total_weight, t.name, t.quantity, t.delivered_date, t.status, t.lost_pending');
        $this->db->select('x.name as customer, k.first_name as user, t.branch'); /* ,w.branch as branch_admin  */
        $this->db->select('GROUP_CONCAT(o.customer_order_id) as customer_order_id, GROUP_CONCAT(x.order_code) as order_code');
        $this->db->from("$this->_inventory_table t");
		$this->db->join("$this->_image_table img", 't.image_id = img.id', 'left');
        $this->db->join("$this->_inventory_item_table o", 't.id = o.package_id', 'left');
        $this->db->join("$this->_warehouse_table d", 'd.id = t.warehouse_id', 'left');
        $this->db->join("$this->_cus_order_table x", 'x.id = o.customer_order_id', 'left');
		
        /* fix show quantity còn thiếu của từng sản phẩm */
		$this->db->join("$this->_customer_item_table codt", 't.customer_order_id = codt.order_id', 'left');
		/* end fix show quantity còn thiếu của từng sản phẩm */
		
        $this->db->join("$this->_user_table k", 'k.id = x.user_id', 'left');
		/* comment for branch wrong */
        /* $this->db->join("$this->_web_order_items_table i", 't.customer_order_id = i.customer_order_id', 'left');
        $this->db->join("$this->_web_orders_table w", 'w.id = i.web_order_id', 'left'); 
		*/

        $this->db->where('t.created_time >=', $start);
        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($warehouse_id > 0)
            $this->db->where('t.warehouse_id', $warehouse_id);

        if ($status != 'all') {
            if ($status == InventoryItemStatus::NOT_FOUND) {
                $this->db->where('t.status', InventoryItemStatus::NOT_FOUND);
            } else if ($status == InventoryItemStatus::LOST) {
                $this->db->where('t.status', InventoryItemStatus::LOST);
            }
        }

        if ($key != null) {
            $this->db->like('t.name', $key, 'both');
            $this->db->or_like('t.web_tracking', $key, 'both');
            $this->db->or_like('t.real_tracking', $key, 'both');
            $this->db->or_like('x.name', $key, 'both');
        }

		if($branch != null) {
			$this->db->where('t.branch', $branch);
		}
		
		$this->db->order_by('t.created_time', 'desc');
		if ($status == InventoryItemStatus::LOST) $this->db->order_by('t.lost_pending', 'desc');
      
        $this->db->limit(PAGING_SIZE);
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count(string $status = 'all', int $warehouse_id = 0, int $start = 0, int $end = 0, $key = null, $branch = null)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_inventory_table t");
        $this->db->join("$this->_warehouse_table d", 'd.id = t.warehouse_id', 'left');
        $this->db->join("$this->_inventory_item_table o", 't.id = o.package_id', 'left');
        $this->db->join("$this->_cus_order_table x", 'x.id = o.customer_order_id', 'left');

        $this->db->where('t.created_time >=', $start);
        if ($end > 0)
            $this->db->where('t.created_time <=', $end);

        if ($warehouse_id > 0)
            $this->db->where('t.warehouse_id', $warehouse_id);

        if ($status != 'all') {
            if ($status == InventoryItemStatus::NOT_FOUND) {
                $this->db->where('t.status', InventoryItemStatus::NOT_FOUND);
            } else if ($status == InventoryItemStatus::LOST) {
                $this->db->where('t.status', InventoryItemStatus::LOST);
            }
        }

        if ($key != null) {
            $this->db->like('t.name', $key, 'both');
            $this->db->or_like('t.web_tracking', $key, 'both');
            $this->db->or_like('t.real_tracking', $key, 'both');
            $this->db->or_like('x.name', $key, 'both');
        }
		
		if($branch != null) {
			$this->db->where('t.branch', $branch);
		}

        return $this->db->count_all_results();
    }

    public function get_items($package_id, $status = 'all')
    {
        if (!is_array($package_id))
            $this->db->where('package_id', $package_id);
        else
            $this->db->where_in('package_id', $package_id);

        if ($status != 'all')
            $this->db->where('status', $status);
        return $this->db->get($this->_inventory_item_table)->result();
    }

    #endregion

    public function get_by_id(int $package_id)
    {
        $this->db->where("id", $package_id);
        return $this->db->get($this->_inventory_table)->row();
    }
	
	public function get_inventory_package_by_web_tracking(string $web_tracking)
    {
        $this->db->where("web_tracking", $web_tracking);
        return $this->db->get($this->_inventory_table)->row();
    }

    /**
     * Tìm sản phẩm đơn hàng của khách theo mã tracking
     * @param $tracking
     * @return array
     */
    public function search_by_tracking($tracking)
    {
        $this->db->select('t.name, t.id, t.order_id, t.tracking_code, t.quantity');
        $this->db->select('o.branch, o.order_code');
        $this->db->from("$this->_customer_item_table t");
        $this->db->join("$this->_cus_order_table o", 't.order_id = o.id', 'left');
		
		/* 
		fixcode:
		Yêu cầu là không cho nhập kho lại những track đã nhập kho rồi (ở trạng thái Đã về VN, Đã giao hàng)
		Vẫn trùng tracking VD track này đã giao hàng nhưng vẫn nhập được kho: 61299990146625275240  
		146625275240
		*/
		$this->db->where('t.status !=', 'shipped');
		$this->db->where('t.status !=', 'in_inventory');
		$this->db->where('t.status !=', 'cancelled');
		/* end fixcode */

        /* if (!is_array($tracking))
            $this->db->like('t.tracking_code', $tracking, 'before');
        else {
            $this->db->like('t.tracking_code', $tracking[0], 'both');
            if (count($tracking) > 0) {
                for ($i = 1; $i < count($tracking); $i++) {
                    $this->db->or_like('t.tracking_code', $tracking[$i], 'both');
                }
            }
        } */
		
		/* fixcode */
		if (!is_array($tracking))
            $this->db->like('t.tracking_code', $tracking, 'before');
        else {
            $this->db->like('t.tracking_code', $tracking[0], 'before');
            if (count($tracking) > 0) {
                for ($i = 1; $i < count($tracking); $i++) {
                    $this->db->or_like('t.tracking_code', $tracking[$i], 'before');
                }
            }
        }
		$this->db->where('t.status !=', 'in_inventory');
		/* end fixcode */

        return $this->db->get()->result();
    }

    /**
     * Thêm kiện hàng vào kho
     * @param $warehouse_id
     * @param $items
     * @return bool
     */
    public function insert($warehouse_id, $items)
    {
        $this->db->trans_begin();
        foreach ($items as $item) {
            $data = [
                'created_time' => time(),
                'real_tracking' => $item['real_tracking'],
                'web_tracking' => $item['web_tracking'],
                'total_weight' => $item['total_weight'],
                'name' => $item['name'],
                'customer_order_id' => is_enull($item['customer_order_id'], null, 'numeric'),
                'quantity' => is_enull($item['quantity'], 0, 'numeric'),
                'delivered_date' => strtotime($item['delivered_date']),
                'status' => InventoryItemStatus::NOT_FOUND,
                'lost_pending' => 1,
                'warehouse_id' => $warehouse_id,
                'branch' => isset($item['branch']) ? is_enull($item['branch'], null) : null
            ];

            $customer_items = json_decode($item['customer_items'], true);
            if (count($customer_items) > 0) {
                if ($data['total_weight'] > 0)
                    $data['status'] = InventoryItemStatus::FOUND;
                elseif ($data['total_weight'] == 0)
                    $data['status'] = InventoryItemStatus::LOST;
            }

            $this->db->insert($this->_inventory_table, $data);
            $package_id = $this->db->insert_id();

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                if (!$this->_insert_items($package_id, $customer_items, $data['total_weight'])) {
                    $this->db->trans_rollback();
                    return false;
                }
            }
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Sản phẩm trong kiện hàng về kho
     * @param int $package_id
     * @param array $items
     * @param float $total_weight
     * @return bool
     */
    private function _insert_items(int $package_id, array $items, $total_weight)
    {
        if (count($items) == 0) return true;
        $this->db->where('package_id', $package_id);
        $this->db->delete($this->_inventory_item_table);

        $data = [];
        foreach ($items as $item) {
            array_push($data, [
                'package_id' => $package_id,
                'customer_order_id' => $item['order_id'],
                'customer_order_item_id' => $item['item_id']
            ]);

            if (is_enull($item['item_id'], null) != null) {
                if (is_enull($total_weight, 0) > 0) {
                    $update_data = [
                        'weight' => is_enull($total_weight, 0, 'numeric'),
                        'status' => CustomerOrderItemStatus::IN_INVENTORY,
                        'imported_time' => null
                        /* 'imported_time' => time() */ /* fixcode Tự động điền ngày về VN khi Quản lý kho đã nhập mã tracking, cân nặng */
                    ];

                    //  Cập nhật cân nặng, phí ship, đã về kho VC
                    $this->db->where('id', $item['item_id']);
                    $this->db->update($this->_customer_item_table, $update_data);
                }
            }
        }

        $this->db->insert_batch($this->_inventory_item_table, $data);
        //$this->_update_customer_items($total_weight, $items);

        return true;
    }

    /**
     * Cập nhật sp trong đơn của khách: khối lượng, đã về kho, phí ship
     * @param $total_weight
     * @param $items
     */
    private function _update_customer_items($total_weight, $items)
    {
        $item_weight = $total_weight;

        foreach ($items as $item) {
            if (is_enull($item_weight, 0) > 0) {
                $update_data = [
                    'weight' => $item_weight,
                    'status' => CustomerOrderItemStatus::IN_INVENTORY,
                    'imported_time' => time()
                ];

                //  Cập nhật cân nặng, phí ship, đã về kho VC
                $this->db->where('id', $item['item_id']);
                $this->db->update($this->_customer_item_table, $update_data);
            }
        }
    }

    /**
     * Cập nhật sản phẩm trong gói hàng về kho
     * @param int $package_id
     * @param array $data
     * @param array $items
     * @return bool
     */
    public function update_package(int $package_id, array $data, array $items = null)
    {
        $allow_keys = ['web_tracking', 'real_tracking', 'lost_pending', 'total_weight', 'status', 'name', 'branch', 'image_id'];

        foreach ($data as $key => $value) {
            if (!in_array($key, $allow_keys)) {
                unset($data[$key]);
            }
        }

        $this->db->where('id', $package_id);
        $this->db->update($this->_inventory_table, $data);

        if ($this->db->trans_status()) {
            if ($items != null) {
                if (count($items) > 0) {
                    if ($this->_insert_items($package_id, $items, $data['total_weight'])) {
                        $this->db->trans_commit();
                        return true;
                    }
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật tổng cân nặng của kiện hàng
     * @param int $package_id
     * @return bool
     */
    public function update_package_total_weight(int $package_id)
    {
        $this->db->trans_begin();
        $this->db->query("update inventory_packages l,
                        (select sum(d.weight) as total_weight
                        from inventory_packages t
                        left join inventory_package_items o on t.id = o.package_id
                        left join customer_order_items d on o.customer_order_item_id = d.id
                        where t.id = $package_id) m
                        set l.total_weight = m.total_weight where l.id = $package_id");
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật tổng cân nặng của kiện hàng theo id sản phẩm
     * @param int $item_id
     * @return bool
     */
    public function update_package_total_weight_by_item(int $item_id)
    {
        $this->db->where('customer_order_item_id', $item_id);
        $row = $this->db->get($this->_inventory_item_table)->row();

        if ($row != null)
            return $this->update_package_total_weight($row->package_id);

        return false;
    }

    /**
     * Danh sách sản phẩm theo kiện hàng về
     * @param $package_id
     * @return array
     */
    public function get_customer_order_items($package_id)
    {
        $this->db->select('o.*, d.path as image_path, t.package_id');
        $this->db->from("$this->_inventory_item_table t");
        $this->db->join("$this->_customer_item_table o", 'o.id = t.customer_order_item_id', 'left');
        $this->db->join("$this->_image_table d", 'o.image_id = d.id', 'left');

        if (is_array($package_id))
            $this->db->where_in('t.package_id', $package_id);
        else
            $this->db->where('t.package_id', $package_id);

        return $this->db->get()->result();
    }

    public function get_item_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_inventory_item_table)->row();
    }

    /**
     * Lấy kiện hàng theo tracking vận chuyển
     * @param $tracking
     * @return mixed
     */
    public function get_by_real_tracking($tracking)
    {
        if (!is_array($tracking)) {
            $this->db->where('real_tracking', $tracking); 
            return $this->db->get($this->_inventory_table)->row();
        }

        $this->db->where_in('real_tracking', $tracking);
        return $this->db->get($this->_inventory_table)->result();
    }

    public function delete_package(int $package_id)
    {
        $this->db->where('id', $package_id);
        $this->db->delete($this->_inventory_table);
    }
}