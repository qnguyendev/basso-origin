<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_term_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Order_term_model extends CI_Model
{
    private $_term_table = 'order_terms';
    private $_step_table = 'order_term_steps';
    private $_term_list_table = 'order_term_list';

    public function insert(int $id, string $name, int $min = 0, int $active = 1, array $steps = null,
                           array $categories = null)
    {
        $this->db->trans_begin();
        $data = [
            'name' => $name,
            'active' => $active,
            'min_fee' => $min
        ];

        if ($id == 0) {
            $this->db->insert($this->_term_table, $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->_term_table, $data);
        }

        if ($this->db->trans_status()) {
            if ($this->_insert_steps($id, $steps)) {
                if ($this->_insert_category($id, $categories)) {
                    $this->db->trans_commit();
                    return true;
                }

                $this->db->trans_rollback();
                return false;
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_steps(int $term_id, array $steps = null)
    {
        $this->db->trans_begin();
        $this->db->where('order_term_id', $term_id);
        $this->db->delete($this->_step_table);

        for ($i = 0; $i < count($steps); $i++) {
            unset($steps[$i]['id']);
            $steps[$i]['order_term_id'] = $term_id;
        }

        $this->db->insert_batch($this->_step_table, $steps);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get_steps($term_id)
    {
        if (!is_array($term_id))
            $this->db->where('order_term_id', $term_id);
        else
            $this->db->where_in('order_term_id', $term_id);

        $this->db->order_by('amount', 'asc');
        return $this->db->get($this->_step_table)->result();
    }

    private function _insert_category(int $term_id, $list)
    {
        $this->db->trans_begin();
        $this->db->where('order_term_id', $term_id);
        $this->db->delete($this->_term_list_table);

        if ($this->db->trans_status()) {
            if ($list != null) {
                if (is_array($list)) {
                    $data = [];
                    foreach ($list as $cat) {
                        if (!isset($cat['name']) || !isset($cat['type']))
                            continue;

                        if (!empty($cat['name']))
                            array_push($data, [
                                'order_term_id' => $term_id,
                                'category' => $cat['name'],
                                'type' => is_enull($cat['type'], OrderTermCategoryType::TINCLUDE)
                            ]);
                    }

                    $this->db->insert_batch($this->_term_list_table, $data);
                    if ($this->db->trans_status()) {
                        $this->db->trans_commit();
                        return true;
                    }

                    $this->db->trans_rollback();
                    return false;
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _get_categories(int $term_id)
    {
        $this->db->where('order_term_id', $term_id);
        return $this->db->get($this->_term_list_table)->result();
    }

    public function get_by_id($id)
    {
        if (is_array($id)) {
            $this->db->where_in('id', $id);
            return $this->db->get($this->_term_table)->result();
        }

        $this->db->where('id', $id);
        return $this->db->get($this->_term_table)->row();
    }

    public function get(int $id = 0, int $page = 0)
    {
        if ($id > 0) {
            $this->db->where('id', $id);
            return $this->db->get($this->_term_table)->row();
        }

        if ($page == 0) {
            $this->db->order_by('name', 'asc');
            $result = $this->db->get($this->_term_table)->result();
        } else {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
            $result = $this->db->get('order_terms')->result();
        }

        $steps = $this->get_steps(array_column($result, 'id'));
        for ($i = 0; $i < count($result); $i++) {
            $result[$i]->steps = [];
            foreach ($steps as $item) {
                if ($item->order_term_id == $result[$i]->id)
                    array_push($result[$i]->steps, $item);
            }

            $result[$i]->categories = $this->_get_categories($result[$i]->id);
        }

        return $result;
    }

    public function count()
    {
        return $this->db->count_all_results('order_terms');
    }

    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete('order_terms');

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get_steps_by_category(array $category)
    {
        $this->db->where_in('category', $category);
        $result = $this->db->get($this->_term_list_table)->result();
        if (count($result)) {
            $term_id = array_unique(array_column($result, 'order_term_id'));
            foreach ($term_id as $id) {
                $_temp = array_filter($result, function ($item) use ($id) {
                    return $item->order_term_id == $id && $item->type == OrderTermCategoryType::EXCLUDE;
                });

                if (count($_temp) == 0) {
                    $this->db->where('order_term_id', $id);
                    return $this->db->get($this->_step_table)->result();
                }
            }
        }

        return [];
    }
}