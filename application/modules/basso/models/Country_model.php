<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Country_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Country_model extends CI_Model
{
    private $_countries_table = 'countries';
    private $_customer_rate_table = 'country_customer_groups';
    private $_customer_order_table = 'customer_orders';

    public function insert($id = 0, $data)
    {
        $this->db->trans_begin();
        if ($id == 0)
            $this->db->insert($this->_countries_table, $data);
        else {
            $this->db->where('id', $id);
            $this->db->update($this->_countries_table, $data);
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get($id = null)
    {
        $this->db->select('t.*, o.name as currency_name, o.symbol as currency_symbol');
        $this->db->from("$this->_countries_table t");
        $this->db->join('currencies o', 't.currency_id = o.id', 'left');

        if ($id == null)
            return $this->db->get()->result();

        if (is_array($id)) {
            $this->db->where_in('t.id', $id);
            return $this->db->get($this->_countries_table)->result();
        }

        $this->db->where('t.id', $id);
        return $this->db->get($this->_countries_table)->row();
    }

    public function delete(int $id)
    {
        if ($this->_update_world_shipping_fee($id)) {
            $this->db->trans_begin();
            $this->db->where('id', $id);
            $this->db->delete($this->_countries_table);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            }

            $this->db->trans_rollback();
            return false;
        }

        return false;
    }

    private function _update_world_shipping_fee(int $id)
    {
        $this->db->where('id', $id);
        $country = $this->db->get($this->_countries_table)->row();
        if ($country != null) {
            $this->db->trans_begin();
            $this->db->where('country_id', $id);
            $this->db->where('world_shipping_fee', 0);
            $this->db->update($this->_customer_order_table, ['world_shipping_fee' => $country->shipping_fee]);

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            }

            $this->db->trans_rollback();
            return false;
        }

        return true;
    }

    /**
     * Tỷ giá cho nhóm khách hàng
     * @param int $country_id
     * @param array $customer_rates
     * @return bool
     */
    public function save_customer_rates(int $country_id, array $customer_rates)
    {
        $this->db->trans_begin();
        $this->db->where('country_id', $country_id);
        $this->db->delete($this->_customer_rate_table);

        for ($i = 0; $i < count($customer_rates); $i++) {
            $customer_rates[$i]['country_id'] = $country_id;
        }

        $this->db->insert_batch($this->_customer_rate_table, $customer_rates);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get_customer_rates(int $country_id)
    {
        $this->db->where('country_id', $country_id);
        return $this->db->get($this->_customer_rate_table)->result();
    }
	
    public function get_customer_group_rates($country_id, $customer_group_id)
    {
        $this->db->where('country_id', $country_id);
        $this->db->where('customer_group_id', $customer_group_id);
        return $this->db->get($this->_customer_rate_table)->result();
    }
}