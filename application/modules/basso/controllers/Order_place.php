<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_place
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Order_label_model $order_place_model
 * @property CI_Form_validation $form_validation
 */
class Order_place extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_place_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Hệ thống'], ['text' => 'Nơi chốt đơn']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $data = $this->order_place_model->get();
        json_success(null, ['data' => $data]);
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $limit = $this->order_place_model->get($id);
        if ($limit == null)
            json_error('Không tìm thấy nơi chốt đơn');

        if ($this->order_place_model->delete($id))
            json_success('Xóa nơi chốt thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $id = $this->input->post('id');
        $name = $this->input->post('name');

        if ($this->order_place_model->insert($id, $name))
            json_success($id == 0 ? 'Thêm nơi chốt thành công' : 'Cập nhật thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}