<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Level
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Currency_model $currency_model;
 */
class Level extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('level_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Quản lý cấp độ thành viên']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        json_success(null, ['data' => $this->level_model->get()]);
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        if ($this->level_model->delete($id))
            json_success('Xóa thành công');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $id = $this->input->post('id');
        $name = $this->input->post('name');

        $result = $this->level_model->insert($id, $name);
        if (!$result)
            json_error('Có lỗi, vui lòng F5 thử lại');

        json_success($id == 0 ? 'Thêm thành công' : 'Cập nhật thành công');
    }
}