<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Subscribe
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Subscribe_model $subscribe_model
 */
class Subscribe extends Cpanel_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('subscribe_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Email đăng ký nhận tin']]);
        return $this->blade->render();
    }

    public function export()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');

        if ($this->form_validation->run()) {
            $time = $this->input->get('time');
            $from = $this->input->get('start');
            $to = $this->input->get('end');

            switch ($time) {
                case 'week':
                    $day = strtotime('this week', time());
                    $start = strtotime(date('Y-m-d 00:00:00', $day));
                    $end = time();
                    break;
                case 'month':
                    $start = strtotime(date('Y-m-1 00:00:00'));
                    $end = time();
                    break;
                case 'custom':
                    $start = strtotime("$from 00:00:00");
                    $end = strtotime("$to 23:59:59");
                    break;
                case 'all':
                    $start = strtotime('2018-10-10');
                    $end = time();
                    break;
                default:
                    $start = strtotime(date('Y-m-d 00:00:00'));
                    $end = time();
                    break;
            }

            $result = $this->subscribe_model->get(0, $start, $end);
            $str = implode(PHP_EOL, array_column($result, 'email'));
            header("Content-type: text/plain");
            header("Content-Disposition: attachment; filename=subscription-list.txt");

            echo $str;
        }
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');

        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $page = $this->input->get('page');
        $page = $page <= 1 ? 1 : $page;


        $time = $this->input->get('time');
        $from = $this->input->get('start');
        $to = $this->input->get('end');

        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $start = strtotime(date('Y-m-d 00:00:00', $day));
                $end = time();
                break;
            case 'month':
                $start = strtotime(date('Y-m-1 00:00:00'));
                $end = time();
                break;
            case 'custom':
                $start = strtotime("$from 00:00:00");
                $end = strtotime("$to 23:59:59");
                break;
            case 'all':
                $start = strtotime('2018-10-10');
                $end = time();
                break;
            default:
                $start = strtotime(date('Y-m-d 00:00:00'));
                $end = time();
                break;
        }

        $result = $this->subscribe_model->get($page, $start, $end);
        $total_item = $this->subscribe_model->count($start, $end);

        json_success(null, ['data' => $result, 'pagination' => Pagination::calc($total_item, $page)]);
    }
}