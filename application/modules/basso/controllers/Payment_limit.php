<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Payment
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Payment_limit_model $payment_limit_model
 * @property CI_Form_validation $form_validation
 */
class Payment_limit extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_limit_model');
    }

    public function index()
    {
        $breadcrumbs = [['text' => 'Thanh toán'], ['text' => 'Hạn mức thanh toán']];
        $this->blade->set('breadcrumbs', $breadcrumbs);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $data = $this->payment_limit_model->get();
        json_success(null, ['data' => $data]);
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->form_validation->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $limit = $this->payment_limit_model->get($id);
        if ($limit == null)
            json_error('Không tìm thấy hạn mức thanh toán');

        if ($this->payment_limit_model->delete($id))
            json_success('Xóa hạn mức thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('limit', null, 'required|numeric');
        $this->form_validation->set_rules('fee', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $id = $this->input->post('id');
        $limit = $this->input->post('limit');
        $fee = $this->input->post('fee');
        $des = $this->input->post('des');

        if ($this->payment_limit_model->insert($id, $limit, $fee, $des))
            json_success($id == 0 ? 'Thêm hạn mức thành công' : 'Cập nhật thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}