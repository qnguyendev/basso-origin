<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_term
 * Danh mục phụ thu
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Order_term_model $order_term_model
 * @property CI_Form_validation $form_validation
 */
class Order_term extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_term_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Hệ thống'], ['text' => 'Phụ thu danh mục']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('page', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $page = $this->input->get('page');
        $result = $this->order_term_model->get(0, $page);
        $total = $this->order_term_model->count();
        $pagination = Pagination::calc($total, $page);

        json_success(null, ['data' => $result, 'pagination' => $pagination]);
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        if($id == 8)
            json_error('Bạn không được phép xóa danh mục phụ thu này');

        $term = $this->order_term_model->get($id);
        if ($term == null)
            json_error('Không tìm thấy danh mục');

        if ($this->order_term_model->delete($id))
            json_success('Xóa danh mục phụ thu thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('active', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $steps = $this->input->post('steps');
        $active = $this->input->post('active');
        $categories = $this->input->post('categories');
        $min_fee = $this->input->post('min_fee');

        if ($this->order_term_model->insert($id, $name, $min_fee, $active, $steps, $categories))
            json_success($id == 0 ? 'Thêm danh mục thành công' : 'Cập nhật thành công');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}