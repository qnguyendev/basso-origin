<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Quản lý warehouse
                        <a href="#" data-bind="click: modal.show" class="float-right" data-bind="click: modal.show">
                            <i class="fa fa-plus"></i>
                            Thêm warehouse
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Ghi chú</th>
                            <th width="200"></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result">
                        <tr>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: note"></td>
                            <td class="text-center">
                                <a href="#" class="btn btn-link" data-bind="click: $root.modal.init">
                                    <i class="fa fa-edit"></i>
                                    Cập nhật
                                </a>
                                <button class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/editor_modal')
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection