function viewModel() {
    let self = this;
    self.id = ko.observable();
    self.limit = ko.observable(0);
    self.fee = ko.observable();
    self.des = ko.observable();
    self.result = ko.mapping.fromJS([]);

    self.add_limit = function () {
        self.id(0);
        self.limit(0);
        self.fee(0);
        self.des(undefined);

        MODAL.show('#editor-modal');
    };

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.edit = function (item) {
        self.id(item.id());
        self.limit(item.limit());
        self.fee(item.fee());
        self.des(item.description());

        MODAL.show('#editor-modal');
    };

    self.delete = function (item) {
        ALERT.confirm('Bạn muốn xóa hạn mức?', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    self.init();
                    ALERT.success(res.message);
                }
            });
        });
    };

    self.save = function () {
        let data = {
            id: self.id(),
            limit: self.limit(),
            fee: self.fee(),
            des: self.des()
        };

        AJAX.post(window.location, data, true, (res) => {
            if (res.error)
                ALERT.error(res.message);
            else {
                ALERT.success(res.message);
                MODAL.hide('#editor-modal');
                self.init();
            }
        });
    };
}

let model = new viewModel();
model.init();
ko.applyBindings(model, document.getElementById('main-content'));