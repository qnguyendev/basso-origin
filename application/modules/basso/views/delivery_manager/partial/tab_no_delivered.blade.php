<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<table class="table table-striped">
    <thead>
    <tr>
        <th class="border-top-0">Ngày mua</th>
        <th class="border-top-0" width="90">Mã ĐH</th>
        <th class="border-top-0">Khách hàng</th>
        <th class="border-top-0">Order number</th>
        <th class="border-top-0">Tracking website</th>
        <th class="border-top-0">Ghi chú</th>
        <th class="border-top-0"></th>
    </tr>
    </thead>
    <tbody data-bind="foreach: result" class="text-center">
    <tr>
        <td data-bind="text: moment.unix(buy_date()).format('DD/MM/YYYY')"></td>
        <td>
            <a data-bind="text: order_code() == null ? customer_order_id() : order_code(), attr: {href: `/basso/customer_order/detail/${customer_order_id()}`}"
               target="_blank"></a>
        </td>
        <td data-bind="text: name"></td>
        <td data-bind="text: order_number"></td>
        <td data-bind="text: tracking_code"></td>
        <td data-bind="popover: '#note-' + id()">
            <input type="text" class="form-control input-sm" data-bind="value: note"/>
            <div data-bind="attr: {id: 'note-' + id()}" style="display: none">
                <div class="popover-heading">
                    Ghi chú
                </div>
                <div class="popover-body">
                    <!--ko if: note() !== undefined-->
                    <!--ko if: note() !== null-->
                    <!--ko if: note() !== ''-->
                    <p data-bind="text: note"></p>
                    <!--/ko-->
                    <!--/ko-->
                    <!--/ko-->
                </div>
            </div>
        </td>
        <td>
            <button class="btn btn-xs btn-primary" data-bind="click: $root.update">
                <i class="fa fa-check"></i>
                Cập nhật
            </button>
        </td>
    </tr>
    </tbody>
</table>