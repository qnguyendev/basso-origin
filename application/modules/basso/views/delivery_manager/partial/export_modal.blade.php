<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="export-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Xuất file Excel</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="form-group">
                    <label>Lựa chọn nhân viên duyệt đơn</label>
                    <select class="form-control" multiple data-bind="selectedOptions: export_modal.user_id, options: tab_modal.users,
                    optionsText: 'name', optionsValue: 'id'">
                    </select>
                </div>
            </form>
            <div class="modal-footer">
                <i class="float-left">Bỏ trống để lựa chọn tất cả</i>
                <button type="button" class="btn btn-success" data-bind="click: export_modal.do">
                    <i class="fa fa-file-export"></i>
                    Xuất file
                </button>
            </div>
        </div>
    </div>
</div>