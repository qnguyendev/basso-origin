<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="update-tracking-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    Cập nhật mã tracking
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <textarea class="form-control" data-bind="value: update_tracking.data" rows="15"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Đóng lại
                </button>
                <button type="button" class="btn btn-success" data-bind="click: update_tracking.post">
                    <i class="fa fa-check"></i>
                    Cập nhật
                </button>
            </div>
        </div>
    </div>
</div>