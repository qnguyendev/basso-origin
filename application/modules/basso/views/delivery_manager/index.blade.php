<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$index = 1;
?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
				 <a href="#" class="float-right" data-bind="click: export_tracking">
                    <i class="fa fa-file-export"></i>
                    Xuất tracking
                </a>
                <a href="#" class="float-right mr-3" data-bind="click: update_tracking.show">
                    <i class="fa fa-mail-bulk"></i>
                    Cập nhật ngày về kho VC
                </a>
				Quản lý vận chuyển
			</div>
        </div>
        <form autocomplete="off" class="card-body">

            <div class="row">
                <div class="form-group col-md-6 col-12 col-lg-3 col-xl-2">
                    <label>Warehouse</label>
                    <select class="form-control" data-bind="value: filter.warehouse_id">
                        <option value="0">Tất cả</option>
                        @foreach($warehouses as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6 col-12 col-lg-3 col-xl-2">
                    <label>Tìm kiếm từ khóa</label>
                    <input type="text" class="form-control" data-bind="value: filter.key"
                           placeholder="Tìm theo tracking, khách hàng..."/>
                </div>
                <div class="form-group col-md-6 col-12 col-lg-3 col-xl-2">
                    <label>&nbsp;</label>
                    <button class="btn btn-success btn-block" data-bind="click: function(){ $root.search(1); }">
                        <i class="fa fa-search"></i>
                        TÌM
                    </button>
                </div>
            </div>

            <div role="tabpanel">
                <ul class="nav nav-tabs" role="tablist">
                    @foreach(DeliveryManager::LIST_STATE as $key=>$value)
                        <li class="nav-item" role="presentation">
                            <a class="nav-link {{$index == 1 ? 'active show' : ''}}" href="#{{$key}}" role="tab"
                               data-toggle="tab" rel="{{$key}}"
                               data-bind="click: function(){set_current('{{$key}}')}">
                                {{$value}}
                            </a>
                        </li>
                        <?php $index++; ?>
                    @endforeach
                </ul>
            </div>

            <div class="tab-content">
                @foreach(DeliveryManager::LIST_STATE as $key=>$value)
                    <div class="tab-pane {{$key == 'all' ? 'active' : null}}" id="{{$key}}" role="tabpanel">
                        <div class="table-responsive">
                            @include('partial/tab_'.$key)
                        </div>
                    </div>
                @endforeach

                <div class="pt-4 pb-2 text-primary text-center" data-bind="visible: result().length === 0">
                    <H4>KHÔNG TÌM THẤY ĐƠN HÀNG</H4>
                </div>
                @include('pagination')
            </div>
        </form>
    </div>
@endsection
@section('script')
	<script src="{{load_js('line-number')}}"></script>
    <script src="{{load_js('index')}}"></script>
@endsection

@section('modal')
	@include('partial/tracking_code_modal')
    @include('partial/update_tracking_code_modal');
    @include('partial/export_modal');
@endsection

@section('head')
    <style>
        #tracking-code-modal textarea {
            width: 100% !important;
            padding-left: 60px;
        }

        #tracking-code-modal .modal-body div textarea {
            position: absolute;
            width: 60px !important;
            top: 22px;
            bottom: 22px;
            background: transparent;
            border: 0;
            left: 15px;
            z-index: 99;
            padding-left: 5px;
            margin: 0 !important;
            color: #777;
            resize: none;
        }
    </style>
@endsection