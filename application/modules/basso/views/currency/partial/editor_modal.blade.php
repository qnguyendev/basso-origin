<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="editor-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    data-bind="text: modal.id() == 0 ? 'Thêm ngoại tệ' : 'Cập nhật ngoại tệ'">
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Tên ngoại tệ
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: modal.name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: modal.name"/>
                </div>
                <div class="form-group">
                    <label>
                        Biểu tượng/viết tắt
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: modal.symbol"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: modal.symbol"/>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: modal.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>