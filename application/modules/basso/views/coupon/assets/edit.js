function viewModel() {
    let self = this;
    self.id = ko.observable(0);
    self.code = ko.observable().extend({required: {message: LABEL.required}});
    self.type = ko.observable('rate').extend({required: {message: LABEL.required}});
    self.amount = ko.observable('0').extend({required: {message: LABEL.required}});
    self.actived = ko.observable(1);
    self.from = ko.observable(moment(new Date()).format('DD-MM-YYYY')).extend({required: {message: LABEL.required}});
    self.to = ko.observable(moment(new Date()).format('DD-MM-YYYY')).extend({required: {message: LABEL.required}});
    self.currencies_rates = ko.observableArray([]);

    self.init = function () {
        for (let i = 0; i < warehouses.length; i++) {
            let item = warehouses[i];
            self.currencies_rates.push({
                currency_name: ko.observable(item.currency_name),
                currency_symbol: ko.observable(item.currency_symbol),
                currency_id: ko.observable(item.id),
                customer_rate: ko.observable(item.customer_rate),
                rate: ko.observable('0').extend({required: {message: LABEL.required}})
            });
        }

        if (window.location.toString().indexOf('/edit/') > 0) {
            self.id(data.id);
            self.code(data.code);
            self.type(data.type);
            self.from(moment.unix(data.from).format('DD-MM-YYYY'));
            self.to(moment.unix(data.to).format('DD-MM-YYYY'));

            for (let i = 0; i < self.currencies_rates().length; i++) {

            }
        }
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                code: self.code(),
                type: self.type(),
                amount: self.amount(),
                actived: self.actived(),
                from: self.from(),
                to: self.to()
            };
        }
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));