function viewModel() {
    let self = this;
    let date = new Date();
    date.setDate(date.getDate() - 14);

    self.result = ko.mapping.fromJS([]);
    self.result_items = ko.mapping.fromJS([]);
	
    self.pagination = ko.observable();
	
    self.name_returned1 = ko.observable();

    self.filter = {
        time: ko.observable('all'),
        status: ko.observable('all'),
        start: ko.observable(moment(date).format('DD-MM-YYYY')),
        end: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        key: ko.observable()
    };

    self.modal = {
        filter: {
            start: ko.observable(moment(date).format('DD-MM-YYYY')),
            end: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
            key: ko.observable()
        },
        result: ko.mapping.fromJS([]),
        pagination: ko.observable(),
        show: function () {
            self.modal.filter.key(undefined);
            self.modal.search(1);
            MODAL.show('#orders-modal');
        },
        search: function (page) {
            let data = {
                start: self.modal.filter.start(),
                end: self.modal.filter.end(),
                key: self.modal.filter.key(),
                page: page,
                action: 'modal-search'
            };

            AJAX.get(window.location, data, true, (res) => {
                if (!res.error) {
                    ko.mapping.fromJS(res.data, self.modal.result);
                    self.modal.pagination(res.pagination);
                } else
                    NOTI.danger(res.message);
            });
        },
        select: function (item) {
            window.location = `/basso/return_order/create/${item.id()}`;
        }
    };

    self.search = function (page) {
        let data = {
            page: page,
            action: 'search',
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end(),
            key: self.filter.key(),
            status: self.filter.status()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            ko.mapping.fromJS(res.items, self.result_items);
            self.pagination(res.pagination);
        });
    };

    self.toggle_row = function (data, event) {
		var _html = "";
		self.result_items.forEach(function(entry) {
			if(data.id() == entry.order_returned_id()) {
				_html += '<tr><td class="text-center p-1"><img src = "/timthumb.php?src='+entry.image_path()+'&w=200&h=200" class="img-fluid ie-fix-flex"/></td><td>'+entry.name_returned()+'</td><td>'+parseInt(entry.price()).toMoney(0)+'</td><td>'+entry.quantity()+'</td><td>'+parseInt(entry.price() * entry.quantity()).toMoney(0)+'</td></tr>';
			}
		});
		
		$(event.currentTarget).parent().parent().next().find(".row_item_detail").html(_html);
        toggle_row(event.currentTarget);
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));