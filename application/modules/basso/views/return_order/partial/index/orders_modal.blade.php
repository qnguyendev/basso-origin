<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="orders-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Chọn đơn hàng
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <table class="table border-vertical">
                    <tr>
                        <th class="text-right">Từ ngày</th>
                        <td><input type="text" class="form-control" data-bind="datePicker: modal.filter.start"/></td>
                        <th class="text-right">đến ngày</th>
                        <td><input type="text" class="form-control" data-bind="datePicker: modal.filter.end"/></td>
                        <td>
                            <input type="text" class="form-control" data-bind="value: modal.filter.key"
                                   placeholder="Tìm mã đơn hàng, email, tên, sđt.."/>
                        </td>
                        <td>
                            <button class="btn btn-primary" data-bind="click: function(){ $root.modal.search(1); }">
                                <i class="fa fa-search"></i>
                                TÌM ĐƠN HÀNG
                            </button>
                        </td>
                    </tr>
                </table>

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Mã ĐH</th>
                        <th>Ngày tạo</th>
                        <th>Nhân viên</th>
                        <th>Khách hàng</th>
                        <th class="text-right">Tổng tiền</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: modal.result" class="text-center">
                    <tr style="cursor: pointer;">
                        <th>
                            <a target="_blank"
                               data-bind="text: order_code() == null ? id() : order_code(), attr: {href: `/basso/customer_order/detail/${id()}`}"></a>
                        </th>
                        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY'), click: $root.modal.select"></td>
                        <td data-bind="text: user() == null ? 'Website' : user(), click: $root.modal.select"></td>
                        <td data-bind="text: name, click: $root.modal.select"></td>
                        <td data-bind="text: parseInt(sub_total()).toMoney(0), click: $root.modal.select" class="text-right"></td>
                    </tr>
                    </tbody>
                </table>

                @include('partial/index/pagination')

                <div class="text-info text-center" data-bind="visible: modal.result().length == 0">
                    <h4>Không tìm thấy đơn hàng</h4>
                </div>
            </form>
        </div>
    </div>
</div>

