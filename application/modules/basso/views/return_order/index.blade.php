<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                Đơn hoàn trả
                <a href="#" class="float-right" data-bind="click: modal.show">
                    <i class="fa fa-plus"></i>
                    Trả hàng
                </a>
            </div>
        </div>
        <div class="card-body">
            <form autocomplete="off" class="form-group row">
                <div class="col-md-6 col-lg-2">
                    <label>Thời gian</label>
                    <select class="form-control" data-bind="value: filter.time">
                        <option value="all">Tất cả</option>
                        <option value="today">Hôm nay</option>
                        <option value="week">Trong tuần</option>
                        <option value="week">Trong tháng</option>
                        <option value="custom">Tùy chọn</option>
                    </select>
                </div>
                <div class="col-md-6 col-lg-2">
                    <label>Từ ngày</label>
                    <input type="text" class="form-control"
                           data-bind="datePicker: filter.start, enable: filter.time() == 'custom'"/>
                </div>
                <div class="col-md-6 col-lg-2">
                    <label>Đến ngày</label>
                    <input type="text" class="form-control"
                           data-bind="datePicker: filter.end, enable: filter.time() == 'custom'"/>
                </div>
                <div class="col-md-6 col-lg-2">
                    <label>Trạng thái</label>
                    <select class="form-control" data-bind="value: filter.status">
                        <option value="all">Tất cả</option>
                        @foreach(CustomerOrdersReturnedStatus::LIST_STATE as $key=>$value)
                            <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 col-lg-2">
                    <label>Tìm kiếm</label>
                    <input type="text" class="form-control" data-bind="value: filter.key"
                           placeholder="Tìm theo khách hàng..."/>
                </div>
                <div class="col-md-6 col-lg-2">
                    <label>&nbsp;</label>
                    <button class="btn btn-primary btn-block" data-bind="click: function(){$root.search(1);}">
                        <i class="fa fa-search"></i>
                        Tìm kiếm
                    </button>
                </div>
            </form>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Mã ĐH</th>
                    <th>Khách hàng</th>
                    <th>Tổng tiền</th>
                    <th>Ngày nhận</th>
                    <th>Ghi chú</th>
                    <td></td>
                </tr>
                </thead>
                <tbody data-bind="foreach: result" class="text-center">
                <tr>
                    <th data-bind="text: $index() + 1"></th>
                    <td>
                        <a data-bind="text: order_code() == null ? order_id() : order_code(), attr: {href: `/basso/customer_order/detail/${order_id()}`}"></a>
                    </td>
                    <td data-bind="text: name"></td>
                    <td data-bind="text: parseInt(sub_total()).toMoney(0)" class="text-right pr-3"></td>
                    <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                    <td data-bind="text: note"></td>
                    <td>
                        <a href="javascript:" class="order-expand" data-bind="click: $root.toggle_row">
                            <i class="fa fa-eye"></i>
                            Chi tiết
                        </a>
                    </td>
                </tr>
                <tr class="tr-collapse">
                    <td colspan="7">
                        <div class="item-detail" style="padding: 15px;">
                            <div class="row">
                                <div class="col-md-12 col-lg-8 offset-lg-2">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="80"></th>
                                            <th>Tên</th>
                                            <th>Giá hoàn</th>
                                            <th>Số lượng</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                        </thead>
                                        <tbody data-bind="foreach: items" class="text-center row_item_detail">
                                        <tr>
                                            <td class="text-center p-1">
                                                <img data-bind="attr: {src: `/timthumb.php?src=${image_path()}&w=200&h=200`}"
                                                     class="img-fluid ie-fix-flex"/>
                                            </td>
                                            <td data-bind="text: name"></td>
                                            <td data-bind="text: parseInt(price()).toMoney(0)"></td>
                                            <td data-bind="text: quantity"></td>
                                            <td data-bind="text: parseInt(price() * quantity()).toMoney(0)"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            @include('pagination')
        </div>
    </div>
@endsection

@section('modal')
    @include('partial/index/orders_modal')
@endsection

@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection