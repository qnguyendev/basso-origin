function viewModel() {
    let self = this;
    ko.utils.extend(self, new imageUploadModel());

    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.current_tab = ko.observable('all');
    self.delivered_date = ko.observable(moment(new Date()).format('DD-MM-YYYY'));
	self.change_branch = ko.observable();

    self.filter = {
        warehouse_id: ko.observable(0),
        date: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        time: ko.observable('all'),
        key: ko.observable(),
        branch: ko.observable()
    };

    self.set_tab = function (status) {
        self.current_tab(status);
        self.search(1);
    };

    self.import_modal = {
        data: ko.observable(),
        items: ko.mapping.fromJS([]),
        warehouse_id: ko.observable(),
		branch: ko.observable(),
        show: function () {
            self.import_modal.data(undefined);
            ko.mapping.fromJS([], self.import_modal.items);
            self.import_modal.errors.showAllMessages(false);

            MODAL.show('#import-modal');
            $('a[rel=tracking-tab]').click();
        },
        search: function () {
            if (self.import_modal.data() !== undefined) {
                let raw = self.import_modal.data().split('\n');
                if (raw.length === 0)
                    NOTI.danger('Bạn chưa nhập tracking');
                else {
                    let data = [];
					
					if(raw.length == 2) {
						if((raw[0] != "" && raw[1] == "") || (raw[0] == "" && raw[1] != "")) {
							ALERT.error('Không tìm thấy tracking bạn vừa nhập.');
							return;
						}
					}
					
                    for (let i = 0; i < raw.length; i++) {
						if(raw[i].trim() != "") { /* fixcode khi bạn tìm tracking nhập kho nếu nhập enter */
							let item = {tracking: raw[i].split('|')[0].trim(), weight: 0};
							if (raw[i].split('|')[1]) {
								item.weight = raw[i].split('|')[1].trim();
							}

							data.push(item);
						}
                    }

                    AJAX.post(window.location, {action: 'search-tracking', data: data}, true, (res) => {
						$('a[rel=result-tab]').click();
						
						/* fixcode when not found tracking nhap kho */					
						/* if(res.items == null) {
							$("#nhap_kho_not_found").css("display", "block");
							$("#nhap_kho_found").css("display", "none");
						} else {
							$("#nhap_kho_not_found").css("display", "none");
							$("#nhap_kho_found").css("display", "block");
							
						} */
						
						if(res.items != null) {
							ko.mapping.fromJS(res.items, self.import_modal.items);
						} else {
							$("#nhap_kho_found table tbody tr").remove();
							ko.mapping.fromJS(res.items, "");
						}
			
						if(res.items == null) 
							ALERT.error('Tracking này đã được nhập kho.');
						if(res.items == "") 
							ALERT.error('Tracking này đã được nhập kho.');
                    });
                }
            } else
                NOTI.danger('Bạn chưa nhập tracking');
        },
        /*delete: function (item) {
            self.import_modal.items.remove(item);
        },*/
        save: function () {
            if (!self.import_modal.isValid())
                self.import_modal.errors.showAllMessages();
            else {
                let items_no_name = self.import_modal.items.filter((x) => {
                    return x.name() == null || x.name() == undefined;
                });

                if (items_no_name.length > 0)
                    NOTI.danger('Vui lòng nhập tên sản phẩm');
				else if(self.import_modal.warehouse_id() == "") {
					NOTI.danger('Vui lòng chọn warehouse');
				} else if(self.change_branch() == "") {
					NOTI.danger('Vui lòng chọn chi nhánh');
				}
                else {
                    let items = [];
                    self.import_modal.items.each((x) => {
                        items.push({
                            web_tracking: x.web_tracking(),
                            real_tracking: x.real_tracking(),
                            weight: x.weight(),
                            name: x.name(),
                            customer_items: ko.toJSON(x.customer_items()),
                            quantity: x.quantity(),
                            /* delivered_date: x.delivered_date(), */ /* fixcode */
                            delivered_date: self.delivered_date,
                            status: x.status(),
                            total_weight: x.weight(),
                            customer_order_id: x.customer_order_id(),
                            branch: self.change_branch() ? self.change_branch() : x.branch()
                        });
                    });

                    let data = {
                        warehouse_id: self.import_modal.warehouse_id(),
                        items: items,
                        action: 'save-package'
                    };

                    AJAX.post(window.location, data, true, (res) => {
                        if (res.error)
                            ALERT.error(res.message);
                        else {
                            ALERT.success(res.message);
                            MODAL.hide('#import-modal');
                            self.search(1);
                        }
                    });
                }
            }
        }
    };

    self.search = function (page) {
        let data = {
            page: page,
            status: self.current_tab(),
            warehouse_id: self.filter.warehouse_id(),
            time: self.filter.time(),
            date: self.filter.date(),
            key: self.filter.key(),
            branch: self.filter.branch()
        };

        AJAX.get(window.location, data, true, (res) => {
            self.pagination(res.pagination);
            ko.mapping.fromJS(res.data, self.result);
            ko.mapping.fromJS(res.items, self.items.list);
        });
    };

    self.toggle_row = function (data, event) {
        let items = self.items.list.filter((x) => {
            return data.id() == x.package_id();
        });

        data.items(items);
        data.items.each((x) => {
            x.package_id(data.id());
        });

        toggle_row(event.currentTarget);
    };

    self.update_package = function (item) {
        let data = {
            action: 'update-package',
            id: item.id(),
            real_tracking: item.real_tracking(),
            web_tracking: item.web_tracking(),
            status: item.status(),
            lost_pending: item.lost_pending(),
            /* total_weight: item.total_weight(), */ /* fixcode */
            total_weight: item.total_product_weight(),
            name: item.name(),
            branch: item.branch
        };

        AJAX.post(window.location, data, true, (res) => {
            if (!res.error) {
                self.search(1);
                NOTI.success(res.message);

                //  Chia đều cân nặng cho các sp match được
                if (item.items().length > 0) {
                    let weight = parseFloat(item.total_weight()) / item.items().length;
                    item.items.each((x) => {
                        x.weight(parseFloat(weight.toFixed(2)));
                    });
                }
            } else
                ALERT.error(res.message);
        });
    };

    self.update_pending_status = function (item) {
        AJAX.post(window.location, {id: item.id(), action: 'update-lost-status'}, true, function (res) {
            item.lost_pending(item.lost_pending() == 0 ? 1 : 0);
            NOTI.success('Cập nhật thành công');
        });

    };

    self.items = {
        list: ko.mapping.fromJS([]),
        upload_image: function (item) {
            self.image_uploader.show(true);
            self.image_uploader.save = function () {
                item.image_id(self.image_uploader.selected_images[0].id);
                item.image_path(self.image_uploader.selected_images[0].path);
                self.image_uploader.close();

                let data = {action: 'upload-image', item_id: item.id(), image_id: item.image_id()};
                AJAX.post(`/basso/customer_order/detail/${item.order_id()}`, data, false, (res) => {
                    if (!res.error)
                        NOTI.success(res.message);
                });
            };
        },
		
		/* fixcode */
		upload_image_notfound: function (item) {
            self.image_uploader.show(true);
            self.image_uploader.save = function () {
                item.image_id(self.image_uploader.selected_images[0].id);
                item.image_path(self.image_uploader.selected_images[0].path);
                self.image_uploader.close();

                let data = {action: 'upload-image', package_id: item.id(), image_id: item.image_id()};
                AJAX.post(`/basso/inventory`, data, false, (res) => {
                    if (!res.error)
                        NOTI.success(res.message);
                });
            };
        },
		
		notfound_remove_image: function (item) {
            let data = {action: 'remove-image', package_id: item.id(), image_id: item.image_id()};
            AJAX.post(`/basso/inventory`, data, false, (res) => {
                if (!res.error) {
                    item.image_path(undefined);
                    NOTI.success(res.message);
                }
            });
        },
		/* end fixcode */
		
        //  Xóa ảnh sản phẩm
        remove_image: function (item) {
            let data = {action: 'remove-image', item_id: item.id(), image_id: item.image_id()};
            AJAX.post(`/basso/customer_order/detail/${item.order_id()}`, data, false, (res) => {
                if (!res.error) {
                    item.image_path(undefined);
                    NOTI.success(res.message);
                }
            });
        },
        update: function (item) {
            let data = {
                action: 'update-item-weight',
                item_id: item.id(),
                weight: item.weight(),
                package_id: item.package_id(),
                order_id: item.order_id(),
            };

            AJAX.post(window.location, data, false, (res) => {
                if (!res.error) {
                    self.result.each((x) => {
                        if (x.id() == item.package_id()) {
                            let total_weight = 0;
                            x.items.each((t) => {
                                total_weight += parseFloat(t.weight());
                            });

                            x.total_weight(total_weight.toFixed(2));
                        }
                    });

                    NOTI.success(res.message);
                } else
                    ALERT.error(res.message);
            });
        }
    };
}

let model = new viewModel();
model.search(1);
ko.validatedObservable(model.import_modal);
ko.applyBindings(model, document.getElementById('main-content'));