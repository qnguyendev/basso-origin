<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title"> 
                <button class="btn btn-success btn-xs text-uppercase float-right" data-bind="click: import_modal.show">
                    <i class="fa fa-file-import"></i>
                    Nhập kho
                </button>
                Quản lý kho
            </div>
        </div>
        <form autocomplete="off" class="card-body">
            @include('partial/filter')
            <div role="tabpanel" style="margin: 0 -1px -1px -1px">
                <!-- Nav tabs-->
                @include('partial/tabs')

                <div class="tab-content p-0">
                    <div class="tab-pane active" id="tab1" role="tabpanel">
                        <div class="table-responsive">
                            @include('partial/tabs/all')
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2" role="tabpanel">
                        <div class="table-responsive">
                            @include('partial/tabs/not_found')
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3" role="tabpanel">
                        <div class="table-responsive">
                            @include('partial/tabs/lost')
                        </div>
                    </div>
                </div>
            </div>
            @include('pagination')
        </form>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection
@section('modal')
    @include('partial/import_modal')
@endsection
@section('css')
    <style>
        @media (min-width: 1200px) {
            .modal-xxl {
                max-width: 1140px;
            }
        }

        @media (min-width: 1600px) {
            .modal-xxl {
                max-width: 1366px;
            }
        }

        @media (min-width: 1920px) {
            .modal-xxl {
                max-width: 1600px;
            }
        }
    </style>
@endsection