<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">Lịch sử báo giá</div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 form-group col-lg-3 col-xl-2">
                    <label>Thời gian</label>
                    <select class="form-control" data-bind="value: filter.time">
                        <option value="today">Hôm nay</option>
                        <option value="week">Trong tuần</option>
                        <option value="month">Trong tháng</option>
                        <option value="all">Tất cả</option>
                        <option value="custom">Tùy chọn</option>
                    </select>
                </div>
                <div class="col-md-6 form-group col-lg-3 col-xl-2">
                    <label>Từ ngày</label>
                    <input type="text" class="form-control"
                           data-bind="datePicker: filter.start, enable: filter.time() == 'custom'"/>
                </div>
                <div class="col-md-6 form-group col-lg-3 col-xl-2">
                    <label>đến ngày</label>
                    <input type="text" class="form-control"
                           data-bind="datePicker: filter.end, enable: filter.time() == 'custom'"/>
                </div>
                <div class="col-md-6 form-group col-lg-3 col-xl-2">
                    <label>&nbsp;</label>
                    <button class="btn btn-dark btn-block" data-bind="click: function(){ $root.search(1);}">
                        <i class="fa fa-search"></i>
                        THÔNG KÊ
                    </button>
                </div>
				
				<div class="col-md-6 form-group col-lg-3 col-xl-4">
                    <label>Số lượt báo giá thành công: <span data-bind="text: count_success"></span> lượt</label>
                    <label>Số lượt báo giá không thành công: <span data-bind="text: count_fail"></span> lượt</label>
                    <label>Số lượt tim kiếm từ khóa link: <span data-bind="text: count_keyword_weblink"></span> lượt</label>
                </div>
            </div>

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Thời gian</th>
                    <th>IpAddress</th>
                    <th>Link sản phẩm</th>
                    <th>Trạng thái</th>
                </tr>
                </thead>
                <tbody data-bind="foreach: result">
                <tr>
                    <th data-bind="text: $index() + 1"></th>
                    <td class="text-center" data-bind="text: moment.unix(created_time()).format('HH:MM DD/MM/YYYY')"></td>
                    <td class="text-center" data-bind="text: ipaddress"></td>
                    <td style="max-width: 300px">
                        <a  data-bind="text: url, attr: {href: url}" target="_blank"></a>
                    </td>
                    <td class="text-center">
                        <span class="badge badge-success" data-bind="visible: is_success() == 1">Thành công</span>
                        <span class="badge badge-danger" data-bind="visible: is_success() == 0">Không thành công</span>
                    </td>
                </tr>
                </tbody>
            </table>
            @include('pagination')
        </div>
    </div>
@endsection

@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection