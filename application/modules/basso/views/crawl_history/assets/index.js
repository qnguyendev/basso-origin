function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.count_success = ko.observable();
    self.count_fail = ko.observable();
    self.count_keyword_weblink = ko.observable();

    self.filter = {
        time: ko.observable('all'),
        start: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        end: ko.observable(moment(new Date()).format('DD-MM-YYYY'))
    };

    self.search = function (page) {
        let data = {
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end(),
            page: page
        };

        AJAX.get(window.location, data, true, (res) => {
                if (!res.error) {
                    ko.mapping.fromJS(res.data, self.result);
                    self.pagination(res.pagination);
                    self.count_success(res.count_success);
                    self.count_fail(res.count_fail);
                    self.count_keyword_weblink(res.count_keyword_weblink);
                } else
                    ALERT.error(res.message);
            }
        );
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));