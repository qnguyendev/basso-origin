<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<html>
<head></head>
<body></body>
<script src="https://tatmart.com/statics/asset/client/js/jquery-1.9.1.js"></script>
<script src="https://tatmart.com/statics/asset/client/js/jquery-ui.js"></script>
<script src="https://tatmart.com/statics/asset/client/js/common.js"></script>
<script src="https://tatmart.com/statics/asset/client/js/jquery-url.js"></script>
<script src="https://tatmart.com/statics/asset/client/js/jquery-param.js"></script>
<script src="https://tatmart.com/statics/asset/client/js/purl.js"></script>
<script>
    var PageControl = {
        gotoCate: function (id) {
            var currentUrl = window.location.href;
            var parsedUrl = $.url(currentUrl);
            var params = parsedUrl.param();
            delete params["c"];
            var newUrl = parsedUrl.data.attr.directory + "?c=" + id + "&" + $.param(params);
            Common.goTo(newUrl);
        },

        gotoBrand: function (id, check) {
            var currentUrl = window.location.href;

            var parsedUrl = $.url(currentUrl);
            var params = parsedUrl.param();
            var newUrl = '';
            delete params['b'];

            if (check === true)
                newUrl = parsedUrl.data.attr.directory + "?b=" + id + "&" + $.param(params);
            else
                newUrl = parsedUrl.data.attr.directory + "?" + $.param(params);
            Common.goTo(newUrl);
        },

        gotoFilterPrice: function () {
            var currentUrl = window.location.href;
            var parsedUrl = $.url(currentUrl);
            var params = parsedUrl.param();
            delete params["f"];
            delete params["t"];
            f = $("#f").val();
            t = $("#t").val();
            var newUrl = parsedUrl.data.attr.directory + "?f=" + f + "&t=" + t + "&" + $.param(params);
            Common.goTo(newUrl);
        },

        gotoAttribute: function () {
            var sPath = "";
            $('input[type=checkbox]').each(function () {
                var sThisVal = (this.checked ? "1" : "0");
                if (sThisVal == true) {
                    myClass = $(this).attr("class");
                    if (myClass == 'data-attribute') {
                        myVal = $(this).val();
                        if (sPath == "") {
                            sPath = myVal;
                        } else {
                            sPath += "," + myVal;
                        }
                    }
                }
            });
            var currentUrl = window.location.href;
            var parsedUrl = $.url(currentUrl);
            var params = parsedUrl.param();
            delete params["p"];
            if (sPath != "") {
                var newUrl = parsedUrl.data.attr.directory + "?p=" + sPath + "&" + $.param(params);
            } else {
                var newUrl = parsedUrl.data.attr.directory + "?" + $.param(params);
            }
            Common.goTo(newUrl);
        },

        gotoSort: function (id) {
            var currentUrl = window.location.href;
            var parsedUrl = $.url(currentUrl);
            var params = parsedUrl.param();
            delete params["s"];
            newUrl = parsedUrl.data.attr.directory + "?s=" + id + "&" + $.param(params);
            Common.goTo(newUrl);
        },

    };

    $(function () {

        var url = $.url(window.location.href);
        console.log(url);

        $(".data-categories").click(function () {
            iD = $(this).attr("data-id");
            PageControl.gotoCate(iD);
        });
        $('.data-brand').change(function () {
            var val = [];
            $('.data-brand:checked').each(function (i) {
                val[i] = $(this).val();
            });
            val.join();
            var iD = val;
            var check = (val.length > 0);
            PageControl.gotoBrand(iD, check);
        });

        // $(".data-brand").change(function(){
        //     iD  = $(this).val();
        //     check   = $(this).is(':checked');
        //     PageControl.gotoBrand(iD, check);
        // });
        $(".data-attribute").change(function () {
            PageControl.gotoAttribute();
        });
        $("#s").change(function () {
            iD = $(this).val();
            PageControl.gotoSort(iD);
        });

        $("#price").click(function () {
            PageControl.gotoFilterPrice();
        });

        $(".choose-price__price").on("keypress keyup blur", function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });


    });
</script>
</html>
