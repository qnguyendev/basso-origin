<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="shipping-edit-modal" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    Thay đổi thông tin giao hàng
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
               <div class="form-group">
                   <label>Mã vận đơn</label>
                   <div class="input-group">
                       <input type="text" class="form-control" data-bind="value: shipping.code">
                   </div>
               </div>
               <div class="form-group">
                   <label>Phí ship</label>
                   <div class="input-group">
                       <input type="text" class="form-control" data-bind="moneyMask: shipping.fee">
                       <div class="input-group-append">
                           <span class="input-group-text">đ</span>
                       </div>
                   </div>
               </div>
               <div class="form-group">
                   <label>Thu COD</label>
                   <div class="input-group">
                       <input type="text" class="form-control" data-bind="moneyMask: shipping.cod_amount">
                       <div class="input-group-append">
                           <span class="input-group-text">đ</span>
                       </div>
                   </div>
               </div>
               <div class="form-group">
                   <label>Đơn vị vận chuyển</label>
                   <div class="input-group">
					  <select class="form-control" id="ddl-shippings" data-bind="value: shipping.shipping_id">
							<option value="0">Tất cả</option>
							@foreach($shippings as $item)
								<option value="{{$item->id}}">{{$item->name}}</option>
							@endforeach
						</select>
                   </div>
				</div>   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-success" data-bind="click: shipping.save">
                    <i class="fa fa-check"></i>
                    Cập nhật
                </button>
            </div>
        </div>
    </div>
</div>