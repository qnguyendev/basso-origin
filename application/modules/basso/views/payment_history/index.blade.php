<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header no-border">
            <div class="card-title border-0">Lịch sử thanh toán</div>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="form-group col-12 col-md-4 col-lg-3">
                    <label>Thời gian</label>
                    <select class="form-control" data-bind="value: filter_time">
                        <option value="all">Tất cả</option>
                        <option value="today">Hôm nay</option>
                        <option value="yesterday">Hôm qua</option>
                        <option value="month">Trong tháng</option>
                        <option value="custom">Tùy chọn</option>
                    </select>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3">
                    <label>Tùy chọn ngày</label>
                    <div class="input-group">
                        <input type="text" class="form-control"
                               data-bind="datePicker: filter_date, enable: filter_time() == 'custom'" disabled="">
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-calendar"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3">
                    <label>Trạng thái</label>
                    <select class="form-control" data-bind="value: filter_status">
                        <option value="all">Tất cả</option>
                        @foreach(PaymentHistoryStatus::LIST_STATE as $key=>$value)
                            <option value="{{$key}}">{{$value['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3">
                    <label>Nhân viên</label>
                    <div class="input-group input-sm">
                        <select class="form-control" data-bind="value: filter_user_id">
                            <option value="0">Tất cả</option>
                            <!--ko foreach: users-->
                            <option data-bind="value: id, text: name"></option>
                            <!--/ko-->
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-primary btn-sm" data-bind="click: function(){$root.search(1)}">
                                <i class="fa fa-search"></i>
                                TÌM
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Mã ĐH</th>
                        <th>Tổng tiền ĐH</th>
                        <th>Số tiền</th>
                        <th>Ngày TT</th>
                        <th>Ngân hàng</th>
                        <th>Nội dung</th>
                        <th>Nhân viên</th>
                        <th>Trạng thái</th>
                        <th>Thao tác</th>
                    </tr>
                    </thead>
                    <tbody class="text-center" data-bind="foreach: result">
                    <tr>
                        <th>
                            <a target="_blank"
                               data-bind="text: order_code() == null ? order_id() : order_code(), attr:
                               {href: '{{base_url('basso/customer_order/detail/')}}' + order_id()}">
                            </a>
                        </th>
                        <td data-bind="text: parseInt(sub_total()).toMoney(0)"></td>
                        <td data-bind="text: parseInt(amount()).toMoney(0)"></td>
                        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                        <td data-bind="text: bank"></td>
                        <td style="max-width: 350px" data-bind="text: note"></td>
                        <td data-bind="text: user"></td>
                        <td>
                            @foreach(PaymentHistoryStatus::LIST_STATE as $key=>$value)
                                <span class="badge badge-{{$value['class']}}"
                                      data-bind="visible: status() == '{{$key}}'">{{$value['name']}}</span>
                            @endforeach
                        </td>
                        <td>						
                            <button class="btn btn-xs btn-success"
                                    data-bind="visible: status() == '{{PaymentHistoryStatus::PENDING}}',
                                    click: function(){ $root.update($data, '{{PaymentHistoryStatus::COMPLETED}}') }">
                                <i class="fa fa-check"></i>
                                Phê duyệt
                            </button>				
							
							<!-- fixcode not test
							<button class="btn btn-xs btn-success"
                                    data-bind="
                                    click: function(){ $root.update($data, '{{PaymentHistoryStatus::COMPLETED}}') }">
                                <i class="fa fa-check"></i>
                                Phê duyệt
                            </button>
							-->
							
                            <button class="btn btn-xs btn-danger"
                                    data-bind="visible: status() == '{{PaymentHistoryStatus::PENDING}}',
                                    click: function(){ $root.update($data, '{{PaymentHistoryStatus::CANCELLED}}') }">
                                <i class="fa fa-sign-out-alt"></i>
                                Hủy TT
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            @include('pagination')
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection