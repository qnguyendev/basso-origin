function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.users = ko.mapping.fromJS([]);
    self.filter_time = ko.observable('all');
    self.filter_date = ko.observable(moment(new Date()).format('DD-MM-YYYY'));
    self.filter_status = ko.observable('all');
    self.filter_user_id = ko.observable(0);

    self.init_data = function () {
        AJAX.get(window.location, {init: true}, true, (res) => {
            ko.mapping.fromJS(res.data, self.users);
            self.search(1);
        });
    };

    self.search = function (page) {
        let data = {
            page: page,
            time: self.filter_time(),
            date: self.filter_date(),
            status: self.filter_status,
            user_id: self.filter_user_id()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
            if (res.data.length == 0)
                ALERT.error('Không có thanh toán cho lựa chọn tìm kiếm');
        });
    };

    self.update = function (item, status) {
        ALERT.confirm('Xác nhận duyệt thanh toán', null, function () {
            AJAX.post(window.location, {status: status, id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    item.status(status);
                    ALERT.success(res.message);
                }
            });
        });
    };
}

let model = new viewModel();
model.init_data();
ko.applyBindings(model, document.getElementById('main-content'));