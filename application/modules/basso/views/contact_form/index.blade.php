<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')

<style>
.cls_is_read{
	background-color: #ccc !important;
}
.table-bordered th, .table-bordered td {
    border: 1px solid #bbb !important;
}
</style>

    <div class="card">
        <div class="card-header">
            <div class="card-title">
                Danh sách form liên hệ
                <a class="float-right" href="#s" data-bind="click: contact_info_modal.show">
                    <i class="fa fa-edit"></i>
                    Sửa thông tin
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <th>#</th>

                    <th>Tên</th>
                    <th>Số điện thoại</th>
                    <th>Thời gian</th>
                    <th>Ghi chú</th>
                    <th></th>
                    </thead>
                    <tbody data-bind="foreach: result" class="text-center">
                    <tr>
                        <!-- ko if: is_read()==1 -->
						<th data-bind="text: $index() + 1" class = "cls_is_read"></th>
                        <td data-bind="text: name" class = "cls_is_read"></td>
                        <td data-bind="text: phone" class = "cls_is_read"></td>
                        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY HH:mm')" class = "cls_is_read"></td>
                        <td data-bind="text: note" class = "cls_is_read"></td>
                        <td class = "cls_is_read">
                            <a href="#" data-bind="click: $root.detail_modal.show">
                                <i class="fa fa-eye"></i>
                                Chi tiết
                            </a>
                        </td>
						<!-- /ko -->
						
						<!-- ko if: is_read()==0 -->
						<th data-bind="text: $index() + 1"></th>
                        <td data-bind="text: name" class = ""></td>
                        <td data-bind="text: phone" class = ""></td>
                        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY HH:mm')" class = ""></td>
                        <td data-bind="text: note" class = ""></td>
                        <td class = "">
                            <a href="#" data-bind="click: $root.detail_modal.show">
                                <i class="fa fa-eye"></i>
                                Chi tiết
                            </a>
                        </td>
						<!-- /ko -->
                    </tr>
                    </tbody>
                </table>
            </div>

            @include('pagination')
        </div>
    </div>
@endsection

@section('script')
    {{ckeditor()}}
    <script src="{{load_js('index')}}"></script>
@endsection

@section('modal')
    @include('partial/contact_info_modal')
    @include('partial/contact_detail_modal')
@endsection