<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a href="javascript:" class="float-right" data-bind="click: group().add">
                            <i class="fa fa-plus"></i>
                            Thêm nhóm
                        </a>
                        Nhóm nhãn đơn lẻ
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th class="text-left">Tên nhóm</th>
                                <th class="text-left">Ghi chú</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tbody class="text-center" data-bind="foreach: group().result">
                            <tr>
                                <th data-bind="text: $index() + 1"></th>
                                <td>
                                    <a href="#" data-bind="text: name, click: $root.group().view"></a>
                                </td>
                                <td data-bind="text: des"></td>
                                <td class="text-center">
                                    <button class="btn btn-xs btn-info" data-bind="click: $root.group().edit">
                                        <i class="icon-pencil"></i>
                                        Sửa
                                    </button>
                                    <button class="btn btn-xs btn-danger" data-bind="click: $root.group().delete">
                                        <i class="icon-trash"></i>
                                        Xóa
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6" data-bind="visible: value().group_id() !== undefined">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a href="javascript:" class="float-right" data-bind="click: value().add">
                            <i class="fa fa-plus"></i>
                            Thêm nhãn
                        </a>
                        <span data-bind="text: value().group_name"></span>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th class="text-left">Tên nhãn</th>
                                <th class="text-left">Ghi chú</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: value().result" class="text-center">
                            <tr>
                                <th data-bind="text: $index() + 1"></th>
                                <td data-bind="text: name"></td>
                                <td data-bind="text: des"></td>
                                <td class="text-center">
                                    <button class="btn btn-xs btn-info" data-bind="click: $root.value().edit">
                                        <i class="icon-pencil"></i>
                                        Sửa
                                    </button>
                                    <button class="btn btn-xs btn-danger" data-bind="click: $root.value().delete">
                                        <i class="icon-trash"></i>
                                        Xóa
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/label_group_modal')
    @include('partial/label_value_modal')
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection