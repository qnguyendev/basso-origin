<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$order = $orders[0];
$index = 1;
?>
<html>
<head>
    <title>Phiếu giao hàng</title>
</head>
<body>
<!--
<table style="width: 100%">
    <tr>
        <td align="center" style="font-size: 23px; font-weight: bold;">PHIẾU GIAO HÀNG</td>
    </tr>
    <tr>
        <td align="center">Ngày {{date('d/m/Y', time())}}</td>
    </tr>
</table>

<table style="width: 100%; margin-top: 1.3em">
    <tr>
        <td>Số lượng đơn: {{count($items)}}</td>
    </tr>
    <tr>
        <td>Tổng tiền thu hộ: {{format_money(array_sum(array_column($orders, 'cod_amount')))}}</td>
    </tr>
</table>

<table style="width: 100%; margin-top: 1em; border: solid 1px #888" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th style="font-weight: bold; border-right: solid 1px #777; text-align: center; padding: 5px">STT</th>
        <th style="font-weight: bold; border-right: solid 1px #777; text-align: center">Khách hàng</th>
        <th style="font-weight: bold; border-right: solid 1px #777; text-align: center">Điện thoại</th>
        <th style="font-weight: bold; border-right: solid 1px #777; text-align: center">Địa chỉ</th>
        <th style="font-weight: bold; border-right: solid 1px #777; text-align: center">Tiền thu hộ</th>
        <th style="font-weight: bold; text-align: center">Ghi chú</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
    <tr>
        <td style="border-top: solid 1px #777; border-right: solid 1px #777; padding: 5px; text-align: center">{{$index}}</td>
            <td style="border-top: solid 1px #777; border-right: solid 1px #777; padding: 5px; text-align: center">{{$order->name}}</td>
            <td style="border-top: solid 1px #777; border-right: solid 1px #777; padding: 5px; text-align: center">{{$order->phone}}</td>
            <td style="border-top: solid 1px #777; border-right: solid 1px #777; padding: 5px; text-align: center">
                {{$order->address}}, {{$order->district}}, {{$order->city}}
            </td>
            <td style="border-top: solid 1px #777; border-right: solid 1px #777; padding: 5px; text-align: center">
                {{format_money($order->cod_amount)}}
            </td>
            <td style="border-top: solid 1px #777; padding: 5px; text-align: center">{{$order->note}}</td>
        </tr>
        <?php $index++; ?>
@endforeach
        </tbody>
    </table>

    <table style="width: 100%; margin-top: 2.3em">
        <tr>
            <td colspan="2" align="center" style="font-size: 17px">CHÚNG TÔI ĐÃ NHẬN ĐỦ ĐƯỢC SỐ ĐƠN ĐƯỢC GIAO</td>
        </tr>
        <tr>
            <td style="width: 50%; padding-top: 1em" align="center">
                Đơn vị giao vận<br/>
                (ký tên)
            </td>
            <td style="width: 50%; padding-top: 1em" align="center">
                Cửa hàng<br/>
                (ký tên)
            </td>
        </tr>
    </table>
-->

<table style="width: 100%" cellpadding="3">
    <tr>
        <td>Khách hàng: {{$order->name}}</td>
    </tr>
    <tr>
        <td>Điện thoại: {{$order->phone}}</td>
    </tr>
    <tr>
        <td>Địa chỉ: {{$order->address}}, {{$order->district}}, {{$order->city}}</td>
    </tr>
    <tr>
        <td>Mã đơn hàng: {{$order->id}}</td>
    </tr>
	<tr>
        <td>Mã vận đơn: {{$order->code}}</td>
    </tr>
    <tr>
        <td>COD: {{format_money(array_sum(array_column($orders, 'cod_amount')))}}</td>
    </tr>
</table>

</body>
<style type="text/css" media="print">
    html, body {
        height: 98%;
    }
</style>
<script>
    window.onload = function () {
        window.print();
    }
</script>