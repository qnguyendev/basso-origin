<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="editor-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    data-bind="text: modal.id() == 0 ? 'Thêm quốc gia' : 'Cập nhật quốc gia'">
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Tên
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: modal.name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: modal.name"/>
                </div>
                <div class="row">
                    <div class="form-group col-12 col-md-6">
                        <label>
                            Tỷ giá khách
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: modal.customer_rate"></span>
                        </label>
                        <div class="input-group">
                            <input type="text" class="text-right form-control" data-bind="moneyMask: modal.customer_rate"/>
                            <div class="input-group-append">
                                <span class="input-group-text">₫</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>
                            Tỷ giá mua
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: modal.buy_rate"></span>
                        </label>
                        <div class="input-group">
                            <input type="text" class="text-right form-control" data-bind="moneyMask: modal.buy_rate"/>
                            <div class="input-group-append">
                                <span class="input-group-text">₫</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>
                            Phí ship về VN
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: modal.shipping_fee"></span>
                        </label>
                        <div class="input-group">
                            <input type="text" class="text-right form-control" data-bind="moneyMask: modal.shipping_fee"/>
                            <div class="input-group-append">
                                <span class="input-group-text">₫</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>Ngoại tệ</label>
                        <select class="form-control" data-bind="value: modal.currency_id">
                            @foreach($currencies as $item)
                                <option value="{{$item->id}}">{{$item->name}} - {{$item->symbol}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: $root.modal.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>