<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="add-item-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-uppercase">Thêm sản phẩm</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12 col-sm-6 col-lg-4">
                            <label>
                                Tên sản phẩm
                                <span class="text-danger">*</span>
                                <span data-bind="validationMessage: item_modal.name"
                                      class="validationMessage"></span>
                            </label>
                            <input type="text" class="form-control" data-bind="value: item_modal.name"/>
                        </div>
                        <div class="form-group col-12 col-sm-6 col-lg-4">
                            <label>Số lượng</label>
                            <input type="number" value="1" min="0" step="1" class="form-control"
                                   data-bind="value: item_modal.quantity, event: {change: item_modal.change_term}"
                                   pattern="([0-9]+)"/>
                        </div>
                        <div class="form-group col-12 col-sm-6 col-lg-4">
                            <label>
								Giá sản phẩm
								<span class="text-danger">*</span>
                                <span data-bind="validationMessage: item_modal.name"
                                      class="validationMessage"></span>
							</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                            <span class="input-group-text"
                                                  data-bind="text: order.currency_symbol"></span>
                                </div>
                                <input id="product_price" type="number" step="0.01" min="0" class="form-control"
                                       data-bind="value: item_modal.price, event: {change: item_modal.change_term}"/>
                            </div>
                        </div>
                        <div class="form-group col-12 col-sm-6 col-lg-4">
                            <label>
                                Danh mục phụ thu
                                <span class="text-danger">*</span>
                            </label>
                            <select class="form-control"
                                    data-bind="value: item_modal.term_id, event: {change: item_modal.change_term}">
                                <option value="">- Chọn -</option>
                                <!--ko foreach: data.order_terms-->
                                <option data-bind="text: name, value: id"></option>
                                <!--/ko-->
                            </select>
                        </div>
                        <div class="form-group col-12 col-sm-6 col-lg-4">
                            <label>Giá phụ thu</label>
                            <input type="text" class="form-control border-0" disabled
                                   data-bind="value: parseInt(item_modal.term_fee()).toMoney(0)">
                        </div>
                        <div class="form-group col-12 col-sm-6 col-lg-4">
                            <label>Giá giá VNĐ</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">đ</span>
                                </div>
                                <input type="text" disabled class="form-control"
                                       data-bind="value: money_format(parseInt(item_modal.price() * to_number(order.customer_rate())))"/>
                            </div>
                        </div>
                        <div class="form-group col-12 col-sm-6 col-lg-4">
                            <label>Ghi chú sản phẩm</label>
                            <input type="text" class="form-control" data-bind="value: item_modal.note"/>
                        </div>
                        <div class="form-group col-12 col-sm-6 col-lg-8">
                            <label>
                                Link web
                                <span class="text-danger">*</span>
                                <span data-bind="validationMessage: item_modal.link"
                                      class="validationMessage"></span>
                            </label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="item_modal_link" data-bind="value: item_modal.link"/>
                                <div class="input-group-append">
                                    <button class="btn btn-sm btn-primary" name="item_modal_crawl" data-bind="click: item_modal.crawl">
                                        <i class="icon-target"></i>
                                        Lấy giá
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4 class="page-header mt-2">Đặc tính sản phẩm</h4>
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Giá trị</th>
                            <th width="50"></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: item_modal.variations" class="text-center">
                        <tr>
                            <td>
                                <input type="text" class="form-control input-sm"
                                       data-bind="value: name, enable: $root.item_modal.variation_editable()"/>
                            </td>
                            <td>
                                <!--ko if: $root.item_modal.variation_editable()-->
                                <input type="text" class="form-control input-sm"
                                       data-bind="value: value, visible: $root.item_modal.variation_editable()"/>
                                <!--/ko-->
                                <!--ko if: !$root.item_modal.variation_editable()-->
                                <select class="form-control input-group" name="item_modal_custom_options" data-bind="value: value, options: sources,
                                    optionsText: $data, optionsValue: $data, visible: !$root.item_modal.variation_editable(), event: {change: $root.item_modal.change_options}"></select>
                                <!--/ko-->
                            </td>
                            <td>
                                <button class="btn btn-xs btn-danger"
                                        data-bind="click: $root.item_modal.delete_variation,
                                        visible: $root.item_modal.variation_editable() == true">
                                    <i class="icon-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3">
                                <button class="btn btn-success btn-block"
                                        data-bind="click: $root.item_modal.add_variation,
                                    enable: item_modal.variation_editable() == true">
                                    <i class="icon-plus"></i>
                                    Thêm thuộc tính
                                </button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </form>
            <div class="modal-footer text-center">
                <button class="btn btn-success text-uppercase" data-bind="click: item_modal.add">
                    <i class="fa fa-plus"></i>
                    Thêm sản phẩm
                </button>
            </div>
        </div>
    </div>
</div>