<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card card-default">
    <div class="card-header">
        <div class="card-title">Chi tiết đơn hàng</div>
    </div>
    <form class="card-body">
        <table class="table border-vertical">
            <tbody>
            <tr>
                <th width="120" class="text-right">Quốc gia</th>
                <td>
                    <select class="form-control input-sm"
                            data-bind="options: data.countries, optionsText: 'name', optionsValue: 'id',
                            value: order.country_id, event: {change: data.change_country}"></select>
                </td>
            </tr>
            <tr>
                <th class="text-right">Website</th>
                <td data-bind="text: order.website"></td>
            </tr>
            <tr>
                <th class="text-right">Ngày tạo đơn</th>
                <td><input type="text" class="form-control input-sm" data-bind="datePicker: order.created_time"></td>
            </tr>
            <!--tr>
                <th class="text-right">Trạng thái</th>
                <td>
                    <select class="form-control input-sm" data-bind="value: order.status" name="order_status">
                        @foreach(CustomerOrderStatus::LIST_STATE as $key=>$value)
                <option value="{{$key}}">{{$value['name']}}</option>
                        @endforeach
                    </select>
                </td>
            </tr-->
            <tr>
                <th class="text-right">Tỷ giá</th>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm">đ</span>
                        </div>
                        <input type="text" class="form-control input-sm text-center"
                               data-bind="moneyMask: order.customer_rate, event: {change: calc_total}"/>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">Phí ship web</th>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm" data-bind="text: order.currency_symbol"></span>
                        </div>
                        <input type="number" step="0.01" min="0" class="form-control input-sm text-center"
                               data-bind="value: order.shipping_fee, event: {change: calc_total}"/>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">Tổng tiền</th>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm" data-bind="text: order.currency_symbol"></span>
                        </div>
                        <input type="number" min="0" step="0.01" disabled class="form-control input-sm text-center"
                               data-bind="value: order.total"/>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="text-right">Thành tiền</th>
                <td data-bind="text: parseInt(order.sub_total()).toMoney(0)"></td>
            </tr>
            <tr>
                <th class="text-right">Ghi chú</th>
                <td>
                    <input type="text" class="form-control" name="order_note" data-bind="value: customer_info.note" autocomplete="off"/>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
    <div class="card-footer text-center">
        <button class="btn btn-success btn-sm text-uppercase"
                data-bind="click: function(){ $root.create_order('{{CustomerOrderStatus::PENDING}}') }">
            <i class="fa fa-plus"></i>
            Tạo đơn hàng
        </button>
        <button class="btn btn-success btn-sm text-uppercase"
                data-bind="click: function(){ $root.create_order('{{CustomerOrderStatus::PROCESSING}}') }">
            <i class="fa fa-check"></i>
            Tạo và duyệt đơn
        </button>
    </div>
</div>

<div class="card card-default">
    <div class="card-header">
        <div class="card-title">
            Thanh toán
            <a href="#" class="float-right" data-tool="card-collapse">
                <i class="fa fa-minus"></i>
            </a>
        </div>
    </div>
    <div class="card-wrapper" style="max-height: 237px; transition: max-height 0.5s ease 0s; overflow: hidden;">
        <form class="card-body">
            <table class="table border-vertical">
                <tbody>
                <tr>
                    <th class="text-right">Thời gian</th>
                    <td><input type="text" class="form-control input-sm"
                               data-bind="dateTimePicker: payment.created_time"/></td>
                </tr>
                <tr>
                    <th class="text-right">Số tiền</th>
                    <td>
                        <div class="input-group">
                            <input type="text" class="form-control input-sm"
                                   data-bind="moneyMask: payment.amount, event: {change: payment.check_valid_amount}"/>
                            <div class="input-group-append">
                                <span class="input-group-text text-sm">đ</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-right">Ngân hàng</th>
                    <td>
                        <select class="form-control input-sm" data-bind="value: payment.bank_id, options: data.banks,
                            optionsText: 'name', optionsValue: 'id'"></select>
                    </td>
                </tr>
                <tr>
                    <th class="text-right">Nội dung</th>
                    <td>
                        <input type="text" class="form-control input-sm" data-bind="value: payment.note"
                               autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <th class="text-right">PTTT</th>
                    <td>
                        <select class="form-control" name="payment_type" data-bind="value: payment.type">
                            @foreach(PaymentType::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>