<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card card-default">
    <div class="card-header">
        <div class="card-title">
            <a href="javascript:" class="float-right" data-bind="click: item_modal.show">
                <i class="fa fa-plus"></i>
                Thêm sản phẩm
            </a>
            Sản phẩm
        </div>
    </div>
    <form autocomplete="off" class="card-body">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th colspan="2" class="text-left">Tên</th>
                <th width="50">SL</th>
                <th class="text-left">Variant</th>
                <th width="120">Giá sản phẩm</th>
                <th width="120">Phụ thu</th>
                <th width="120">Giá VNĐ</th>
                <th width="60"></th>
            </tr>
            </thead>
            <tbody data-bind="foreach: items.list">
            <tr>
                <td class="text-center p-1">
                    @include('partial/item_image_upload')
                </td>
                </td>
                <th class="text-left">
                    <a href="javascript:" class="order-expand" data-bind="text: name, click: $root.toggle_row"></a>
                </th>
                <td class="text-center" data-bind="text: quantity"></td>
                <td>
                    <!--ko foreach: variations-->
                    <b data-bind="text: name"></b>: <span data-bind="text: value"></span><br/>
                    <!--/ko-->
                </td>
                <td class="text-center">
                    <span data-bind="text: $root.order.currency_symbol"></span>
                    <span data-bind="text: parseFloat(price()).toFixed(2)"></span>
                </td>
                <td class="text-center" data-bind="text: parseInt(term_fee()).toMoney(0)"></td>
                <td class="text-center"
                    data-bind="text: parseInt(price() * quantity() * $root.to_number($root.order.customer_rate())).toMoney(0)"></td>
                <td class="text-center">
                    <a href="#" class="btn-link text-dark" data-bind="click: $root.items.remove">
                        <i class="fa fa-trash"></i>
                        Xóa
                    </a>
                </td>
            </tr>
            <tr class="tr-collapse">
                <td colspan="8">
                    <div class="item-detail" style="padding: 15px;">
                        <table class="table border-vertical table-striped">
                            <tbody>
                            <tr>
                                <th class="text-right">Sản phẩm</th>
                                <td>
                                    <input type="text" class="form-control input-sm" data-bind="value: name"/>
                                </td>
                                <th class="text-right">Yêu cầu</th>
                                <td class="text-center">
                                    <select class="form-control input-sm text-center"
                                            data-bind="value: requirement, enable: status() == '{{CustomerOrderItemStatus::PENDING}}'">
                                        @foreach(CustomerOrderItemRequirement::LIST_STATE as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">
                                    Giá mua
                                    <span data-bind="text: $root.order.currency_symbol"></span>
                                </th>
                                <td>
                                    <input type="number" min="0" step="0.01" class="form-control input-sm"
                                           data-bind="value: price, event: {change: function(){$root.items.change_term($data, term_id());}}">
                                </td>
                                <th class="text-right">Tình trạng</th>
                                <td>
                                    <select class="form-control input-sm"
                                            data-bind="options: $root.data.item_status, value: status,
                                            optionsText: 'text', optionsValue: 'id'">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">Số lượng</th>
                                <td><input type="number" class="form-control input-sm" min="0" step="1"
                                           data-bind="value: quantity, event: {change: function(){$root.items.change_term($data, term_id());}}">
                                </td>
                                <th class="text-right">Ghi chú</th>
                                <td><input type="text" class="form-control input-sm" data-bind="value: note"></td>
                            </tr>
                            <tr>
                                <th class="text-right">Website</th>
                                <td data-bind="text: website"></td>
                                <th class="text-right">Link sản phẩm</th>
                                <td>
                                    <input type="text" class="form-control input-sm"
                                           data-bind="value: link, event: {change: $root.items.change_link}">
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">Mã tracking</th>
                                <td>
                                    <input type="text" class="form-control input-sm" data-bind="value: tracking_code"/>
                                </td>
                                <th class="text-right">Danh mục</th>
                                <td>
                                    <select class="form-control input-sm"
                                            data-bind="options: $root.data.order_terms, optionsText: 'name',
                                            optionsValue: 'id', value: term_id, event: {change: function(){$root.items.change_term($data, term_id());}}">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td></td>
                                <th class="text-right">Phụ thu</th>
                                <td data-bind="text: parseInt(term_fee()).toMoney(0)"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            </tr>
            </tbody>
        </table>
    </form>
</div>