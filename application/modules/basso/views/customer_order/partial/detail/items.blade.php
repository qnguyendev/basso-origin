<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card card-default">
    <div class="card-header">
        <div class="card-title">
            <a href="javascript:" class="float-right"
               data-bind="click: item_modal.show, visible: (order.info.status() == '{{CustomerOrderStatus::PENDING}}'
            || order.info.status() == '{{CustomerOrderStatus::PROCESSING}}') && order.info.items_bought() == 0">
                <i class="fa fa-plus"></i>
                Thêm sản phẩm
            </a>
            Sản phẩm
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th width="80"></th>
                <th class="text-left">Tên</th>
                <th>Variant</th>
                <th width="50">SL</th>
                <th width="80">Giá SP</th>
                <th width="80">Phụ thu</th>
                <th width="110">Phí vận chuyển</th>
                <th width="80">Tổng giá SP (VND)</th>
                <th width="80"></th>
            </tr>
            </thead>
            <tbody data-bind="foreach: items.list">
            <tr>
                <td class="text-center p-1">
                    @include('partial/item_image_upload')
                </td>
                <th class="text-left">
                    <a href="javascript:" class="order-expand" data-bind="text: name, click: $root.toggle_row"></a>
                </th>
                <td>
                    <!--ko foreach: variations-->
                    <b data-bind="text: name"></b>: <span data-bind="text: value"></span><br/>
                    <!--/ko-->
                </td>
                <td class="text-center" data-bind="text: quantity"></td>
                <td class="text-center">
                    <span data-bind="text: $root.order.info.currency_symbol"></span>
                    <span data-bind="text: parseFloat(price()).toFixed(2)"></span>
                </td>
                <td class="text-center" data-bind="text: parseInt(term_fee()).toMoney(0)"></td>
                <td class="text-center" data-bind="text: parseInt(item_world_shipping_fee()).toMoney(0)"></td>
                <td class="text-center">
                    <span data-bind="text: parseFloat(TongGiaMua()).toMoney(0)"></span>
				</td>
                <td class="text-center">
                    <a href="#" class="btn-link text-danger"
                       data-bind="click: $root.items.remove,
                            visible: $root.order.info.status() == '{{CustomerOrderStatus::PROCESSING}}'
                            && (status() == '{{CustomerOrderItemStatus::PENDING}}'
                            || status() == '{{CustomerOrderItemRequirement::WAIT_MORE}}'
                            || status() == '{{CustomerOrderItemRequirement::BUY_NOW}}'
                            || status() == '{{CustomerOrderItemRequirement::BIDING}}'
                            || status() == '{{CustomerOrderItemStatus::NOT_AVAILABLE}}'
                            )">
                        <i class="fa fa-trash"></i>
                        Xóa
                    </a>
                </td>
            </tr>
            <tr class="tr-collapse">
                <td colspan="8">
                    <div class="item-detail" style="padding: 15px;">
                        <table class="table border-vertical table-striped table-borderless">
                            <tbody>
                            <tr>
                                <th class="text-right">Sản phẩm</th>
                                <td>
                                    <input type="text" class="form-control input-sm"
                                           data-bind="value: name, enable: $root.order.info.status() === '{{CustomerOrderStatus::PROCESSING}}'"/>
                                </td>
                                <th class="text-right">Yêu cầu</th>
                                <td class="text-center">
                                    <select class="form-control input-sm text-center"
                                            data-bind="value: requirement, enable: status() == '{{CustomerOrderItemStatus::PENDING}}',
                                            event: {change: $root.items.update}">
                                        @foreach(CustomerOrderItemRequirement::LIST_STATE as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">Số lượng</th>
                                <td>
                                    <input type="number" class="form-control input-sm" min="1" step="1"
                                           data-bind="value: quantity, event: {change: $root.items.change_quantity}">
                                </td>
                                <th class="text-right">Tình trạng</th>
                                <td>
                                    <div class="btn-group w-100">
                                        <button class="btn dropdown-toggle btn-secondary btn-xs w-100" type="button"
                                                data-toggle="dropdown" aria-expanded="false">
                                        @foreach(CustomerOrderItemStatus::LIST_STATE as $key=>$value)
                                            <!--ko if: status() === '{{$key}}'-->
                                            {{$value['name']}}
                                            <!--/ko-->
                                            @endforeach
                                        </button>
                                        <div class="dropdown-menu" role="menu" x-placement="bottom-start"
                                             data-bind="foreach: $root.data.item_status">
                                            <a class="dropdown-item" href="#"
                                               data-bind="click: function(){$root.items.change_status($data, $parent.status(), $parent.id())}">
                                                <!--ko if: id() == $parent.status()-->
                                                <b data-bind="text: name"></b>
                                                <!--/ko-->
                                                <!--ko if: id() != $parent.status()-->
                                                <span data-bind="text: name"></span>
                                                <!--/ko-->
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">
                                    Giá SP
                                    <span data-bind="text: $root.order.info.currency_symbol"></span>
                                </th>
                                <td>
                                    <input type="number" min="0" step="0.01" class="form-control input-sm"
                                           data-bind="value: price, event: {change: function(){$root.items.change_term($data, term_id());}}">
                                </td>
                                <th class="text-right">Cân nặng (kg)</th>
                                <td>
                                    <table class="table table-borderless">
                                        <tr>
                                            <td class="p-0">
                                                <input readonly type="number" min="0" step="0.01" class="form-control input-sm"
                                                       data-bind="value: weight, event: {change: $root.items.change_weight}"/>
                                            </td>
                                            <td data-bind="text: `x ${quantity()} =`" width="50" align="center"></td>
                                            <td class="p-0">
                                                <input readonly type="number" min="0" step="0.01" class="form-control input-sm"
                                                       data-bind="value: total_weight, event: {change: $root.items.change_total_weight}"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">Website</th>
                                <td data-bind="text: website"></td>
                                <th class="text-right">Link sản phẩm</th>
                                <td>
                                    <input type="text" class="form-control input-sm" data-bind="value: link,
                                    event: {change: $root.items.change_link}"/>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">Ghi chú</th>
                                <td data-bind="popover: '#item-note-order-'+ $index()">
                                    <input type="text" class="form-control input-sm" data-bind="value: note"/>
                                
									<div data-bind="attr: {id: 'item-note-order-'+ $index()}" style="display: none">
										<div class="popover-heading">
											Ghi chú
										</div>
										<div class="popover-body">
											<!--ko if: note() !== undefined-->
											<!--ko if: note() !== null-->
											<!--ko if: note() !== ''-->
											<p data-bind="text: note"></p>
											<!--/ko-->
											<!--/ko-->
											<!--/ko-->
										</div>
									</div>
								</td>
                                <th class="text-right">Danh mục</th>
                                <td>
                                    <div class="btn-group w-100">
                                        <button class="btn dropdown-toggle btn-secondary btn-xs w-100" type="button"
                                                data-toggle="dropdown" aria-expanded="false"
                                                data-bind="text: $root.data.get_order_term(term_id()),
                                                enable: $root.order.info.status() === '{{CustomerOrderStatus::PROCESSING}}'">
                                        </button>
                                        <div class="dropdown-menu" role="menu" x-placement="bottom-start"
                                             data-bind="foreach: $root.data.order_terms">
                                            <a href="#" class="dropdown-item" data-bind="visible: $parent.term_id() !== id(),
                                                    text: name, click: function(){ $root.items.change_term($parent, id()); }"></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">Order Number</th>
                                <td data-bind="text: order_number() == null ? '-' : order_number()"></td>
                                <th class="text-right">Mã tracking</th>
                                <td>
                                    <input type="text" readonly class="form-control input-sm" data-bind="value: tracking_code"/>
                                </td>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-right">Delivered date</th>
                                <td>
                                    <input type="text" class="form-control input-sm"
                                           data-bind="datePicker: delivered_date" disabled />
                                </td>
                                <th class="text-right">Phí ship quốc tế</th>
                                <!-- <td data-bind="text: parseInt(weight() * $root.order.info.country_shipping_fee()).toMoney(0)"> -->
								<td data-bind="text: parseInt(item_world_shipping_fee()).toMoney(0)">
                            </tr>
                            <tr>
                                <th class="text-right">Phụ thu</th>
                                <td data-bind="text: parseInt(term_fee()).toMoney(0)"></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="text-center mt-2">
                            <button class="btn btn-sm btn-primary text-uppercase"
                                    data-bind="click: $root.items.update,
                                    visible: $root.order.info.status() != '{{CustomerOrderStatus::CANCELLED}}'
                                        && $root.order.info.status() != '{{CustomerOrderStatus::COMPLETED}}'">
                                <i class="fa fa-check"></i>
                                Cập nhật
                            </button>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>