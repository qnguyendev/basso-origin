<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card card-default">
    <div class="card-header">
        <div class="card-title">
            Thanh toán
        </div>
    </div>
    <div class="card-body">
        <table class="table border-vertical table-hover">
            <tbody>
            <tr>
                <th width="10"></th>
                <td>Phí ship web</td>
                <td class="text-right" width="200">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-sm" data-bind="text: order.info.currency_symbol"></span>
                        </div>
                        <input type="number" min="0" step="0.01" class="input-sm form-control text-right"
                               data-bind="value: order.info.web_shipping_fee, event: {change: calc_total}"/>
                    </div>
                </td>
            </tr>
            <tr>
                <th width="10"></th>
                <td>Tổng tiền</td>
                <td class="text-right">
                    <span data-bind="text: order.info.currency_symbol"></span>
                    <span data-bind="text: parseFloat(order.info.total()).toFixed(2)"></span>
                </td>
            </tr>
            <tr>
                <th width="10"></th>
                <td class="border-top">Ship quốc tế</td>
                <td class="text-right border-top"
                    data-bind="text: parseInt(order.info.world_shipping_fee()).toMoney(0)"></td>
            </tr>
            <tr>
                <th></th>
                <td>Ship nội địa</td>
                <td class="text-right" data-bind="text: parseInt(order.info.inter_shipping_fee()).toMoney(0)"></td>
            </tr>
            <tr>
                <th></th>
                <td>Tổng tiền VND</td>
                <td class="text-right" data-bind="text: parseInt(order.info.origin_sub_total()).toMoney(0)"></td>
            </tr>
            <tr>
                <th></th>
                <td>Phí dịch vụ <span data-bind="text: order.info.fee_percent()"></span>%</td>
                <td class="text-right" data-bind="text: `${parseInt(order.info.origin_sub_total() * order.info.fee_percent()/100).toMoney(0)}`"></td>
            </tr>
            <tr>
                <th></th>
                <td class="border-top">Giảm giá</td>
                <td class="text-right border-top">
                    <div class="input-group">
                        <!--ko if: order.info.discount_type() == '{{DiscountType::PERCENT}}'-->
                        <input type="number" min="1"
                               data-bind="value: order.info.discount_total, event: {change: calc_total}"
                               class="input-sm form-control text-right"/>
                        <!--/ko-->
                        <!--ko if: order.info.discount_type() == '{{DiscountType::AMOUNT}}'-->
                            <input type="text"
                                   data-bind="moneyMask: order.info.discount_total, event: {change: calc_total}"
                                   class="input-sm form-control text-right"/>
                        <!--/ko-->
                        <div class="input-group-append">
                            <select class="form-control input-sm"
                                    data-bind="value: order.info.discount_type, event: {change: calc_total}, disable: order.info.discount_total() > 0 || order.info.discount_code() != null">
                                <option value="{{DiscountType::PERCENT}}">Phần trăm</option>
                                <option value="{{DiscountType::AMOUNT}}">Số tiền</option>
                            </select>
                        </div>
                    </div>
                </td>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>Thành tiền</td>
                <td class="text-right" data-bind="text: parseInt(order.info.sub_total()).toMoney(0)"></td>
            </tr>
            <tr>
                <th></th>
                <td>Đã thanh toán</td>
                <td class="text-right" data-bind="text: parseInt(order.info.total_paid()).toMoney(0)"></td>
            </tr>
            <tr>
                <th></th>
				<!-- ko if: order.info.total_paid() == 0 -->
                <td>Số tiền cần thanh toán - <span class="text-right" data-bind="text: order.info.first_total_limit()"></span>%</td>
                <td class="text-right"
                    data-bind="text: parseInt(order.info.first_total_deposit()).toMoney(0)"></td>
				<!-- /ko -->
				
				<!-- ko if: order.info.total_paid() > 0 -->
				<td>Còn lại</td>
                <td class="text-right"
                    data-bind="text: parseInt(order.info.sub_total() - order.info.total_paid()).toMoney(0)"></td>
				<!-- /ko -->
            </tr>
            </tbody>
        </table>
        <br>
        @include('partial/detail/payment_history')
    </div>
</div>
