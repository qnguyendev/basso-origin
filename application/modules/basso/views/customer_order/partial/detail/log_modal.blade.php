<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<!-- Modal -->
<div class="modal fade" id="log-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Nhật ký thao tác đơn hàng</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-stripe">
                    <thead>
                    <tr>
                        <th>Thời gian</th>
                        <th>Nội dung</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: log_modal.result">
                    <tr>
                        <td class="text-center"
                            data-bind="text: moment.unix(created_time()).format('HH:mm DD/MM/YYYY')"></td>
                        <td data-bind="text: content" class="text-left"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Thoát</button>
            </div>
        </div>
    </div>
</div>
