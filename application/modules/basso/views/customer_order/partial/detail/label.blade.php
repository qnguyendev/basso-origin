<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>

<div class="card card-default">
    <div role="tabpanel" style="margin: 0 -1px -1px -1px">
        <!-- Nav tabs-->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" href="#ordertracking" aria-controls="ordertracking" role="tab"
                   data-toggle="tab">
                    Thông tin
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="#orderlabel" aria-controls="orderlabel" role="tab"
                   data-toggle="tab">
                    Nhãn đơn hàng
                </a>
            </li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane" id="orderlabel" role="tabpanel">
                <div class="form-group">
                    <div class="input-group">
                        <select class="form-control input-sm"
                                data-bind="options: data.label_groups, value: label.group_id, optionsText: 'name', optionsValue: 'id'">
                        </select>
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-primary" data-bind="click: label.add">
                                <i class="fa fa-plus"></i>
                                THÊM
                            </button>
                        </div>
                    </div>
                </div>

                <table class="table border-vertical">
                    <tbody data-bind="foreach: label.list">
                    <tr>
                        <th class="text-left" width="100" data-bind="text: group_name"></th>
                        <td>
                            <select class="form-control input-sm"
                                    data-bind="options: values, value: value_id, optionsText: 'name', optionsValue: 'id'">
                            </select>
                        </td>
                        <td width="30">
                            <button class="btn btn-xs btn-danger" data-bind="click: $root.label.remove">
                                <i class="icon-trash"></i>
                                Xóa
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane active" id="ordertracking" role="tabpanel">
                <table class="table border-vertical table-striped">
                    <tbody>
                    <tr>
                        <th class="text-left">Quốc gia</th>
                        <td>
                            <button class="btn dropdown-toggle btn-secondary btn-xs p-1 w-100" type="button"
                                    data-toggle="dropdown" aria-expanded="false"
                                    data-bind="text: order.info.country, enable: order.info.status() == '{{CustomerOrderStatus::PROCESSING}}'">
                            </button>
                            <div class="dropdown-menu" role="menu" x-placement="bottom-start"
                                 data-bind="foreach: data.countries">
                                <a class="dropdown-item" href="#"
                                   data-bind="text: name, visible: $root.order.info.country_id() != id(),
                                   click: $root.data.change_country"></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Website</th>
                        <td data-bind="text: order.info.website"></td>
                    </tr>
                    <tr>
                        <th class="text-left">Tỷ giá</th>
                        <td>
                            <div class="input-group">
                                <input type="text"
                                       data-bind="moneyMask: order.info.currency_rate, event: {change: calc_total}"
                                       class="input-sm form-control text-right"/>
                                <div class="input-group-append">
                                    <span class="input-group-text text-sm" style="width: 50px">đ</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Cân nặng</th>
                        <td>
                            <div class="input-group">
                                <input type="number" min="0" step="0.01" class="input-sm form-control text-right"
                                       data-bind="value: order.info.total_weight, event: {change: order.change_weight}"/>
                                <div class="input-group-append">
                                    <span class="input-group-text text-sm" style="width: 50px">kg</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-left">Chi nhánh</th>
                        <td>
                            <select class="form-control input-sm" name="branch" data-bind="value: order.info.branch">
                                @foreach(Branch::LIST_STATE as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    @if($order->discount_code != null)
                        <tr>
                            <th class="text-left">Mã giảm giá</th>
                            <td>
                                <strong>{{$order->discount_code}}</strong>
                                @if($order->discount_type == DiscountType::AMOUNT)
                                    <i>({{format_money($order->discount_total)}})</i>
                                @else
                                    <i>(
                                        <span data-bind="text: order.info.discount_total()"></span>% - tối
                                        đa {{format_money($order->discount_max)}}
                                        )</i>
                                @endif
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card-footer text-center">
        <button class="btn btn-sm btn-success text-uppercase"
                data-bind="click: order.approve, visible: order.info.status() == '{{CustomerOrderStatus::PENDING}}'">
            <i class="fa fa-check"></i>
            Duyệt đơn hàng
        </button>
        <button class="btn btn-sm btn-success text-uppercase"
                data-bind="click: order.save,
                visible: order.info.status() !== '{{CustomerOrderStatus::CANCELLED}}' && order.info.status() !== '{{CustomerOrderStatus::PENDING}}'">
            <i class="fa fa-recycle"></i>
            Cập nhật
        </button>
    </div>
</div>
