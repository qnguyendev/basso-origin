<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="edit-customer-modal" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    Thay đổi thông tin khách
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-sm-6 col-12">
                        <label>
                            Họ tên
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: customer_info.name"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: customer_info.name, enable: $root.order.info.customer_id() == null"/>
                    </div>
                    <div class="form-group col-sm-6 col-12">
                        <label>Điện thoại</label>
                        <input type="text" class="form-control" disabled data-bind="value: customer_info.phone"/>
                    </div>
                    <div class="form-group col-sm-6 col-12">
                        <label>Email</label>
                        <input type="text" class="form-control" data-bind="value: customer_info.email"/>
                    </div>
                    <div class="form-group col-sm-6 col-12">
                        <label>Nơi chốt đơn</label>
                        <select class="form-control" data-bind="options: data.order_places, value: order.info.order_place_id,
                            optionsText: 'name', optionsValue: 'id'"></select>
                    </div>
                    <div class="form-group col-sm-6 col-12">
                        <label>Ghi chú</label>
                        <input type="text" class="form-control" data-bind="value: customer_info.note"/>
                    </div>
                    <div class="form-group col-sm-6 col-12">
                        <label>Brand</label>
                        <select data-bind="value: order.info.brand" class="form-control">
                            @foreach(BassoBrand::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12">
                        <label>
                            Địa chỉ
                            <span class="text-danger">*</span>
                            <span class="validationMessage"
                                  data-bind="validationMessage: customer_info.shipping_address"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: customer_info.shipping_address"/>
                    </div>
                    <div class="form-group col-sm-6 col-12">
                        <label>
                            Tỉnh/thành
                            <span class="text-danger">*</span>
                        </label>
                        <select class="form-control" name="city"
                                data-bind="options: data.cities, optionsText: 'name', optionsValue: 'id',
                                value: customer_info.city_id, event: {change: data.change_city}"></select>
                    </div>
                    <div class="form-group col-sm-6 col-12">
                        <label>
                            Quận/huyện
                            <span class="text-danger">*</span>
                        </label>
                        <select class="form-control" name="district"
                                data-bind="options: data.districts, optionsText: 'name', optionsValue: 'id',
                                value: customer_info.district_id"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-success text-uppercase" data-bind="click: customer_info.save">
                    <i class="fa fa-check"></i>
                    Cập nhật
                </button>
            </div>
        </div>
    </div>
</div>