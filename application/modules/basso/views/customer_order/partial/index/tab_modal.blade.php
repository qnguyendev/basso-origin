<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="tab-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">
                    <span data-bind="text: tab_modal.id() == 0 ? 'Thêm bộ lọc' : 'Cập nhật bộ lọc'"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12 col-lg-6">
                        <label>
                            Tên bộ lọc
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: tab_modal.name"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: tab_modal.name"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Thứ tự hiển thị</label>
                        <input type="number" class="form-control" data-bind="value: tab_modal.order"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Trạng thái thanh toán</label>
                        <select class="form-control select2" multiple name="payment_status"
                                data-bind="selectedOptions: tab_modal.payment_status">
                            @foreach(OrderPaymentStatus::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Tình trạng sản phẩm</label>
                        <select class="form-control select2" multiple name="item_status"
                                data-bind="selectedOptions: tab_modal.item_status">
                            @foreach(CustomerOrderItemStatus::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Yêu cầu sản phẩm</label>
                        <select class="form-control select2" multiple name="item_requirement"
                                data-bind="selectedOptions: tab_modal.item_requirement">
                            @foreach(CustomerOrderItemRequirement::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Nhân viên</label>
                        <select class="form-control select2" name="staff_id"
                                data-bind="options: tab_modal.users, optionsText: 'name', optionsValue: 'id',
                                selectedOptions: tab_modal.staff_id"
                                multiple></select>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Tình trạng xuất kho</label>
                        <select class="form-control select2" multiple name="shipping_status"
                                data-bind="selectedOptions: tab_modal.shipping_status">
                            @foreach(CustomerOrderShipStatus::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Tracking</label>
                        <select class="form-control select2" multiple name="tracking_status"
                                data-bind="selectedOptions: tab_modal.tracking_status">
                            @foreach(CustomerOrderItemTracking::LIST_STATE as $key=>$value)
                                @if($key != CustomerOrderItemTracking::ALL)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <!--ko foreach: tab_modal.labels-->
                    <div class="form-group col-md-12 col-lg-6">
                        <label data-bind="text: name"></label>
                        <select class="form-control select2"
                                data-bind="options: values, optionsText: 'name', optionsValue: 'id',
                                selectedOptions: $root.tab_modal.label_id, attr: {rel: id}"
                                multiple></select>
                    </div>
                    <!--/ko-->
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Trạng thái đơn hàng</label>
                        <select class="form-control select2" multiple name="order_status"
                                data-bind="selectedOptions: tab_modal.order_status">
                            @foreach(CustomerOrderStatus::LIST_STATE as $key=>$value)
                                @if($key != CustomerOrderItemTracking::ALL)
                                    <option value="{{$key}}">{{$value['name']}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-bind="click: tab_modal.save">
                    <em class="fa fa-check"></em>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>