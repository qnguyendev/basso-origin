<table style="width: 100%; margin: 10px 0 30px" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4" style="padding: 10px 0">
			<strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
				THÔNG TIN CHUYỂN KHOẢN
			</strong>
		</td>
	</tr>
	<tr>
		<td align="center" width="50%"
			style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
			<strong>Ngân hàng Techcombank</strong>
		</td>
		<td align="center" width="50%"
			style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
			<strong>Ngân hàng BIDV</strong>
		</td>
	</tr>
	<tr>
		<td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0">
			<ul style="margin: 0; list-style: none; padding: 0">
				<li>Chủ TK: Nguyễn Nga Linh</li>
				<li>Số TK: 19027938141017</li>
				<li>Chi nhánh Mỹ Đình, HN</li>
			</ul>
		</td>
		<td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0; border-left: 0">
			<ul style="margin: 0; list-style: none; padding: 0">
				<li>Chủ TK: Nguyễn Nga Linh</li>
				<li>Số TK: 12610000159281</li>
				<li>Chi nhánh Núi Trúc, HN</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td align="center"
			style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: 0">
			<strong>Ngân hàng Ngoại Thương Vietcombank</strong>
		</td>
	</tr>
	<tr>
		<td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0">
			<ul style="margin: 0; list-style: none; padding: 0">
				<li>Chủ TK: Nguyễn Nga Linh</li>
				<li>Số TK: 0451000224940</li>
				<li>Chi nhánh Thành Công, HN</li>
			</ul>
		</td>
	</tr>
</table>