<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $current_user, $user_roles;
?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                <a href="#" class="float-right" data-bind="click: export_tracking">
                    <i class="fa fa-file-export"></i>
                    Xuất tracking
                </a>
                <a href="#" class="float-right mr-3" data-bind="click: update_tracking.show">
                    <i class="fa fa-mail-bulk"></i>
                    Cập nhật tracking
                </a>
                <a href="{{base_url('basso/customer_order/create')}}" class="float-right mr-3">
                    <i class="fa fa-plus"></i>
                    Tạo đơn
                </a>
                <a href="#" class="float-right mr-3" data-bind="click: export_modal.show">
                    <i class="fa fa-file-export"></i>
                    Xuất Excel
                </a>
                Danh sách đơn khách hàng
            </div>
        </div>
        <div class="card-body">

            <form class="row">
                <div class="form-group col-sm-12 col-md-4 col-xl-2">
                    <label>Thời gian</label>
                    <select class="form-control" data-bind="value: filter.time, event: {change: filter.change_time}">
                        <option value="all">Tất cả</option>
                        <option value="week">Trong tuần</option>
                        <option value="month">Trong tháng</option>
                        <option value="custom">Tùy chọn</option>
                    </select>
                </div>
                <div class="form-group col-sm-12 col-md-4 col-xl-2">
                    <label><em class="icon-calendar"></em> Từ ngày</label>
                    <input type="text" class="form-control"
                           data-bind="datePicker: filter.from, enable: filter.time() == 'custom'"/>
                </div>
                <div class="form-group col-sm-12 col-md-4 col-xl-2">
                    <label><em class="icon-calendar"></em> Đến ngày</label>
                    <input type="text" class="form-control"
                           data-bind="datePicker: filter.to, enable: filter.time() == 'custom'"/>
                </div>
				
				<?php
					global $user_roles;
					if(!in_array("order_manager", $user_roles)){
				?>
                <div class="form-group col-sm-12 col-md-4 col-xl-2">
                    <label>Nhân viên phục trách</label>
                    <select class="form-control" data-bind="value: filter.staff_id">
                        <option value="0">Tất cả</option>
                        <!--ko foreach: tab_modal.users-->
                        <option data-bind="value: id, text: name"></option>
                        <!--/ko-->
                    </select>
                </div>
				<?php } ?>
					
                <div class="form-group col-sm-12 col-md-4 col-xl-2">
                    <label>Lọc đơn hàng</label>
                    <div class="input-group">
                        <input type="text" class="form-control" data-bind="value: filter.key"
                               placeholder="Tìm theo mã order, tracking, website..." autocomplete="off"/>
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-primary" data-bind="click: function(){$root.search(1);}">
                                <i class="fa fa-search"></i>
                                Tìm
                            </button>
                        </div>
                    </div>
                </div>
            </form>

            <div role="tabpanel">
                <!-- Nav tabs-->
                <ul class="nav nav-tabs" role="tablist">
                    <!--ko foreach: tabs.list-->
                    <li class="nav-item position-relative" role="presentation">
                        <button class="position-absolute delete-tab" href="#" data-bind="click: $root.tabs.delete">
                            x
                        </button>
                        <a role="tab" data-toggle="tab" href="#"
                           data-bind="click: $root.tabs.change, css: id() == $root.tabs.current().id() ? 'nav-link active' : 'nav-link',
                            dbClick: $root.tab_modal.edit">
                            <span data-bind="text: name"></span>
                            <span data-bind="text: `(${$root.tabs.total_item()})`, visible: id() == $root.tabs.current().id()"></span>
                        </a>
                    </li>
                    <!--/ko-->
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:" data-bind="click: tab_modal.show">
                            <i class="fa fa-plus"></i>
                        </a>
                    </li>
                </ul>

                @include('partial/index/items')
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('partial/index/tab_modal')
    @include('partial/index/tracking_code_modal')
    @include('partial/index/update_tracking_code_modal');
    @include('partial/index/export_modal');
@endsection

@section('script')
    <script src="{{load_js('line-number')}}"></script>
    <script src="{{load_js('index')}}"></script>
@endsection

@section('css')
    <style>
        .delete-tab {
            top: -10px;
            left: 50%;
            margin-left: -9px;
            opacity: 0;
            transition: all .7s ease;
            border: 0;
            line-height: 1;
            cursor: pointer;
            width: 16px;
            height: 16px;
            border-radius: 50%;
            text-align: center;
            color: #FFF;
            background: Red;
            z-index: 9999;
            font-weight: bold;
            padding: 0;
        }

        .nav-item:hover .delete-tab {
            opacity: .8;
        }

        .nav-tabs > .nav-item > .nav-link {
            padding: 10px 15px;
        }
    </style>
@endsection
@section('head')
    <style>
        #tracking-code-modal textarea {
            width: 100% !important;
            padding-left: 60px;
        }

        #tracking-code-modal .modal-body div textarea {
            position: absolute;
            width: 60px !important;
            top: 22px;
            bottom: 22px;
            background: transparent;
            border: 0;
            left: 15px;
            z-index: 99;
            padding-left: 5px;
            margin: 0 !important;
            color: #777;
            resize: none;
        }
    </style>
@endsection