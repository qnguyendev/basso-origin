<?php
$tmp_email = ""; $tmp_telelphone = ""; 
$tmp_website = "";
$tmp_background_color = ""; 

if($order->brand == "asale") {
	$tmp_email = "allsale.vn@gmail.com";
	$tmp_telelphone = "096 991 5006";
	$tmp_website = "asale.vn";
	$tmp_background_color = "#b81526";
} else {
	$tmp_email = "cskh@basso.vn";
	$tmp_telelphone = "0965 687 790";
	$tmp_website = "basso.vn";
	$tmp_background_color = "#00354E";
}
$tongiavnd = 0;
?>

<div style="max-width: 720px; margin: 0 auto">
    <?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
    <table style="width: 100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="left" style="background-color: <?php echo $tmp_background_color; ?>; padding:20px">
                <!-- <img src="https://basso.eprtech.com/theme/basso/statics/images/basso_logo.jpg"/> -->
				<?php include(APPPATH."/modules/basso/views/customer_order/partial/email_parts/".$order->brand."/logo.blade.php"); ?>
            </td>
            <td align="center"
                style="color: #FFF; background-color: <?php echo $tmp_background_color; ?>; padding:20px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6">
				<!--
                <strong>CÔNG TY TNHH SẢN XUẤT VÀ THƯƠNG MẠI THIÊN LONG</strong><br/>
                <small> Mã số thuế: 0101474581<br/>
                    Chi nhánh HN: 17 Tố Hữu, P. Trung Văn, Q. Nam Từ Liêm, HN<br/>
                    Chi nhánh SG: 60 Lê Trung Nghĩa, P12, Q. Tân Bình, TP. HCM<br/>
                    Hotline: 0965687790 - Website: www.basso.eprtech.com
                </small>
				-->
				<?php include(APPPATH."/modules/basso/views/customer_order/partial/email_parts/".$order->brand."/header_company_info.blade.php"); ?>
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 0" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" style="color: #00354E; padding:20px;">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 20px; line-height: 1.7">
                    HÓA ĐƠN ĐẶT HÀNG
                </strong>
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 10px 0 20px" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4" style="padding: 10px 0">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">THÔNG
                    TIN ĐƠN HÀNG</strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
                <strong>Mã đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none">
                #{{$order->order_code}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                <strong>Hình thức thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none">
                {{$order->payment_method}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Ngày đặt hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{date('d/m/Y', time())}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Trạng thái thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{$order->payment_status}} 
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Tổng giá trị đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->sub_total)}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Số tiền đã thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->total_paid)}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Trạng thái đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                Đã tiếp nhận
            </td>
			
			@if($order->total_paid == 0 && $order->first_total_limit && $order->first_total_deposit)
				<td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Số tiền cần thanh toán - {{$order->first_total_limit}}%</strong>
				</td>
				<td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
					{{format_money($order->first_total_deposit)}}
				</td>
			@endif
			
			@if($order->total_paid > 0 || ($order->total_paid == 0 && !$order->first_total_limit && !$order->first_total_deposit))
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Số tiền còn lại</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->sub_total - $order->total_paid)}}
            </td>
			@endif
        </tr>
    </table>

    <table style="width: 100%; margin: 20px 0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" style="padding: 10px 0" width="50%">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                    THÔNG TIN ĐẶT HÀNG
                </strong>
            </td>
            <td colspan="2" style="padding: 10px 0" width="50%">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                    THÔNG TIN NHẬN HÀNG
                </strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                <strong>Họ tên</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->name}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                <strong>Họ tên</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->name}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                <strong>Email</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->email }}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                <strong>Email</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->email }}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none">
                <strong>Điện thoại</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->phone}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none">
                <strong>Điện thoại</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->phone}}
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none; border-top: none">
                <strong>Địa chỉ</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->shipping_address}}, {{$order->district}}, {{$order->city}}
            </td>
        </tr>
    </table>

        <table style="width: 100%; margin: 20px 0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="8" style="padding: 10px 0">
                    <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                        CHI TIẾT ĐƠN HÀNG
                    </strong>
                </td>
            </tr>
            <tr>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
                    <strong>STT</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                    <strong>Hình ảnh</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                    <strong>Tên sản phẩm</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none"
                    width="80">
                    <strong>Giá SP</strong>
                </td>
                <td style="width: 50px; font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none"
                    align="center">
                    <strong>Số lượng</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                    <strong>Phụ thu</strong>
                </td>
                <td style="width: 120px; font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                    <strong>Phí vận chuyển</strong>
                </td>
                <td style="width: 120px; font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                    <strong>Tổng giá SP</strong>
                </td>
            </tr>
            @for($i = 0; $i < count($items); $i++)
                <tr>
                    <?php $item = $items[$i]; ?>
                    <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none; text-align: center"
                        align="center">
                        {{($i + 1)}}
                    </td>
					<td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                        align="center">
                        @if($item->image_path != null)
                            <img src="{{$item->image_path}}" width="80" height="80" alt="">
                        @else
                            <img src="/assets/img/no-image-available.jpg" width="80" height="80"/>
                        @endif
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                       <a href="{{$item->link}}" target="_blank">
                            {{$item->name}}
                        </a>
                        @if($item->variations != null)
                            @if(!empty($item->variations))
                                <br/>
                                <?php $item->variations = json_decode(json_encode($item->variations), true);?>
                                @foreach($item->variations as $var)
                                    <strong>{{$var['name']}}</strong>: {{$var['value']}}<br/>
                                @endforeach
                            @endif
                        @endif
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                        align="center">
                        {{$order->currency_symbol}}
                        {{number_format($item->price  + $item->web_shipping_fee, 2, '.', ',')}}
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                        align="center">
                        {{$item->quantity}}
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                        align="center">
                        {{format_money($item->term_fee)}}
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                        align="center">
                        {{format_money($item->item_world_shipping_fee)}}
                    </td>
					<?php
					$item_total = $item->quantity * $item->price * $order->currency_rate +
						$item->term_fee * $item->quantity +
						$item->item_world_shipping_fee * $item->quantity;
					$tongiavnd += $item_total;
					?>
                    <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                        align="center">
                    {{format_money($item_total)}}
					</td>
                </tr>
            @endfor
            
        <tr>
            <td align="right" colspan="7"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                <strong>Giảm giá</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                align="center">
                <strong>{{format_money($order->discount_amount)}}</strong>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="7"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
               <strong>Phí dịch vụ {{$order->fee_percent}}%</strong>
				<?php 
				/*$percent_amount = round(100*$order->total_paid/$order->sub_total);
				if($percent_amount >= 50 && $percent_amount < 80){ ?>
				<strong>Phí dịch vụ (+4%)</strong>
				<?php } elseif($percent_amount >= 80 && $percent_amount < 100){ ?>
				<strong>Phí dịch vụ (+2%)</strong>
				<?php } else { ?>
				<strong>Phí dịch vụ</strong>
				<?php } */?>            
				</td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                align="center">
                <strong>{{format_money(($tongiavnd * $order->fee_percent)/100)}}</strong>
            </td>
        </tr>
		<?php if (strpos($order->website, 'ebay') !== false) { ?>
		<tr>
			<td align="right" colspan="7"
				style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
				<strong>Phí vận chuyển quốc tế</strong>
			</td>
			<td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
				align="center">
				<strong>{{format_money($order->world_shipping_fee_total)}}</strong>
			</td>
		</tr>
		<?php } ?>
		
        <tr>
            <td align="right" colspan="7"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                <strong>Phí vận chuyển nội địa</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center">
               <strong>{{format_money($order->inter_shipping_fee)}}</strong>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="7"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                <strong>Tổng giá trị đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none; text-align: center"
                align="center">
                <strong>{{format_money($order->sub_total)}}</strong>
            </td>
        </tr>
        </table>

    <p style="color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
		<!-- {{$note_label}} -->
		<strong><i>(*) Phí vận chuyển quốc tế:</i></strong> Là phí vận chuyển từ Mỹ/Anh/Tây Ban Nha/… về Việt Nam. Phí
        ship được tính theo cân nặng thực tế khi hàng về Việt Nam, đơn giá <strong>{{format_money($order->world_shipping_fee)}}/kg</strong>
    </p>

	<!--
    <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
        LƯU Ý:
    </strong>

    <div style="color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        <ul style="margin: 5px; padding: 0">
            <li>
                Thời gian hàng về Việt Nam: Dự kiến 2 – 3 tuần.
            </li>
            <li>
                Basso chỉ cam kết mua đúng sản phẩm, mẫu mã, số lượng từ người bán theo yêu cầu của khách hàng, không
                cam kết chất lượng sản phẩm, không đổi trả lại sản phẩm sau khi đã mua hàng.
            </li>
            <li>
                Basso cam kết hoàn lại tiền đặt cọc trong trường hợp không mua được hàng hóa.
            </li>
        </ul>
    </div>
	-->
	
	<?php include(APPPATH."/modules/basso/views/customer_order/partial/email_parts/".$order->brand."/note.blade.php"); ?>
	
	<!--
	<strong>Số tiền cần thanh toán 100%: {{format_money($order->sub_total)}}</strong>
	<p>Nội dung chuyển khoản: thanh toan #{{$order->order_code}}</p>
	<p>Để đảm bảo quý khách mua được những sản phẩm đơn hàng này với giá như trên.</p>
	-->
	
	<strong>Số tiền cần thanh toán 100%: {{format_money($order->sub_total)}}</strong>
	<p>Nội dung chuyển khoản: thanh toan #{{$order->order_code}}</p>
	<p>Để đảm bảo quý khách mua được những sản phẩm đơn hàng này với giá như trên.</p>

    <strong style="color: #00354E; margin:10px 0; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        <?php echo ucfirst($tmp_website); ?> xin cảm ơn quý khách đã tin tưởng và rất hân hạnh được phục vụ Quý khách.
    </strong>

    <table style="width: 100%; margin: 20px 0 0; height: 140px" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%" valign="top" style="color: #00354E; font-family: Arial, Helvetica, sans-serif"
                align="center">
                KHÁCH HÀNG
            </td>
            <td width="50%" valign="top" style="color: #00354E; font-family: Arial, Helvetica, sans-serif"
                align="center">
                BÊN BÁN
            </td>
        </tr>
    </table>
</div>

<script>
    window.onload = function () {
        window.print();
    }
</script>