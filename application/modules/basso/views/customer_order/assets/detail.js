ko.bindingHandlers.popover = {
    init: function (element) {
        let $element = $(element);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            if ($element.data('bs.popover')) {
                try {
                    $element.popover('destroy');
                } catch (err) {
                }
            }
        });
    },
    update: function (element, valueAccessor) {
        let $element = $(element), value = ko.unwrap(valueAccessor());
        $element.popover({
            html: true,
            animation: true,
            placement: 'left',
            trigger: 'click',
            content: function () {
                return $(value).find(".popover-body").html();
            },
            title: function () {
                return $(value).find(".popover-heading").html();
            }
        });
    }
};

function viewModel() {
    let self = this;
    ko.utils.extend(self, new imageUploadModel());

    //  Dữ liệu dùng chung
    self.data = {
        item_status: ko.mapping.fromJS([]),
        cities: ko.mapping.fromJS([]),
        districts: ko.mapping.fromJS([]),
        order_places: ko.mapping.fromJS([]),
        order_terms: ko.mapping.fromJS([]),
        label_groups: ko.mapping.fromJS([]),
        labels: ko.mapping.fromJS([]),
        shipping_agencies: ko.mapping.fromJS([]),   //  Đơn vị vận chuyển
        countries: ko.mapping.fromJS([]),
        banks: ko.mapping.fromJS([]),
        init: function () {
            localStorage.setItem('prev_url', 'customer_order_detail');
            AJAX.get(window.location, {init: 'data'}, true, (res) => {
                self.order.info.currency_rate(moneyFormat(res.order.currency_rate));
                ko.mapping.fromJS(res.items, self.items.list);
                ko.mapping.fromJS(res.item_status, self.data.item_status);
                ko.mapping.fromJS(res.cities, self.data.cities);
                ko.mapping.fromJS(res.districts, self.data.districts);
                ko.mapping.fromJS(res.order_places, self.data.order_places);
                ko.mapping.fromJS(res.order_terms, self.data.order_terms);
                ko.mapping.fromJS(res.label_groups, self.data.label_groups);
                ko.mapping.fromJS(res.labels, self.data.labels);
                ko.mapping.fromJS(res.shipping_agencies, self.data.shipping_agencies);
                ko.mapping.fromJS(res.shipping_histories, self.shipping.histories);
                ko.mapping.fromJS(res.countries, self.data.countries);
                ko.mapping.fromJS(res.banks, self.data.banks);
                self.label.init(res.order.labels);
				
				console.log("case wd");
				console.log(res.order.world_shipping_fee);

                let total_weight = 0;
				let items_bought_count = 0;
                self.items.list.each((x) => {
                    if (x.weight() != undefined) {
                        if (x.weight() != null)
                            total_weight += parseFloat(x.weight());
                    }
					if(x.status() != "pending"){
						items_bought_count = items_bought_count+1;
					}
					x.TongGiaMua((x.price() * x.quantity() * res.order.currency_rate) + parseFloat(x.term_fee()) + parseFloat(x.item_world_shipping_fee()));
                });
                self.order.info.total_weight(total_weight.toFixed(2));
				self.order.info.items_bought(items_bought_count);
				
                self.customer_info.name(res.order.name);
                self.customer_info.email(res.order.email);
                self.customer_info.phone(res.order.phone);
                self.customer_info.shipping_address(res.order.shipping_address);
                self.customer_info.note(res.order.note);
                self.customer_info.city(res.order.city);
                self.customer_info.city_id(res.order.city_id);
                self.customer_info.district(res.order.district);
                self.customer_info.district_id(res.order.district_id);

                self.order.info.id(res.order.id);
                self.order.info.code(res.order.order_code);
                self.order.info.created_time(res.order.created_time);
                self.order.info.website(res.order.website);
                self.order.info.country(res.order.country);
                self.order.info.country_id(res.order.country_id);
                self.order.info.currency_symbol(res.order.currency_symbol);
                self.order.info.total(res.order.total);
                self.order.info.sub_total(parseInt(res.order.sub_total));
                self.order.info.status(res.order.status);
                self.order.info.order_place(res.order.order_place);
                self.order.info.order_place_id(res.order.order_place_id);
                self.order.info.web_shipping_fee(res.order.web_shipping_fee);
                self.order.info.inter_shipping_fee(res.order.inter_shipping_fee);
                self.order.info.discount_total(res.order.discount_type == 'percent' ? res.order.discount_total : moneyFormat(res.order.discount_total));
                self.order.info.discount_max(res.order.discount_max);
                self.order.info.discount_type(res.order.discount_type);
                self.order.info.discount_code(res.order.discount_code == null ? null : (res.order.discount_code == '' ? null : res.order.discount_code));
                self.order.info.total_paid(res.order.total_paid);
                self.order.info.country_shipping_fee(res.order.country_shipping_fee);
                self.order.info.brand(res.order.brand);
                self.order.info.customer_id(res.order.customer_id);
                self.order.info.total_weight(parseFloat(res.order.total_weight).toFixed(2));
                self.order.info.fee_percent(res.order.fee_percent);
                self.order.info.first_total_deposit(res.order.first_total_deposit);
                self.order.info.first_total_limit(res.order.first_total_limit);
                self.order.info.world_shipping_fee_total(res.order.world_shipping_fee_total);

                if (res.order.branch != null)
                    self.order.info.branch(res.order.branch);
                else
                    self.order.info.branch($('select[name=branch] option:first').val());

                //let world_shipping_fee = 0;
                //self.items.list.each((x) => {
                    //world_shipping_fee += x.weight() * self.order.info.country_shipping_fee();
                    /*if (x.delivered_date() != null) {
                        x.delivered_date(moment.unix(x.delivered_date()).format('DD-MM-YYYY'));
                    }*/
                //});
				
                //self.order.info.world_shipping_fee(res.order.world_shipping_fee_total);
                self.calc_total();
            });
        },
        //  Lấy danh sách quận huyện khi thay đổi tỉnh thành
        change_city: function () {
            if ($('#edit-customer-modal').is(':visible') || $('#shipping-modal').is(':visible')) {
                AJAX.get(window.location, {city_id: self.customer_info.city_id()}, true, (res) => {
                    ko.mapping.fromJS(res.data, self.data.districts);
                    if (self.customer_info.district_id() != undefined)
                        $('#edit-customer-modal select[name=district]').val(self.customer_info.district_id()).trigger('change');
                });
            }
        },
        get_order_term: function (term_id) {
            let terms = self.data.order_terms.filter((x) => {
                return x.id() === term_id;
            });
            return terms.length === 0 ? null : terms[0].name;
        },
        change_country: function (item) {
            ALERT.confirm('Xác nhận thay đổi quốc gia?', 'Tỷ giá, thanh toán cũng sẽ thay đổi theo', function () {
                self.order.info.country_id(item.id());
                self.order.info.country(item.name());
                self.order.info.country_shipping_fee(item.shipping_fee());
				
				/* self.order.info.web_shipping_fee(item.shipping_fee()); */
				
                self.order.info.currency_rate(moneyFormat(item.customer_rate()));
                self.order.info.currency_symbol(item.currency_symbol());
                self.calc_total();
                self.order.save();
            });
        }
    };

    self.shipping = {
        histories: ko.mapping.fromJS([]),
    };

    //  Thông tin đơn hàng
    self.order = {
        info: {
            id: ko.observable(),
            code: ko.observable(),
            created_time: ko.observable(),
            status: ko.observable(),
            order_place: ko.observable(),
            order_place_id: ko.observable(),
            website: ko.observable(),
            country: ko.observable(),
            country_id: ko.observable(),
            currency_rate: ko.observable(),
            currency_symbol: ko.observable(),
            web_shipping_fee: ko.observable(),
            country_shipping_fee: ko.observable(),
            world_shipping_fee: ko.observable(),
            inter_shipping_fee: ko.observable(),
            total: ko.observable(),
            discount_code: ko.observable(),
            discount_total: ko.observable(),
            discount_max: ko.observable(),
            discount_type: ko.observable('amount'),
            sub_total: ko.observable(),
            total_paid: ko.observable(),
            brand: ko.observable(),
            total_weight: ko.observable(0),
            branch: ko.observable(),
            customer_id: ko.observable(),
            origin_sub_total: ko.observable(),
            fee_percent: ko.observable(0),
			items_bought: ko.observable(0),
			first_total_deposit: ko.observable(),
			first_total_limit: ko.observable(),
			world_shipping_fee_total: ko.observable(),
        },
        change_status: function (status) {
            self.order.info.status(status);
        },
        //  Thay đổi tổng khối lượng đơn hàng. Chia đều cho các sản phẩm
        change_weight: function () {
            let total_item = self.items.list().length;
            self.items.list.each((x) => {
                x.weight((self.order.info.total_weight() / total_item).toFixed(2));
            });

            self.order.info.total_weight(parseFloat(self.order.info.total_weight()).toFixed(2));
            self.calc_total();
        },
        //  Cập nhật đơn hàng
        save: function () {
            let data = {
                action: 'update-order',
                total: self.order.info.total(),
                country_id: self.order.info.country_id(),
                currency_rate: toNumber(self.order.info.currency_rate()),
                currency_symbol: self.order.info.currency_symbol(),
                web_shipping_fee: self.order.info.web_shipping_fee(),
                world_shipping_fee: self.order.info.world_shipping_fee(),
                discount_total: toNumber(self.order.info.discount_total()),
                discount_type: self.order.info.discount_type(),
                branch: self.order.info.branch(),
                labels: [],
                items: []
            };

            self.label.list.each((x) => {
                data.labels.push({group_id: x.group_id(), label_id: x.value_id()});
            });

            self.items.list.each((x) => {
                data.items.push({
                    id: x.id(),
                    weight: x.weight()
                });
            });

            AJAX.post(window.location, data, false, function (res) {
                if (!res.error)
                    NOTI.success(res.message);
                else
                    NOTI.danger(res.message);
            });
        },
        //  Duyệt đươn hàng
        approve: function () {
            ALERT.confirm('Xác nhận duyệt đơn hàng', null, () => {
                AJAX.post(window.location, {action: 'approve-order'}, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        self.order.info.status(res.order_status);
                        NOTI.success(res.message);
                    }
                });
            });
        },
        cancel: function () {
            ALERT.confirm('Xác hủy đơn hàng', null, () => {
                AJAX.post(window.location, {action: 'cancel-order'}, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        self.order.info.status(res.order_status);
                        NOTI.success(res.message);
                    }
                });
            });
        },
        save_note: function () {
            let data = {
                action: 'save-note',
                note: self.customer_info.note()
            };

            AJAX.post(window.location, data, true, function (res) {
                NOTI.success('Lưu ghi chú thành công');
            });
        }
    };

    //  Danh sách sản phẩm
    self.items = {
        list: ko.mapping.fromJS([]),
        change_term: function (item, term_id) {
            item.price(parseFloat(item.price()).toFixed(2));
            let fee = 0, total = item.price();
            let valid_step = 0;

            for (let i = 0; i < self.data.order_terms().length; i++) {
                let x = self.data.order_terms()[i];
                if (x.id() == term_id) {

                    for (let j = 0; j < x.steps().length; j++) {
                        let t = x.steps()[j];
                        if (total >= parseInt(t.step()) && parseInt(t.step()) >= valid_step) {
                            valid_step = parseInt(t.step());
                            fee = parseFloat(t.amount());
                            if (t.type() == 'percent') {
                                fee = parseFloat(t.amount()) * total / 100 * toNumber(self.order.info.currency_rate());
                                if (fee < parseInt(x.min_fee()) * toNumber(self.order.info.currency_rate()))
                                    fee = parseInt(x.min_fee()) * toNumber(self.order.info.currency_rate());
                            }
                        }
                    }

                    break;
                }
            }

            item.term_id(term_id);
            item.term_fee(fee);
            self.calc_total();
        },
        remove: function (item) {
            ALERT.confirm(`Xóa sản phẩm ${item.name()}?`, 'Sản phẩm sẽ bị xóa hoàn toàn khỏi đơn hàng', function () {
                AJAX.delete(window.location, {action: 'delete-item', id: item.id()}, false, (res) => {
                    if (!res.error) {
                        self.items.list.remove(item);

                        var total_weight = 0;
                        self.items.list.each((x) => {
                            total_weight += parseFloat(x.weight());
                        });

                        self.order.info.total_weight(total_weight.toFixed(2));

                        self.calc_total();
                        self.order.save();

                        if (self.items.list().length === 0)
                            self.order.info.website(undefined);
                    } else {
                        ALERT.error(res.message);
                    }
                });
            });
        },
        update: function (item, event) {
            let data = {
                action: 'update-item',
                item_id: item.id(),
                name: item.name(),
                quantity: item.quantity(),
                price: item.price(),
                note: item.note(),
                term_id: item.term_id(),
                term_fee: item.term_fee(),
                status: item.status(),
                tracking_code: item.tracking_code(),
                weight: item.weight(),
                requirement: item.requirement(),
                delivered_date: item.delivered_date(),
                link: item.link()
            };
			
			console.log("Link");
			console.log(item.link());

            AJAX.post(window.location, data, false, (res) => {
                if (!res.error) {
                    self.toggle_row(item, event);
                    self.calc_total();
                    self.order.save();

                    if (res.status != null)
                        item.status(res.status);

                } else ALERT.error(res.message);
            });
        },
        change_weight: function (item) {
            let total_weight = 0;
            item.total_weight(parseFloat(parseFloat(item.weight()) * parseInt(item.quantity())).toFixed(2));
            self.items.list.each((x) => {
                if (x.weight() != undefined) {
                    if (x.weight() != null)
                        total_weight += parseFloat(x.weight()) * parseInt(x.quantity());
                }
            });
            self.order.info.total_weight(total_weight.toFixed(2));
            item.weight(parseFloat(item.weight()).toFixed(2));
            self.calc_total();
        },
        change_quantity: function (item) {
            item.total_weight(parseFloat(parseInt(item.quantity()) * parseFloat(item.weight())).toFixed(2));
            self.items.change_weight(item);
        },
        change_total_weight: function (item) {
            item.weight(parseFloat(parseFloat(item.total_weight()) / parseInt(item.quantity())).toFixed(2));
            self.items.change_weight(item);
        },
        //  Thay đổi link sản phẩm
        change_link: function (item) {
            if (!self.item_modal.check_valid_url(item.link())) {
                NOTI.danger('Link sản phẩm không hợp lệ');
                return;
            }

            let website = self.item_modal.get_website(item.link());
            if (website !== self.order.info.website()) {
                ALERT.error(`Bạn chỉ được thêm sản phẩm từ ${self.order.info.website()}`);
                return;
            }

            item.website(website);
            if (self.items.list().length == 1)
                self.order.info.website(website);
        },
        //  Thay đổi tình trạng sản phẩm đơn hàng
        change_status: function (status, item_status, item_id) {
            let old_index = 0;
            self.data.item_status.each((x) => {
                if (x.id() === item_status) {
                    old_index = x.index();
                }
            });

            if (status.index() < old_index) {
                NOTI.danger(`Bạn không được phép chọn tình trạng ${status.name()}`);
                return;
            }

            self.items.list.each((x) => {
                if (x.id() == item_id) {
                    x.status(status.id());
                }
            });
        },
        //  Up ảnh đại diện sản phẩm
        upload_image: function (item) {
            self.image_uploader.show(true);
            self.image_uploader.save = function () {
                item.image_id(self.image_uploader.selected_images[0].id);
                item.image_path(self.image_uploader.selected_images[0].path);
                self.image_uploader.close();

                let data = {action: 'upload-image', item_id: item.id(), image_id: item.image_id()};
                AJAX.post(window.location, data, false, (res) => {
                    if (!res.error)
                        NOTI.success(res.message);
                });
            };
        },
        //  Xóa ảnh sản phẩm
        remove_image: function (item) {
            let data = {action: 'remove-image', item_id: item.id(), image_id: item.image_id()};
            AJAX.post(window.location, data, false, (res) => {
                if (!res.error) {
                    item.image_path(undefined);
                    NOTI.success(res.message);
                }
            });
        },
        update_requirement: function (item) {
            let data = {
                action: 'update-item',
                item_id: item.id(),
                requirement: item.requirement()
            };

            AJAX.post(window.location, data, false, (res) => {
                if (!res.error) {
                    self.toggle_row(item, event);
                    self.calc_total();
                    NOTI.success(res.message);
                } else ALERT.error(res.message);
            });
        }
    
	};

    //  Thông tin khách hàng
    self.customer_info = {
        name: ko.observable().extend({required: {message: LABEL.required}}),
        phone: ko.observable().extend({required: {message: LABEL.required}}),
        shipping_address: ko.observable().extend({required: {message: LABEL.required}}),
        email: ko.observable(),
        note: ko.observable(),
        city: ko.observable(),
        city_id: ko.observable(),
        district: ko.observable(),
        district_id: ko.observable(),
        edit: function () {
            MODAL.show('#edit-customer-modal');
        },
        //  Cập nhật thông tin khách hàng
        save: function () {
            if (!self.customer_info.isValid())
                self.customer_info.errors.showAllMessages();
            else {
                self.customer_info.errors.showAllMessages(false);
                let data = {
                    action: 'customer_info',
                    name: self.customer_info.name(),
                    email: self.customer_info.email(),
                    order_place_id: self.order.info.order_place_id(),
                    shipping_address: self.customer_info.shipping_address(),
                    city_id: self.customer_info.city_id(),
                    district_id: self.customer_info.district_id(),
                    note: self.customer_info.note(),
                    brand: self.order.info.brand()
                };

                AJAX.post(window.url, data, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        let city = self.data.cities.filter((x) => {
                            return x.id() === self.customer_info.city_id();
                        })[0];

                        let district = self.data.districts.filter((x) => {
                            return x.id() === self.customer_info.district_id();
                        })[0];

                        let order_place = self.data.order_places.filter((x) => {
                            return x.id() === self.order.info.order_place_id();
                        })[0];

                        self.customer_info.district(district.name());
                        self.customer_info.city(city.name());
                        self.order.info.order_place(order_place.name());
                        MODAL.hide('#edit-customer-modal');
                        NOTI.success(res.message);
                    }
                });
            }
        }
    };

    //  Thanh toán
    self.payment = {
        histories: ko.mapping.fromJS([]),
        created_time: ko.observable(moment(new Date()).format('DD-MM-YYYY HH:mm')),
        amount: ko.observable().extend({required: {message: LABEL.required}}),
        note: ko.observable().extend({required: {message: LABEL.required}}),
        type: ko.observable($('select[name=payment_type] option:first').val()).extend({
            required: {message: LABEL.required}
        }),
        bank_id: ko.observable(),
        //  Thêm thanh toán
        add: function () {
            if (!self.payment.isValid()) {
                self.payment.errors.showAllMessages();
				/* fixcode: insert note after insert product */
				if($('input[name=payment_amount]').val() == "") {
					$('input[name=payment_amount]').addClass("validationElement");
				} else {
					$('input[name=payment_amount]').removeClass("validationElement");
				}
			
            } else {
                if (toNumber(self.payment.amount()) + parseInt(self.order.info.total_paid()) > self.order.info.sub_total()) {
                    let amount = self.order.info.sub_total() - self.order.info.total_paid();
                    NOTI.danger(`Bạn chỉ được thanh toán tối đa ${parseInt(amount).toMoney(0)}`);
                    return;
                }

                let data = {
                    action: 'payment',
                    amount: toNumber(self.payment.amount()),
                    created_time: self.payment.created_time(),
                    note: self.payment.note(),
                    type: self.payment.type(),
                    bank_id: self.payment.bank_id()
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error) NOTI.danger(res.message);
                    else {
                        self.payment.init();
                        self.payment.created_time(moment(new Date()).format('DD-MM-YYYY HH:mm'));
                        self.payment.amount(undefined);
                        self.payment.note(undefined);
                        self.payment.errors.showAllMessages(false);
                        ALERT.success(res.message);
                    }
                });
            }
        },
        //  Lịch sử thanh toán, tổng tiền thanh toán của đơn hàng
        init: function () {
            AJAX.get(window.location, {init: 'payment'}, false, (res) => {
                ko.mapping.fromJS(res.data, self.payment.histories);
                self.order.info.total_paid(res.total);
            });
        },
        edit: function (item) {
            self.payment.edit_modal.amount(moneyFormat(item.amount()));
            self.payment.edit_modal.id(item.id());
            MODAL.show('#payment-edit-modal');
        },
        edit_modal: {
            amount: ko.observable(),
            id: ko.observable(),
            save: function () {
                let data = {
                    id: self.payment.edit_modal.id(),
                    amount: toNumber(self.payment.edit_modal.amount()),
                    action: 'update-payment'
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        let total_paid = 0;
                        self.payment.histories.each((x) => {
                            if (x.id() == self.payment.edit_modal.id()) {
                                x.amount(data.amount);
                            }

                            total_paid += parseInt(x.amount());
                        });

                        self.order.info.total_paid(total_paid);
                        self.calc_total();
						//Check neu status don hang set ve PENDING thi cap nhat status don hang
						if(res.order_status != undefined){
							self.order.info.status(res.order_status);
						}
                        MODAL.hide('#payment-edit-modal');
                        NOTI.success(res.message);
                    }
                });
            }
        }
    };

    //  Modal thêm sản phẩm
    self.item_modal = {
        name: ko.observable().extend({required: {message: LABEL.required}}),
        link: ko.observable().extend({required: {message: LABEL.required}}),
        quantity: ko.observable(1),
        price: ko.observable(),
        term_id: ko.observable().extend({required: {message: LABEL.required}}),
        term_fee: ko.observable(0),
        color: ko.observable(),
        size: ko.observable(),
        note: ko.observable(),
        variations: ko.mapping.fromJS([]),
        variation_editable: ko.observable(true),
        image_path: ko.observable(undefined),
        image_id: ko.observable(undefined),
        //  Thay đổi danh mục phụ thu
        change_term: function () {
            let fee = 0, total = self.item_modal.price();
            let valid_step = 0;

            for (let i = 0; i < self.data.order_terms().length; i++) {
                let x = self.data.order_terms()[i];
                if (x.id() == self.item_modal.term_id()) {

                    for (let j = 0; j < x.steps().length; j++) {
                        let t = x.steps()[j];
                        if (total >= parseInt(t.step()) && parseInt(t.step()) >= valid_step) {
                            valid_step = parseInt(t.step());
                            fee = parseFloat(t.amount());

                            if (t.type() == 'percent') {
                                /*fee = parseFloat(t.amount()) * total / 100 * toNumber(self.order.info.currency_rate());
                                if (fee < parseInt(x.min_fee()) * toNumber(self.order.info.currency_rate()))
                                    fee = parseInt(x.min_fee()) * toNumber(self.order.info.currency_rate());*/

                                let amount_fee = parseFloat(t.amount()) * total / 100;
                                if (amount_fee < x.min_fee())
                                    amount_fee = x.min_fee();
                                fee = amount_fee * toNumber(self.order.info.currency_rate());
                            }
                        }
                    }

                    break;
                }
            }

            self.item_modal.term_fee(fee);
        },
        //  Hiện modal
        show: function () {
            self.item_modal.name(undefined);
            self.item_modal.link(undefined);
            self.item_modal.price(0);
            self.item_modal.quantity(1);
            self.item_modal.term_fee(0);
            self.item_modal.term_id(undefined);
            self.item_modal.color(undefined);
            self.item_modal.size(undefined);
            self.item_modal.note(undefined);
            self.item_modal.image_id(undefined);
            self.item_modal.image_path(undefined);
            self.item_modal.variation_editable(true);
            ko.mapping.fromJS([], self.item_modal.variations);

            self.item_modal.change_term();
            self.item_modal.errors.showAllMessages(false);
            MODAL.show('#add-item-modal');
        },
        //  Thêm sản phẩm vào danh sách
        add: function () {
            if (!self.item_modal.isValid())
                self.item_modal.errors.showAllMessages();
            else {
                if (!self.item_modal.check_valid_url(self.item_modal.link())) {
                    NOTI.danger('Link web không hợp lệ');
                    return;
                }

                let item = {
                    name: ko.observable(self.item_modal.name()),
                    quantity: ko.observable(),
                    link: ko.observable(self.item_modal.link()),
                    website: ko.observable(self.item_modal.get_website(self.item_modal.link())),
                    variations: ko.mapping.fromJS([]),
                    note: ko.observable(self.item_modal.note()),
                    tracking_code: ko.observable(),
                    term_id: ko.observable(self.item_modal.term_id()),
                    term_fee: ko.observable(self.item_modal.term_fee()),
                    price: ko.observable(self.item_modal.price()),
                    status: ko.observable(),
                    shipped: ko.observable(0),
                    image_path: ko.observable(self.item_modal.image_path()),
                    image_id: ko.observable(self.item_modal.image_id()),
                    order_number: ko.observable()
                };

                self.item_modal.variations.each((x) => {
                    if (x.name() != undefined && x.value() != undefined) {
                        if (x.name() != null && x.value() != null) {
                            if (x.name() != '' && x.value() != '') {
                                item.variations.push({
                                    name: ko.observable(x.name()),
                                    value: ko.observable(x.value())
                                });
                            }
                        }
                    }
                });

                if (self.item_modal.quantity() !== undefined) {
                    if (self.item_modal.quantity() !== null) {
                        item.quantity(self.item_modal.quantity());
                    }
                }

                if (self.order.info.website() !== undefined) {
                    if (item.website() !== self.order.info.website()) {
                        ALERT.error(`Bạn chỉ được thêm sản phẩm từ ${self.order.info.website()}`);
                        return;
                    }
                } else {
                    //  Gán website cho đơn hàng
                    self.order.info.website(item.website());
                }

                if (self.data.item_status().length > 0)
                    item.status(self.data.item_status()[0].id());

                let data = {
                    action: 'add-item',
                    name: item.name(),
                    quantity: item.quantity(),
                    link: item.link(),
                    website: item.website(),
                    price: item.price(),
                    variations: ko.toJSON(item.variations()),
                    note: item.note(),
                    term_id: item.term_id(),
                    term_fee: item.term_fee(),
                    image_id: item.image_id()
                };

                AJAX.post(window.location, data, false, (res) => {
                    if (!res.error) {
                        ko.mapping.fromJS(res.data, self.items.list);
                        self.calc_total();
                        self.order.save();
                        MODAL.hide('#add-item-modal');
                    } else {
                        ALERT.error(res.message);
                    }
                });
            }
        },
        //  Lấy tên website từ link sản phẩm
        get_website: function (str) {
            let a = document.createElement('a');
            a.href = str;
            return a.hostname.replace('www.', '');
        },
        //  Kiểm tra url có hợp lệ hay không
        check_valid_url: function (str) {
            let regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
            if (regexp.test(str))
                return true;
            return false;
        },
        add_variation: function () {
            if (self.item_modal.variation_editable()) {
                self.item_modal.variations.push({
                    name: ko.observable(),
                    value: ko.observable(),
                    sources: ko.observableArray([])
                });
            }
        },
        delete_variation: function (t) {
            if (self.item_modal.variation_editable())
                self.item_modal.variations.remove(t);
        },
        crawl: function () {
            //#region Verify url
            if (self.item_modal.link() == undefined) {
                NOTI.danger('Chưa nhập link sản phẩm');
                return;
            }

            if (self.item_modal.link() == null) {
                NOTI.danger('Chưa nhập link sản phẩm');
                return;
            }

            if (self.item_modal.link() == '') {
                NOTI.danger('Chưa nhập link sản phẩm');
                return;
            }

            if (!self.item_modal.check_valid_url(self.item_modal.link())) {
                ALERT.error('Link web không hợp lệ');
                return;
            }
            //#endregion

            AJAX.post('/basso/customer_order/crawl', {url: self.item_modal.link()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    self.item_modal.variation_editable(false);
                    self.item_modal.name(res.data.title);
                    self.item_modal.price(res.data.price);
                    self.item_modal.variations([]);
                    self.item_modal.image_path(res.data.image_path);
                    self.item_modal.image_id(res.data.image_id);

                    for (let i = 0; i < res.data.variations.length; i++) {
                        self.item_modal.variations.push({
                            name: ko.observable(res.data.variations[i].name),
                            value: ko.observable(res.data.variations[i].value),
                            sources: ko.observableArray(res.data.variations[i].sources)
                        });
                    }
                }
            });
        }
    };

    //  Nhãn, nhóm nhãn
    self.label = {
        group_id: ko.observable(),
        list: ko.observableArray([]),
        //  Nạp dữ liệu
        init: function (data) {
            _.each(data, (t) => {
                //  Lấy tên nhóm
                let group_label = self.data.label_groups.filter((x) => {
                    return x.id() === t.group_id;
                })[0];
                let lables = self.data.labels.filter((x) => {
                    return x.group_id() === t.group_id;
                });

                let item = {
                    group_id: ko.observable(t.group_id),
                    group_name: ko.observable(group_label.name()),
                    values: ko.mapping.fromJS([]),
                    value_id: ko.observable(t.label_id)
                };

                ko.mapping.fromJS(lables, item.values);
                self.label.list.push(item);
            });
        },
        //  Thêm nhóm nhãn vào đơn hàng
        add: function () {
            let exist = self.label.list.filter((x) => {
                return x.group_id() === self.label.group_id();
            }).length > 0;

            //  Nếu nhóm nhãn không tồn tại
            if (!exist) {
                let data = self.data.labels.filter((x) => {
                    return x.group_id() === self.label.group_id();
                });

                if (data.length === 0) return;

                //  Lấy tên nhóm
                let group_label = self.data.label_groups.filter((x) => {
                    return x.id() === self.label.group_id();
                })[0];

                let item = {
                    group_id: ko.observable(self.label.group_id()),
                    group_name: ko.observable(group_label.name()),
                    values: ko.mapping.fromJS([]),
                    value_id: ko.observable(data[0].id)
                };

                ko.mapping.fromJS(data, item.values);
                self.label.list.push(item);
            }
        },
        //  Xóa nhóm nhãn
        remove: function (item) {
            self.label.list.remove(item);
        },
        //  Lưu nhóm nhãn
        save: function () {
            let data = [];
            self.label.list.each((x) => {
                data.push({group_id: x.group_id(), label_id: x.value_id()});
            });

            AJAX.post(window.location, {action: 'label', data: data}, true, (res) => {
                if (res.error) ALERT.error(res.message);
                else NOTI.success(res.message);
            });
        }
    };

    self.shipping_modal = {
        items: ko.mapping.fromJS([]),
        selected_items: ko.observableArray([]),
        total: ko.observable(0),
        include_fee: ko.observable(false),
        customer_info: {
            name: ko.observable().extend({required: {message: LABEL.required}}),
            phone: ko.observable().extend({required: {message: LABEL.required}}),
            address: ko.observable().extend({required: {message: LABEL.required}}),
            note: ko.observable(),
            city_id: ko.observable().extend({required: {message: LABEL.required}}),
            district_id: ko.observable().extend({required: {message: LABEL.required}})
        },
        shipping_info: {
            id: ko.observable(),
            code: ko.observable(),
            fee: ko.observable('0'),
            shipping_fee: ko.observable('0'),
            cod_amount: ko.observable('0'),
            weight: ko.observable(0.0),
            size: ko.observable()
        },
        show: function () {
            if (self.data.shipping_agencies().length == 0) {
                ALERT.error('Không tìm thấy đơn vị vận chuyển');
                return;
            }
            self.shipping_modal.shipping_info.id(self.data.shipping_agencies()[0].id);
            self.shipping_modal.shipping_info.code(undefined);
            self.shipping_modal.shipping_info.fee('0');
            self.shipping_modal.shipping_info.cod_amount('0');
            self.shipping_modal.shipping_info.weight(0.0);
            self.shipping_modal.shipping_info.size(undefined);
            self.shipping_modal.selected_items([]);

            self.shipping_modal.customer_info.name(self.customer_info.name());
            self.shipping_modal.customer_info.phone(self.customer_info.phone());
            self.shipping_modal.customer_info.address(self.customer_info.shipping_address());
            self.shipping_modal.customer_info.city_id(self.customer_info.city_id());
            self.shipping_modal.customer_info.district_id(self.customer_info.district_id());
            self.shipping_modal.customer_info.note(undefined);
            self.shipping_modal.total(0);
            self.data.change_city();

            let items = self.items.list.filter((x) => {
                return x.shipped() == 0;
            });
            ko.mapping.fromJS(items, self.shipping_modal.items);
            self.shipping_modal.errors.showAllMessages(false);

            let total = parseInt(self.order.info.sub_total()) - parseInt(self.order.info.total_paid());
            self.shipping_modal.shipping_info.cod_amount(moneyFormat(total));
            MODAL.show('#shipping-modal');
        },
        save: function () {
            if (!self.shipping_modal.isValid()) {
                self.shipping_modal.errors.showAllMessages();
            } else if (self.shipping_modal.selected_items().length === 0)
                NOTI.danger('Chưa chọn sản phẩm');
            else {
                let data = {
                    shipping_id: self.shipping_modal.shipping_info.id(),
                    shipping_fee: toNumber(self.shipping_modal.shipping_info.shipping_fee().toString()),
                    fee: toNumber(self.shipping_modal.shipping_info.fee().toString()),
                    cod_amount: toNumber(self.shipping_modal.shipping_info.cod_amount().toString()),
                    code: self.shipping_modal.shipping_info.code(),
                    weight: self.shipping_modal.shipping_info.weight(),
                    size: self.shipping_modal.shipping_info.size(),
                    name: self.shipping_modal.customer_info.name(),
                    phone: self.shipping_modal.customer_info.phone(),
                    address: self.shipping_modal.customer_info.address(),
                    note: self.shipping_modal.customer_info.note(),
                    city_id: self.shipping_modal.customer_info.city_id(),
                    district_id: self.shipping_modal.customer_info.district_id(),
                    items: self.shipping_modal.selected_items(),
                    action: 'add-shipping'
                };

                ALERT.confirm('Xác nhận thêm đơn vận chuyển?', null, () => {
                    AJAX.post(window.location, data, true, (res) => {
                        if (!res.error) {
                            ko.mapping.fromJS(res.data, self.shipping.histories);
                            ALERT.success(res.message);
                            MODAL.hide('#shipping-modal');

                            let inter_shipping_fee = parseInt(self.order.info.inter_shipping_fee());
                            inter_shipping_fee += data.fee;
                            self.order.info.inter_shipping_fee(inter_shipping_fee);
                            self.items.list.each((x) => {
                                x.shipped(1);
                            });
                            self.calc_total();
                        } else
                            ALERT.error(res.message);
                    });
                });
            }
        },
        check_all: function (data, event) {
            if ($(event.currentTarget).is(':checked')) {
                self.shipping_modal.items.each((x) => {
                    if (!self.shipping_modal.selected_items().includes(x.id()))
                        self.shipping_modal.selected_items.push(x.id());
                });
            } else {
                self.shipping_modal.selected_items([]);
            }
        },
        calc_total: function () {
			console.log("case 2");
            if (self.shipping_modal.include_fee()) {
                let total = parseInt(self.order.info.sub_total()) - parseInt(self.order.info.total_paid());
                total += toNumber(self.shipping_modal.shipping_info.fee());
                self.shipping_modal.shipping_info.cod_amount(moneyFormat(total));
            } else {
                let total = parseInt(self.order.info.sub_total()) - parseInt(self.order.info.total_paid());
                self.shipping_modal.shipping_info.cod_amount(moneyFormat(total));
            }
        },
		
		ship_cancel: function () {
			let data = {
				action: 'cancel-shipping',
			};

			AJAX.post(window.location, data, false, (res) => {;
				if (!res.error) {
					location.reload();
				} else {
					ALERT.error(res.message);
				} 
			});
		}
    };

    self.send_email = function (type) {
        ALERT.confirm('Xác nhận gửi email', null, () => {
            AJAX.post(window.location, {action: 'send-email', type: type}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    ALERT.success(res.message);
                    MODAL.hide('#send-email-modal');
                }
            });
        });
    };

    self.toggle_row = function (data, event) {
        toggle_row(event.currentTarget);
    };

    //  Tính tiền đơn hàng
    self.calc_total = function () {
		console.log("case 3");
        let total = 0;
        let sub_total = 0;
        var discount_amount = 0;

        if (self.order.info.discount_type() == 'percent')
            self.order.info.discount_total(toNumber(self.order.info.discount_total()));
        else
            self.order.info.discount_total(moneyFormat(self.order.info.discount_total()));

		var _tongGiaMua = 0;
        self.items.list.each((item) => {
            if (item.quantity() !== undefined) {
                if (item.quantity() !== null) {
                    //  Kiểm tra nếu số lượng sản phẩm không phải là số
                    if (isNaN(item.quantity())) {
                        item.quantity(toNumber(item.quantity().toString()));
                    }

                    total += item.quantity() * parseFloat(item.price());
                    sub_total += item.quantity() * parseInt(item.term_fee());
					_tongGiaMua += item.TongGiaMua(); 
                }
            }
        });
		
        /* self.order.info.total(total); */
		/* fixcode: update TongTien = phi_ship_web + tong_tien */
		/* check neu shipping_fee == empty thi gan bang 0 */
		if(self.order.info.web_shipping_fee()==""){
			self.order.info.web_shipping_fee(0);
		}
		
        //self.order.info.origin_sub_total((total * toNumber(self.order.info.currency_rate())) + self.order.info.inter_shipping_fee(0));
		self.order.info.origin_sub_total(_tongGiaMua);
		
		//Tổng tiền đã bao gồm web_shipping_fee (Phí ship web) 
        self.order.info.total(total + parseFloat(self.order.info.web_shipping_fee()));
        //self.order.info.sub_total(parseFloat(self.order.info.currency_rate()) * total + parseFloat(self.order.info.world_shipping_fee()) * parseFloat(self.order.info.total_weight()));
			
		//Công thức tính world_shipping fee đã có sự thay đổi, nó đc tính trong controller
        /* let world_shipping = parseFloat(self.order.info.total_weight()) * parseInt(self.order.info.country_shipping_fee()); */
		let world_shipping = parseFloat(self.order.info.world_shipping_fee_total());	
		
        self.order.info.world_shipping_fee(world_shipping);
		
        sub_total += (parseFloat(self.order.info.web_shipping_fee()) + total) * toNumber(self.order.info.currency_rate())
            + parseInt(self.order.info.world_shipping_fee());
        sub_total += parseInt(self.order.info.inter_shipping_fee());
        let discount_total = toNumber(self.order.info.discount_total());

        if (discount_total >= 0) {
            if (self.order.info.discount_type() == 'percent' && discount_total > 100)
                self.order.info.discount_total(100);

            if (discount_total < 0)
                self.order.info.discount_total(0);

            if (self.order.info.discount_type() == 'amount') {
                sub_total -= discount_total;
                discount_amount = discount_total;
            } else {
                discount_amount = sub_total * discount_total / 100;
                if (discount_amount > parseInt(self.order.info.discount_max()) && parseInt(self.order.info.discount_max()) > 0)
                    discount_amount = parseInt(self.order.info.discount_max());

                sub_total -= discount_amount;
            }
        }

        sub_total = sub_total * (100 + parseInt(self.order.info.fee_percent())) / 100;
        sub_total = Math.floor(sub_total);
        sub_total = sub_total <= 5 ? 0 : sub_total;

        self.order.info.sub_total(sub_total);
        self.order.info.first_total_deposit(sub_total - self.order.info.total_paid());
        self.order.info.first_total_limit((self.order.info.first_total_deposit()*100)/sub_total);
    };

    self.to_number = function (str) {
        return toNumber(str);
    };

    self.money_format = function (num) {
        return moneyFormat(num);
    }
	
    self.log_modal = {
        result: ko.mapping.fromJS([]),
        show: function () {
            AJAX.get(window.location, {init: 'logs'}, true, (res) => {
                ko.mapping.fromJS(res.data, self.log_modal.result);
                MODAL.show('#log-modal');
            });
        }
    };
}

let model = new viewModel();
model.data.init();
model.payment.init();
ko.validatedObservable(model.payment);
ko.validatedObservable(model.item_modal);
ko.validatedObservable(model.customer_info);
ko.validatedObservable(model.shipping_modal);
ko.applyBindings(model, document.getElementById('main-content'));