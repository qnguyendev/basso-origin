<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Article
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property Term_model $term_model
 * @property Article_model $article_model
 * @property CI_Upload $upload
 * @property CI_Config $config
 * @property Minify $minify
 * @property Image_model $image_model
 * @property CI_Form_validation $form_validation
 */
class Article extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
            return redirect('admin/auth/login');

        $this->load->model('admin/term_model');
        $this->load->model('article_model');
        $this->load->model('admin/image_model');
		$this->load->model('basso/contact_form_model');
    }

    public function index()
    {
        $terms = $this->term_model->get(0, 'post');
        $breadcrumbs = [['text' => 'Quản lý bài viết']];
        $this->blade
            ->set('page_title', 'Quản lý tin tức')
            ->set('breadcrumbs', $breadcrumbs)
            ->set('terms', $terms)->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        $key = $this->input->get('key');
        $term_id = $this->input->get('term_id');

        if ($page != null) {
            if (is_numeric($page)) {
                $page = $page < 1 ? 1 : $page;
                $result = $this->article_model->get($page, $key, $term_id);
                $pagination = Pagination::calc($this->article_model->count($key, $term_id), $page,
                    $this->config->item('result_per_page'));

                json_success(null, ['data' => $result, 'pagination' => $pagination]);
            }
        }

        die;
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $article = $this->article_model->get_by_id($id);

        if ($article != null) {
            if ($article->image_id != null)
                $this->image_model->set_active($article->image_id, 0);
            $this->article_model->delete($id);

            json_success('Xóa bài viết thành công');
        }
        json_error('Không tìm tháy bài viết yêu cầu');
    }

    public function create()
    {
        $terms = $this->term_model->get(0, 'post_page');
        $breadcrumbs = [['text' => 'Bài viết'], ['text' => 'Thêm bài viết']];

        $this->blade->set('image_uploader', true);
        return $this->blade
            ->set('page_title', 'Thêm tin tức')
            ->set('breadcrumbs', $breadcrumbs)
            ->set('terms', $terms)->render('editor');
    }

    public function edit($id)
    {
        if ($id == null)
            return redirect('/admin/term');

        if (!is_numeric($id))
            return redirect('/admin/term');

        $terms = $this->term_model->get(0, 'post_page');
        $breadcrumbs = [['text' => 'Bài viết'], ['text' => 'Sửa bài viết']];

        $this->blade->set('image_uploader', true);
        return $this->blade
            ->set('page_title', 'Cập nhật tin tức')
            ->set('breadcrumbs', $breadcrumbs)
            ->set('terms', $terms)->render('editor');
    }

    public function GET_edit($id)
    {
        $term = $this->article_model->get_by_id($id);
        if ($term == null)
            json_error('Không tìm thấy tin tức', ['url' => '/admin/article']);

        json_success(null, ['data' => $term, 'terms' => $this->article_model->get_terms($id)]);
    }

    /**
     * Tạo mới/cập nhật tin tức
     */
    public function POST_save()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin yêu cầu');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $slug = $this->input->post('slug');
        $des = $this->input->post('des');
        $content = $this->input->post('content');
        $active = $this->input->post('active');
        $featured = $this->input->post('featured');
        $created_time = $this->input->post('created_time');
        $terms = $this->input->post('terms');
        $seo_title = $this->input->post('seo_title');
        $seo_des = $this->input->post('seo_des');
        $seo_index = $this->input->post('seo_index');
        $image = $this->input->post('image');

        $slug = $slug == null
            ? url_title($name, '-', true)
            : url_title($slug, '-', true);
        $slug = $this->article_model->create_slug($id, $slug);

        $data = [
            'name' => $name,
            'slug' => $slug,
            'image_id' => null,
            'image_path' => null,
            'short_content' => $des,
            'content' => $content,
            'active' => $active,
            'featured' => $featured,
            'created_time' => strtotime($created_time),
            'seo_title' => $seo_title,
            'seo_description' => $seo_des,
            'seo_index' => $seo_index
        ];

        //  Set các ảnh cũ chưa active
        if ($id > 0) {
            $article = $this->article_model->get_by_id($id);
            if ($article->image_id != null)
                $this->image_model->set_active($article->image_id, 0);
        }

        #region Hình ảnh upload
        if ($image != null) {
            $this->image_model->set_active($image);     //  Đánh dấu ảnh được sử dụng
            $img = $this->image_model->get($image);     //  Lấy thông tin ảnh
            if ($img != null) {
                $data['image_path'] = $img->path;
                $data['image_id'] = $img->id;
            }
        }
        #endregion

        $user = $this->ion_auth->user()->row();
        $this->article_model->insert($user->id, $id, $data, $terms);

        success_msg($id == 0 ? 'Thêm bài viết thành công' : 'Cập nhật bài viết thành công');
        json_success(null, ['url' => '/admin/article/']);
    }


    /**
     * Thay đổi trạng thái ẩn/hiện
     */
    public function POST_change_active()
    {
        $id = $this->input->post('id');
        $active = $this->input->post('active');
        if ($id != null && $active != null) {
            $this->article_model->change_active($id, $active);
            json_success('Cập nhật thành công');
        }

        json_error('Yêu cầu không tồn tại');
    }

    /**
     * Thay đổi bài viết nổi bật
     */
    public function POST_change_featured()
    {
        $id = $this->input->post('id');
        $featured = $this->input->post('featured');
        if ($id != null && $featured != null) {
            $this->article_model->change_featured($id, $featured);
            json_success('Cập nhật thành công');
        }

        json_error('Yêu cầu không tồn tại');
    }
}
