<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Error
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 */
class Errors extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function no_permission()
    {
        return $this->blade->render();
    }
}