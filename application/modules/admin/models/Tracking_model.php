<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Tracking_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Tracking_model extends CI_Model
{
    public function insert($id, $name, $active, $code, $position = 'head', $page = null)
    {
        $data = [
            'name' => $name,
            'active' => $active,
            'code' => $code,
            'position' => $position
        ];

        if ($page != null) {
            if (is_array($page)) {
                if (in_array(-1, $page))
                    $data['note'] = 'home';
            }
        }else{
                    $data['note'] = 'all';
		}

        if ($id == 0) {
            $this->db->insert('tracking_codes', $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update('tracking_codes', $data);
        }

        $this->db->where('tracking_id', $id);
        $this->db->delete('tracking_code_page');

        if ($page != null) {
            if (is_array($page)) {
                $data = array();
                foreach ($page as $page_id) {
                    if ($page_id != -1)
                        array_push($data, array('page_id' => $page_id, 'tracking_id' => $id));
                }

                if (count($data) > 0)
                    $this->db->insert_batch('tracking_code_page', $data);
            }
        }

        return $id;
    }

    /**
     * Danh sách ID page được hiển thị mã
     * @param $tracking_id
     * @return array
     */
    public function get_pages($tracking_id)
    {
        $this->db->where('tracking_id', $tracking_id);
        $result = $this->db->get('tracking_code_page')->result();
        return array_column($result, 'page_id');
    }

    public function get_by_id($id)
    {
        $this->db->where("id", $id);
        return $this->db->get('tracking_codes')->row();
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tracking_codes');
    }

    public function get($page = 1)
    {
        $this->db->offset(($page - 1) * $this->config->item('result_per_page'));
        $this->db->limit($this->config->item('result_per_page'));
        $this->db->order_by('name', 'asc');
        return $this->db->get('tracking_codes')->result();
    }

    public function count()
    {
        return $this->db->count_all_results('tracking_codes');
    }
}