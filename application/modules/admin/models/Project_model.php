<?php

if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Project_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Project_model extends \CI_Model
{
    /**
     * Thêm mới, cập nhật dự án
     * @param int $id
     * @param string $name
     * @param array|null $data
     * @param array|null $images
     * @param array|null $terms
     */
    public function insert(int $id, string $name, array $data, array $images = null, array $terms = null)
    {
        $data['name'] = $name;
        if ($id == 0) {
            $data['slug'] = $this->create_slug($id, url_title($name, '-', true));
            $data['created_time'] = time();
            $this->db->insert('projects', $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update('projects', $data);
        }

        $this->db->where('project_id', $id);
        $this->db->delete('projects_terms');
        if ($terms != null) {
            if (is_array($terms)) {
                $data = [];
                foreach ($terms as $term_id)
                    array_push($data, ['term_id' => $term_id, 'project_id' => $id]);

                if (count($data) > 0)
                    $this->db->insert_batch('projects_terms', $data);
            }
        }

        $this->db->where('project_id', $id);
        $this->db->delete('projects_images');
        if ($images != null) {
            if (is_array($images)) {
                $data = [];
                foreach ($images as $item)
                    array_push($data, ['image_id' => $item['image_id'], 'project_id' => $id, 'image_path' => $item['image_path']]);

                if (count($data) > 0)
                    $this->db->insert_batch('projects_images', $data);
            }
        }
    }

    public function get_by_id(int $id)
    {
        $this->db->where('id', $id);
        return $this->db->get('projects')->row();
    }

    public function get_images(int $project_id)
    {
        $this->db->select('image_id as id, image_path as path');
        $this->db->from('projects_images');
        $this->db->where('project_id', $project_id);
        return $this->db->get()->result();
    }

    public function get_terms(int $project_id)
    {
        $this->db->where('project_id', $project_id);
        return $this->db->get('projects_terms')->result();
    }

    public function get(int $page = 1)
    {
        $this->db->select('t.*, (select image_path from projects_images where project_id = t.id limit 1) as image');
        $this->db->select('GROUP_CONCAT(d.name) as terms');
        $this->db->from('projects t');
        $this->db->join('projects_terms o', 'o.project_id = t.id', 'left');
        $this->db->join('terms d', 'd.id = o.term_id', 'left');
        $this->db->limit(PAGING_SIZE);
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count()
    {
        return $this->db->count_all_results('members');
    }

    public function delete(int $id)
    {
        $this->db->where('id', $id);
        $this->db->delete('projects');
    }

    private function create_slug(int $id, string $slug)
    {
        $count = 0;
        $_slug = $slug;             // Create temp name
        while (true) {
            $this->db->where('id !=', $id);
            $this->db->where('slug', $_slug);
            if ($this->db->count_all_results('projects') == 0) break;
            $_slug = $slug . '-' . (++$count);  // Recreate new temp name
        }
        return $_slug;      // Return temp name
    }
}