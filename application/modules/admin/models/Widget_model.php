<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Widget_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Widget_model extends CI_Model
{
    /**
     * @param int $id
     * @param string $type
     * @param string $title
     * @param bool $show_title
     * @param int $active
     * @param string $description
     * @param int $order
     * @param string $options
     * @param array $area
     * @return int
     */
    public function save(int $id, string $type, string $title, bool $show_title, int $active = 1,
                         string $description = null, int $order, string $options = null, array $area = null)
    {
        $data = array(
            'title' => $title,
            'show_title' => $show_title,
            'order' => $order,
            'options' => $options,
            'description' => $description,
            'active' => $active
        );

        if ($id == 0) {
            $data['type'] = $type;
            $this->db->insert('widgets', $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update('widgets', $data);
        }

        $this->db->where('widget_id', $id);
        $this->db->delete('widgets_display');

        if (is_array($area)) {
            if (count($area) > 0) {
                $data = array();
                foreach ($area as $t)
                    array_push($data, array('widget_id' => $id, 'area_id' => $t));

                $this->db->insert_batch('widgets_display', $data);
            }
        }

        return $id;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function get_by_id(int $id)
    {
        $this->db->where('id', $id);
        return $this->db->get('widgets')->row();
    }

    /**
     * Lấy danh sách vị trí hiển thị theo widget
     * @param int $widget_id
     * @return array
     */
    public function get_area(int $widget_id)
    {
        $this->db->where('widget_id', $widget_id);
        return $this->db->get('widgets_display')->result();
    }

    /**
     * Xóa widget
     * @param int $id
     */
    public function delete(int $id)
    {
        $this->db->where('id', $id);
        $this->db->delete('widgets');
    }

    public function get($page, $position, $type, $active, $key)
    {
        $this->db->select('t.id, t.title, t.show_title, t.order, GROUP_CONCAT(o.area_id) as area, t.type, t.active');
        $this->db->from('widgets t');
        $this->db->join('widgets_display o', 't.id = o.widget_id', 'left');
        $this->db->order_by('t.order', 'asc');

        if ($position != 'all')
            $this->db->where('o.area_id', $position);
        if ($type != 'all')
            $this->db->where('t.type', $type);
        if ($active != -1)
            $this->db->where('t.active', $active);

        if ($key != null)
            $this->db->like('t.title', $key, 'both');

        $this->db->limit(PAGING_SIZE);
        $this->db->offset(($page - 1) * PAGING_SIZE);

        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count($position, $type, $active, $key)
    {
        $this->db->select('t.id, t.title, t.show_title, t.order, GROUP_CONCAT(o.area_id) as area, t.type, t.active');
        $this->db->from('widgets t');
        $this->db->join('widgets_display o', 't.id = o.widget_id', 'left');
        $this->db->order_by('t.order', 'asc');

        if ($position != 'all')
            $this->db->where('o.area_id', $position);
        if ($type != 'all')
            $this->db->where('t.type', $type);
        if ($active != -1)
            $this->db->where('t.active', $active);

        if ($key != null)
            $this->db->like('t.title', $key, 'both');

        $this->db->group_by('t.id');
        return $this->db->count_all_results();
    }
}