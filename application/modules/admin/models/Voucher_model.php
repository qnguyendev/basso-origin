<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Voucher_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Voucher_model extends CI_Model
{
    public function insert(int $id, string $code, array $data)
    {
        if ($id == 0) {
            $data['code'] = strtoupper(trim($code));
            $this->db->insert('vouchers', $data);
        } else {
            $this->db->where('id', $id);
            $this->db->update('vouchers', $data);
        }
    }

    public function get_by_code(string $code = null)
    {
        $this->db->where('code', $code);
        return $this->db->get('vouchers')->row();
    }

    public function get(int $id = null)
    {
        if ($id == null) {
            return $this->db->get('vouchers')->result();
        }

        $this->db->where('id', $id);
        return $this->db->get('vouchers')->row();
    }

    public function delete(int $id)
    {
        $this->db->where('id', $id);
        $this->db->delete('vouchers');
    }

    public function change_active(int $id, int $active)
    {
        $this->db->where('id', $id);
        $this->db->update('vouchers', ['active' => $active]);
    }
}