<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_Setting_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Order_setting_model extends CI_Model
{
    /**
     * Danh sách hình thức thanh toán trong hệ thống
     */
    public function get()
    {
        return $this->db->get('payments')->result();
    }

    /**
     * Thay đổi trạng thái ẩn/hiện
     * @param $id
     * @param $active
     */
    public function change_active($id, $active)
    {
        $this->db->where('id', $id);
        $this->db->update('payments', array('active' => $active));
    }

    /**
     * Cập nhật phương thức thanh toán
     * @param $id
     * @param $name
     * @param $des
     * @param $app_key
     * @param $app_secret
     */
    public function update($id, $name, $des, $app_key, $app_secret)
    {
        $data = array(
            'name' => $name,
            'description' => $des,
            'app_key' => $app_key,
            'app_secret' => $app_secret
        );

        $this->db->where('id', $id);
        $this->db->update('payments', $data);
    }

    /**
     * Danh sách tài khoản ngân hàng
     * @return array
     */
    public function get_banks()
    {
        $this->db->select('t.*');
        $this->db->from('payment_detail t');
        $this->db->join('payments o', 'o.id = t.payment_id', 'left');
        $this->db->where('o.type', 'bank-transfer');

        return $this->db->get()->result();
    }

    /**
     * Cập nhật ds tài khoản ngân hàng
     * @param $payemnt_id
     * @param $banks
     */
    public function insert_banks($payemnt_id, array $banks = null)
    {
        $this->db->truncate('payment_detail');

        if ($banks != null) {
            $data = [];

            foreach ($banks as $bank) {
                if ($bank['bank_name'] != null && $bank['bank_number'] != null && $bank['bank_account'] != null && $bank['bank_branch'] != null) {
                    array_push($data, [
                        'payment_id' => $payemnt_id,
                        'bank_name' => $bank['bank_name'],
                        'bank_number' => $bank['bank_number'],
                        'bank_account' => $bank['bank_account'],
                        'bank_branch' => $bank['bank_branch']
                    ]);
                }
            }

            if (count($data) > 0)
                $this->db->insert_batch('payment_detail', $data);
        }
    }
}