<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Brand_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_Config $config
 */
class Brand_model extends CI_Model
{
    public function insert($id, $data)
    {
        if ($id == 0) {
            $this->db->insert('brands', $data);
        } else {
            $this->db->where('id', $id);
            $this->db->update('brands', $data);
        }
    }

    public function change_active($id, $active)
    {
        $this->db->where('id', $id);
        $this->db->update('brands', array('active' => $active));
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('brands');
    }

    public function get($page = 1)
    {
        if ($page == 0) {
            $this->db->select('id, name, image_path');
            $this->db->order_by('name', 'asc');
            return $this->db->get('brands')->result();
        }

        $this->db->select('t.*, (select count(id) from products where brand_id = t.id) as product_count');
        $this->db->from('brands t');
        $this->db->limit($this->config->item('result_per_page'));
        $this->db->offset(($page - 1) * $this->config->item('result_per_page'));
        return $this->db->get()->result();
    }

    public function count()
    {
        return $this->db->count_all_results('brands');
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('brands')->row();
    }

    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('brands')->row();
    }

    public function create_slug($id, $slug)
    {
        $count = 0;
        $_slug = $slug;             // Create temp name
        while (true) {
            $this->db->where('id !=', $id);
            $this->db->where('slug', $_slug);
            if ($this->db->count_all_results('brands') == 0) break;
            $_slug = $slug . '-' . (++$count);  // Recreate new temp name
        }
        return $_slug;      // Return temp name
    }
}