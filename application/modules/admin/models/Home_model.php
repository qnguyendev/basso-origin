<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Home_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Home_model extends CI_Model
{
    public function insert_district($name, $city_id)
    {
        $data = array('name' => $name, 'city_id' => $city_id);
        $this->db->insert('districts', $data);
    }

    public function orders($status, $start, $end)
    {
        $this->db->select('id, name, created_time, sub_total, shipping_status');
        if (!is_array($status))
            $this->db->where('shipping_status', $status);
        else
            $this->db->where_in('shipping_status', $status);

        $this->db->where('created_time >=', $start);
        $this->db->where('created_time <=', $end);
        return $this->db->get('orders')->result();
    }

    public function recent_posts($limit = 5)
    {
        $this->db->select('t.id, t.image_path, t.name, t.slug, t.active, t.featured, t.created_time, GROUP_CONCAT(o.name) as terms');
        $this->db->from('articles t');
        $this->db->join('articles_terms d', 'd.article_id = t.id', 'left');
        $this->db->join('terms o', 'd.term_id = o.id', 'left');
        $this->db->limit($limit);
        $this->db->group_by('t.id');

        return $this->db->get()->result();
    }
}