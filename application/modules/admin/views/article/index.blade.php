<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">
                <a href="{{base_url('admin/article/create')}}" class="float-right">
                    <i class="fa fa-plus"></i>
                    Thêm bài viết
                </a>
                Danh sách bài viết
            </div>
        </div>
        <form autocomplete="off" class="card-body">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Nhập tên bài viết"
                           data-bind="value: filter_key, event: {change: function(){$root.search(1)}}">
                    <div class="input-group-append">
                        <button class="btn btn-sm btn-primary" data-bind="click: function(){$root.search(1)}"
                                style="margin: 0">
                            <i class="fa fa-search"></i>
                            Tìm kiếm
                        </button>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th style="width: 50px">ID</th>
                    <th width="80">Ảnh</th>
                    <th width="70">Views</th>
                    <th>Tiêu đề</th>
                    <th style="width: 140px">Thời gian</th>
                    <th style="width: 95px">Ẩn/hiện</th>
                    <th style="width: 95px">Nổi bật</th>
                    <th>Danh mục</th>
                    <th width="80"></th>
                </tr>
                </thead>
                <tbody data-bind="foreach: result">
                <tr>
                    <th data-bind="text: id" class="text-center"></th>
                    <td class="text-center">
                        <!--ko if: image_path() != null -->
                        <img data-bind="attr: {src: '/timthumb.php?src=' + image_path() + '&w=50&h=50'}, visible: image_path() != null"/>
                        <!--/ko-->
                        <!--ko if: image_path() == null-->
                        <img src="/timthumb.php?src=/assets/img/dummy.png&w=50&h=50"/>
                        <!--/ko-->
                    </td>
                    <td class="text-center" data-bind="text: views"></td>
                    <td class="text-left">
                        <a data-bind="attr: {href: '{{base_url('admin/article/edit/')}}' + id()}, text: name"
                           class="font-weight-bold">
                        </a>
                    </td>
                    <td class="text-center"
                        data-bind="text: moment.unix(created_time()).format('HH:mm DD/MM/YYYY')"></td>
                    <td class="text-center">
                        <!--ko if: active() == 1-->
                        <a class="btn btn-xs btn-success" href="javascript:"
                           data-bind="click: $root.change_active">
                            <i class="fa fa-eye"></i>
                            Hiện
                        </a>
                        <!--/ko-->
                        <!--ko if: active() == 0-->
                        <a class="btn btn-xs btn-danger" href="javascript:"
                           data-bind="click: $root.change_active">
                            <i class="fa fa-eye-slash"></i>
                            Ẩn
                        </a>
                        <!--/ko-->
                    </td>
                    <td class="text-center">
                        <!--ko if: featured() == 1-->
                        <a class="btn btn-xs btn-warning" href="javascript:"
                           data-bind="click: $root.change_featured">
                            <i class="fa fa-star"></i>
                        </a>
                        <!--/ko-->
                        <!--ko if: featured() == 0-->
                        <a class="btn btn-xs btn-secondary" href="javascript:" data-bind="click: $root.change_featured">
                            <i class="fa fa-star"></i>
                        </a>
                        <!--/ko-->
                    </td>
                    <td class="text-center" data-bind="text: terms"></td>
                    <td class="text-center">
                        <a href="javascript:" class="text-danger" data-bind="click: $root.delete">
                            <i class="icon-trash"></i>
                            Xóa
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
            @include('pagination')
        </form>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection
