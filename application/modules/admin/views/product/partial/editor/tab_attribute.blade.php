<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="form-group">
    <div class="input-group" data-bind="attributes().length > 0">
        <select class="form-control" name="attributes"
                data-bind="value: attribute_id">
            <!--ko foreach: attributes-->
            <option data-bind="text: name, value: id, disable: $root.selected_attribute_id().includes(id)"></option>
            <!--/ko-->
        </select>
        <div class="input-group-append">
            <button class="btn btn-sm btn-primary" data-bind="click: add_attribute">
                <i class="now-ui-icons ui-1_simple-add"></i>
                Thêm
            </button>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered" data-bind="visible: selected_attributes().length > 0">
    <thead>
    <tr>
        <th style="width: 140px" class="text-left">Đặc tính</th>
        <th>Giá trị</th>
        <th width="60"></th>
    </tr>
    </thead>
    <tbody data-bind="foreach: selected_attributes" class="text-center">
    <tr>
        <td class="text-left" data-bind="text: attribute_name"></td>
        <td class="text-left">
            <select class="form-control select2" multiple
                    data-bind="selectedOptions: value_id, options: values, optionsText: 'name', optionsValue: 'id'"></select>
        </td>
        <td>
            <a href="javascript:" class="text-danger font-weight-bold" data-bind="click: $root.delete_attribute">
                <i class="icon-trash"></i>
                Xóa
            </a>
        </td>
    </tr>
    </tbody>
</table>
