<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Đặc tính sản phẩm
            <a class="float-right" href="javascript:" data-bind="click: show_attribute">
                <i class="fa fa-plus"></i>
                Thêm đặc tính
            </a>
        </div>
    </div>

    <div class="card-body" data-bind="visible: is_show_attribute()">
    @include('partial/editor/tab_attribute')
    <!--ul class="nav nav-pills nav-pills-info justify-content-center" role="tablist"
            data-bind="visible: is_show_attribute() && attributes().length > 0">
            <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#attribute_tab" role="tablist"
                   rel="billing_history">
                    Đặc tính
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#variants_tab" role="tablist">
                    Mẫu mã
                </a>
            </li>
        </ul>

        <br/>

        <div class="tab-content tab-space" data-bind="visible: is_show_attribute() && attributes().length > 0">
            <div class="tab-pane active" id="attribute_tab">

            </div>

            <div class="tab-pane" id="variants_tab">
            </div>
        </div-->
    </div>
</div>
