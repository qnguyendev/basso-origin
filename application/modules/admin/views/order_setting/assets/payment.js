function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.id = ko.observable();
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.des = ko.observable();
    self.type = ko.observable();
    self.active = ko.observable();
    self.app_key = ko.observable();
    self.app_secret = ko.observable();
    self.banks = ko.observableArray([]);

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.change_active = function (item) {
        AJAX.post('/admin/order_setting/change_active', {
            id: item.id(),
            active: item.active() == 1 ? 0 : 1
        }, true, () => {
            item.active(item.active() == 1 ? 0 : 1);
            NOTI.success('Cập nhật thành công');
        });
    };

    self.edit = function (item) {
        self.id(item.id());
        self.name(item.name());
        self.des(item.description());
        self.active(item.active());
        self.app_key(item.app_key());
        self.app_secret(item.app_secret());
        self.type(item.type());
        self.errors.showAllMessages(false);

        if (item.type() !== 'bank-transfer')
            MODAL.show('#payment-detail-modal');
        else {
            AJAX.get(window.location, {type: item.type()}, true, (res) => {
                self.banks(res.banks);
                MODAL.show('#bank_list_modal');
            });
        }
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                name: self.name(),
                des: self.des(),
                app_key: self.app_key(),
                app_secret: self.app_secret(),
                type: self.type()
            };

            if (self.type() == 'bank-transfer')
                data.bank_list = self.banks();

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    self.init();

                    NOTI.success(res.message);
                    MODAL.hide('#payment-detail-modal');
                    MODAL.hide('#bank_list_modal');
                }
            });
        }
    };

    self.remove_bank = function (item) {
        self.banks.remove(item);
    };

    self.add_bank = function () {
        self.banks.push({
            bank_name: ko.observable(),
            bank_number: ko.observable(),
            bank_account: ko.observable(),
            bank_branch: ko.observable()
        });
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));