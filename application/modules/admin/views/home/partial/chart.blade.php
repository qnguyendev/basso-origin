<?php if (!defined('BASEPATH')) exit('No direct access script allowed') ?>
<div class="row">
    <div class="col-md-6 form-group col-lg-3">
        <label>Thời gian</label>
        <select class="form-control" data-bind="value: filter.time">
            <option value="today">Hôm nay</option>
            <option value="week">Trong tuần</option>
            <option value="month">Trong tháng</option>
            <option value="all">Tất cả</option>
            <option value="custom">Tùy chọn</option>
        </select>
    </div>
    <div class="col-md-6 form-group col-lg-3">
        <label>Từ ngày</label>
        <input type="text" class="form-control" data-bind="datePicker: filter.start, enable: filter.time() == 'custom'"
               disabled="">
    </div>
    <div class="col-md-6 form-group col-lg-3">
        <label>đến ngày</label>
        <input type="text" class="form-control" data-bind="datePicker: filter.end, enable: filter.time() == 'custom'"
               disabled="">
    </div>
    <div class="col-md-6 form-group col-lg-3">
        <label>&nbsp;</label>
        <button class="btn btn-dark btn-block" data-bind="click: search">
            <i class="fa fa-search"></i>
            THÔNG KÊ
        </button>
    </div>
</div>

<div class="row">
    <div class="col-12 col-md-6">
        <!-- START card-->
        <div class="card flex-row align-items-center align-items-stretch border-0">
            <div class="col-4 d-flex align-items-center bg-warning-dark justify-content-center rounded-left">
                <em class="icon-pie-chart fa-3x"></em>
            </div>
            <div class="col-8 py-3 rounded-right">
                <div class="h2 mt-0" data-bind="text: summary.revenue"></div>
                <div class="text-uppercase">tổng Doanh thu</div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <!-- START date widget-->
        <div class="card flex-row align-items-center align-items-stretch border-0">
            <div class="col-4 d-flex align-items-center bg-success-dark justify-content-center rounded-left">
                <em class="icon-basket-loaded fa-3x"></em>
            </div>
            <div class="col-8 py-3 rounded-right">
                <div class="h2 mt-0" data-bind="text: summary.total_order"></div>
                <div class="text-uppercase">đơn hàng</div>
            </div>
        </div>
        <!-- END date widget-->
    </div>
</div>

<div class="card">
    <div class="card-body">
        <canvas id="chart" class="chartjs-render-monitor" height="400"></canvas>
    </div>
</div>