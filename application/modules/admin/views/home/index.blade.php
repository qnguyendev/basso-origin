<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-7">
            @include('partial/chart')
        </div>

        <div class="col-md-12 col-lg-5">

            <div class="card">
                <div class="card-header">
                    <div class="card-title">Đơn hàng chưa xử lý</div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="80">Mã đơn</th>
                            <th class="text-left">Khách hàng</th>
                            <th>Thời gian</th>
                            <th class="text-right" width="120">Tổng tiền</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($waiting as $item)
                            <tr>
                                <th>
                                    <a href="{{base_url('admin/order/detail/'.$item->id)}}">
                                        #{{$item->id}}
                                    </a>
                                </th>
                                <td class="text-left">{{$item->name}}</td>
                                <td>{{date('H:i d/m/Y', $item->created_time)}}</td>
                                <td class="text-right">{{format_money($item->sub_total)}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-12">
            <!-- START card-->
            <div class="card bg-purple-light pt-2 b0">
                <div class="px-2">
                    <em class="icon-pencil fa-lg float-right"></em>
                    <div class="h2 mt-0">35</div>
                    <div class="text-uppercase">Annotations</div>
                </div>
                <div data-sparkline="" data-type="line" data-width="100%" data-height="75px" data-line-color="#7266ba"
                     data-chart-range-min="0" data-fill-color="#7266ba" data-spot-color="#7266ba"
                     data-min-spot-color="#7266ba" data-max-spot-color="#7266ba" data-highlight-spot-color="#7266ba"
                     data-highlight-line-color="#7266ba" data-values="1,3,4,5,7,8" style="margin-bottom: -2px"
                     data-resize="true">
                    <canvas width="420" height="75"
                            style="display: inline-block; width: 420.75px; height: 75px; vertical-align: top;"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/assets/vendor/chart.js/dist/Chart.js"></script>
    <script src="{{load_js('report')}}"></script>
@endsection