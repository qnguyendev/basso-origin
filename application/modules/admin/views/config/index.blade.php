<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $theme_config;
$active_post = false;
$active_product = false;

foreach ($theme_config['term'] as $term) {
    if ($term['id'] == 'post')
        $active_post = $term['active'];
    else if ($term['id'] == 'product')
        $active_product = $term['active'];
}
?>
@layout('cpanel_layout')
@section('content')
    <form method="post" enctype="multipart/form-data">
        <div class="card card-default">
            <div class="card-header">
                <div class="card-title">
                    <a href="#" class="float-right" onclick="formSubmit()">
                        <i class="fa fa-check"></i>
                        Cập nhật
                    </a>
                    Cấu hình Website
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-4">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" class="form-control filestyle" name="logo"
                                   accept="image/jpg, image/jpeg, image/png" style="z-index: -1;">
                            <?php if ($logo != null) : ?>
                            <div class="card card-product">
                                <div class="card-img text-center" style="padding: 10px 0;">
                                    <img src="{{$logo}}" style="max-width: 100%; height: auto"/>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label>Favourite icon</label>
                            <input type="file" class="form-control filestyle" name="fav_icon"
                                   accept="image/jpg, image/jpeg, image/png" style="z-index: -1;">
                            <?php if ($fav_icon != null) : ?>
                            <div class="card card-product">
                                <div class="card-img text-center" style="padding: 10px 0;">
                                    <img src="{{$fav_icon}}" style="max-width: 100%; height: auto"/>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Facebook</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        https://www.facebook.com/
                                    </div>
                                </div>
                                <input type="text" class="form-control" name="facebook" value="{{$facebook}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Twitter</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        https://twitter.com/
                                    </div>
                                </div>
                                <input type="text" class="form-control" name="twitter" value=""{{$twitter}}/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Instagram</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        https://instagram.com/
                                    </div>
                                </div>
                                <input type="text" class="form-control" name="instagram" value="{{$instagram}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Google+</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        https://plus.google.com/
                                    </div>
                                </div>
                                <input type="text" class="form-control" name="google_plus" value="{{$google_plus}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Pinterest</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        https://www.pinterest.com/
                                    </div>
                                </div>
                                <input type="text" class="form-control" name="pinterest" value="{{$pinterest}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Pinterest</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        https://www.youtube.com/
                                    </div>
                                </div>
                                <input type="text" class="form-control" name="youtube" value="{{$youtube}}"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="card-body">
                        @if($active_product)
                            <div class="form-group">
                                <label>Sản phẩm hiển thị mỗi trang</label>
                                <input type="number" min="0" name="products_per_page" value="{{$products_per_page}}"
                                       max="100" step="1" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Sản phẩm liên quan</label>
                                <input type="number" min="0" name="related_products" value="{{$related_products}}"
                                       max="100" step="1" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <div class="checkbox c-checkbox">
                                    <label>
                                        <input class="form-check-input" type="checkbox"
                                               {{boolval($redirect_to_cart) ? 'checked' : ''}}
                                               name="redirect_to_cart" value="true"/>
                                        <span class="fa fa-check"></span>
                                        Chuyển đến giỏ hàng sau khi thêm sản phẩm
                                    </label>
                                </div>
                            </div>
                        @endif
                        @if($active_post)
                            <div class="form-group">
                                <label>Bài viết hiển thị mỗi trang</label>
                                <input type="number" min="0" name="articles_per_page" value="{{$articles_per_page}}"
                                       max="100" step="1" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Bài viết liên quan</label>
                                <input type="number" min="0" name="related_articles" value="{{$related_articles}}"
                                       max="100" step="1" class="form-control"/>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('script')
    <script>
        function formSubmit() {
            $('form[method=post]').submit();
        }
    </script>
@endsection