function viewModel() {
    let self = this;

    self.result = ko.mapping.fromJS([]);
    self.roles = ko.mapping.fromJS([]);

    self.id = ko.observable(0);
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.email = ko.observable().extend({
        required: {message: LABEL.required},
        email: {message: LABEL.invalid_email}
    });
    self.pass = ko.observable().extend({
        required: {
            onlyIf: function () {
                return self.id() == 0;
            },
            message: LABEL.required
        }
    });
    self.roles = ko.observableArray([]);
    self.active = ko.observable(1);
    self.phone = ko.observable();

    self.init = function () {
        AJAX.get(window.location, {init: true}, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.create = function () {
        self.id(0);
        self.name(undefined);
        self.email(undefined);
        self.pass(undefined);
        self.errors.showAllMessages(false);
        MODAL.show('#editor_modal');
    };

    self.edit = function (item) {
        AJAX.get(window.location, {id: item.id()}, true, (res) => {
            self.id(res.data.id);
            self.name(res.data.name);
            self.email(res.data.email);
            self.roles(res.data.roles);
            $('select[multiple]').val(res.data.roles).trigger('change');
            self.active(res.data.active);
            self.phone(res.data.phone);

            self.errors.showAllMessages(false);
            MODAL.show('#editor_modal');
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa tài khoản', null, () => {
            AJAX.post('/admin/user/delete', {id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    ALERT.success(res.message);
                    self.init();
                }
            });
        });
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                name: self.name(),
                email: self.email(),
                pass: self.pass(),
                roles: self.roles(),
                active: self.active(),
                phone: self.phone()
            };

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    ALERT.success(res.message);
                    MODAL.hide('#editor_modal');
                    self.init();
                }
            });
        }
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));