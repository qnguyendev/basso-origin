<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card card-default">
    <div class="card-header">
        <h6 class="card-title">Chú thích</h6>
    </div>
    <div class="card-body">
        <div class="accordion" id="accordionExample">
            <div class="card card-default mb-1">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a href="#" class="text-inherit" data-toggle="collapse" data-target="#collapseOne"
                           aria-expanded="true" aria-controls="collapseOne">
                            Quản trị viên
                        </a>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                     data-parent="#accordionExample">
                    <div class="card-body">
                        Phân quyền cao nhất, sử dụng đầy đủ mọi chức năng của hệ thống
                    </div>
                </div>
            </div>
            <div class="card card-default mb-1">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <a class="text-inherit" href="#" data-toggle="collapse"
                           data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Quản lý sản phẩm
                        </a>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <ol>
                            <li>Danh sách sản phẩm</li>
                            <li>Thêm mới/cập nhật/xóa sản phẩm</li>
                            <li>Quản lý thương hiệu</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card card-default mb-1">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <a href="#" class="text-inherit" data-toggle="collapse"
                           data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Quản lý đơn hàng
                        </a>
                    </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        Quản lý đơn hàng, xác nhận thanh toán, cập nhật đơn hàng...
                    </div>
                </div>
            </div>
            <div class="card card-default mb-1">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <a href="#" class="text-inherit" data-toggle="collapse"
                           data-target="#collapseFouth" aria-expanded="false" aria-controls="collapseFouth">
                            Quản lý nội dung
                        </a>
                    </h5>
                </div>
                <div id="collapseFouth" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <dd>
                            Danh mục:
                            <ol>
                                <li>Danh sách danh mục</li>
                                <li>Cập nhật mô tả, thông tin SEO</li>
                                <li>Thêm, sửa danh mục bài viết</li>
                            </ol>
                        </dd>
                        <dd>
                            Bài viết:
                            <ol>
                                <li>Danh sách bài viết</li>
                                <li>Thêm, sửa, xóa bài viết</li>
                                <li>Đánh dấu bài nổi bật</li>
                                <li>Cập nhật mô tả, thông tin SEO</li>
                            </ol>
                        </dd>
                    </div>
                </div>
            </div>
			
			<div class="card card-default mb-1">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <a href="#" class="text-inherit" data-toggle="collapse"
                           data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Quản lý kho
                        </a>
                    </h5>
                </div>
                <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <dd>
                            Danh mục:
                            <ol>
                                <li>Quản lý kho</li>
                                <li>Quản lý vận chuyển</li>
                            </ol>
                        </dd>
                    </div>
                </div>
            </div>
			
			<div class="card card-default mb-1">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <a href="#" class="text-inherit" data-toggle="collapse"
                           data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Kế toán
                        </a>
                    </h5>
                </div>
                <div id="collapseSix" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <dd>
                            Danh mục:
                            <ol>
                                <li>Kế toán</li>
                                <li>Quản đơn đặt hàng</li>
                                <li>Quản lý đơn admin</li>
                            </ol>
                        </dd>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
