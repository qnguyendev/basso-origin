<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="editor_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    data-bind="text: id() == 0 ? 'Thêm slide' : 'Cập nhật slide'"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="form-group">
                    <label>
                        Tên ghi nhớ
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: name"/>
                </div>
                <div class="form-group">
                    <label>Tiêu đề nội dung</label>
                    <input type="text" class="form-control" data-bind="value: title"/>
                </div>
                <div class="form-group">
                    <label>Nội dung trên slide</label>
                    <textarea type="text" class="form-control" data-bind="value: content" rows="5" cols="10"></textarea>
                </div>
                <div class="form-group">
                    <label>Thể loại</label>
                    <select class="form-control" data-bind="value: type, enable: id() === 0">
                        <option value="image">Ảnh</option>
                        <option value="youtube">Video youtube</option>
                    </select>
                </div>
                <div class="form-group" data-bind="visible: type() == 'youtube'">
                    <label>
                        ID video
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: youtube_id"></span>
                    </label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                https://www.youtube.com/watch?v=
                            </div>
                        </div>
                        <input type="text" class="form-control" data-bind="value: youtube_id"
                               placeholder="VH3mWd28Ndg"/>
                    </div>
                </div>
                <div class="form-group" data-bind="visible: type() == 'image'">
                    @include('partial/single_image_upload')
                </div>
                <div class="row">
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Link cần chuyển</label>
                        <input type="text" class="form-control" data-bind="value: redirect_url"/>
                    </div>
                    <div class="form-group col-md-12 col-lg-6">
                        <label>Tiêu đề button</label>
                        <input type="text" class="form-control" data-bind="value: button_text"/>
                    </div>
                </div>
            </form>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: $root.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>