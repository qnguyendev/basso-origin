<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
?><!DOCTYPE html>
<html lang="en">
@include('backend/head')
<body id="reset-form">
<div class="wrapper">
    <div class="block-center mt-4 wd-xl">
        <!-- START card-->
        <div class="card card-flat">
            <div class="card-header text-center bg-dark">
                <a href="#">
                    <img class="block-center rounded" src="/assets/img/logo.png" alt="Image">
                </a>
            </div>
            <div class="card-body">
                <p class="text-center">
                    Nhập địa chỉ email đăng nhập của bạn, chúng tôi sẽ gửi cho bạn mật khẩu mới.
                </p>
                <div class="form-group">
                    <div class="input-group with-focus">
                        <input class="form-control border-right-0" data-bind="value: email" type="email"
                               placeholder="Email đăng nhập" autocomplete="off">
                        <div class="input-group-append">
                           <span class="input-group-text text-muted bg-transparent border-left-0">
                              <em class="fa fa-envelope"></em>
                           </span>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-block text-uppercase" data-bind="click: reset">Khôi phục</button>
            </div>
            <div class="card-footer">
                <a href="{{base_url('admin/auth/login')}}">Quay lại đăng nhập</a>
            </div>
        </div>
    </div>
</div>
@include('backend/loader')
</body>
</html>
<script src="/assets/vendor/modernizr/modernizr.custom.js"></script>
<script src="/assets/vendor/jquery/dist/jquery.js"></script>
<script src="/assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/assets/vendor/moment/min/moment-with-locales.js"></script>
<script src='/assets/js/core/knockout-3.4.2.js'></script>
<script src='/assets/js/core//knockout.validation.min.js'></script>
<script src='/assets/js/resources/string.js'></script>
<script src="/assets/vendor/sweetalert/dist/sweetalert.min.js"></script>
<script src="/assets/vendor/hullabaloo.js"></script>
<script src='/assets/js/resources/functions.js'></script>
<script src='/assets/js/core/ko.extends.js'></script>
<script src="/assets/js/reset.js"></script>