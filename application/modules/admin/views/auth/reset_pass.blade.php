<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$error_msg = $this->session->flashdata('error_msg');
?><!DOCTYPE html>
<html lang="en">
@include('backend/head')
<body id="reset-pass-form">
<div class="wrapper">
    <div class="block-center mt-4 wd-xl">
        <!-- START card-->
        <div class="card card-flat">
            <div class="card-header text-center bg-dark">
                <a href="#">
                    <img class="block-center rounded" src="/assets/img/logo.png" alt="Image">
                </a>
            </div>
            <div class="card-body">
                @if(isset($error_msg))
                    <div class="alert alert-warning">{{$error_msg}}</div>
                @endif
                <form class="mb-3" method="post" autocomplete="off">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" disabled value="{{$email}}" class="form-control" name="email"/>
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu mới</label>
                        <input type="password" name="pass" required class="form-control" autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <label>Xác nhận mật khẩu</label>
                        <input type="password" name="re_pass" required class="form-control"/>
                    </div>
                    <div class="clearfix">
                        <div class="float-right">
                            <a class="text-muted" href="{{base_url('admin/auth/login')}}">Đăng nhập?</a>
                        </div>
                    </div>
                    <input type="hidden" value="{{$email}}" name="email"/>
                    <input type="hidden" value="{{$token}}" name="token"/>
                    <button data-bind="click: login" class="btn btn-block btn-info mt-3">Đổi mật khẩu</button>
                </form>
            </div>
        </div>
    </div>
</div>
@include('backend/loader')
</body>
</html>