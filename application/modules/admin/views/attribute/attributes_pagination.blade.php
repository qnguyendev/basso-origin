<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<br/>
<div class="row">
    <div data-bind="with: attributes().pagination, visible: attributes().result().length > 0" class="col-12">
        <ul class="pagination pull-left" style="margin: 0 0 0 3px">
            <li data-bind="disable: current_page == 1" class="page-item">
                <a href="javascript:" class="page-link"
                   data-bind="click: function() {$root.attributes().init(current_page - 1)}, visible: current_page > 1">
                    <span aria-hidden="true"><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>
                    Trước
                </a>
            </li>
            <li data-bind="disable: current_page == total_page" class="page-item">
                <a href="javascript:" class="page-link"
                   data-bind="click: function() {$root.attributes().init(current_page + 1)}, visible: current_page < total_page">
                    Sau
                    <span aria-hidden="true"><i class="fa fa-angle-double-right" aria-hidden="true"></i></span>
                </a>
            </li>
        </ul>

        <div class="pull-right">
            Trang số
            <input type="number" class="text-center" pattern="[0-9]{1,3}" style="width: 60px"
                   data-bind="value: current_page, attr: {min: 1, max: total_page}, event: { change: function() { $root.attributes().init(current_page) } }"
                   min="1" max="460">
            trên <span data-bind="text: total_page"></span> trang. Tổng <span data-bind="text: total_item"></span>
            kết quả.
        </div>
    </div>
</div>