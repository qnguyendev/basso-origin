<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="tab-pane" id="tab_customer_info" role="tabpanel">
    <div class="row">
        <div class="form-group col-sm-6">
            <label>
                Họ tên khách hàng
                <span class="text-danger">*</span>
                <span class="validationMessage"
                      data-bind="validationMessage: customer().name"></span>
            </label>
            <input type="text" class="form-control"
                   data-bind="value: customer().name"/>
        </div>
        <div class="form-group col-sm-6">
            <label>
                Số điện thoại
                <span class="text-danger">*</span>
                <span class="validationMessage"
                      data-bind="validationMessage: customer().phone"></span>
            </label>
            <input type="text" class="form-control"
                   data-bind="value: customer().phone"/>
        </div>
        <div class="form-group col-sm-6">
            <label>
                Email
                <span class="text-danger">*</span>
                <span class="validationMessage"
                      data-bind="validationMessage: customer().email"></span>
            </label>
            <input type="text" class="form-control"
                   data-bind="value: customer().email"/>
        </div>
        <div class="form-group col-sm-6">
            <label>
                Địa chỉ
                <span class="text-danger">*</span>
                <span class="validationMessage"
                      data-bind="validationMessage: customer().address"></span>
            </label>
            <input type="text" class="form-control"
                   data-bind="value: customer().address"/>
        </div>
        <div class="form-group col-sm-6">
            <label>
                Tỉnh/thành
                <span class="text-danger">*</span>
                <span class="validationMessage"
                      data-bind="validationMessage: customer().name"></span>
            </label>
            <select class="form-control select2" name="cities" style="width: 100%"
                    data-bind="value: customer().city_id, event: {change: modal().change_city}">
                @foreach($cities as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-sm-6">
            <label>
                Quận/huyện
                <span class="text-danger">*</span>
                <span class="validationMessage"
                      data-bind="validationMessage: customer().name"></span>
            </label>
            <select class="form-control select2" style="width: 100%"
                    data-bind="options: modal().districts, optionsText: 'name',
                                                    optionsValue: 'id', value: customer().district_id">
            </select>
        </div>
        <div class="form-group col-sm-6">
            <label>
                Phương thức thanh toán
                <span class="text-danger">*</span>
                <span class="validationMessage"
                      data-bind="validationMessage: customer().payment_id"></span>
            </label>
            <select class="form-control" name="payment_method"
                    data-bind="value: customer().payment_id">
                @foreach($payment_method as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-sm-6">
            <label>Ghi chú đơn hàng</label>
            <input type="text" class="form-control"
                   data-bind="value: customer().note"/>
        </div>
    </div>
</div>
