<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="add_product_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Thêm sản phẩm
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" data-bind="value: filter_product"
                               placeholder="Nhập mã hoặc tên sản phẩm"/>
                        <div class="input-group-append">
                            <button class="btn btn-primary" data-bind="click: $root.search_product">
                                <i class="fa fa-search"></i>
                                Tìm
                            </button>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered" data-bind="visible: filter_result().length > 0">
                    <thead>
                    <tr>
                        <th width="80"></th>
                        <th class="text-left">Sản phẩm</th>
                        <th class="text-right" width="90">Giá bán</th>
                        <th width="110"></th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: filter_result" class="text-center">
                    <tr>
                        <td>
                            <img data-bind="attr: {src: '/timthumb.php?src=' + image() + '&w=50&h=50'}"/>
                        </td>
                        <td class="text-left p-3">
                            <span data-bind="text: name"></span>
                            <br/>
                            <!--ko foreach: attributes-->
                            <select data-bind="value: value_id, options: values, optionsText: 'name', optionsValue: 'id'">
                            </select>
                            <!--/ko-->
                        </td>
                        <td class="text-right" data-bind="text: parseInt(price()).toMoney(0)"></td>
                        <td>
                            <a href="#" class="text-success font-weight-bold" data-bind="click: $root.add_product">
                                <i class="icon-plus"></i>
                                Thêm
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>