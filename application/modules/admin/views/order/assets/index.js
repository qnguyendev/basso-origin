function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.filter_time = ko.observable('all');
    self.filter_date = ko.observable(moment(new Date()).format('YYYY-MM-DD'));
    self.filter_status = ko.observable('all');
    self.filter_key = ko.observable();
    self.filter_labels = ko.observableArray([]);

    self.init = function () {
        self.search(1);
        self.modal().change_city();

        let payment_id = $('select[name=payment_method] option:first').val();
        self.customer().payment_id(payment_id);

        $('#create_order_modal').on('hidden.bs.modal', function () {
            self.modal().filter_product(undefined);
            self.modal().sub_total(0);
            self.modal().discount_total(0);
            ko.mapping.fromJS([], self.modal().filter_result);
            ko.mapping.fromJS([], self.modal().products);

            self.customer().name(undefined);
            self.customer().phone(undefined);
            self.customer().email(undefined);
            self.customer().address(undefined);
            self.customer().note(undefined);
        });
    };

    self.search = function (page) {
        let data = {
            page: page,
            time: self.filter_time(),
            date: self.filter_date(),
            status: self.filter_status(),
            key: self.filter_key(),
            labels: self.filter_labels()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.modal = ko.observable({
        filter_product: ko.observable(),
        filter_result: ko.mapping.fromJS([]),
        products: ko.mapping.fromJS([]),
        sub_total: ko.observable(),   //  Tổng đon hàng cần thanh toán
        discount_total: ko.observable(0),
        cities: ko.mapping.fromJS([]),
        districts: ko.mapping.fromJS([]),
        search: function () {
            if (self.modal().filter_product() === undefined || self.modal().filter_product() === null) {
                ko.mapping.fromJS([], self.modal().filter_result);
                NOTI.danger('Vui lòng nhập mã hoặc tên sản phẩm');
                return;
            }

            if (self.modal().filter_product().length === 0) {
                ko.mapping.fromJS([], self.modal().filter_result);
                NOTI.danger('Vui lòng nhập mã hoặc tên sản phẩm');
                return;
            }

            AJAX.get(window.location, {filter_key: self.modal().filter_product()}, true, (res) => {
                ko.mapping.fromJS(res.data, self.modal().filter_result);
            });
        },
        re_calc_detail: function (item) {
            if (item != undefined)
                item.sub_total(item.quantity() * item.price());

            let sub_total = 0, discount_total = parseInt(self.modal().discount_total());
            for (let i = 0; i < self.modal().products().length; i++)
                sub_total += parseInt(self.modal().products()[i].sub_total());

            sub_total -= discount_total;
            self.modal().sub_total(sub_total < 0 ? 0 : sub_total);
        },
        add_product: function (item) {
            let in_list = false;
            for (let i = 0; i < self.modal().products().length; i++) {
                if (item.id() == self.modal().products()[i].id()) {
                    let prod_att = [];  //  List giá trị thuộc tính sản phẩm trong đơn hàng
                    let new_att = [];   //  List giá trị thuộc tính sản phẩm được thêm vào
                    let prod = self.modal().products()[i];

                    for (let j = 0; j < prod.attributes().length; j++)
                        prod_att.push(prod.attributes()[j].value_id());

                    for (let j = 0; j < item.attributes().length; j++)
                        new_att.push(item.attributes()[j].value_id());

                    let filter_result = prod_att.filter(function (n) {
                        return new_att.indexOf(n) >= 0;
                    });

                    in_list = filter_result.length == prod_att.length;
                    if (in_list)
                        break;
                }
            }

            if (!in_list) {
                let prod = {
                    id: ko.observable(item.id()),
                    name: ko.observable(item.name()),
                    price: ko.observable(item.price()),
                    quantity: ko.observable(1),
                    image: ko.observable(item.image()),
                    total: ko.observable(0),
                    sub_total: ko.observable(item.price()),
                    attributes: ko.observableArray([]),
                    data: []
                };

                //  Lấy tên thuộc tính, tên giá trị thuộc tính
                for (let i = 0; i < item.attributes().length; i++) {
                    for (let j = 0; j < item.attributes()[i].values().length; j++) {
                        if (item.attributes()[i].values()[j].id() == item.attributes()[i].value_id()) {
                            prod.attributes.push({
                                name: ko.observable(item.attributes()[i].name()),
                                value: ko.observable(item.attributes()[i].values()[j].name()),
                                attribute_id: ko.observable(item.attributes()[i].attribute_id()),
                                value_id: ko.observable(item.attributes()[i].value_id())
                            });
                            break;
                        }
                    }
                }

                self.modal().products.push(prod);

                $('#headingOne').click();
            } else
                NOTI.danger('Sản phẩm đã có trong đơn hàng');

            self.modal().re_calc_detail(undefined);
        },
        delete_product: function (item) {
            self.modal().products.remove(item);
            self.modal().re_calc_detail(item);
        },
        change_city: function () {
            if (self.customer().city_id() == null || self.customer().city_id() == undefined) {
                let city_id = $('select[name=cities] option:first').val();
                self.customer().city_id(city_id);
            }

            AJAX.get(window.location, {city_id: self.customer().city_id()}, false, (res) => {
                ko.mapping.fromJS(res.data, self.modal().districts);
            });
        },
        save: function () {
            if (!self.customer().isValid())
                self.customer().errors.showAllMessages();
            else {
                if (self.modal().products().length === 0) {
                    NOTI.danger('Bạn chưa chọn sản phẩm');
                } else {
                    //  Cập nhật danh sách thuộc tính của sản phẩm
                    for (let i = 0; i < self.modal().products().length; i++) {

                        let attributes = [];
                        for (let j = 0; j < self.modal().products()[i].attributes().length; j++) {
                            attributes.push({
                                attribute_id: self.modal().products()[i].attributes()[j].attribute_id(),
                                attribute_value_id: self.modal().products()[i].attributes()[j].value_id()
                            })
                        }
                        //  Lưu giá trị thuộc tính cho sản phẩm
                        self.modal().products()[i].data = attributes;
                    }

                    let data = {
                        discount: self.modal().discount_total(),
                        products: self.modal().products(),
                        customer: self.customer()
                    };

                    AJAX.post(window.location, data, true, (res) => {
                        if (res.error)
                            NOTI.danger(res.message);
                        else {
                            NOTI.danger(res.message);
                            MODAL.hide('#create_order_modal');
                            self.search(1);
                        }
                    });
                }
            }
        }
    });

    self.customer = ko.observable({
        name: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        phone: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        email: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        address: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        note: ko.observable(),
        city_id: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        district_id: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        payment_id: ko.observable().extend({
            required: {message: LABEL.required}
        })
    });
}

let model = new viewModel();
model.init();
ko.validatedObservable(model.customer());
ko.applyBindings(model, document.getElementById('main-content'));