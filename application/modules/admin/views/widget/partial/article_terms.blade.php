<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="row">
    <div class="form-group col-12 col-sm-6">
        <label>Sắp xếp</label>
        <select class="form-control" data-bind="value: article_terms().order">
            <option value="asc">Tên tăng dần</option>
            <option value="desc">Tên giảm dần</option>
        </select>
    </div>
    <div class="form-group col-12 col-sm-6">
        <label>Hiển thị số bài</label>
        <select class="form-control" data-bind="value: article_terms().show_count">
            <option value="0">Không</option>
            <option value="1">Có</option>
        </select>
    </div>
</div>