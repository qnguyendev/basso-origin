<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="form-group">
    <label>Mô tả widget (hiển thị dưới tiêu đề)</label>
    <input type="text" class="form-control" data-bind="value: products().des"/>
</div>
<div class="row">
    <div class="form-group col-12 col-sm-6">
        <label>Tùy chọn</label>
        <select class="form-control" data-bind="value: products().order">
            <option value="newest">Sản phẩm mới</option>
            <option value="featured">Sản phẩm nổi bật</option>
            <option value="sale">Sản phẩm giảm giá</option>
            <option value="random">Sản phẩm ngẫu nhiên</option>
        </select>
    </div>
    <div class="form-group col-12 col-sm-6">
        <label>Số lượng sản phẩm</label>
        <input type="number" min="1" class="form-control" value="1" data-bind="value: products().limit"/>
    </div>
</div>