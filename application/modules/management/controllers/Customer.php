<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Customer
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Customer_group_model $customer_group_model
 * @property Common_model $common_model
 * @property Customer_model $customer_model
 * @property Payment_history_model $payment_history_model
 * @property Customer_order_model $customer_order_model
 * @property Excel $excel
 * @property Order_term_model $order_term_model
 * @property Country_model $country_model
 */
class Customer extends Cpanel_Controller
{
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('customer_group_model');
        $this->load->model('customer_model');
        $this->load->model('common_model');
        $this->load->model('basso/payment_history_model');
        $this->load->model('basso/customer_order_model');
        $this->load->model('basso/contact_form_model');
    }

    #region Khách hàng
    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Danh sách khàng']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('action', null, 'required');
        if ($this->form_validation->run() == false)
            json_error(' Yêu cầu không hợp lệ');

        switch ($this->input->get('action')) {
            case 'init':
                $cities = $this->common_model->get_cities();
                $groups = $this->customer_group_model->get();
                json_success(null, ['cities' => $cities, 'groups' => $groups]);
                break;

            case 'search':
                $page = $this->input->get('page');
                $total_paid = $this->input->get('total_paid');
                $group_id = $this->input->get('group_id');
                $key = $this->input->get('key');
				$from = $this->input->get('start');
				$to = $this->input->get('end');
                $start = ($from != null && $from !="")? strtotime("$from 00:00:00"):0;
                $end = ($to != null && $to !="")? strtotime("$to 23:59:59"):0;
				
				/* fixcode only show customer orders of current staff */
				global $current_user, $user_roles;
				$staff_id = null;
				if(in_array("order_manager", $user_roles) && !in_array("admin", $user_roles)){
					$staff_id = $current_user->user_id;
				}
				/* end fixcode */
				
                $result = $this->customer_model->get_in_duration($page, $total_paid, $group_id,$start,$end, $key, $staff_id);
                $total = $this->customer_model->count_in_duration($total_paid, $group_id,$start,$end, $key, $staff_id);

                json_success(null, ['data' => $result, 'pagination' => Pagination::calc($total, $page)]);
                break;

            case 'change-city':
                $id = $this->input->get('id');
                $districts = $this->common_model->get_districts($id);
                array_unshift($districts, [
                    'id' => null,
                    'city_id' => $id,
                    'name' => '- Lựa chọn -'
                ]);

                json_success(null, ['data' => $districts]);
                break;

            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $action = $this->input->post('action');
        switch ($action) {
            case 'add':
                $this->form_validation->set_rules('name', null, 'required');
                $this->form_validation->set_rules('phone', null, 'required');
                $this->form_validation->set_rules('gender', null, 'required');
                if (!$this->form_validation->run())
                    json_error('Vui lòng nhập đủ thông tin');

                $name = $this->input->post('name');
                $phone = $this->input->post('phone');
                $email = strtolower(trim($this->input->post('email')));
                $gender = $this->input->post('gender');
                $address = $this->input->post('address');
                $city_id = $this->input->post('city_id');
                $district_id = is_enull($this->input->post('district_id'), null, 'numeric');
                $note = $this->input->post('note');
                $group_id = $this->input->post('group_id');

                if ($email != null) {
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                        json_error('Email không hợp lệ');

                    if ($this->ion_auth->email_check($email) > 0)
                        json_error('Email đã tồn tại');

                } else
                    $email = md5(uniqid()) . '@no_email.com';

                $phone = str_replace(' ', '', $phone);
                $customer = $this->customer_model->get_by_phone($phone);
                if ($customer != null)
                    json_error('Số điện thoại đã có trong hệ thống');

                $result = $this->ion_auth->register($email, '123456', $email, [
                    'first_name' => $name,
                    'phone' => $phone
                ], [6]);

                if (!$result)
                    json_error('Có lỗi, vui lòng F5 thử lại');
				
				
                $this->customer_model->update_info($result, [
                    'gender' => $gender,
                    'address' => $address,
                    'city_id' => $city_id,
                    'district_id' => $district_id,
                    'note' => $note,
                    'customer_group_id' => $group_id
                ]);
				
				$customer_info = [
					'id' =>  $result,
                    'gender' => $gender,
                    'address' => $address,
                    'city_id' => $city_id,
                    'district_id' => $district_id,
                    'note' => $note,
                    'customer_group_id' => $group_id,
					'email' => $email,
					'name' => $name,
					'phone' => $phone
				];
				
                json_success('Thêm khách hàng thành công',['data'=> $customer_info]);
                break;
            case 'import':
                $data = $this->input->post('data');
                if (!is_array($data))
                    json_error('ERROR');

                if (count($data) < 3)
                    json_error("$data[0] | ERROR");

                $name = $data[0];
                $phone = strval($data[1]);
                $address = $data[2];

                if (substr($phone, 0, 1) != '0')
                    $phone = "0$phone";

                $phone = str_replace(' ', '', $phone);
                $customer = $this->customer_model->get_by_phone($phone);
                if ($customer != null)
                    json_error("$name|$phone|Đã có trong hệ thống");

                $email = md5(uniqid()) . '@no_email.com';
                $result = $this->ion_auth->register($email, '123456', $email, [
                    'first_name' => $name,
                    'phone' => $phone
                ], [6]);

                $this->customer_model->update_info($result, ['address' => $address]);
                json_success("$name|$phone|SUCCESS");
                break;
			
			case 'export':
                $page = $this->input->post('page');
                $total_paid = $this->input->post('total_paid');
                $group_id = $this->input->post('group_id');
                $key = $this->input->post('key');
				$from = $this->input->post('start');
				$to = $this->input->post('end');
                $start = ($from != null && $from !="")? strtotime("$from 00:00:00"):0;
                $end = ($to != null && $to !="")? strtotime("$to 23:59:59"):0;
                $result = $this->customer_model->get_in_duration($page, $total_paid, $group_id,$start,$end, $key);

				$this->session->set_flashdata('export_list', $result);
                json_success(null, ['redirect_url' => '/management/customer/export_list']);
				break;
			
            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }
    #endregion

    #region Nhóm khách hàng
    public function group()
    {
        $this->blade->set('breadcrumbs', [
            ['text' => 'Khách hàng', 'url' => base_url('management/customer')],
            ['text' => 'Quản lý nhóm']
        ]);
        return $this->blade->render();
    }

    public function GET_group()
    {
        json_success(null, ['data' => $this->customer_group_model->get()]);
    }

    public function DELETE_group()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $group = $this->customer_group_model->get($id);
        if ($group == null)
            json_error('Không tìm thấy nhóm khách hàng');

        if ($this->customer_group_model->delete($id))
            json_success('Xóa nhóm thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_group()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập thông tin yêu cầu');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $des = $this->input->post('des');

        if ($this->customer_group_model->insert($id, $name, $des))
            json_success($id == 0 ? 'Thêm nhóm thành cônng' : 'Cập nhật thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }
    #endregion

    #region Chi tiết khách hàng
    public function detail($id)
    {
        if (!is_numeric($id)) {
            error_msg('Không tìm thấy khách hàng');
            return redirect(base_url('management/customer'));
        }

        $cus = $this->customer_model->get_by_id($id);
		
		if ($cus == null) {
            error_msg('Không tìm thấy khách hàng');
            return redirect(base_url('management/customer'));
        }

        $this->blade->set('customer', $cus);
        $this->blade->set('payment_histories', $this->payment_history_model->get_by_customer(1, $id));

        $orders = $this->customer_order_model->get_orders_by_customer(0, $id);
		
		/* fixcode */
		if(count($orders))
			$items = $this->customer_order_model->get_items(array_column($orders, 'id'));
		else $items = null;
		
		if($items != null){
			for ($i = 0; $i < count($items); $i++) {
				$items[$i]->variations = json_decode($items[$i]->variations);
			}
		}

        $this->blade->set('orders', $orders);
        $this->blade->set('order_items', $items);
        $this->blade->set('breadcrumbs', [['text' => 'Chi tiết khách hàng']]);

        return $this->blade->render();
    }

    public function GET_detail($id)
    {
        $action = $this->input->get('action');
        if ($action == null)
            json_error('Yêu cầu không hợp lệ');

        switch ($action) {
            case 'init':
                $customer = $this->customer_model->get_by_id($id);
                if (strpos($customer->email, '@no_email.com') !== false)
                    $customer->email = null;

                $cities = $this->common_model->get_cities();
                $groups = $this->customer_group_model->get();

                if ($customer->city_id != null)
                    $districts = $this->common_model->get_districts($customer->city_id);
                else
                    $districts = $this->common_model->get_districts($cities[0]->id);

                json_success(null, [
                    'cities' => $cities,
                    'districts' => $districts,
                    'groups' => $groups,
                    'customer' => $customer
                ]);
                break;
            case 'change-city':
                $id = $this->input->get('id');
                $districts = $this->common_model->get_districts($id);
                json_success(null, ['data' => $districts]);
                break;
            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function POST_detail($id)
    {
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $action = $this->input->post('action');
        switch ($action) {
            case 'update-customer':
                $this->form_validation->set_rules('name', null, 'required');
                $this->form_validation->set_rules('phone', null, 'required');
                $this->form_validation->set_rules('gender', null, 'required');
                if (!$this->form_validation->run())
                    json_error('Vui lòng nhập đủ thông tin');

                $name = $this->input->post('name');
                $phone = $this->input->post('phone');
                $gender = $this->input->post('gender');
                $address = $this->input->post('address');
                $city_id = $this->input->post('city_id');
                $district_id = is_enull($this->input->post('district_id'), null, 'numeric');
                $note = $this->input->post('note');
                $group_id = $this->input->post('group_id');
                $email = strtolower(trim($this->input->post('email')));

                $result = $this->ion_auth->update($id, [
                    'first_name' => $name,
                    'phone' => $phone
                ]);

                if (!$result)
                    json_error('Có lỗi, vui lòng F5 thử lại');

                $update_data = [
                    'gender' => $gender,
                    'address' => $address,
                    'city_id' => $city_id,
                    'district_id' => $district_id,
                    'note' => $note,
                    'customer_group_id' => $group_id
                ];
                if (isset($email)) {
                    if ($email != null) {
                        if (!empty($email)) {
                            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                                $customer = $this->customer_model->get_by_email($email);
                                if (isset($customer) && $customer != null) {
                                    if ($customer->id>0 && $customer->id != $id) {
                                        json_error('Địa chỉ email đã có trong hệ thống');
                                    }
                                }

                                $this->ion_auth->update($id, [
                                    'email' => $email
                                ]);
                            }
                            else
                                json_error('Địa chỉ email không hợp lệ');
                        }
                    }
                }

                $this->customer_model->update_info($id, $update_data);

                success_msg('Cập nhật thông tin khách hàng thành công');
                json_success(null);
                break;
            case 'export':
                $orders = $orders = $this->customer_order_model->get_orders_by_customer(0, $id);
                if (count($orders) == 0)
                    json_error('Khách hàng chưa phát sinh đơn hàng');

                $items = $this->customer_order_model->get_items(array_column($orders, 'id'));
                for ($i = 0; $i < count($items); $i++) {
                    $items[$i]->variations = json_decode($items[$i]->variations);
                }

                $payments = $this->payment_history_model->get_by_customer(0, $id);
                $customer = $this->customer_model->get_by_id($id);

                $data = [
                    'customer' => $customer,
                    'items' => $items,
                    'orders' => $orders,
                    'payments' => $payments
                ];

                $this->session->set_flashdata('export', $data);
                json_success(null, ['redirect_url' => '/management/customer/export']);
                break;
			
            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function GET_DELETE_detail($id) /* fixcode DELETE_detail  to  GET_DELETE_detail */
    {
        $this->ion_auth->delete_user($id);
        success_msg('Đã xóa khách hàng', 'success');
        json_success('Đã xóa khách hàng', ['redirect_url' => '/management/customer/']);
    }

    public function export()
    {
		/* if() { */ /* continue working */
		
        if ($this->session->flashdata('export') == null)
            return redirect('/management/customer');

        $this->load->model('basso/order_term_model');
        $this->load->model('basso/country_model');
        $data = $this->session->flashdata('export');
        $orders = $data['orders'];
        $items = $data['items'];
        $payments = $data['payments'];
        $customer = $data['customer'];

        $countries = $this->country_model->get(array_column($orders, 'country_id'));
        $list_terms = $this->order_term_model->get_by_id(array_column($items, 'term_id'));

        $this->load->library('excel');
        $row_count = 9; 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet1');

        #region Khách hàng
        $this->excel->getActiveSheet()->setCellValue('A1', "Khách hàng: $customer->name");
        $this->excel->getActiveSheet()->setCellValue('A2', "Điện thoại: $customer->phone");
        $this->excel->getActiveSheet()->setCellValue('A3', "Địa chỉ: $customer->address, " . $customer->district . ', ' . $customer->city);
        #endregion
		
        #region Khởi tạo header
        $this->excel->getActiveSheet()->setCellValue("A{$row_count}", 'Ngày');
        $this->excel->getActiveSheet()->setCellValue("B{$row_count}", 'Mã ĐH');
        $this->excel->getActiveSheet()->setCellValue("C{$row_count}", 'Tình trạng ĐH');
        $this->excel->getActiveSheet()->setCellValue("D{$row_count}", 'Quốc gia');
        $this->excel->getActiveSheet()->setCellValue("E{$row_count}", 'Web');
        $this->excel->getActiveSheet()->setCellValue("F{$row_count}", 'STT');
        $this->excel->getActiveSheet()->setCellValue("G{$row_count}", 'Tên SP');
        $this->excel->getActiveSheet()->setCellValue("H{$row_count}", 'Tình trạng SP');
        $this->excel->getActiveSheet()->setCellValue("I{$row_count}", 'Danh mục SP');
        $this->excel->getActiveSheet()->setCellValue("J{$row_count}", 'Variant');
        $this->excel->getActiveSheet()->setCellValue("K{$row_count}", 'Ghi chú SP');
        $this->excel->getActiveSheet()->setCellValue("L{$row_count}", 'Giá SP');
        $this->excel->getActiveSheet()->setCellValue("M{$row_count}", 'SL');
        $this->excel->getActiveSheet()->setCellValue("N{$row_count}", 'Ship web');
        $this->excel->getActiveSheet()->setCellValue("O{$row_count}", 'Tổng tiền');
        $this->excel->getActiveSheet()->setCellValue("P{$row_count}", 'Phụ thu');
        $this->excel->getActiveSheet()->setCellValue("Q{$row_count}", 'Phí ship Qte');
        $this->excel->getActiveSheet()->setCellValue("R{$row_count}", 'Phí ship nội địa');
        $this->excel->getActiveSheet()->setCellValue("S{$row_count}", 'Giảm giá');
        $this->excel->getActiveSheet()->setCellValue("T{$row_count}", 'Thành tiền VNĐ');
        $this->excel->getActiveSheet()->setCellValue("U{$row_count}", 'Đã thanh toán');
        /* $this->excel->getActiveSheet()->setCellValue("V{$row_count}", 'Số dư'); */
        $this->excel->getActiveSheet()->setCellValue("V{$row_count}", 'Ngày thanh toán');
        $this->excel->getActiveSheet()->setCellValue("W{$row_count}", 'PPTT');
        $this->excel->getActiveSheet()->getStyle("A9:W9")->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('0a48ad');
        $this->excel->getActiveSheet()->getStyle("A9:W9")->applyFromArray([
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'size' => 12
            ]
        ]);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        #endregion

        #region fil data
        $row_count = 10;
        $orders = array_filter($orders, function ($t) {
            return $t->status != CustomerOrderStatus::CANCELLED;
        });
		 #endregion
        foreach ($orders as $order) {

            $_web = explode('.', $order->website);
            if (count($_web) > 1)
                unset($_web[count($_web) - 1]);
            $order->website = implode('.', $_web);

            #region
            $this->excel->getActiveSheet()->setCellValue("A$row_count", date('d/m/Y', $order->created_time));
            $this->excel->getActiveSheet()->setCellValue("B$row_count", $order->order_code);
            if (in_array($order->status, array_keys(CustomerOrderStatus::LIST_STATE)))
                $order->status = CustomerOrderStatus::LIST_STATE[$order->status]['name'];
            $this->excel->getActiveSheet()->setCellValue("C{$row_count}", $order->status);
            $this->excel->getActiveSheet()->setCellValue("D{$row_count}", $order->country);
            $this->excel->getActiveSheet()->setCellValue("E{$row_count}", $order->website);
            $this->excel->getActiveSheet()->setCellValue("N{$row_count}",
                "{$order->currency_symbol} {$order->web_shipping_fee}");
            $this->excel->getActiveSheet()->setCellValue("S{$row_count}", $order->discount_total);
            #ednregion

            $order_items = array_filter($items, function ($t) use ($order) {
                return $t->order_id == $order->id;
            });

            $item_index = 1;
            $country = null;
            $country_index = array_search($order->country_id, array_column($countries, 'id'));
            if ($country_index >= 0)
                $country = $countries[$country_index];

            #region sản phẩm
            foreach ($order_items as $item) {
                $_row_count = $row_count + $item_index - 1;

                $this->excel->getActiveSheet()->setCellValue("F{$_row_count}", $item_index);
                $this->excel->getActiveSheet()->setCellValue("G{$_row_count}", $item->name);
                $this->excel->getActiveSheet()->getCell("G{$_row_count}")->getHyperlink()->setUrl($item->link);

                if (isset(CustomerOrderItemStatus::LIST_STATE[$item->status]))
                    $item->status = CustomerOrderItemStatus::LIST_STATE[$item->status]['name'];
                $this->excel->getActiveSheet()->setCellValue("H{$_row_count}", $item->status);

                $term = null;
                $term_index = array_search($item->term_id, array_column($list_terms, 'id'));
                if ($term_index >= 0)
                    $term = $list_terms[$term_index]->name;

                $this->excel->getActiveSheet()->setCellValue("I{$_row_count}", $term);
                $variant = '';
                if ($item->variations != null) {
                    if (!empty($item->variations)) {
                        try {
                            if (!is_array($item->variations))
                                $variations = json_decode($item->variations);
                            else
                                $variations = $item->variations;

                            try {
                                if (count($variations) > 0) {
                                    foreach ($variations as $v) {
                                        $variant .= $v->name . ': ' . $v->value . PHP_EOL;
                                    }
                                }
                            } catch (Exception $ex) {
                            }
                        } catch (Exception $ex) {
                        }
                    }
                }

                $this->excel->getActiveSheet()->setCellValue("J{$_row_count}", $variant);
                $this->excel->getActiveSheet()->getStyle("J{$_row_count}")->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->setCellValue("K{$_row_count}", $item->note);
                $this->excel->getActiveSheet()->setCellValue("L{$_row_count}", "{$order->currency_symbol} {$item->price}");
                $this->excel->getActiveSheet()->setCellValue("M{$_row_count}", $item->quantity);
                $this->excel->getActiveSheet()->setCellValue("P{$_row_count}",
                    is_enull($item->term_fee, 0) * $item->quantity);

                $this->excel->getActiveSheet()->getStyle("P$row_count")->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);

                $this->excel->getActiveSheet()->setCellValue("O{$_row_count}",
                    "{$order->currency_symbol} " . ($item->price * $item->quantity));

                $weight = is_enull($item->weight, 0);
                $world_shipping_fee = 0;
                if ($country != null)
                    $world_shipping_fee = $weight * floatval($country->shipping_fee);

                $this->excel->getActiveSheet()->setCellValue("Q{$_row_count}", intval($world_shipping_fee));
                $this->excel->getActiveSheet()->getStyle("Q$row_count")->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);

                $sub_total = ($item->price * $order->currency_rate + $item->term_fee) * $item->quantity;
                $this->excel->getActiveSheet()->setCellValue("T{$_row_count}", $sub_total);
                $this->excel->getActiveSheet()->getStyle("T$row_count")->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);

                $item_index++;
            }
            #endregion

            #region thanh toán
            $payment_index = 1;
            $order_payments = array_filter($payments, function ($t) use ($order) {
                return $t->status == PaymentHistoryStatus::COMPLETED && $t->order_id == $order->id && $t->amount > 0;
            });

            if (count($order_payments) > 0) {
                $total_amount = 0;
                foreach ($order_payments as $payment) {
                    if ($payment->amount <= 0)
                        continue;

                    $_row_count = $row_count + $payment_index - 1;
                    $total_amount += $payment->amount;

                    $this->excel->getActiveSheet()->setCellValue("U$_row_count", $payment->amount);
                    $this->excel->getActiveSheet()->getStyle("U$row_count")->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);


                    /* $this->excel->getActiveSheet()->setCellValue("V$_row_count", ($order->sub_total - $total_amount));
                    $this->excel->getActiveSheet()->getStyle("V$row_count")->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2); */

                    $this->excel->getActiveSheet()->setCellValue("V$_row_count", PHPExcel_Shared_Date::PHPToExcel(date('Y-m-d', $payment->created_time)));

                    $payment_type = null;
                    if (in_array($payment->type, array_keys(PaymentType::LIST_STATE)))
                        $payment->type = PaymentType::LIST_STATE[$payment->type];

                    $this->excel->getActiveSheet()->setCellValue("W$_row_count", $payment->type);
                    $payment_index++;
                }
            } else {
                $this->excel->getActiveSheet()->setCellValue("U$row_count", null);
                /* $this->excel->getActiveSheet()->setCellValue("V$row_count", $order->sub_total);
                $this->excel->getActiveSheet()->getStyle("V$row_count")->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2); */
            }
            #endregion

            if ($item_index > $payment_index)
                $row_count += $item_index - 1;
            else
                $row_count += $payment_index - 1;
        }

        $sub_total = array_sum(array_column($orders, 'sub_total'));
        $total_paid = array_sum(array_column($orders, 'total_paid'));
        $this->excel->getActiveSheet()->setCellValue("T$row_count", $sub_total);
        $this->excel->getActiveSheet()->getStyle("T$row_count")->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);

        $this->excel->getActiveSheet()->setCellValue("U$row_count", $total_paid);
        $this->excel->getActiveSheet()->getStyle("U$row_count")->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);

		#region Thành Tiền
		$this->excel->getActiveSheet()->setCellValue('A5', "Tổng tiền hàng VNĐ:");	
        $this->excel->getActiveSheet()->setCellValue('A6', "Đã thanh toán VNĐ:");
        $this->excel->getActiveSheet()->setCellValue('A7', "Số tiền còn lại VNĐ:");
		
        $this->excel->getActiveSheet()->setCellValue('B5', $sub_total);
		$this->excel->getActiveSheet()->getStyle("B5")->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
			
        $this->excel->getActiveSheet()->setCellValue('B6', $total_paid);
		$this->excel->getActiveSheet()->getStyle("B6")->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
		
		$tong_con_lai = $sub_total - $total_paid;
        $this->excel->getActiveSheet()->setCellValue('B7', $tong_con_lai);
		$this->excel->getActiveSheet()->getStyle("B7")->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
			
		$this->excel->getActiveSheet()->getStyle("B5:B7")->applyFromArray([
            'font' => [
                'bold' => true,
                'color' => ['rgb' => '000'],
                'size' => 12
            ]
        ]);
		
        #endregion

        /* $this->excel->getActiveSheet()->setCellValue("V$row_count", ($sub_total - $total_paid));
        $this->excel->getActiveSheet()->getStyle("V$row_count")->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2); */
			
        #endregion

        #region Sửa style, xuất file
        $this->excel->getActiveSheet()->getStyle("A9:W{$row_count}")->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle("A9:W{$row_count}")->applyFromArray([
            'font' => ['size' => 12]
        ]);

        $MAX_COL = $this->excel->getActiveSheet()->getHighestDataColumn();
        $MAX_COL_INDEX = PHPExcel_Cell::columnIndexFromString($MAX_COL);
        for ($index = 0; $index <= $MAX_COL_INDEX; $index++) {
            $col = PHPExcel_Cell::stringFromColumnIndex($index);
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(TRUE);
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . $customer->name . '.xls');
        header('Cache-Control: max-age=0');

        $this->session->unset_userdata('export-items');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        #endregion
		
    }
	
	
	
	public function export_list()
    {
		 if ($this->session->flashdata('export_list') == null)
            return redirect('/management/customer');
		
		$data = $this->session->flashdata('export_list');

        $this->load->library('excel');
        $row_count = 3; 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet1');

        #region Khách hàng
        $this->excel->getActiveSheet()->setCellValue('A1', "DANH SÁCH KHÁCH HÀNG");
        
        #endregion
		
        #region Khởi tạo header
        $this->excel->getActiveSheet()->setCellValue("A{$row_count}", 'Tên KH');
        $this->excel->getActiveSheet()->setCellValue("B{$row_count}", 'Email');
        $this->excel->getActiveSheet()->setCellValue("C{$row_count}", 'SĐT');
        $this->excel->getActiveSheet()->setCellValue("D{$row_count}", 'Địa chỉ');
        $this->excel->getActiveSheet()->setCellValue("E{$row_count}", 'Tổng doanh thu');
        $this->excel->getActiveSheet()->setCellValue("F{$row_count}", 'Tổng tiền đang GD');
        $this->excel->getActiveSheet()->setCellValue("G{$row_count}", 'Đã thanh toán');
        $this->excel->getActiveSheet()->setCellValue("H{$row_count}", 'Số tiền còn thiếu');

        $this->excel->getActiveSheet()->getStyle("A3:H3")->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('0a48ad');
        $this->excel->getActiveSheet()->getStyle("A3:H3")->applyFromArray([
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'size' => 12
            ]
        ]);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		
		$this->excel->getActiveSheet()->mergeCells('A1:H1');
		$this->excel->getActiveSheet()->getStyle("A1:H1")->applyFromArray([
            'font' => [
                'size' => 15
            ]
        ]);
		
		$this->excel->getActiveSheet()->getStyle("A1:H1")->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
		$this->excel->getActiveSheet()->getStyle('A1:H1')
				->getFill()->applyFromArray(array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array(
						 'rgb' => 'ffff00'
					)
				));
		
        #endregion
        foreach ($data as $customer) {
		#region
			$total_sale = $this->customer_model->get_subtotal_bought_of_customer($customer->id);
			$sub_total = ($customer->sub_total != null && $customer->sub_total != "")?$customer->sub_total : 0;
			$total_paid = ($customer->total_paid != null && $customer->total_paid != "")?$customer->total_paid : 0;
			$debit_total = $sub_total - $total_paid;
			$row_count = $row_count+1;
            $this->excel->getActiveSheet()->setCellValue("A{$row_count}", $customer->name);
            $this->excel->getActiveSheet()->setCellValue("B{$row_count}", $customer->email);
            $this->excel->getActiveSheet()->setCellValue("C{$row_count}", $customer->phone);
            $this->excel->getActiveSheet()->setCellValue("D{$row_count}", $customer->address);
            $this->excel->getActiveSheet()->setCellValue("E{$row_count}", $total_sale);
			$this->excel->getActiveSheet()->getStyle("E$row_count")->getNumberFormat()
				->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
				
            $this->excel->getActiveSheet()->setCellValue("F{$row_count}", $sub_total);
			$this->excel->getActiveSheet()->getStyle("F$row_count")->getNumberFormat()
				->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
            $this->excel->getActiveSheet()->setCellValue("G{$row_count}",$total_paid );
			$this->excel->getActiveSheet()->getStyle("G$row_count")->getNumberFormat()
				->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
            $this->excel->getActiveSheet()->setCellValue("H{$row_count}", $debit_total);
			$this->excel->getActiveSheet()->getStyle("H$row_count")->getNumberFormat()
				->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);
				
            /* $this->excel->getActiveSheet()->setCellValue("F{$row_count}", $customer->group); */
            #ednregion

		}
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=DS_KhachHang.xls');
        header('Cache-Control: max-age=0');

        /* $this->session->unset_userdata('export-items'); */
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        #endregion
    }
	
	
	
    #endregion
}