<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Billing
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Billing_model $billing_model
 * @property Excel $excel
 * @property Web_payment_model $web_payment_model
 */
class Billing extends Cpanel_Controller
{
    private $_user;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('billing_model');
        $this->load->model('basso/web_payment_model');
		$this->load->model('basso/contact_form_model');
        global $current_user;
        $this->_user = $current_user;
    }

    #region Phiếu thu
    public function in()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Quản lý thu chi'], ['text' => 'Phiếu thu']]);
        return $this->blade->render();
    }

    public function GET_in()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('payment_method', null, 'required');
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        //  Danh sách phiếu thu
        $page = $this->input->get('page');
        $group = $this->input->get('payment_method');
        $time = $this->input->get('time');
        $start = $this->input->get('start');
        $end = $this->input->get('end');
        $key = $this->input->get('key');

        $page = !is_numeric($page) ? 1 : ($page < 1 ? 1 : $page);
        $from = $to = 0;
        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $from = strtotime(date('Y-m-d 00:00:00', $day));
                $to = time();
                break;
            case 'today':
                $from = strtotime(date('Y-m-d 00:00:00'));
                break;
            case 'custom':
                $from = strtotime("$start 00:00:00");
                $to = strtotime("$end 23:59:59");
                break;
            case 'month':
                $from = strtotime(date('Y-m-1 00:00:00'));
                break;
        }

        $result = $this->billing_model->get(BillingType::IN, $page, $from, $to, $group, $key);
        $total_item = $this->billing_model->count(BillingType::IN, $from, $to, $group, $key);
        $pagination = Pagination::calc($total_item, $page, PAGING_SIZE);
        $terms = $this->billing_model->term_get();

        json_success(null, [
            'data' => $result,
            'pagination' => $pagination,
            'terms' => $terms,
            'total' => $this->billing_model->sum(BillingType::IN, $from, $to, $group, $key)
        ]);
    }

    public function DELETE_in()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $billing = $this->billing_model->get_by_id($id);
        if ($billing == null)
            json_error('Không tìm thấy phiếu thu');

        $this->billing_model->delete($id);
        json_success('Xóa phiếu thu thành công');
    }

    public function POST_in()
    {
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('group', null, 'required');
        $this->form_validation->set_rules('payment_method', null, 'required');
        $this->form_validation->set_rules('amount', null, 'required|numeric');
        $this->form_validation->set_rules('term_id', null, 'required|numeric');

        if (!$this->form_validation->run())
            json_error('Vui lòng nhập thông tin bắt buộc');

        $time = strtotime($this->input->post('time'));
        $name = $this->input->post('name');
        $amount = $this->input->post('amount');
        $phone = $this->input->post('phone');
        $des = $this->input->post('des');
        $term_id = $this->input->post('term_id');
        $group = $this->input->post('group');
        $payment_method = $this->input->post('payment_method');

        $this->billing_model->insert($time, BillingType::IN, $term_id, $name, $amount, $phone,
            $des, $this->_user->id, $group, $payment_method);
        json_success('Đã thêm phiếu thu');
    }
    #endregion

    #region Phiếu chi
    public function out()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Quản lý thu chi'], ['text' => 'Phiếu chi']]);
        $this->blade->set('payment_method', $this->web_payment_model->get());
        return $this->blade->render();
    }

    public function GET_out()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('payment_method', null, 'required');
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        //  Danh sách phiếu thu
        $page = $this->input->get('page');
        $group = $this->input->get('payment_method');
        $time = $this->input->get('time');
        $start = $this->input->get('start');
        $end = $this->input->get('end');
        $key = $this->input->get('key');

        $page = !is_numeric($page) ? 1 : ($page < 1 ? 1 : $page);
        $from = $to = 0;
        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $from = strtotime(date('Y-m-d 00:00:00', $day));
                $to = time();
                break;
            case 'today':
                $from = strtotime(date('Y-m-d 00:00:00'));
                break;
            case 'custom':
                $from = strtotime("$start 00:00:00");
                $to = strtotime("$end 23:59:59");
                break;
            case 'month':
                $from = strtotime(date('Y-m-1 00:00:00'));
                break;
        }

        $result = $this->billing_model->get(BillingType::OUT, $page, $from, $to, $group, $key);
        $total_item = $this->billing_model->count(BillingType::OUT, $from, $to, $group, $key);
        $pagination = Pagination::calc($total_item, $page, PAGING_SIZE);
        $terms = $this->billing_model->term_get();

        json_success(null, [
            'data' => $result,
            'pagination' => $pagination,
            'terms' => $terms,
            'total' => $this->billing_model->sum(BillingType::OUT, $from, $to, $group, $key)
        ]);
    }

    public function DELETE_out()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $billing = $this->billing_model->get_by_id($id);
        if ($billing == null)
            json_error('Không tìm thấy phiếu chi');

        if ($this->billing_model->delete($id))
            json_success('Xóa phiếu chi thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_out()
    {
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('group', null, 'required');
        $this->form_validation->set_rules('payment_method', null, 'required');
        $this->form_validation->set_rules('amount', null, 'required|numeric');
        $this->form_validation->set_rules('term_id', null, 'required|numeric');

        if (!$this->form_validation->run())
            json_error('Vui lòng nhập thông tin bắt buộc');

        $time = strtotime($this->input->post('time'));
        $name = $this->input->post('name');
        $amount = $this->input->post('amount');
        $phone = $this->input->post('phone');
        $des = $this->input->post('des');
        $term_id = $this->input->post('term_id');
        $group = $this->input->post('group');
        $payment_method = $this->input->post('payment_method');

        if ($this->billing_model->insert($time, BillingType::OUT, $term_id, $name, $amount, $phone,
            $des, $this->_user->id, $group, $payment_method))
            json_success('Đã thêm phiếu chi');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }
    #endregion

    #region Phân loại thu chi
    public function term()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Phiếu thu chi'], ['text' => 'Phân loại']]);
        return $this->blade->render();
    }

    public function GET_term()
    {
        json_success(null, ['data' => $this->billing_model->term_get()]);
    }

    public function DELETE_term()
    {
        $id = $this->input->input_stream('id');
        if (!is_numeric($id))
            json_error('Không tìm thấy yêu cầu');

        $term = $this->billing_model->term_get($id);
        if ($term == null)
            json_error('Phân loại không tồn tại');

        if ($this->billing_model->term_delete($id))
            json_success('Xóa phân loại thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_term()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập thông tin bắt buộc');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $des = $this->input->post('des');

        if ($this->billing_model->term_insert($id, $name, $des))
            json_success($id == 0 ? 'Thêm phân loại thành công' : 'Cập nhật phân loại thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }
    #endregion

    #region Sổ quỹ
    public function fund()
    {
        $this->blade->set('breadcrumbs', [
            ['text' => 'Báo cáo thu chi'],
            ['text' => 'Sổ quỹ']
        ]);
        return $this->blade->render();
    }

    public function GET_fund()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('group', null, 'required');
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        //  Danh sách phiếu thu
        $page = $this->input->get('page');
        $group = $this->input->get('group');
        $time = $this->input->get('time');
        $start = $this->input->get('start');
        $end = $this->input->get('end');

        $page = !is_numeric($page) ? 1 : ($page < 1 ? 1 : $page);
        $from = $to = 0;
        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $from = strtotime(date('Y-m-d 00:00:00', $day));
                $to = time();
                break;
            case 'today':
                $from = strtotime(date('Y-m-d 00:00:00'));
                break;
            case 'custom':
                $from = strtotime("$start 00:00:00");
                $to = strtotime("$end 23:59:59");
                break;
            case 'month':
                $from = strtotime(date('Y-m-1 00:00:00'));
                break;
        }

        $result = $this->billing_model->get(null, $page, $from, $to, $group);
        $total_item = $this->billing_model->count(null, $from, $to, $group);
        $total_in = $this->billing_model->sum(BillingType::IN, $from, $to, $group);
        $total_out = $this->billing_model->sum(BillingType::OUT, $from, $to, $group);

        json_success(null, [
            'result' => $result,
            'pagination' => Pagination::calc($total_item, $page),
            'total_in' => $total_in,
            'total_out' => $total_out
        ]);
    }

    public function fund_export()
    {
        $group = $this->input->get('group');
        $time = $this->input->get('time');
        $start = $this->input->get('start');
        $end = $this->input->get('end');

        $from = $to = 0;
        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $from = strtotime(date('Y-m-d 00:00:00', $day));
                $to = time();
                break;
            case 'today':
                $from = strtotime(date('Y-m-d 00:00:00'));
                break;
            case 'custom':
                $from = strtotime("$start 00:00:00");
                $to = strtotime("$end 23:59:59");
                break;
            case 'month':
                $from = strtotime(date('Y-m-1 00:00:00'));
                break;
        }

        $result = $this->billing_model->get(null, 0, $from, $to, $group);
        $this->load->library('excel');

        $row_count = 1;
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet1');

        #region khởi tạo header
        $this->excel->getActiveSheet()->setCellValue("A{$row_count}", 'STT');
        $this->excel->getActiveSheet()->setCellValue("B{$row_count}", 'Loại phiếu');
        $this->excel->getActiveSheet()->setCellValue("C{$row_count}", 'Ngày ghi nhận');
        $this->excel->getActiveSheet()->setCellValue("D{$row_count}", 'Mã phiếu');
        $this->excel->getActiveSheet()->setCellValue("E{$row_count}", 'Người nhận/nộp');
        $this->excel->getActiveSheet()->setCellValue("F{$row_count}", 'PTTT');
        $this->excel->getActiveSheet()->setCellValue("G{$row_count}", 'Tiền thu');
        $this->excel->getActiveSheet()->setCellValue("H{$row_count}", 'Tiền chi');
        $this->excel->getActiveSheet()->setCellValue("I{$row_count}", 'Mô tả');
        $this->excel->getActiveSheet()->getStyle("A1:I1")->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('0a48ad');
        $this->excel->getActiveSheet()->getStyle("A1:I1")->applyFromArray([
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'size' => 12
            ]
        ]);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        #endregion

        foreach ($result as $item) {
            $row_count++;
            $this->excel->getActiveSheet()->setCellValue("A{$row_count}", $row_count - 1);
            $this->excel->getActiveSheet()->setCellValue("B{$row_count}", $item->term);
            $this->excel->getActiveSheet()->setCellValue("C{$row_count}", date('d/m/Y', $item->created_time));
            $this->excel->getActiveSheet()->setCellValue("D{$row_count}", '#' . $item->id);
            $this->excel->getActiveSheet()->setCellValue("E{$row_count}", $item->name);

            foreach (BillingPaymentMethod::LIST_STATE as $key => $value) {
                if ($item->payment_method == $key)
                    $item->payment_method = $value;
            }

            $this->excel->getActiveSheet()->setCellValue("F{$row_count}", $item->payment_method);
            if ($item->type == BillingType::IN)
                $this->excel->getActiveSheet()->setCellValue("G{$row_count}", format_money($item->amount));
            else
                $this->excel->getActiveSheet()->setCellValue("H{$row_count}", format_money($item->amount));

            $this->excel->getActiveSheet()->setCellValue("I{$row_count}", $item->description);
        }

        #region footer
        $total_in = $this->billing_model->sum(BillingType::IN, $from, $to, $group);
        $total_out = $this->billing_model->sum(BillingType::OUT, $from, $to, $group);

        $row_count++;
        $this->excel->getActiveSheet()->setCellValue("G{$row_count}", format_money($total_in));
        $this->excel->getActiveSheet()->setCellValue("H{$row_count}", format_money($total_out));
        #endregion

        #region Sửa style, xuất file
        $this->excel->getActiveSheet()->getStyle("A1:I{$row_count}")->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle("A1:I{$row_count}")->applyFromArray([
            'font' => ['size' => 12]
        ]);

        $MAX_COL = $this->excel->getActiveSheet()->getHighestDataColumn();
        $MAX_COL_INDEX = PHPExcel_Cell::columnIndexFromString($MAX_COL);
        for ($index = 0; $index <= $MAX_COL_INDEX; $index++) {
            $col = PHPExcel_Cell::stringFromColumnIndex($index);
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(TRUE);
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . time() . '.xls');
        header('Cache-Control: max-age=0');

        $this->session->unset_userdata('export-items');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        #endregion
    }
    #endregion

    #region Export excel
    public function export()
    {
        $this->load->library('excel');
        $group = $this->input->get('payment_method');
        $time = $this->input->get('time');
        $start = $this->input->get('start');
        $end = $this->input->get('end');
        $key = $this->input->get('key');
        $type = $this->input->get('type');

        $from = $to = 0;
        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $from = strtotime(date('Y-m-d 00:00:00', $day));
                $to = time();
                break;
            case 'today':
                $from = strtotime(date('Y-m-d 00:00:00'));
                break;
            case 'custom':
                $from = strtotime("$start 00:00:00");
                $to = strtotime("$end 23:59:59");
                break;
            case 'month':
                $from = strtotime(date('Y-m-1 00:00:00'));
                break;
        }

        $result = $this->billing_model->get($type, 0, $from, $to, $group, $key);
        $row_count = 1;

        #region Khởi tạo header
        $this->excel->getActiveSheet()->setCellValue("A{$row_count}", 'Mã phiếu');
        $this->excel->getActiveSheet()->setCellValue("B{$row_count}", 'Đối tượng');
        $this->excel->getActiveSheet()->setCellValue("C{$row_count}", 'Loại phiếu');
        $this->excel->getActiveSheet()->setCellValue("D{$row_count}", 'PTTT');
        $this->excel->getActiveSheet()->setCellValue("E{$row_count}", 'Người tạo');
        $this->excel->getActiveSheet()->setCellValue("F{$row_count}", $type == BillingType::IN ? 'Số tiền thu' : 'Số tiền chi');
        $this->excel->getActiveSheet()->setCellValue("G{$row_count}", 'Ngày ghi nhận');
        $this->excel->getActiveSheet()->setCellValue("H{$row_count}", 'Nội dung');
        $this->excel->getActiveSheet()->getStyle("A1:H1")->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('0a48ad');
        $this->excel->getActiveSheet()->getStyle("A1:H1")->applyFromArray([
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'ffffff'],
                'size' => 12
            ]
        ]);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);

        #region fill data
        $row_count = 2;
        $total = 0;
        foreach ($result as $item) {
			$group_name = $item->group == 'customer'? 'Khách hàng' : ($item->group == 'shipping'? 'Đối tác vận chuyển':'');
            $this->excel->getActiveSheet()->setCellValue("A{$row_count}", $item->id);
            $this->excel->getActiveSheet()->setCellValue("B{$row_count}", $group_name);
            $this->excel->getActiveSheet()->setCellValue("C{$row_count}", $item->term);
            $this->excel->getActiveSheet()->setCellValue("D{$row_count}", $item->payment_method);
            $this->excel->getActiveSheet()->setCellValue("E{$row_count}", $item->user);
            $this->excel->getActiveSheet()->setCellValue("F{$row_count}", $item->amount);
            $this->excel->getActiveSheet()->setCellValue("G{$row_count}", PHPExcel_Shared_Date::PHPToExcel(date('Y-m-d', $item->time)));
            $this->excel->getActiveSheet()->setCellValue("H{$row_count}", $item->description);

            $total += $item->amount;
            $row_count++;
        }

        $this->excel->getActiveSheet()->setCellValue("F$row_count", $total);
        $this->excel->getActiveSheet()->getStyle("F2:F$row_count")->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2);

        $this->excel->getActiveSheet()->getStyle("G2:G$row_count")->getNumberFormat()
            ->setFormatCode('d/m/Y');
        #endregion

        #region Sửa style, xuất file
        $this->excel->getActiveSheet()->getStyle("A1:H{$row_count}")->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle("A1:H{$row_count}")->applyFromArray([
            'font' => ['size' => 12]
        ]);

        $MAX_COL = $this->excel->getActiveSheet()->getHighestDataColumn();
        $MAX_COL_INDEX = PHPExcel_Cell::columnIndexFromString($MAX_COL);
        for ($index = 0; $index <= $MAX_COL_INDEX; $index++) {
            $col = PHPExcel_Cell::stringFromColumnIndex($index);
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(TRUE);
        }

        $this->excel->getActiveSheet()->getStyle("F2:F$row_count")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=' . ($type == BillingType::IN ? 'Phiếu thu' : 'Phiếu chi') . '.xls');
        header('Cache-Control: max-age=0');

        $this->session->unset_userdata('export-items');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        #endregion
    }
    #endregion
}