<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Shipping
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Shipping_model $shipping_model
 * @property CI_Form_validation $form_validation
 */
class Shipping extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('shipping_model');
		$this->load->model('basso/contact_form_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Hệ thống'], ['text' => 'Đơn vị vận chuyển']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $data = $this->shipping_model->get();
        json_success(null, ['data' => $data]);
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->form_validation->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $shipping = $this->shipping_model->get($id);
        if ($shipping == null)
            json_error('Không tìm thấy đơn vị vận chuyển');

        $this->shipping_model->delete($id);
        json_success('Xóa đơn vị vận chuyển thành công');
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $website = $this->input->post('website');

        if ($id == null || $name == null)
            json_error('Vui lòng nhập đủ thông tin');

        $this->shipping_model->insert($id, $name, $phone, $website);
        json_success($id == 0 ? 'Thêm thành công' : 'Cập nhật thành công');
    }
}