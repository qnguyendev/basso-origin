<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Customer_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Customer_model extends CI_Model
{
    private $_customer_table = 'users';
    private $_customer_group_table = 'customer_groups';
    private $_customer_info_table = 'users_info';
    private $_customer_role_table = 'users_roles';
    private $_customer_order_table = 'customer_orders';
	
    private $web_orders = 'web_orders';
    private $web_order_items = 'web_order_items';

    public function find($key)
    {
        $this->db->select('t.id, t.first_name as name, t.phone, t.email, o.address, o.city_id, o.district_id');
        $this->db->from("$this->_customer_table t");
        $this->db->join("$this->_customer_info_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_role_table d", 't.id = d.user_id', 'left');

        $this->db->where('d.group_id', 6);
        $this->db->where("(t.first_name like '%$key%' or t.phone like '%$key%')");

        $this->db->limit(10);
        $this->db->group_by('t.id');
        return $this->db->get($this->_customer_table)->result();
    }

    public function update_info(int $user_id, array $data)
    {
        $this->db->trans_begin();
        if ($this->_get_user_info($user_id) != null) {
            $this->db->where('user_id', $user_id);
            $this->db->update($this->_customer_info_table, $data);
        } else {
            $data['user_id'] = $user_id;
            $this->db->insert($this->_customer_info_table, $data);
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _get_user_info(int $user_id)
    {
        $this->db->where('user_id', $user_id);
        return $this->db->get($this->_customer_info_table)->row();
    }

    public function get($page = 1, $total_paid = 0, $group_id = 0, $key = null)
    {
        $this->db->select('t.first_name as name, t.phone, t.email, d.address, o.user_id as id, k.name as group');
        $this->db->select('sum(x.sub_total) as sub_total, sum(x.total_paid) as total_paid');
        $this->db->from("$this->_customer_table t");
        $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join("$this->_customer_group_table k", 'k.id = d.customer_group_id', 'left');
        $this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left');

        if ($group_id > 0)
            $this->db->where('d.customer_group_id', $group_id);

        if ($key != null)
            $this->db->where("(t.phone like '%$key%' or t.email like '%$key%' or t.first_name like '%$key%')");

        /*if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid or total_paid is null)");*/
		
		if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid)");
		
		/* fixcode */
        /* $this->db->where('o.group_id', 6);
        $this->db->where("(x.status != '" . CustomerOrderStatus::CANCELLED . "' or x.status is null)"); */
		/* end fixcode */
		
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }
	
	
    public function get_in_duration($page = 1, $total_paid = 0, $group_id = 0, $start_date, $end_date, $key = null, $staff_id = null)
    {
        $this->db->select('t.first_name as name, t.phone, t.email, d.address, o.user_id as id, k.name as group');
        $this->db->select('sum(x.sub_total) as sub_total, sum(x.total_paid) as total_paid');
        $this->db->from("$this->_customer_table t");
        $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join("$this->_customer_group_table k", 'k.id = d.customer_group_id', 'left');
        $this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left');

        if ($group_id > 0)
            $this->db->where('d.customer_group_id', $group_id);

        if ($key != null)
            $this->db->where("(t.phone like '%$key%' or t.email like '%$key%' or t.first_name like '%$key%')");

        /*if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid or total_paid is null)");*/
		
		if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid)");
		
		if ($start_date > 0)
            $this->db->where("(created_on >= $start_date)");
		if ($end_date > 0)
            $this->db->where("(created_on <= $end_date)");
		
		/* fixcode */
        /* $this->db->where('o.group_id', 6);
        $this->db->where("(x.status != '" . CustomerOrderStatus::CANCELLED . "' or x.status is null)"); */
		/* end fixcode */
		
		/* fixcode only show customer orders of current staff
		if ($staff_id != null) {
			$this->db->where('x.approve_user_id', $staff_id);
		} */
		
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }
	
	public function get_BAK($page = 1, $total_paid = 0, $group_id = 0, $key = null)
    {
        $this->db->select('t.first_name as name, t.phone, t.email, d.address, o.user_id as id, k.name as group');
        $this->db->select('sum(x.sub_total) as sub_total, sum(x.total_paid) as total_paid');
        $this->db->from("$this->_customer_table t");
        $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join("$this->_customer_group_table k", 'k.id = d.customer_group_id', 'left');
        $this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left');

        if ($group_id > 0)
            $this->db->where('d.customer_group_id', $group_id);

        if ($key != null)
            $this->db->where("(t.phone like '%$key%' or t.email like '%$key%' or t.first_name like '%$key%')");

        /*if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid or total_paid is null)");*/
		
		if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid)");
		
        $this->db->where('o.group_id', 6);
        $this->db->where("(x.status != '" . CustomerOrderStatus::CANCELLED . "' or x.status is null)");
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

	
	public function count($total_paid = 0, $group_id = 0, $key = null)
    {
        $this->db->select('t.id');
        $this->db->select('sum(x.sub_total) as sub_total, sum(x.total_paid) as total_paid');
        $this->db->from("$this->_customer_table t");
		
		/* fixcode */
        /* $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join("$this->_customer_group_table k", 'k.id = o.group_id', 'left');
        $this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left'); */
		/* end fixcode */

        if ($group_id > 0)
            $this->db->where('d.customer_group_id', $group_id);

        if ($key != null)
            $this->db->where("(t.phone like '%$key%' or t.email like '%$key%' or t.first_name like '%$key%')");

        /* if ($total_paid > 0)
             $this->db->where("(total_paid >= $total_paid or total_paid is null)");*/
		 
		if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid)");
		
		/* fixcode */
        /* $this->db->where('o.group_id', 6); */
		/* end fixcode */

        return $this->db->count_all_results();
    }

   public function count_in_duration($total_paid = 0, $group_id = 0,int $start_date,int $end_date, $key = null, $staff_id = null)
    {
        $this->db->select('t.id');
        $this->db->select('sum(x.sub_total) as sub_total, sum(x.total_paid) as total_paid');
        $this->db->from("$this->_customer_table t");
		
		$this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left');
		
		/* fixcode */
        /* $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join("$this->_customer_group_table k", 'k.id = o.group_id', 'left');
        $this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left'); */
		/* end fixcode */

        if ($group_id > 0)
            $this->db->where('d.customer_group_id', $group_id);

        if ($key != null)
            $this->db->where("(t.phone like '%$key%' or t.email like '%$key%' or t.first_name like '%$key%')");

        /* if ($total_paid > 0)
             $this->db->where("(total_paid >= $total_paid or total_paid is null)");*/
		 
		if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid)");
		
		if ($start_date > 0)
            $this->db->where("(created_on >= $start_date)");
		if ($end_date > 0)
            $this->db->where("(created_on <= $end_date)");
		
		/* fixcode only show customer orders of current staff */
		if ($staff_id != null) {
			$this->db->where('x.approve_user_id', $staff_id);
		}
		
		/* fixcode */
        /* $this->db->where('o.group_id', 6); */
		/* end fixcode */

        return $this->db->count_all_results();
    }

	public function count_BAK($total_paid = 0, $group_id = 0, $key = null)
    {
        $this->db->select('t.id');
        $this->db->select('sum(x.sub_total) as sub_total, sum(x.total_paid) as total_paid');
        $this->db->from("$this->_customer_table t");
        $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join("$this->_customer_group_table k", 'k.id = o.group_id', 'left');
        $this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left');

        if ($group_id > 0)
            $this->db->where('d.customer_group_id', $group_id);

        if ($key != null)
            $this->db->where("(t.phone like '%$key%' or t.email like '%$key%' or t.first_name like '%$key%')");

        /* if ($total_paid > 0)
             $this->db->where("(total_paid >= $total_paid or total_paid is null)"); */
		 
		if ($total_paid > 0)
            $this->db->where("(total_paid >= $total_paid)");
		
        $this->db->where('o.group_id', 6);

        return $this->db->count_all_results();
    }

    public function get_by_id(int $user_id)
    {
        $this->db->select('t.first_name as name, t.phone, t.email, o.user_id as id, k.name as group');
        $this->db->select('sum(x.sub_total) as sub_total, sum(x.total_paid) as total_paid');
        $this->db->select('l.name as city, m.name as district, d.address, d.note, d.birthday, d.gender');
        $this->db->select('d.city_id, d.district_id, d.customer_group_id as group_id');
        $this->db->from("$this->_customer_table t");
        $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join("$this->_customer_group_table k", 'k.id = d.customer_group_id', 'left');
        $this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left');
        $this->db->join('cities l', 'l.id = d.city_id', 'left');
        $this->db->join('districts m', 'm.id = d.district_id', 'left');
        $this->db->where('t.id', $user_id);
        /* $this->db->where('x.status !=', CustomerOrderStatus::CANCELLED); */  /* fixcode: this code not need */
        return $this->db->get()->row();
    }

    public function get_by_email(string $email)
    {
        $this->db->select('t.first_name as name, t.phone, t.email, o.user_id as id, k.name as group');
        $this->db->select('sum(x.sub_total) as sub_total, sum(x.total_paid) as total_paid');
        $this->db->select('l.name as city, m.name as district, d.address, d.note, d.birthday, d.gender');
        $this->db->select('d.city_id, d.district_id, d.customer_group_id as group_id');
        $this->db->from("$this->_customer_table t");
        $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join("$this->_customer_group_table k", 'k.id = d.customer_group_id', 'left');
        $this->db->join("$this->_customer_order_table x", 'x.customer_id = t.id', 'left');
        $this->db->join('cities l', 'l.id = d.city_id', 'left');
        $this->db->join('districts m', 'm.id = d.district_id', 'left');
        $this->db->where('t.email', $email);
        return $this->db->get()->row();
    }

    public function get_by_phone(string $phone)
    {
        $this->db->where('phone', $phone);
        return $this->db->get($this->_customer_table)->row();
    }

    public function get_info_by_id(int $user_id)
    {
        $this->db->select('t.first_name as name, t.phone, t.email, o.user_id as id');
        $this->db->select('l.name as city, m.name as district, d.address, d.note, d.birthday, d.gender');
        $this->db->select('d.city_id, d.district_id, d.customer_group_id as group_id');
        $this->db->from("$this->_customer_table t");
        $this->db->join("$this->_customer_role_table o", 't.id = o.user_id', 'left');
        $this->db->join("$this->_customer_info_table d", 't.id = d.user_id', 'left');
        $this->db->join('cities l', 'l.id = d.city_id', 'left');
        $this->db->join('districts m', 'm.id = d.district_id', 'left');
        $this->db->where('t.id', $user_id);
        return $this->db->get()->row();
    }
	
	public function get_subtotal_bought_of_customer(int $customer_id)
    {
        $this->db->select('sum(t.sub_total) as sub_total');
        $this->db->from("$this->web_orders t");
		$this->db->join("$this->web_order_items o", 't.id = o.web_order_id', 'left');
		$this->db->join("$this->_customer_order_table c", 'c.id = o.customer_order_id', 'left');
        $this->db->where('t.status', 'bought'); 
        $this->db->where('c.customer_id', $customer_id);
		$result = $this->db->get()->row('sub_total');
		/* print_r("<pre>");
		print_r($result);
		print_r("</pre>");
		die; */
        return $result;
    }
}