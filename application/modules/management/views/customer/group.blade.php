<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Quản lý nhóm khách hàng
                        <a class="float-right" href="#" data-bind="click: add">
                            <i class="fa fa-plus"></i>
                            Thêm nhóm
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên</th>
                            <th>Mô tả</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: description"></td>
                            <td>
                                <button data-bind="click: $root.edit" class="btn btn-link btn-xs btn-primary">
                                    <i class="fa fa-edit"></i>
                                    Cập nhật
                                </button>
                                <button class="btn btn-danger btn-xs" data-bind="click: $root.edit">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/group/editor_modal')
@endsection
@section('script')
    <script src="{{load_js('group')}}"></script>
@endsection