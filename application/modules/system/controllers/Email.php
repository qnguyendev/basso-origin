<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Email
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Option_model $option_model
 */
class Email extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('basso/contact_form_model');
    }

    public function index()
    {
        $breadcrumb = [['text' => 'Cài đặt email']];
        $data = ['smtp_user' => null,
            'smtp_host' => null,
            'smtp_port' => null,
            'smtp_pass' => null,
            'email_display_name' => null,
            'email_display_email' => null,
            'email_display_email_asale' => null,
            'email_display_name_asale' => null
        ];

        foreach ($data as $key => $value) {
            $option = $this->option_model->get($key);
            if ($option != null)
                $data[$key] = $option->value;
        }
        return $this->blade
            ->set('breadcrumbs', $breadcrumb)
            ->render($data);
    }

    public function POST_index()
    {
        $valid_keys = ['smtp_user', 'smtp_host', 'smtp_port', 'smtp_pass', 'email_display_name',
            'email_display_email', 'email_display_email_asale', 'email_display_name_asale'];

        foreach ($_POST as $key => $value) {
            if (in_array($key, $valid_keys))
                $this->option_model->save($key, $value);
        }

        success_msg('Cập nhật thành công');
        return redirect(current_url());
    }
}