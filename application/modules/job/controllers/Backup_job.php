<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Backup
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Option_model $option_model
 * @property Backup_model $backup_model
 * @property CI_DB_utility $dbutil
 */
class Backup_job extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->input->is_cli_request() or exit("Execute via command line: php index.php migrate");

        $config_file = "./theme/config.php";
        if (!is_file($config_file))
            return;

        require "./theme/config.php";
        if (!isset($theme_config))
            $theme_config = [];

        $this->load->helper('file');
        $this->load->model('system/backup_model');
    }

    public function run()
    {
        $this->load->dbutil();
        $prefs = [
            'format' => 'zip',                       // gzip, zip, txt
            'filename' => 'backup_db.sql'              // File name - NEEDED ONLY WITH ZIP FILES
        ];
        $backup = $this->dbutil->backup($prefs);

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $random_string_name = substr(str_shuffle($permitted_chars), 0, 8);

        $file_name = 'backup-' . date('Y-m-d-H-i-s') . "-$random_string_name" . '.zip';
        $file_path = "./backups/$file_name";
        write_file($file_path, $backup);

        $this->backup_model->save_history($file_name, filesize($file_path));
    }

    /**
     * Xóa các file backup cũ
     * @throws DropboxException
     */
    public function clean()
    {
        $backups = $this->backup_model->get_older_backups(7);
        foreach ($backups as $backup) {
            $file_path = "./backups/" . $backup->file_name;
            $this->backup_model->delete($backup->id);

            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
    }
}