<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Email
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Option_model $option_model
 * @property CI_Email $email
 * @property Email_model $email_model
 */
class Email_job extends Worker_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->input->is_cli_request() or exit("Execute via command line: php index.php migrate");

        $this->load->library('email');
        $this->load->model('email_model');
    }

    private function _init()
    {
        $config = array(
            'protocol' => 'smtp',
            'mailtype' => 'html',
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'bcc_batch_mode' => true
        );

        $item = $this->option_model->get('smtp_host');
        if ($item != null)
            $config['smtp_host'] = $item->value;

        $item = $this->option_model->get('smtp_port');
        if ($item != null) {
            $config['smtp_port'] = intval($item->value);
            if (intval($item->value) == 465)
                $config['smtp_crypto'] = 'ssl';
            else if (intval($item->value) == 587)
                $config['smtp_crypto'] = 'tls';
        }

        $item = $this->option_model->get('smtp_user');
        if ($item != null)
            $config['smtp_user'] = $item->value;

        $item = $this->option_model->get('smtp_pass');
        if ($item != null)
            $config['smtp_pass'] = $item->value;

        $this->email->initialize($config);
    }

    public function run($id = null)
    {
        $this->_init();
		$email_model = $this->email_model->get_emails_for_send();
		foreach($email_model as $email){
			$id = $email->id;
			$data = $this->email_model->get($id);
			$this->send($data);
		}
    }
	/* 
	public function run($id = null)
    {
        $this->_init();
        if ($this->worker_enable) {		
		log_message('info', 'AAAAAAAAAAAAAAAAAAAAAA'.$id);
                    $data = $this->email_model->get($id);
                $this->send($data);
            while ($this->worker_status) {
                $data = $this->reverse_job('email', false);
		log_message('info', 'AAAAAAAAAAAAAAAAAAAAAA'.$data);
                if (is_numeric($data))
                    $data = $this->email_model->get($id);
                $this->send($data);
            }
        } else {
            $data = $this->email_model->get($id);
            $this->send($data);
        }
    } */

    private function send($data)
    {
        if ($data != null) {
            if (!isset($data->sent))
                $data->sent = false;

            if (!boolval($data->sent)) {
                $display_email = $this->email->smtp_user;
                $display_name = 'Noreply';

                #region Lấy hiển thị
                $config = $this->option_model->get('email_display_name');
                $config_asale = $this->option_model->get('email_display_name_asale');
                
                if ($config != null) {
                    if (!empty($config->value))
                        $display_name = $config->value;
					if (!empty($config_asale->value))
                        $display_name_asale = $config_asale->value;
                }

                $config = $this->option_model->get('email_display_email');
				$config_asale = $this->option_model->get('email_display_email_asale');
                if ($config != null) {
                    if (!empty($config->value))
                        $display_email = $config->value;
					if (!empty($config_asale->value))
                        $display_email_asale = $config_asale->value;
                }
                #endregion
				
				$pos_asle = strpos(strtolower($data->subject), "asale");
				if ($pos_asle) {
					$display_name = $display_name_asale;
					/* $display_email = $display_email_asale; */
				}

                $this->email->from($display_email, $display_name);
				
				/* $pos_asle = strpos(strtolower($data->subject), "asale");
				if ($pos_asle) $this->email->from($display_email_asale, $display_name_asale); */
	
                $this->email->to($data->to);
                $this->email->subject($data->subject);
                $this->email->message($data->content);

                if (isset($data->bcc)) {
                    if ($data->bcc != null)
                        $this->email->bcc($data->bcc);
                }
                if ($this->email->send()) {
                    if (isset($data->id)) {
                        if ($data->id != null){
                            $this->email_model->set_sent($data->id);
						}
                    }
                } else
                    $this->email_model->set_log($data->id,$this->email->print_debugger());
            }
        }
    }

    /**
     * Xóa email gửi thành công 30 ngày trước
     */
    public function clean()
    {
        $this->email_model->clean(15);
    }
}