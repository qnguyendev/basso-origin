<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Basso
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 */
class Basso_Controller extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('basso/warehouse_model');
    }
}