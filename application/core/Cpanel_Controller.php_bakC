<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Cpanel
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_URI $uri
 * @property CI_Router $router
 */
class Cpanel_Controller extends \MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $GLOBALS['ci'] =& get_instance();
        $this->load->helper('common');
        load_site_config();

        $this->load->helper('cpanel');
        $this->load->helper('directory');
        $this->load->helper('string_format');
        $this->load->model('admin/image_model');
        $this->load->library('queue');

        //  Nếu chưa đăng nhập
        if (!$this->ion_auth->logged_in()) {
            if ($this->input->is_ajax_request())
                json_error(null, ['expired' => true, 'redirect_url' => LOGIN_URL]);

            return redirect(LOGIN_URL);
        }

        if (!$this->input->is_ajax_request())
            set_cookie('prev_url', "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
                3600, null, '/');

        $current_user = $this->ion_auth->user()->row();
        $user_roles = $this->ion_auth->get_users_groups($current_user->id)->result();

        //  User đăng nhập
        $GLOBALS['current_user'] = $current_user;

        //  Roles của user hiện tại;
        $GLOBALS['user_roles'] = array_column($user_roles, 'name');

        $module = strtolower($this->router->fetch_module());
        $controller = strtolower($this->router->fetch_class());
        $action = strtolower($this->router->fetch_method());

        #region Kiểm tra role chức năng
        $roles = system_roles();

        if ($controller != 'errors') {
            $valid_permission = false;
            //  Có module
            if (in_array($module, array_keys($roles))) {
                $_module = $roles[$module];
                if (array_key_exists($controller, $_module)) {
                    $_controller = $_module[$controller];

                    //  Nếu phân quyền cho toàn bộ action trong controller
                    if (count($_controller) > 0) {
                        if (is_numeric(array_keys($_controller)[0])) {
                            if (count(array_intersect($_controller, array_column($user_roles, 'name'))) == 0) {
                                $this->_no_permission();
                            } else
                                $valid_permission = true;
                        }
                    }

                    if (!$valid_permission) {
                        //  Có action
                        if (array_key_exists($action, $_controller)) {
                            $common_roles = array_intersect($_controller[$action], array_column($user_roles, 'name'));
                            $valid_permission = count($common_roles) > 0;
                        } else
                            $valid_permission = false;
                    }
                } else
                    $valid_permission = false;
            }

            if (!$valid_permission) {
                $this->session->set_flashdata('no_permission', 'no permission');
                $this->_no_permission();
            }
        }
        #endregion
    }

    private function _no_permission()
    {
        if ($this->input->is_ajax_request())
            json_error('Bạn không có quyền thực hiện tao tác này');

        return redirect(base_url('admin/errors/no_permission'), 'location', 301);
    }

    public function _remap($method)
    {
        $param_offset = 2;
        if ($this->input->method(true) != 'GET') {
            if ($this->input->method(true) == 'POST') {
                if (in_array($method, ['create', 'edit']))
                    $method = 'POST_save';
                else
                    $method = $this->input->method(true) . '_' . $method;
            } else
                $method = $this->input->method(true) . '_' . $method;
        } else {
            if ($this->input->is_ajax_request())
                $method = 'GET_' . $method;
        }

        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        if (method_exists($this, $method))
            return call_user_func_array(array($this, $method), $params);

        show_error("$method can not found");
    }
}
