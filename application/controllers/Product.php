<?php

if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Product
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Product_model $product_model
 * @property Term_model $term_model
 * @property CI_Cart $cart
 * @property Option_model $option_model
 */
class Product extends \Site_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
    }

    public function category($term, $page = 1)
    {
        //  Sắp xếp: theo tên, giá tăng dần, giá giảm dần
        $order_by = $this->input->get('order');
        if (!in_array($order_by, ['new', 'name', 'price-asc', 'price-desc']))
            $order_by = null;

        $filter = [];
        if ($this->input->get('filter') != null)
            $filter = explode(':', $this->input->get('filter'));


        if ($term->type == 'product_page')
            $term->id = 0;

        //  Tổng số sản phẩm tìm được
        $total = $this->product_model->count_by_term($term->id, $filter);

        //  Số sp trên mỗi trang
        $limit = 15;
        $config = $this->option_model->get('products_per_page');
        if ($config != null)
            $limit = intval($config->value);

        //  Phân trang
        $pagination = Pagination::calc($total, $page, $limit);

        if ($pagination->total_page > 0) {
            if ($page > $pagination->total_page) {
                $page = $pagination->total_page;
                if ($page == 1)
                    return redirect(base_url($term->slug), 'location', 301);
                return redirect(base_url($term->slug . '/' . $page), 'location', 301);
            }
        }

        $products = $this->product_model->get_by_term($term->id, $page, $limit, $order_by, $filter);
        //  Lấy ảnh cho sản phẩm
        for ($i = 0; $i < count($products); $i++)
            $products[$i]->images = $this->product_model->get_images($products[$i]->id);

        #region Thuộc tính sản phẩm cho filter
        $attributes = $this->product_model->get_attributes();
        $attributes_list = [];
        $attributes_id = array_unique(array_column($attributes, 'attribute_id'));

        foreach ($attributes_id as $id) {
            $attr_index = array_search($id, array_column($attributes, 'attribute_id'));
            $item = new stdClass();
            $item->attribute = $attributes[$attr_index]->attribute;
            $item->type = $attributes[$attr_index]->type;
            $item->values = [];

            $values = array_filter($attributes, function ($t) use ($id) {
                return $t->attribute_id == $id;
            });

            if (count($values) == 0) continue;
            foreach ($values as $val) {
                $url = current_url();
                if (in_array($val->id, $filter))
                    $_filter = array_diff($filter, [$val->id]);
                else {
                    $_filter = [$val->id];
                    $_filter = array_merge($_filter, $filter);
                }

                $url .= '?filter=' . join(':', $_filter);
                $value = new stdClass();
                $value->name = $val->name;
                $value->value = $val->value;
                $value->id = $val->id;
                $value->url = $url;
                $value->actived = in_array($val->id, $filter);

                array_push($item->values, $value);
            }
            array_push($attributes_list, $item);
        }
        #endregion

        return $this->blade
            ->set('term', $term)
            ->set('products', $products)
            ->set('attributes', $attributes_list)
            ->set('pagination', $pagination)
            ->render('product/category');
    }

    //  Chi tiết sản phẩm
    public function detail($slug)
    {
        $product = $this->product_model->get_by_slug($slug);
        if ($product == null)
            return $this->blade->render('errors/index');

        $product->images = $this->product_model->get_images($product->id);
        $product->tags = $this->product_model->get_tags($product->id);
        $product->terms = $this->product_model->get_terms($product->id);
        $product->attributes = [];

        #region Thuộc tính sản phẩm
        $attributes = $this->product_model->get_attributes($product->id);
        if (count($attributes) > 0) {
            $attrbiute = array_unique(array_column($attributes, 'attribute_id'));
            foreach ($attrbiute as $index => $value) {
                $item = new stdClass();
                $attribute_id = $attributes[$index]->attribute_id;
                $item->name = $attributes[$index]->attribute;
                $item->type = $attributes[$index]->type;
                $item->values = array_filter($attributes, function ($t) use ($attribute_id) {
                    return $t->attribute_id == $attribute_id;
                });

                array_push($product->attributes, $item);
            }
        }
        #endregion

        //  Sản phẩm cùng nhóm
        $grouped_products = [];
        if (count($product->tags) > 0)
            $grouped_products = $this->product_model->get_by_tags(array_column($product->tags, 'id'));

        //  Sản phẩm liên quan
        $limit = 15;
        $config = $this->option_model->get('related_products');
        if ($config != null)
            $limit = intval($config->value);

        //  Sản phẩm liên quan (cùng danh mục)
        $related_products = $this->product_model->get_upsell_product($product->id);
        if (count($related_products) == 0)
            $related_products = $this->product_model->get_related(array_column($product->terms, 'id'), $limit);

        if (count($related_products) > 0) {
            //  Lấy ảnh cho sản phẩm
            for ($i = 0; $i < count($related_products); $i++)
                $related_products[$i]->images = $this->product_model->get_images($related_products[$i]->id);
        }

        $related_articles = $this->product_model->get_related_articles($product->id);

        set_head_title($product->seo_title == null ? $product->name : $product->seo_title);
        set_head_des($product->seo_description == null ? $product->description : $product->seo_description);
        set_head_index($product->seo_index);
        set_head_image(count($product->images) > 0 == null ? null : base_url() . $product->images[0]->path);

        return $this->blade
            ->set('product', $product)
            ->set('related_products', $related_products)
            ->set('grouped_products', $grouped_products)
            ->set('related_articles', $related_articles)
            ->render();
    }

    //  Thêm sản phẩm vào giỏ hàng
    public function POST_detail($slug)
    {
        $quantity = $this->input->post('quantity');
        $prod = $this->product_model->get_by_slug($slug);

        $data = [
            'id' => $prod->id,
            'qty' => intval($quantity),
            'price' => $prod->price,
            'sale_price' => null,
            'name' => $prod->slug,
            '_name' => $prod->name,
            'slug' => $prod->slug,
            'image' => null
        ];

        $images = $this->product_model->get_images($prod->id);
        if (count($images) > 0)
            $data['image'] = $images[0]->path;

        if (has_sale_price($prod))
            $data['sale_price'] = $prod->sale_price;

        $attribute = $this->input->post('attribute');
        if ($attribute != null) {
            $values = $this->product_model->get_attribute_values($attribute);
            usort($values, function ($item1, $item2) {
                return $item1->id <=> $item2->id;
            });

            $data['attributes'] = $values;
            $data['id'] = $prod->id . '-' . join('-', array_column($values, 'id'));
        }

        $this->cart->insert($data);
        $this->session->set_flashdata('added_to_cart', "Đã thêm sản phẩm <b>{$prod->name}</b> vào giỏ hàng");

        $config = $this->option_model->get('redirect_to_cart');
        if ($config != null) {
            $redirect = boolval($config->value);
            if ($redirect)
                return redirect(CART_URL);
        }

        return redirect(current_url());
    }

    /**
     * Tìm kiếm sản phẩm
     * @param int $page
     * @return string|void
     */
    public function search($page = 1)
    {
        if ($this->input->get('q') == null)
            return redirect(base_url(), 'location', 301);

        $key = $this->input->get('q'); //  Tổng số sản phẩm tìm được
        $total = $this->product_model->search_count($key);

        //  Số sp trên mỗi trang
        $limit = 15;
        $config = $this->option_model->get('products_per_page');
        if ($config != null)
            $limit = intval($config->value);

        //  Phân trang
        $pagination = Pagination::calc($total, $page, $limit);

        if ($pagination->total_page >= 1) {
            if ($page > $pagination->total_page) {
                $page = $pagination->total_page;
                if ($page == 1)
                    return redirect(SEARCH_URL . '?q=' . $key, 'location', 301);
                return redirect(SEARCH_URL . "?q=$key", 'location', 301);
            }
        }

        $products = $this->product_model->search($key, $page, $limit);
        if (count($products) == 1)
            return redirect(product_url($products[0]->slug), 'location', 301);

        //  Lấy ảnh cho sản phẩm
        for ($i = 0; $i < count($products); $i++)
            $products[$i]->images = $this->product_model->get_images($products[$i]->id);

        set_head_title("Tìm kiếm từ khóa $key");
        set_head_index(false);

        return $this->blade
            ->set('products', $products)
            ->set('pagination', $pagination)
            ->render();
    }
}