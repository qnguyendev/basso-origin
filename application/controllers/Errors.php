<?php

if (!defined("BASEPATH")) exit('No direct access script allowed');

/**
 * Class Errors
 * @property Ion_auth_model|Ion_auth $ion_auth
 * @property CI_Output $output
 * @property Blade $blade
 * @property CI_Router $router
 */
class Errors extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (uri_string() != 'error_404')
            return redirect(base_url('error_404'));
        
        $this->output->set_status_header(404);

        if (is_cli()) {
            $this->blade->set('heading', 'Not Found');
            $this->blade->set('message', 'The controller/method pair you requested was not found.');
        } else {
            $this->blade->set('heading', '404 Page Not Found');
            $this->blade->set('message', 'The page you requested was not found.');
        }

        set_head_title('Error 404');
        set_head_index(true);

        return $this->blade->render('errors/index');
    }
}