<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Sitemap
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Output $output
 * @property Article_model $article_model
 * @property Product_model $product_model
 */
class Seo extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function robots()
    {
        $this->output->set_header("Content-Type: text/plain");
        $file = fopen('./robots.txt', "r") or exit("Unable to open file!");
        $data = [];
        while (!feof($file)) {
            array_push($data, fgets($file));
        }
        fclose($file);

        return $this->blade->render(['data' => $data]);
    }

    public function sitemap()
    {
        $this->output->set_header("Content-Type: text/xml;charset=utf-8");
        $data = [
            'post_sitemap' => false,
            'product_sitemap' => false,
            'page_sitemap' => false
        ];

        foreach ($data as $key => $value) {
            $option = $this->option_model->get($key);
            if ($option != null)
                $data[$key] = boolval($option->value) == 1;
        }

        return $this->blade->render($data);
    }

    public function sitemap_articles()
    {
        $this->output->set_header("Content-Type: text/xml;charset=utf-8");
        return $this->blade->render(['data' => $this->article_model->get_sitemap()]);
    }

    public function sitemap_products()
    {
        $this->output->set_header("Content-Type: text/xml;charset=utf-8");
        return $this->blade->render(['data' => $this->product_model->get_sitemap()]);
    }

    public function sitemap_pages()
    {
        $this->output->set_header("Content-Type: text/xml;charset=utf-8");
        return $this->blade->render(['data' => $this->term_model->get_by_type('page')]);
        //return $this->blade->render(['data' => $this->term_model->get_sitemap('post_page')]);
    }
    public function category_sitemap()
    {
        $this->output->set_header("Content-Type: text/xml;charset=utf-8");

        return $this->blade->render(['data' => $this->term_model->get_by_type('post_page')]);
    }
}
