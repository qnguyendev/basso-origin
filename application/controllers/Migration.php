<?php

/**
 * Migration Controller Class for Codeigniter
 *
 * This class provides a Command Line Interface
 * for developers to use Codeigniter's Migrations
 * functionality. It has been configured to only
 * respond to CLI requests hence not accessible
 * via the browser. However running it via a browser
 * is very much possible with a few tweaks in the code
 * A detailed info about the available methods
 * can be found via $this::help()
 *
 * No configurations are required before using this
 * class as it configures everything requried by
 * itself apart from a working database connection.
 *
 * @package     Migrations for Codeigniter
 * @subpackage
 * @category
 * @author      Muhammad Aimal <aimal.azmi.13@gmail.com>
 * @link
 */

/**
 * Class Migration
 * @property CI_Migration $migration
 * @property CI_Config $config
 * @property Blade $blade
 */
class Migration extends MY_Controller
{

    /**
     * Migration File Name
     *
     * @var string
     */
    protected $_migration_file_name = '';

    /**
     * Migration Path
     *
     * @var string
     */
    protected $_migration_path = '';

    /**
     * Database table with migration info
     *
     * @var string
     */
    protected $_migration_table = 'migrations';

    /**
     * Table name to be used in the migration file
     *
     * @var string
     */
    protected $_migration_file_table = '';

    /**
     * Class Version Info
     *
     * @var string
     */
    protected $_class_version = '1.0';

    /**
     * Info file
     *
     * @var string
     */
    protected $_info_file_name = BASEPATH . 'database/migrate_help.conf';

    /**
     * @var object $_ci codeigniter
     */
    private $_ci = NULL;

    /**
     * @var string migration folder name
     */
    private $_migration_folder_name = 'migrations';

    /**
     * @var array collect used timestamp
     */
    private $_timestamp_set = array();

    /**
     * @var array migration table set
     */
    protected $migration_table_set = [];
    /**
     * @var string path
     */
    protected $path = '';

    /**
     * @var array skip table name set
     */
    protected $skip_tables = ['migrations'];

    /**
     * @var bool add view
     */
    protected $add_view = FALSE;


    /**
     * @var string database name
     */
    private $_db_name = '';

    /**
     * Class constructor
     *
     * @return    void
     */
    public function __construct()
    {
        parent::__construct();

        // The migration controller will only be accessible
        // throught the CLI.
        if (is_cli() === FALSE) {
            show_404();
        }

        if (!isset($this->_ci)) {
            $this->_ci =& get_instance();
        }

        $this->path = APPPATH . $this->_migration_folder_name;

        $this->_ci->load->database();

        // Load the Helper classes
        $this->load->helper('file');

        // Check whether migrations are turned off
        $this->_migrations_status();

        // Load the libraries
        $this->load->library('migration');

        // Set the Migration Path to the migration folder
        $this->_migration_path = $this->config->item('migration_path');

        // Check if the application/migrations folder
        // exist and create it if it doesnot
        if (!is_dir($this->_migration_path)) {
            // The migration directory wasnot found
            echo 'The application/migrations folder doesnot exist' . PHP_EOL;

            echo 'Creating the application/migrations folder...' . PHP_EOL;

            if (!mkdir($this->_migration_path)) {
                // There was an eror creating the path
                show_error('Could not create the directory application/migrations! Try again and if the error persists try creating it manually!');
            }

            // Directory created successfully
            echo 'Directory application/migrations created successfully!' . PHP_EOL;
        }

        // Customize the migrations table
        // created by CI to suit our needs
        $this->_customize_migrations_table();
        $this->load->helper('db');
    }

    // --------------------------------------------------------------------

    /**
     * Creates Migration Files
     *
     * @param string $file_name Migration File Name
     * @return void
     */
    public function index($file_name = '')
    {
        echo 'Enter migration file name: ';

        $file_name = trim(fgets(STDIN));

        // Check if a valid File Name was passed
        if (empty($file_name) OR $file_name === "") {
            show_error('Please enter a valid migration file name');
        }

        // Set the value
        $this->_migration_file_name = $file_name;

        echo 'Database Table name: ';

        $migration_file_table = trim(fgets(STDIN));

        // Check if a valid File Name was passed
        if (empty($migration_file_table) OR $migration_file_table === "") {
            show_error('Please enter a valid table name for the migration file!');
        }

        // Migration File table name
        $this->_migration_file_table = $migration_file_table;

        // Format the file name
        $file_name = $this->_format_file_name($file_name);

        // Get the migration version
        $migration_version = $this->_get_migration_version();

        // Append the migration number/timestamp
        // to the file name
        $file_full_name = $migration_version . '_' . $file_name . '.php';

        // Create the complete path to the file
        $file = $this->_migration_path . $file_full_name;

        // Get the default migration file content
        $file_content = $this->_get_file_content($file_name);

        // Create the file and throw error if could not create
        $this->_change_migrate_order();
        if (!write_file($file, $file_content)) {
            // Error creating the file
            $error = 'Could not create file!' . PHP_EOL;

            show_error($error);
        }

        // Update the migration version in Schema
        $this->_update_latest_file_version($migration_version);

        // File created successfully
        echo 'Migration File "' . $file_full_name . '" successfully created!' . PHP_EOL;

        return;
    }

    // --------------------------------------------------------------------

    /**
     * Calls the Migration::current()
     *
     * @return void
     */
    public function current()
    {
        if ($this->migration->current() === FALSE) {
            show_error($this->migration->error_string());
        }

        echo "Migrated to the current version!";
    }

    // --------------------------------------------------------------------

    /**
     * Calls the Migration::latest()
     *
     * @return void
     */
    public function latest()
    {
        if ($this->migration->latest() === FALSE) {
            show_error($this->migration->error_string());
        }

        echo "Migrated to the latest version!";
    }

    public function version($version)
    {
        if ($this->migration->version($version) === false)
            show_error($this->migration->error_string());
        echo 'Migrate version: ' . $version;
    }

    // --------------------------------------------------------------------

    /**
     * Rollback to a migration version
     *
     * @param int $version Rollback Migration Version
     * @return void
     */
    public function rollback($version = 0)
    {
        if ($this->migration->version(abs($version)) === FALSE) {
            show_error($this->migration->error_string());
        }

        echo "Migrated to the version - " . $version;
    }

    // --------------------------------------------------------------------

    /**
     * Resets the migration table and
     * Deletes all the migartion files
     * Deletes all the tables created
     * by the migrations aswell
     *
     * @param bool $rollback rollback to zero
     * @return void
     */
    public function reset($rollback = TRUE)
    {
        $this->_prompt("This function will delete all the files in the migrations folder," .
            " delete all relevant tables and also reset the migrations table in DB.");

        // Rollback migrations to version '0'
        if (($rollback === TRUE) && ($this->db->table_exists($this->_migration_table) === TRUE)) {
            if ($this->rollback(0) === FALSE) {
                show_error("There was an error resetting the migrations!");
            }
        }

        // DROP the migartions table
        if ($this->dbforge->drop_table($this->_migration_table, TRUE) === FALSE) {
            show_error("Table 'Migrations' could not be deleted!");
        }

        //Delete all the migartion files
        if (delete_files($this->_migration_path) === FALSE) {
            show_error("Could not all the migration files!");
        }

        echo "Migartions have been reset!";
    }

    // --------------------------------------------------------------------

    /**
     * Rollbacks the migrations one step back
     *
     * @return void
     */
    public function last()
    {
        $rollback_version = $this->_get_version() == 0 ? 0 : (int)($this->_get_version() - 1);

        if ($rollback_version) {
            $this->rollback($rollback_version);
        } else {
            show_error("Looks like your migrations table hasn't been configured properly!" .
                " Try reseting the migarations using migrate::reset() and try again!");
        }
    }

    // --------------------------------------------------------------------

    /**
     * Information about using this class
     *
     * @return void
     */
    public function help()
    {
        if (!file_exists($this->_info_file_name))
            show_error("Couldn't find the info file!");

        $file_content = file_get_contents($this->_info_file_name);

        if ($file_content) {
            $file_content = str_replace('#', $this->_class_version, $file_content);
            echo $file_content;
        }
    }

    // --------------------------------------------------------------------

    /**
     * Information about the migrations
     *
     * @return void
     */
    public function info()
    {
        $content = 'No migrations found!';

        $headings = ['Ran', 'Migration'];

        $migrations = get_filenames($this->_migration_path);

        if ((is_array($migrations)) && (count($migrations) > 0)) {
            $row_width = 0;
            $content = '';
            $header = '';
            $rows = [];
            $version = $this->_get_version('version');

            foreach ($headings as $key => $value) {
                $header .= '| ' . $value . ' ';
            }

            foreach ($migrations as $key => $value) {
                if (!empty($value)) {
                    $string = '| T   | ';

                    if ((int)$this->_get_migration_number($value) > $version) {
                        $string = '| F   | ';
                    }

                    $string = $string . str_replace('.php', '', $value) . ' ';
                    $rows[] = $string;
                    $row_width = (strlen($string) > $row_width) ? strlen($string) : $row_width;
                }
            }

            if (count($rows) > 0) {
                foreach ($rows as $row) {
                    $content .= str_pad($row, $row_width, " ", STR_PAD_RIGHT) . '|' . PHP_EOL;
                }

                // Lower layer
                $content = $content . '+' . str_pad("", $row_width - 1, "-", STR_PAD_LEFT) . '+' . PHP_EOL;
            }

            // Paddings
            $header = str_pad($header, $row_width, " ", STR_PAD_RIGHT) . '|' . PHP_EOL;

            // Upper layer
            $header = '+' . str_pad("", $row_width - 1, "-", STR_PAD_LEFT) . '+' . PHP_EOL . $header;

            // Lower layer
            $header = $header . '+' . str_pad("", $row_width - 1, "-", STR_PAD_LEFT) . '+' . PHP_EOL;

            // Mix it up
            $content = PHP_EOL . $header . $content;
        }

        echo $content;
    }

    // --------------------------------------------------------------------

    /**
     * Checks whether the migrations are
     * ENABLED or DISABLED in the application
     * Enables if disabled after user's permission
     *
     * @return void
     */
    protected function _migrations_status()
    {
        $migration_config_file = APPPATH . 'config/migration.php';

        // Check if config/migration.php exists
        if (!file_exists($migration_config_file)) {
            show_error("It seems the config/migration.php file doesnot exist. That file is necessary!");
        }

        // Migrations Config File exists, load it
        $this->config->load('migration');

        // Migration Config file exists
        // and migrations are enabled
        if ($this->config->item('migration_enabled') === TRUE)
            return;

        // Recheck if the migrations aren't enabled
        if ($this->config->item('migration_enabled') !== TRUE) {
            echo PHP_EOL . 'It looks like the migrations have been disabled in the config file.' . PHP_EOL;

            // Ask to enable migrations
            $this->_prompt("Enable migrations?");

            $migration_file_content = file_get_contents($migration_config_file);

            if (!empty($migration_file_content)) {
                $migration_file_content = str_replace("config['migration_enabled'] = FALSE", "config['migration_enabled'] = TRUE", $migration_file_content);

                // write the changes to the file
                if (!write_file($migration_config_file, $migration_file_content)) {
                    $error = 'There was an error while configuring changes in the config/migration.php file!';
                    $error .= PHP_EOL . 'If the error persists simply goto projectfolder/application/config/migration.php' . PHP_EOL;
                    $error .= ' and set $' . 'config[migration_enabled] = TRUE';
                    show_error($error);
                }

                // Reload configuration
                echo PHP_EOL . 'Reloading Configurations...' . PHP_EOL;
                $this->config->set_item('migration_enabled', TRUE);

                // Configurations were changed successfully
                if ($this->config->item('migration_enabled') === TRUE) {
                    echo PHP_EOL . 'Migrations Enabled!' . PHP_EOL . PHP_EOL;
                    return;
                }
            }

            // If the program has reached till here it
            // means that the migration configurations
            // were not set properly at all
            show_error("Looks like your migration configurations aren't set properly. Try getting a fresh copy of config/migration.php and retry!");
        }
    }

    // --------------------------------------------------------------------

    /**
     * Performs regex and only allows
     * letters and underscore.
     *
     * @return Mixed. File name on success, show_error() on wrong file name
     */
    protected function _customize_migrations_table()
    {
        // The CI will take care of validating
        // and creating the table. We only need
        // to alter it to add 'latest_file_version'
        // in the table

        // Check if the field has already
        // been added to the table
        if ($this->db->field_exists('latest_file_version', $this->_migration_table) === TRUE) {
            return;
        }

        $fields = array(
            'latest_file_version' => array('type' => 'INT', 'default' => 0)
        );

        if ($this->dbforge->add_column($this->_migration_table, $fields) === FALSE) {
            show_error("Coud not add field 'latest_file_version' to the migrations table!");
        }
    }

    // --------------------------------------------------------------------

    /**
     * Replaces spaces with '_'
     * @param string $file_name
     * @return string
     */
    protected function _format_file_name($file_name = '')
    {
        return str_replace(' ', '_', $file_name);
    }

    // --------------------------------------------------------------------

    /**
     * Returns the default code found in the
     * migration file. The best way to do this
     * is not from a function but since I didn't
     * want to create an extra file as sometimes
     * it causes developers the hassle to locate
     * and validate the files and everything.
     *
     * However I recommend cutting $file_content
     * from the function and and pasting it to a
     * file and then returning it from there so
     * that this controller doesnt contain this
     * extra bit of code
     *
     * @param string $file_name Name of Migration File
     * @return string
     */
    protected function _get_file_content($file_name = '')
    {
        $this->_migration_file_table;
        return $this->blade->render('template',
            ['file_name' => $file_name, 'table_name' => $this->_migration_file_table], true);
    }

    // --------------------------------------------------------------------

    /**
     * Returns the latest migration version number
     *
     * @return int $migration_version Migration Version
     */
    protected function _get_migration_version()
    {
        // By default we'll use the timestamps
        // for version control
        $migration_version = date("YmdHis");

        // If the migration type has been set to
        // sequential, we'll set the migration
        // version to sequential
        if ($this->config->item('migration_type') === 'sequential') {
            $migration_version = (int)((int)$this->_get_version() + 1);
            $migration_version = str_pad($migration_version, 3, "0", STR_PAD_LEFT);
        }

        return $migration_version;
    }

    // --------------------------------------------------------------------

    /**
     * Returns the latest file version
     * from the migrations table
     *
     * @return    int    last migration file version
     * @param string $select Name of row to return
     */
    protected function _get_version($select = 'latest_file_version')
    {
        $row = $this->db->select($select)->get($this->_migration_table)->row();
        return $row ? $row->$select : '0';
    }

    // --------------------------------------------------------------------

    /**
     * Stores the latest file version
     *
     * @param    string $migration Migration reached
     * @return    void
     */
    protected function _update_latest_file_version($file_version)
    {
        $this->db->update($this->_migration_table, array(
            'latest_file_version' => $file_version
        ));
    }

    // --------------------------------------------------------------------

    /**
     * Revere the migration type to the
     * given type. i.e. if the migration
     * files were sequenced through timestamps
     */
    protected function _reverse_migration_type()
    {
        // Later
    }

    // --------------------------------------------------------------------

    /**
     * Prompt Function
     *
     * @return void
     */
    protected function _prompt($msg = '')
    {
        echo PHP_EOL . $msg . PHP_EOL;
        echo "Continue? (Y/N) - ";

        $stdin = fopen('php://stdin', 'r');
        $response = fgetc($stdin);
        if ($response != 'Y' && $response != 'y') {
            echo "Opertaion Terminated!.\n";
            exit;
        }
    }

    // --------------------------------------------------------------------

    /**
     * Extracts the migration number from a filename
     *
     * @param    string $migration
     * @return    string    Numeric portion of a migration filename
     */
    protected function _get_migration_number($migration)
    {
        return sscanf($migration, '%[0-9]+', $number)
            ? $number : '0';
    }

    // --------------------------------------------------------------------

    /**
     * Generate Migrations Files
     *
     * @param string $tables
     *
     * @return boolean|string
     */
    public function generate($tables = '*')
    {
        // check tables not empty
        if (empty($tables)) {
            echo 'InvalidParameter::tables';
            return FALSE;
        }

        // get database name
        $this->_db_name = $this->_ci->db->database;

        if ($tables === '*') {
            $query = $this->_ci->db->query('SHOW FULL TABLES FROM ' . $this->_ci->db->protect_identifiers($this->_db_name));

            // collect tables of migration
            $migration_table_set = array();

            // confirm table num
            if ($query->num_rows() > 0) {
                $table_name_key = "Tables_in_{$this->_db_name}";

                foreach ($query->result_array() as $table_info) {
                    if (isset($table_info[$table_name_key]) && $table_info[$table_name_key] !== '') {
                        //$table_name = $table_info[$table_name_key];

                        // check if table in skip arrays, if so, go next
                        if (in_array($table_info[$table_name_key], $this->skip_tables)) {
                            continue;
                        }

                        // skip views
                        if (strtolower($table_info['Table_type']) == 'view') {
                            continue;
                        }

                        $migration_table_set[] = $table_info["Tables_in_{$this->_db_name}"];
                    }
                }
            }

            if ($this->_ci->db->dbprefix($this->_db_name) !== '') {
                array_walk($migration_table_set, [$this, '_remove_database_prefix']);
            }

            $this->migration_table_set = $migration_table_set;
        } else {
            $this->migration_table_set = is_array($tables) ? $tables : explode(',', $tables);
        }


        if (!empty($this->migration_table_set)) {
            // create migration file or override it.
            foreach ($this->migration_table_set as $table_name) {
                $file_content = $this->get_file_content($table_name);

                if (!empty($file_content)) {
                    $this->write_file($table_name, $file_content);
                    continue;
                }
            }

            echo "Create migration success!";
            return TRUE;
        } else {
            echo "Empty table set!";
            return FALSE;
        }
    }

    /**
     * _remove_database_prefix
     *
     * @param string $table_name
     *
     * @return void
     */
    private function _remove_database_prefix(&$table_name)
    {
        // insensitive replace
        $table_name = str_ireplace($this->_ci->db->dbprefix, '', $table_name);
    }

    /**
     * get_file_content
     *
     * @param string $table_name table name
     *
     * @return string $file_content
     */
    public function get_file_content($table_name)
    {
        $file_content = '<?php ';
        $file_content .= 'defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');' . "\n\n";
        $file_content .= '/** ' . PHP_EOL;
        $file_content .= '* Class Migration_create_' . $table_name . PHP_EOL;
        $file_content .= '* @property CI_DB_forge $dbforge' . PHP_EOL;
        $file_content .= '*/' . PHP_EOL;
        $file_content .= "class Migration_create_{$table_name} extends CI_Migration" . "\n";
        $file_content .= '{' . "\n";
        $file_content .= $this->get_function_up_content($table_name);
        $file_content .= $this->get_function_down_content($table_name);
        $file_content .= "\n" . '}' . "\n";

        // replace tab into 4 space
        $file_content = str_replace("\t", '    ', $file_content);

        return $file_content;
    }

    /**
     * writeFile
     *
     * @param string $table_name table name
     * @param string $file_content file content
     *
     * @return void
     */
    public function write_file($table_name, $file_content)
    {
        $file = $this->open_file($table_name);
        fwrite($file, $file_content);
        fclose($file);
    }

    /**
     * openFile
     *
     * @param string $table_name table name
     *
     * @return bool|resource
     */
    public function open_file($table_name)
    {
        $timestamp = $this->_get_timestamp($table_name);

        $file_path = $this->path . '/' . $timestamp . '_create_' . $table_name . '.php';

        // Open for reading and writing.
        // Place the file pointer at the beginning of the file and truncate the file to zero length.
        // If the file does not exist, attempt to create it.
        $file = fopen($file_path, 'w+');

        if (!$file) {
            return FALSE;
        }

        // add this timestamp to timestamp ser
        $this->_timestamp_set[] = $timestamp;

        return $file;
    }

    /**
     * _get_timestamp get
     *
     * @param $table_name
     * @return string
     */
    private function _get_timestamp($table_name)
    {
        // get timestamp
        $query = $this->_ci->db->query(' SHOW TABLE STATUS WHERE Name = \'' . $table_name . '\'');

        $engines = $query->row_array();

        $timestamp = date('YmdHis', strtotime($engines['Create_time']));

        while (in_array($timestamp, $this->_timestamp_set)) {
            $timestamp += 1;
        }

        return $timestamp;
    }

    /**
     * Base on table name create migration up function
     *
     * @param $table_name
     *
     * @return string
     */
    public function get_function_up_content($table_name)
    {
        $str = "\n\t" . '/**' . "\n";
        $str .= "\t" . ' * up (create table)' . "\n";
        $str .= "\t" . ' *' . "\n";
        $str .= "\t" . ' * @return void' . "\n";
        $str .= "\t" . ' */' . "\n";

        $str .= "\t" . 'public function up()' . "\n";
        $str .= "\t" . '{' . "\n";

        $query = $this->_ci->db->query("SHOW FULL FIELDS FROM {$this->_ci->db->dbprefix($table_name)} FROM {$this->_db_name}");

        if ($query->result() === NULL) {
            return FALSE;
        }

        $columns = $query->result_array();

        $add_key_str = '';

        $add_field_str = "\t\t" . '$this->dbforge->add_field(array(' . "\n";

        foreach ($columns as $column) {
            // field name
            $add_field_str .= "\t\t\t'{$column['Field']}' => array(" . "\n";

            preg_match('/^(\w+)\(([\d]+(?:,[\d]+)*)\)/', $column['Type'], $match);

            if ($match === []) {
                preg_match('/^(\w+)/', $column['Type'], $match);
            }

            $add_field_str .= "\t\t\t\t'type' => '" . strtoupper($match[1]) . "'," . "\n";

            if (isset($match[2])) {
                switch (strtoupper($match[1])) {
                    //type enum need extra handle
                    case 'ENUM':
                        $enum_constraint_str = str_replace(',', ', ', $match[2]);
                        $add_field_str .= "\t\t\t\t'constraint' => [" . $enum_constraint_str . "],\n";
                        break;
                    default:
                        $add_field_str .= "\t\t\t\t'constraint' => '" . strtoupper($match[2]) . "'," . "\n";
                        break;
                }
            }

            $add_field_str .= (strstr($column['Type'], 'unsigned')) ? "\t\t\t\t'unsigned' => TRUE," . "\n" : '';

            $add_field_str .= ((string)$column['Default'] !== '') ? "\t\t\t\t'default' => '" . $column['Default'] . "'," . "\n" : '';

            $add_field_str .= ((string)$column['Comment'] !== '') ? "\t\t\t\t'comment' => " . str_replace("'", "\\'", $column['Comment']) . "',\n" : '';

            $add_field_str .= ($column['Null'] !== 'NO') ? "\t\t\t\t'null' => TRUE," . "\n" : '';

            $add_field_str .= "\t\t\t)," . "\n";

            if ($column['Key'] == 'PRI') {
                $add_key_str .= "\t\t" . '$this->dbforge->add_key("' . $column['Field'] . '", TRUE);' . "\n";
            }
        }

        $add_field_str .= "\t\t));" . "\n";

        $str .= "\n\t\t" . '// Add Fields.' . "\n";
        $str .= $add_field_str;

        $str .= ($add_key_str !== '') ? "\n\t\t" . '// Add Primary Key.' . "\n" . $add_key_str : '';

        // create db

        $query = $this->_ci->db->query(' SHOW TABLE STATUS WHERE Name = \'' . $table_name . '\'');

        $engines = $query->row_array();

        $attributes_str = "\n\t\t" . '$attributes = array(' . "\n";;
        $attributes_str .= ((string)$engines['Engine'] !== '') ? "\t\t\t'ENGINE' => '" . $engines['Engine'] . "'," . "\n" : '';
        $attributes_str .= ((string)$engines['Comment'] !== '') ? "\t\t\t'COMMENT' => '\\'"
            . str_replace("'", "\\'", $engines['Comment']) . "\\'',\n" : '';
        $attributes_str .= "\t\t" . ');' . "\n";

        $str .= "\n\t\t" . '// Table attributes.' . "\n";
        $str .= $attributes_str;

        $str .= "\n\t\t" . '// Create Table ' . $table_name . "\n";
        $str .= "\t\t" . '$this->dbforge->create_table("' . $table_name . '", TRUE, $attributes);' . "\n";

        $str .= "\n\t" . '}' . "\n";

        return $str;
    }

    /**
     * Base on table name create migration down function
     *
     * @param string $table_name table name
     *
     * @return string
     */
    public function get_function_down_content($table_name)
    {
        $function_content = "\n\t" . '/**' . "\n";
        $function_content .= "\t" . ' * down (drop table)' . "\n";
        $function_content .= "\t" . ' *' . "\n";
        $function_content .= "\t" . ' * @return void' . "\n";
        $function_content .= "\t" . ' */' . "\n";

        $function_content .= "\t" . 'public function down()' . "\n";
        $function_content .= "\t" . '{' . "\n";
        $function_content .= "\t\t" . '// Drop table ' . $table_name . "\n";
        $function_content .= "\t\t" . '$this->dbforge->drop_table("' . $table_name . '", TRUE);' . "\n";
        $function_content .= "\t" . '}' . "\n";

        return $function_content;
    }

    private function _change_migrate_order()
    {
        $file_list = glob($this->_migration_path . '*.php', GLOB_BRACE);
        for ($i = 0; $i < count($file_list); $i++) {
            $index = $i + 2;
            $file_info = pathinfo($file_list[$i]);
            $old_index = substr($file_info['filename'], 0, 3);
            $new_index = $index < 10 ? "00$index" : ($index < 100 ? "0$index" : $index);
            $new_file = $this->_migration_path
                . str_replace("$old_index", $new_index, $file_info['filename'])
                . ".{$file_info['extension']}";
            rename($file_list[$i], $new_file);
        }
    }
}