 <?php defined('BASEPATH') OR exit('No direct script access allowed');

use Zalo\Zalo;
use Zalo\ZaloEndPoint;
/**
 * s Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation $form_validation The form validation library
 * @property Blade blade template
 * @property CI_Input $input
 * @property CI_Session $session
 */
class Auth extends Site_Controller
{
    private $_cart;
    private $_config;
    private $_zalo;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('google');
        global $cart;
        $this->_cart = $cart;
        $this->load->model('cart_model');
        $this->load->model('management/customer_model');
		$this->_config = array(
			'app_id' => $this->config->item('zalo_app_id'),
			'app_secret' => $this->config->item('zalo_app_secret'),
			'callback_url' => $this->config->item('zalo_callback_url')
		);
		/* $this->_zalo = new Zalo($this->_config); */
    }

	
	public function oauth2callback(){
		$google_data=$this->google->validate();
		if(isset($google_data)){	
			$name = $google_data['name'];
			$email =	$google_data['email'];
			$result = $this->ion_auth->login_with_google($email,$name);
		
		//Gán dữ liệu từ db cho session cart 
		$this->set_cart_data();
		}
		redirect(base_url());
	}
	
	public function zalocallback(){
		$helper = $this->_zalo->getRedirectLoginHelper();
		$oauthCode = isset($_GET['code']) ? $_GET['code'] : "THIS NOT CALLBACK PAGE !!"; // get oauthoauth code from url params
		
		$accessToken = $helper->getAccessToken($this->_config['callback_url']); // get access token
		if ($accessToken != null) {
			$params = ['fields' => 'id,name,birthday,gender,picture'];
			$response = $this->_zalo->get(ZaloEndpoint::API_GRAPH_ME, $accessToken, $params);
			$result = $response->getDecodedBody(); // result
			if($result != null){
				$birthday = $result['birthday'];
				$gender = $result['gender'];
				$name = $result['name'];
				$id = $result['id'];
				$picture = $result['picture']['data']['url'];
				
				$result = $this->ion_auth->login_with_zalo($id,$name,$birthday,$gender);
				//Gán dữ liệu từ db cho session cart 
				$this->set_cart_data();
			}
		}
		redirect(base_url());
	}
    /**
     * login
     */
    public function login()
    {
		if ($this->input->method() == 'post') {
            if ($this->ion_auth->login($this->input->post('identity'),
                $this->input->post('password'), true)) {
                if ($this->ion_auth->in_group('supporters'))
                    return redirect('lich-hen');
			
                return redirect('/');
            }

            $this->session->set_flashdata('error', 'Tài khoản không hợp lệ');
        }

        return $this->blade->render();
    }

    public function POST_login()
    {
        $this->form_validation->set_rules('email', null, 'required|valid_email');
        $this->form_validation->set_rules('pass', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng thông tin đăng nhập');

        $email = trim(strtolower($this->input->post('email')));
        $pass = $this->input->post('pass');
        if (!$this->ion_auth->login($email, $pass, true))
            json_error('Sai email đăng nhập/mật khẩu');
		
		//Gán dữ liệu từ db cho session cart 
		$this->set_cart_data();
        json_success(null);
    }

	private function set_cart_data(){
		$this->session->unset_userdata('cart_contents');

		$customer_id = $this->session->userdata("user_id");
		$items = $this->cart_model->get_by_customer($customer_id);
		
		foreach ($items as $object) {
			$item = json_decode($object->content);
			$this->cart->product_name_rules = '[:print:]';
			$this->cart->insert(json_decode(json_encode($item), true));
		}
		$cart_data = $this->session->userdata('cart_contents');
	}
    /**
     * Log the user out
     */
    public function logout()
    {
		$customer_id = $this->session->userdata("user_id");
		$this->cart_model->delete_by_customer($customer_id);
		 foreach ($this->_cart as $item) {
			$this->cart_model->create($item->id,$item->rowid,json_encode($item),$customer_id);
		}
		// print_r("<pre>");
		// print_r($this->_cart);
		// print_r("</pre>");
		// die();
        $this->ion_auth->logout();
        //$this->cart->insert($this->_cart);
		
		

        redirect(base_url());
    }

    public function profile()
    {
        if (!$this->ion_auth->logged_in())
            redirect('/');

        $data['user'] = $this->ion_auth->user()->row();
        $data['page_title'] = 'Thông tin cá nhân';

        if ($this->input->is_ajax_request()) {

            if ($this->input->get('init') != null) {
                echo json_encode($data['user']);
            }

            if ($this->input->method() == 'post') {
                $first_name = $this->input->post('first_name');
                $last_name = $this->input->post('last_name');
                $phone = $this->input->post('phone');
                $pass = $this->input->post('pass');
                $re_pass = $this->input->post('re_pass');

                if ($first_name == null || $last_name == null || $phone == null) {
                    echo json_encode(array('error' => true, 'message' => 'Vui lòng nhập các thông tin bắt buộc'));
                    die;
                }

                if ($pass != null) {
                    if (strlen($pass) < 6 || $pass != $re_pass) {
                        echo json_encode(array('error' => true, 'message' => 'Xác nhận mật khẩu không đúng'));
                        die;
                    }
                }

                $update = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $phone
                );

                if ($pass != null)
                    $update['password'] = $pass;
                $this->ion_auth->update($data['user']->id, $update);

                echo json_encode(array('error' => false));
            }

            die;
        }
        $this->blade->render('auth/profile', $data);
    }
}
